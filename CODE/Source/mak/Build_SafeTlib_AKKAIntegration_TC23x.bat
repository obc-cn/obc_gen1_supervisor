@ECHO off
SETLOCAL

REM * 
REM * Change to containing directory
REM *

cd %~dp0


REM *
REM * Setup the compiler and Archiver
REM *

::SET RLM_LICENSE=spwplic13002@5059

::Read devtools' paths from the local machine (defined at devtools.path.defines file)
for /f "tokens=2 delims==" %%i in ('%WINDIR%\system32\findstr.exe /b /c:"DEVTOOLS_COMPILER_SAFETLIB_PATH" %~dp0\..\..\Utils\devtools.path.defines') do (
    set compiler_path=%%i
)

REM * checked
SET COMPILER_PATH=%compiler_path%
REM * checked
SET HIGHTEC_PATH=%compiler_path%

REM * checked
SET CCTC="%HIGHTEC_PATH%/bin/tricore-gcc.exe"
REM * checked
SET ARTC="%HIGHTEC_PATH%/bin/tricore-ar.exe" 


REM *
REM * Setup the output folders
REM *
SET OUT_LIB=..\out\safetlib\lib
SET OUT_OBJ=..\out\safetlib\obj
SET OUT_LST=..\out\safetlib\lst
SET OUT_ERR=..\out\safetlib\err
if not exist %OUT_LIB% mkdir %~dp0\%OUT_LIB%
if not exist %OUT_OBJ% mkdir %~dp0\%OUT_OBJ%
if not exist %OUT_LST% mkdir %~dp0\%OUT_LST%
if not exist %OUT_ERR% mkdir %~dp0\%OUT_ERR%


REM *
REM * Compiler options
REM *
REM SET COPT=--core=tc1.6.x --iso=99 --eabi-compliant --integer-enumeration --language=-comments,-gcc,+volatile,-strings --switch=auto --align=0 --default-near-size=0 --default-a0-size=0 --default-a1-size=0 -O2ROPYG --tradeoff=4 -D_TASKING_C_TRICORE_=1 -c -g --source -Wa-Ogs -Wa-gAHLs --emit-locals=-equ,-symbols -Wa--error-limit=42

REM * compiler options need to be checked
SET COPT=-c -std=iso9899:1990 -ansi -ffreestanding -fno-short-enums -fpeel-loops -falign-functions=4 -funsigned-bitfields  -ffunction-sections -fno-ivopts -fno-peephole2 -O3 -mtc161 -D_GNU_C_TRICORE_=1 -I "%compiler_path%/tricore/include/machine" -save-temps=obj -fno-asm -Wundef -Wp,-std=iso9899:1990 -frecord-gcc-switches -fsection-anchors -nostartfiles -g3 -W -Wall -Wuninitialized -maligned-data-sections
REM Compilation working (but test id 4 and 9 fails):
REM SET COPT=-c -std=iso9899:1990 -ansi -ffreestanding -fno-short-enums -fpeel-loops -falign-functions=4 -funsigned-bitfields  -ffunction-sections -fno-ivopts -fno-peephole2 -O3 -mtc161 -D_GNU_C_TRICORE_=1 -I "$( COMPILER_PATH)/tricore/include/machine"-save-temps=obj -fno-asm -Wundef -Wp,-std=iso9899:1990 -frecord-gcc-switches -fsection-anchors -nostartfiles -g3 -W -Wall -Wuninitialized -maligned-data-sections
REM New try to solve Tst ID 4 and 9
REM SET COPT=-c -std=c99 -ansi -ffreestanding -fno-short-enums -fpeel-loops -falign-functions=4 -funsigned-bitfields  -ffunction-sections -fno-ivopts -fno-peephole2 -O3 -mtc161 -D_GNU_C_TRICORE_=1 -I "$( COMPILER_PATH)/tricore/include/machine"-save-temps=obj -fno-asm -Wundef -Wp,-std=iso9899:1990 -frecord-gcc-switches -fsection-anchors -nostartfiles -g3 -W -Wall -Wuninitialized -maligned-data-sections
REM SET COPT=-c -std=c99 -ffreestanding -fno-short-enums -fpeel-loops -falign-functions=4 -funsigned-bitfields -ffunction-sections -fno-ivopts -fno-peephole2 -O3 -mtc161 -D_GNU_C_TRICORE_=1 -D_GNU_C_TRICORE_=1 -I "$( COMPILER_PATH)/tricore/include/machine" -save-temps -fno-asm -Wundef -frecord-gcc-switches -fsection-anchors -nostartfiles -g3 -W -Wall -Wuninitialized -maligned-data-sections
REM /Work fine with two compilers, pre and run test checked/ SET COPT= -DBRS_DERIVATIVE_TC23X -DBRS_OSC_CLK=20 -DBRS_TIMEBASE_CLOCK=100 -DBRS_OS_USECASE_OSGEN7 -DBRS_EVA_BOARD_TRIBOARD_TC2x4 -DBRS_PLATFORM_AURIX -DBRS_COMP_HIGHTEC -DBRS_CORE1_START_IMMEDIATE=0 -DBRS_CORE2_START_IMMEDIATE=0 -c  -save-temps -std=c99 -ffreestanding -fno-short-enums -fpeel-loops -falign-functions=4 -funsigned-bitfields -ffunction-sections -fno-ivopts -fno-peephole2 -O3 -mtc161 -D_GNU_C_TRICORE_=1 -save-temps -fno-asm -Wundef -frecord-gcc-switches -fsection-anchors -nostartfiles -g3 -W -Wall -Wuninitialized -maligned-data-sections
REM SET CUST_INCLUDE=-I../BSW/Aurix_MC-ISAR/general/tricore/inc/TC23x
REM SET CUST_INCLUDE=-I"../../../../ThirdParty/Mcal_Tc2xx/Supply/MC-ISAR_AS4XX_AURIX_TC23X_AB_PB_BASE_V600_REL-372/TC23x_ABstep/Aurix_MC-ISAR/tricore_general/ssc/inc" -I"../../../../ThirdParty/Mcal_Tc2xx/Supply/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/Common/TC23x" -I"../../../../bsw/_common/" -I"include" -I"gendata" -I"gendata/inc"
REM SET CUST_INCLUDE=-I"./../../../../ThirdParty/Mcal_Tc2xx/Supply/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/Common/TC23x"

REM SET SL_INCLUDE=-I"./01_IfxStl/Common" -I"./01_IfxStl/Common/TC23x" -I"./02_StlConfiguration/01_TresosConfig/02_UsedWorkcopyTresosOutput/output/inc" -I"./01_IfxStl/SafeWdgCD/SafeWdgIf/inc" -I"./01_IfxStl/SafeWdgCD/SafeWdgExtTlf/inc" -I"./01_IfxStl/SafeWdgCD/SafeWdgInt/inc" -I"./01_IfxStl/SafeWdgCD/SafeWdgQspi/inc" -I"./01_IfxStl/SafeTlibCD/SMU/inc" -I"./01_IfxStl/SafeTlibCD/TestHandler/inc" -I"./01_IfxStl/SafeTlibCD/MicroTestLib/inc"
REM SET SL_INCLUDE=-I"../../../../thirdparty/mcal_tc2xx/supply/pro-sil_aurix_safetlib_extended_tc23xab_as403_mr5_rel635/tc23x_abstep/01_safetlib/safetlibcd/testhandler/inc/" -I"../../../../thirdparty/mcal_tc2xx/supply/pro-sil_aurix_safetlib_extended_tc23xab_as403_mr5_rel635/tc23x_abstep/01_safetlib/common/" -I"../../../../thirdparty/mcal_tc2xx/supply/pro-sil_aurix_safetlib_extended_tc23xab_as403_mr5_rel635/tc23x_abstep/01_safetlib/cfg/inc/" -I"../../../../thirdparty/mcal_tc2xx/supply/pro-sil_aurix_safetlib_extended_tc23xab_as403_mr5_rel635/tc23x_abstep/01_safetlib/safetlibcd/microtestlib/inc/" -I"../../../../thirdparty/mcal_tc2xx/supply/pro-sil_aurix_safetlib_extended_tc23xab_as403_mr5_rel635/tc23x_abstep/01_safetlib/safetlibcd/smu/inc/" -I"../../../../thirdparty/mcal_tc2xx/supply/mc-isar_as4xx_aurix_tc23x_ab_pb_base_v600_rel-372/tc23x_abstep/aurix_mc-isar/general/tricore/inc/tc23x/" 


REM *
REM * Include folders
REM *
SET FULL_INCLUDE=-I"..\BaseSW\thirdparty\Mcal_Tc2xx\MC-ISAR_AS4XX_AURIX_TC23X_AB_PB_BASE_V600_REL-372\TC23x_ABstep\Aurix_MC-ISAR\adc_infineon_tricore\ssc\inc" -I"..\BaseSW\thirdparty\Mcal_Tc2xx\MC-ISAR_AS4XX_AURIX_TC23X_AB_PB_BASE_V600_REL-372\TC23x_ABstep\Aurix_MC-ISAR\dio_infineon_tricore\ssc\inc"  -I"..\BaseSW\thirdparty\Mcal_Tc2xx\MC-ISAR_AS4XX_AURIX_TC23X_AB_PB_BASE_V600_REL-372\TC23x_ABstep\Aurix_MC-ISAR\general\tricore\inc\TC23X"  -I"..\BaseSW\thirdparty\Mcal_Tc2xx\MC-ISAR_AS4XX_AURIX_TC23X_AB_PB_BASE_V600_REL-372\TC23x_ABstep\Aurix_MC-ISAR\gpt_infineon_tricore\ssc\inc"  -I"..\BaseSW\thirdparty\Mcal_Tc2xx\MC-ISAR_AS4XX_AURIX_TC23X_AB_PB_BASE_V600_REL-372\TC23x_ABstep\Aurix_MC-ISAR\icu_17\ssc\inc"  -I"...\BaseSW\thirdparty\Mcal_Tc2xx\MC-ISAR_AS4XX_AURIX_TC23X_AB_PB_BASE_V600_REL-372\TC23x_ABstep\Aurix_MC-ISAR\integration_general\inc"  -I"..\BaseSW\thirdparty\Mcal_Tc2xx\MC-ISAR_AS4XX_AURIX_TC23X_AB_PB_BASE_V600_REL-372\TC23x_ABstep\Aurix_MC-ISAR\irq_infineon_tricore\ssc\inc"  -I"..\BaseSW\thirdparty\Mcal_Tc2xx\MC-ISAR_AS4XX_AURIX_TC23X_AB_PB_BASE_V600_REL-372\TC23x_ABstep\Aurix_MC-ISAR\mcu_infineon_tricore\ssc\inc"  -I"..\BaseSW\thirdparty\Mcal_Tc2xx\MC-ISAR_AS4XX_AURIX_TC23X_AB_PB_BASE_V600_REL-372\TC23x_ABstep\Aurix_MC-ISAR\port_infineon_tricore\ssc\inc"  -I"..\BaseSW\thirdparty\Mcal_Tc2xx\MC-ISAR_AS4XX_AURIX_TC23X_AB_PB_BASE_V600_REL-372\TC23x_ABstep\Aurix_MC-ISAR\pwm_17_gtm_infineon_tricore\ssc\inc"  -I"..\BaseSW\thirdparty\Mcal_Tc2xx\MC-ISAR_AS4XX_AURIX_TC23X_AB_PB_BASE_V600_REL-372\TC23x_ABstep\Aurix_MC-ISAR\spi_infineon_tricore\ssc\inc"  -I"..\BaseSW\thirdparty\Mcal_Tc2xx\MC-ISAR_AS4XX_AURIX_TC23X_AB_PB_BASE_V600_REL-372\TC23x_ABstep\Aurix_MC-ISAR\tricore_general\ssc\inc"  -I"..\BaseSW\thirdparty\Mcal_Tc2xx\MC-ISAR_AS4XX_AURIX_TC23X_AB_PB_BASE_V600_REL-372\TC23x_ABstep\Aurix_MC-ISAR\fee_infineon_tricore\ssc\inc"  -I"..\BaseSW\thirdparty\Mcal_Tc2xx\MC-ISAR_AS4XX_AURIX_TC23X_AB_PB_BASE_V600_REL-372\TC23x_ABstep\Aurix_MC-ISAR\fls_17_pmu_infineon_tricore\ssc\inc"  -I"..\BaseSW\thirdparty\Mcal_Tc2xx\PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635\TC23x_ABstep\Aurix_MC-ISAR\irq_infineon_tricore\ssc\inc"  -I"..\BaseSW\thirdparty\Mcal_Tc2xx\PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635\TC23x_ABstep\01_SafeTlib\common"  -I"..\BaseSW\thirdparty\Mcal_Tc2xx\PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635\TC23x_ABstep\01_SafeTlib\SafeTlibCD\MicroTestLib\inc"  -I"..\BaseSW\thirdparty\Mcal_Tc2xx\PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635\TC23x_ABstep\01_SafeTlib\SafeTlibCD\SMU\inc"  -I"..\BaseSW\thirdparty\Mcal_Tc2xx\PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635\TC23x_ABstep\01_SafeTlib\SafeTlibCD\TestHandler\inc"  -I"..\BaseSW\microsar\BSW\_common" -I"..\BaseSW\microsar\BSW\bswm" -I"..\BaseSW\microsar\BSW\can" -I"..\BaseSW\microsar\BSW\canif" -I"..\BaseSW\microsar\BSW\cansm" -I"..\BaseSW\microsar\BSW\cantp" -I"..\BaseSW\microsar\BSW\cantrcv_30_tja1145" -I"..\BaseSW\microsar\BSW\canxcp" -I"..\BaseSW\microsar\BSW\com" -I"..\BaseSW\microsar\BSW\comm" -I"..\BaseSW\microsar\BSW\crc" -I"..\BaseSW\microsar\BSW\dcm" -I"..\BaseSW\microsar\BSW\dem" -I"..\BaseSW\microsar\BSW\det" -I"..\BaseSW\microsar\BSW\ecum" -I"..\BaseSW\microsar\GenData" -I"..\BaseSW\microsar\BSW\ipdum" -I"..\BaseSW\microsar\BSW\mcal_tc23x" -I"..\BaseSW\microsar\BSW\mcal_tc2xx" -I"..\BaseSW\microsar\BSW\memif" -I"..\BaseSW\microsar\BSW\nvm" -I"..\BaseSW\microsar\BSW\os" -I"..\BaseSW\microsar\BSW\pdur"  -I"..\BaseSW\microsar\BSW\vstdlib" -I"...\BaseSW\microsar\BSW\wdg_30_tle4278g" -I"..\BaseSW\microsar\BSW\wdgif" -I"..\BaseSW\microsar\BSW\wdgm" -I"..\BaseSW\microsar\BSW\xcp" -I"%compiler_path%\tricore\include" -I"%compiler_path%\tricore\include\machine" -I"..\BaseSW\microsar\GenData" -I"..\BaseSW\microsar\GenData\components" -I"..\BaseSW\microsar\GenData\inc" -I"..\BaseSW\microsar\ManData\inc"
REM -I"..\..\..\..\bsw\tc23x_abstep\aurix_mc-isar\uart_infineon_tricore\ssc\inc"


REM *
REM * Objects to link
REM *
::SET SBST_OBJ=../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SBST/SBST_Kernel_CoreTest.o ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SBST/SBST_Kernel_ISG.o ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SBST/SBST_Kernel_TestCode.o ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SBST/SBST_RelocTable.o ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SBST/SBST_TC16E_TestCode.o


REM *
REM * Setup build configurations
REM *
REM SET TLF_ABIST_COMPILING_MODE=ENABLE
SET TLF_ABIST_COMPILING_MODE=DISABLE
REM TC234LP_32F200N has no LMU
SET LMU_COMPILING_MODE=DISABLE  
REM SET LMU_COMPILING_MODE=ENABLE


REM *
REM * Perform actions
REM *
SET ERROR=0

REM A) Clean the SafeTlibCD Library
IF "%1"=="clean" (
	ECHO.
	ECHO ***********  Start Clean: SafeTlibCD library  ************
	ECHO.
	GOTO :CLEAN
)

REM B) Link the SafeTlibCD Library
IF "%1"=="link" (
	ECHO.
	ECHO ***********  Start Link: SafeTlibCD library  ************
	ECHO.
	GOTO :LINK
)

REM C) Build+Link the SafeTlibCD Library
ECHO.
ECHO ***********  Start Build: SafeTlibCD library  ************
ECHO.

REM Debug option for building a debug SafeTlibCD Library
IF "%1"=="debug" (
	ECHO.
	ECHO. WARNING: SMU Debug config enabled: 'Smu_PBCfg_Dbg.c to compile'
	ECHO.
	SET SMU_PBCFG=Smu_PBCfg_Dbg 
) ELSE ( 
	SET SMU_PBCFG=Smu_PBCfg )
    
REM Clear the object file list
SET OBJ_LIST=

REM compile each of the 03_HtxStlIf source files
REM CALL :CC ./03_HtxStlIf/ SafeExtWdg
REM CALL :CC ./03_HtxStlIf/ Callback
REM CALL :CC ./03_HtxStlIf/ Integration
REM CALL :CC ./03_HtxStlIf/ PreCond

REM ECHO TLF_ABIST_COMPILING_MODE = %TLF_ABIST_COMPILING_MODE%
REM IF "%TLF_ABIST_COMPILING_MODE%" == "ENABLE" (
REM     CALL :CC ./03_HtxStlIf/ Tlf_Abist
REM )

REM compile common files
REM CALL :CC ../../../../bsw/../thirdparty/mcal_tc2xx/supply/mc-isar_as4xx_aurix_tc23x_ab_pb_base_v600_rel-372/tc23x_abstep/aurix_mc-isar/tricore_general/ssc/src Mcal_TcLib
REM CALL :CC ../../../../BSW/../ThirdParty/Mcal_Tc2xx/Supply/MC-ISAR_AS4XX_AURIX_TC23X_AB_PB_BASE_V600_REL-372/TC23x_ABstep/Aurix_MC-ISAR/tricore_general/ssc/src Mcal_TcLib
REM CALL :CC ../../../../bsw/../thirdparty/mcal_tc2xx/supply/mc-isar_as4xx_aurix_tc23x_ab_pb_base_v600_rel-372/tc23x_abstep/aurix_mc-isar/tricore_general/ssc/src Mcal_WdgLib
REM CALL :CC ../../../../BSW/../ThirdParty/Mcal_Tc2xx/Supply/MC-ISAR_AS4XX_AURIX_TC23X_AB_PB_BASE_V600_REL-372/TC23x_ABstep/Aurix_MC-ISAR/tricore_general/ssc/src Mcal_WdgLib
REM CALL :CC ../../../../bsw/../ThirdParty/Mcal_Tc2xx/Supply/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/Common Sl_FlsErrPtrn
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/Common Sl_FlsErrPtrn
REM CALL :CC ../../../../BSW/../ThirdParty/Mcal_Tc2xx/Supply/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/Common Sl_Ipc
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/Common Sl_Ipc

REM compile each of the SafeTlibCD source files
CALL :CC ../BaseSW/microsar/GenData/src CpuBusMpuLfmTst_LCfg
CALL :CC ../BaseSW/microsar/GenData/src CpuMpuTst_LCfg
CALL :CC ../BaseSW/microsar/GenData/src IRTst_LCfg
CALL :CC ../BaseSW/microsar/GenData/src IomTst_LCfg
CALL :CC ../BaseSW/microsar/GenData/src PflashMonTst_LCfg
CALL :CC ../BaseSW/microsar/GenData/src PhlSramTst_LCfg
CALL :CC ../BaseSW/microsar/GenData/src SfrTst_LCfg
CALL :CC ../BaseSW/microsar/GenData/src %SMU_PBCFG%
CALL :CC ../BaseSW/microsar/GenData/src SpbTst_LCfg
CALL :CC ../BaseSW/microsar/GenData/src SramEccTst_LCfg
CALL :CC ../BaseSW/microsar/GenData/src SriTst_LCfg
CALL :CC ../BaseSW/microsar/GenData/src TstHandler_PBCfg
CALL :CC ../BaseSW/microsar/GenData/src WdgTst_LCfg
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src ClkmTst
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src CpuBusMpuLfmTst
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src CpuMpuTst
::CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src CpuSbstTst
REM FCE doesn't supported by TC23x - view Aurix_SafeTlib_UserManual page 314
REM CALL :CC ./SafeTlibCD/MicroTestLib/src  FceTst
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src IRTst
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src IRTst_IRTab
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src IomTst
IF "%LMU_COMPILING_MODE%" == "ENABLE" (
REM LmuBusMpuLfmTst doesn't supported by TC23x - view Aurix_SafeTlib_UserManual page 320
REM CALL :CC ./../../../../ThirdParty/Mcal_Tc2xx/Supply/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src  LmuBusMpuLfmTst
REM LmuRegAccProtTst doesn't supported by TC23x - view Aurix_SafeTlib_UserManual page 325
REM CALL :CC ./../../../../ThirdParty/Mcal_Tc2xx/Supply/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src  LmuRegAccProtTst
)
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src LockStepTst
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src Mtl_Trap
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src Mtl_TrapTab
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src PflashMonTst
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src PhlSramTst
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src PhlSramTst_MemDef
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src PmuEccEdcTst
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src PmuEccEdcTst_RefPtrnDef
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src RegAccProtTst
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src SffTst
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src SfrCmpTst
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src SfrCrcTst
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src SmuTst
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src SpbTst
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src SramEccTst
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src SramEccTst_MemDef
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src SriTst
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src TrapTst
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src VltmTst
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src WdgTst
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/MicroTestLib/src PmuEccEdcTst_PtrnDef
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/SMU/src Smu
CALL :CC ../BaseSW/thirdparty/Mcal_Tc2xx/PRO-SIL_AURIX_SAFETLIB_EXTENDED_TC23XAB_AS403_MR5_REL635/TC23x_ABstep/01_SafeTlib/SafeTlibCD/TestHandler/src TstHandler

GOTO :LINK




:CLEAN
	REM Remove the temporary files and the libraries
    ECHO Cleaning files from: %OUT_OBJ%
    IF EXIST %OUT_OBJ% RMDIR /s /q %OUT_OBJ% >NUL 2>&1
    if not %ERRORLEVEL% == 0 SET ERROR=1

    ECHO Cleaning files from: %OUT_LIB%
    IF EXIST %OUT_LIB% RMDIR /s /q %OUT_LIB% >NUL 2>&1
    if not %ERRORLEVEL% == 0 SET ERROR=1

    ECHO Cleaning files from: %OUT_LST%
    IF EXIST %OUT_LST% RMDIR /s /q %OUT_LST% >NUL 2>&1
    if not %ERRORLEVEL% == 0 SET ERROR=1

    ECHO Cleaning files from: %OUT_ERR%
    IF EXIST %OUT_ERR% RMDIR /s /q %OUT_ERR% >NUL 2>&1
    if not %ERRORLEVEL% == 0 SET ERROR=1

    if ERROR == 1 goto END_NOK
    goto :END_OK




REM helper to automate the compile task
:CC
    REM Construct the compilation options
    
    SET OBJ=%OUT_OBJ%/%2.o
	REM ECHO %OBJ%

    SET SRC=%1/%2.c
	REM ECHO %SRC%	

	REM	SET COMP=%CCTC% -o %OBJ_O% %CUST_INCLUDE% %SL_INCLUDE% %COPT% %SRC%
    SET COMP=%CCTC% -o %OBJ% %FULL_INCLUDE% %COPT% %SRC%
    
    REM Execute the compilation command
    ECHO Compiling file: %SRC%

    %COMP% 2> %OUT_ERR%/%2.err
    if not %ERRORLEVEL% == 0 SET ERROR=1

    ::ECHO %COMP%
    TYPE %OUT_ERR%\%2.err

    REM Organize compilation products
    MOVE %OUT_OBJ%\%2.i %OUT_LST%\%2.i >NUL 2>&1
    MOVE %OUT_OBJ%\%2.s %OUT_LST%\%2.s >NUL 2>&1

    REM Update the object file list
    
    if "%OBJ_LIST%"=="" (
        SET OBJ_LIST=%OBJ%
    ) ELSE (
        SET OBJ_LIST=%OBJ_LIST% %OBJ%
    )
    GOTO :EOF




:LINK
    REM Execute the linking command
	REM %ARTC% %OUT_LIB%/HTX_SAFETLIB.a %OBJ_LIST% %SBST_OBJ% -cr
	ECHO.
	ECHO Linking files:
	::ECHO %OBJ_LIST% %SBST_OBJ%
    ECHO %OBJ_LIST%

	::%ARTC% -cr %OUT_LIB%/HTX_SAFETLIB.a %OBJ_LIST% %SBST_OBJ% > %OUT_ERR%\HTX_SAFETLIB.err
    %ARTC% -cr %OUT_LIB%/HTX_SAFETLIB.a %OBJ_LIST% > %OUT_ERR%\HTX_SAFETLIB.err
    if not %ERRORLEVEL% == 0 SET ERROR=1

	TYPE %OUT_ERR%\HTX_SAFETLIB.err

    if ERROR == 1 goto END_NOK
    goto :END_OK




:END_NOK
	ECHO.
	ECHO ERR: Process failed
	ECHO.
	ECHO ***********  Finish Process: SafeTlibCD library  ***********
	ECHO.




:END_OK
	ECHO.
	ECHO OK: Process success! :)
	ECHO.
	ECHO ***********  Finish Process: SafeTlibCD library  ***********
	ECHO.

