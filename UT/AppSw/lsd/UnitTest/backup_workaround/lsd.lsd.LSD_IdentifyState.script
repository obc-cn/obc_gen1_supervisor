$testobject {
	$faultinjections {
		$faultinjection {
			$branch "IF+IF/THEN"
			$description """"""
			$before_decision '''
				LSD_zone_compensate[LSD_DRIVE].threshold_high = 0;
			'''
		}
	}

	$testcase 1 {
		$name ""
		$uuid "763a4296-3763-4a04-862d-010ba7f90ada"
		$specification """
			+ IN
			  + GLOBALS
			    - LSD_rawValue_in:9
			  + FUNCTION RETURNED VALUES
			    - LSD_CompensateThreshold():LOCK: 20-10 UNLOCK: 40-30 DRIVE: 60-50
			  + PARAMETERS
			    - LSD_battery_volt:ID: 70
			+ OUT
			  - Function Return:LSD_UNDEFINED_ZONE
			  - LSD_zone:LOCK: 20-10 UNLOCK: 40-30 DRIVE: LSD_SNA_U8-LSD_SNA_U8
			+ IN/OUT
			  - CompensateThreshold:Id
		"""

		$teststep 1.1 {
			$name ""
			$uuid "a3138800-49d1-4f00-8116-3bd5cf8f954a"
			$inputs {
				LSD_CompensateThreshold() = {40, 30, 20, 10}
				LSD_rawValue_in = 9
				LSD_zone[0] {
					threshold_high = 10
					threshold_low = 20
				}
				LSD_zone[1] {
					threshold_high = 30
					threshold_low = 40
				}
				LSD_zone[2] {
					threshold_high = 50
					threshold_low = 60
				}
				LSD_battery_volt = 70
			}
			$outputs {
				LSD_CompensateThreshold(LDS_ThresholdIN) = {30, 40, 10, 20}
				LSD_CompensateThreshold(LSD_battery_volt) = {70, 70, 70, 70}
				LSD_CompensateThreshold(LSD_compensationGain) = {LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_HIGH, LSD_BATTERY_GAIN_HIGH}
				return LSD_DRIVE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 2 {
		$name ""
		$uuid "69afc82f-054f-415c-89d0-de41ea5249e9"
		$specification """
			+ IN
			  + GLOBALS
			    - LSD_rawValue_in:10
			  + FUNCTION RETURNED VALUES
			    - LSD_CompensateThreshold():LOCK: 20-10 UNLOCK: 40-30 DRIVE: 60-50
			  + PARAMETERS
			    - LSD_battery_volt:ID: 70
			+ OUT
			  - Function Return:LSD_LOCK
			  - LSD_zone:LOCK: 20-10 UNLOCK: 40-30 DRIVE: LSD_SNA_U8-LSD_SNA_U8
			+ IN/OUT
			  - CompensateThreshold:Id
		"""

		$teststep 2.1 {
			$name ""
			$uuid "bdf63c20-8d7e-4ef4-9e16-8c1bc0bea4ce"
			$inputs {
				LSD_CompensateThreshold() = {40, 30, 20, 10}
				LSD_rawValue_in = 10
				LSD_zone[0] {
					threshold_high = 10
					threshold_low = 20
				}
				LSD_zone[1] {
					threshold_high = 30
					threshold_low = 40
				}
				LSD_zone[2] {
					threshold_high = 50
					threshold_low = 60
				}
				LSD_battery_volt = 70
			}
			$outputs {
				LSD_CompensateThreshold(LDS_ThresholdIN) = {30, 40, 10, 20}
				LSD_CompensateThreshold(LSD_battery_volt) = {70, 70, 70, 70}
				LSD_CompensateThreshold(LSD_compensationGain) = {LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_HIGH, LSD_BATTERY_GAIN_HIGH}
				return LSD_LOCK
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 3 {
		$name ""
		$uuid "9724e38f-25b8-464d-867b-1fd156fa8dd7"
		$specification """
			+ IN
			  + GLOBALS
			    - LSD_rawValue_in:11
			  + FUNCTION RETURNED VALUES
			    - LSD_CompensateThreshold():LOCK: 20-10 UNLOCK: 40-30 DRIVE: 60-50
			  + PARAMETERS
			    - LSD_battery_volt:ID: 70
			+ OUT
			  - Function Return:LSD_LOCK
			  - LSD_zone:LOCK: 20-10 UNLOCK: 40-30 DRIVE: LSD_SNA_U8-LSD_SNA_U8
			+ IN/OUT
			  - CompensateThreshold:Id
		"""

		$teststep 3.1 {
			$name ""
			$uuid "b62fbb5c-bea1-4676-bbd2-e04b0c15d768"
			$inputs {
				LSD_CompensateThreshold() = {40, 30, 20, 10}
				LSD_rawValue_in = 11
				LSD_zone[0] {
					threshold_high = 10
					threshold_low = 20
				}
				LSD_zone[1] {
					threshold_high = 30
					threshold_low = 40
				}
				LSD_zone[2] {
					threshold_high = 50
					threshold_low = 60
				}
				LSD_battery_volt = 70
			}
			$outputs {
				LSD_CompensateThreshold(LDS_ThresholdIN) = {30, 40, 10, 20}
				LSD_CompensateThreshold(LSD_battery_volt) = {70, 70, 70, 70}
				LSD_CompensateThreshold(LSD_compensationGain) = {LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_HIGH, LSD_BATTERY_GAIN_HIGH}
				return LSD_LOCK
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 4 {
		$name ""
		$uuid "5f6fc262-2d4a-4b22-83b4-cf4817a1c930"
		$specification """
			+ IN
			  + GLOBALS
			    - LSD_rawValue_in:19
			  + FUNCTION RETURNED VALUES
			    - LSD_CompensateThreshold():LOCK: 20-10 UNLOCK: 40-30 DRIVE: 60-50
			  + PARAMETERS
			    - LSD_battery_volt:ID: 70
			+ OUT
			  - Function Return:LSD_LOCK
			  - LSD_zone:LOCK: 20-10 UNLOCK: 40-30 DRIVE: LSD_SNA_U8-LSD_SNA_U8
			+ IN/OUT
			  - CompensateThreshold:Id
		"""

		$teststep 4.1 {
			$name ""
			$uuid "09d4817f-3e04-4c92-85a9-afeb36e7c3b9"
			$inputs {
				LSD_CompensateThreshold() = {40, 30, 20, 10}
				LSD_rawValue_in = 19
				LSD_zone[0] {
					threshold_high = 10
					threshold_low = 20
				}
				LSD_zone[1] {
					threshold_high = 30
					threshold_low = 40
				}
				LSD_zone[2] {
					threshold_high = 50
					threshold_low = 60
				}
				LSD_battery_volt = 70
			}
			$outputs {
				LSD_CompensateThreshold(LDS_ThresholdIN) = {30, 40, 10, 20}
				LSD_CompensateThreshold(LSD_battery_volt) = {70, 70, 70, 70}
				LSD_CompensateThreshold(LSD_compensationGain) = {LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_HIGH, LSD_BATTERY_GAIN_HIGH}
				return LSD_LOCK
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 5 {
		$name ""
		$uuid "e8c4b033-5570-4c1c-9d4f-69562b0b8ec0"
		$specification """
			+ IN
			  + GLOBALS
			    - LSD_rawValue_in:20
			  + FUNCTION RETURNED VALUES
			    - LSD_CompensateThreshold():LOCK: 20-10 UNLOCK: 40-30 DRIVE: 60-50
			  + PARAMETERS
			    - LSD_battery_volt:ID: 70
			+ OUT
			  - Function Return:LSD_LOCK
			  - LSD_zone:LOCK: 20-10 UNLOCK: 40-30 DRIVE: LSD_SNA_U8-LSD_SNA_U8
			+ IN/OUT
			  - CompensateThreshold:Id
		"""

		$teststep 5.1 {
			$name ""
			$uuid "e375ec9c-6101-435f-bbec-934ce7cd04b4"
			$inputs {
				LSD_CompensateThreshold() = {40, 30, 20, 10}
				LSD_rawValue_in = 20
				LSD_zone[0] {
					threshold_high = 10
					threshold_low = 20
				}
				LSD_zone[1] {
					threshold_high = 30
					threshold_low = 40
				}
				LSD_zone[2] {
					threshold_high = 50
					threshold_low = 60
				}
				LSD_battery_volt = 70
			}
			$outputs {
				LSD_CompensateThreshold(LDS_ThresholdIN) = {30, 40, 10, 20}
				LSD_CompensateThreshold(LSD_battery_volt) = {70, 70, 70, 70}
				LSD_CompensateThreshold(LSD_compensationGain) = {LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_HIGH, LSD_BATTERY_GAIN_HIGH}
				return LSD_LOCK
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 6 {
		$name ""
		$uuid "8c350c55-11a5-4119-adaf-4ef74df3142d"
		$specification """
			+ IN
			  + GLOBALS
			    - LSD_rawValue_in:21
			  + FUNCTION RETURNED VALUES
			    - LSD_CompensateThreshold():LOCK: 20-10 UNLOCK: 40-30 DRIVE: 60-50
			  + PARAMETERS
			    - LSD_battery_volt:ID: 70
			+ OUT
			  - Function Return:LSD_UNDEFINED_ZONE
			  - LSD_zone:LOCK: 20-10 UNLOCK: 40-30 DRIVE: LSD_SNA_U8-LSD_SNA_U8
			+ IN/OUT
			  - CompensateThreshold:Id
		"""

		$teststep 6.1 {
			$name ""
			$uuid "f75ab793-6224-45df-a9eb-b61ce5262649"
			$inputs {
				LSD_CompensateThreshold() = {40, 30, 20, 10}
				LSD_rawValue_in = 21
				LSD_zone[0] {
					threshold_high = 10
					threshold_low = 20
				}
				LSD_zone[1] {
					threshold_high = 30
					threshold_low = 40
				}
				LSD_zone[2] {
					threshold_high = 50
					threshold_low = 60
				}
				LSD_battery_volt = 70
			}
			$outputs {
				LSD_CompensateThreshold(LDS_ThresholdIN) = {30, 40, 10, 20}
				LSD_CompensateThreshold(LSD_battery_volt) = {70, 70, 70, 70}
				LSD_CompensateThreshold(LSD_compensationGain) = {LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_HIGH, LSD_BATTERY_GAIN_HIGH}
				return LSD_DRIVE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 7 {
		$name ""
		$uuid "5d7853ee-1b77-4650-aeff-638adf0303c4"
		$specification """
			+ IN
			  + GLOBALS
			    - LSD_rawValue_in:29
			  + FUNCTION RETURNED VALUES
			    - LSD_CompensateThreshold():LOCK: 20-10 UNLOCK: 40-30 DRIVE: 60-50
			  + PARAMETERS
			    - LSD_battery_volt:ID: 70
			+ OUT
			  - Function Return:LSD_UNDEFINED_ZONE
			  - LSD_zone:LOCK: 20-10 UNLOCK: 40-30 DRIVE: LSD_SNA_U8-LSD_SNA_U8
			+ IN/OUT
			  - CompensateThreshold:Id
		"""

		$teststep 7.1 {
			$name ""
			$uuid "3605a45b-4985-4a79-82e9-64ed94101f37"
			$inputs {
				LSD_CompensateThreshold() = {40, 30, 20, 10}
				LSD_rawValue_in = 29
				LSD_zone[0] {
					threshold_high = 10
					threshold_low = 20
				}
				LSD_zone[1] {
					threshold_high = 30
					threshold_low = 40
				}
				LSD_zone[2] {
					threshold_high = 50
					threshold_low = 60
				}
				LSD_battery_volt = 70
			}
			$outputs {
				LSD_CompensateThreshold(LDS_ThresholdIN) = {30, 40, 10, 20}
				LSD_CompensateThreshold(LSD_battery_volt) = {70, 70, 70, 70}
				LSD_CompensateThreshold(LSD_compensationGain) = {LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_HIGH, LSD_BATTERY_GAIN_HIGH}
				return LSD_DRIVE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 8 {
		$name ""
		$uuid "c55a45f8-ae94-4417-8832-a864b5032489"
		$specification """
			+ IN
			  + GLOBALS
			    - LSD_rawValue_in:30
			  + FUNCTION RETURNED VALUES
			    - LSD_CompensateThreshold():LOCK: 20-10 UNLOCK: 40-30 DRIVE: 60-50
			  + PARAMETERS
			    - LSD_battery_volt:ID: 70
			+ OUT
			  - Function Return:LSD_UNLOCK
			  - LSD_zone:LOCK: 20-10 UNLOCK: 40-30 DRIVE: LSD_SNA_U8-LSD_SNA_U8
			+ IN/OUT
			  - CompensateThreshold:Id
		"""

		$teststep 8.1 {
			$name ""
			$uuid "e62a49d0-f1ad-42ef-8c8d-82d9e5339445"
			$inputs {
				LSD_CompensateThreshold() = {40, 30, 20, 10}
				LSD_rawValue_in = 30
				LSD_zone[0] {
					threshold_high = 10
					threshold_low = 20
				}
				LSD_zone[1] {
					threshold_high = 30
					threshold_low = 40
				}
				LSD_zone[2] {
					threshold_high = 50
					threshold_low = 60
				}
				LSD_battery_volt = 70
			}
			$outputs {
				LSD_CompensateThreshold(LDS_ThresholdIN) = {30, 40, 10, 20}
				LSD_CompensateThreshold(LSD_battery_volt) = {70, 70, 70, 70}
				LSD_CompensateThreshold(LSD_compensationGain) = {LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_HIGH, LSD_BATTERY_GAIN_HIGH}
				return LSD_UNLOCK
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 9 {
		$name ""
		$uuid "764eb70b-ce2f-4d24-ba6a-d625fc223a6d"
		$specification """
			+ IN
			  + GLOBALS
			    - LSD_rawValue_in:31
			  + FUNCTION RETURNED VALUES
			    - LSD_CompensateThreshold():LOCK: 20-10 UNLOCK: 40-30 DRIVE: 60-50
			  + PARAMETERS
			    - LSD_battery_volt:ID: 70
			+ OUT
			  - Function Return:LSD_UNLOCK
			  - LSD_zone:LOCK: 20-10 UNLOCK: 40-30 DRIVE: LSD_SNA_U8-LSD_SNA_U8
			+ IN/OUT
			  - CompensateThreshold:Id
		"""

		$teststep 9.1 {
			$name ""
			$uuid "aec04a54-9cf2-4a56-80fe-ea9cfdab5d3f"
			$inputs {
				LSD_CompensateThreshold() = {40, 30, 20, 10}
				LSD_rawValue_in = 31
				LSD_zone[0] {
					threshold_high = 10
					threshold_low = 20
				}
				LSD_zone[1] {
					threshold_high = 30
					threshold_low = 40
				}
				LSD_zone[2] {
					threshold_high = 50
					threshold_low = 60
				}
				LSD_battery_volt = 70
			}
			$outputs {
				LSD_CompensateThreshold(LDS_ThresholdIN) = {30, 40, 10, 20}
				LSD_CompensateThreshold(LSD_battery_volt) = {70, 70, 70, 70}
				LSD_CompensateThreshold(LSD_compensationGain) = {LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_HIGH, LSD_BATTERY_GAIN_HIGH}
				return LSD_UNLOCK
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 10 {
		$name ""
		$uuid "0102ba68-7e5e-43ce-b17f-8db5eba1eb26"
		$specification """
			+ IN
			  + GLOBALS
			    - LSD_rawValue_in:39
			  + FUNCTION RETURNED VALUES
			    - LSD_CompensateThreshold():LOCK: 20-10 UNLOCK: 40-30 DRIVE: 60-50
			  + PARAMETERS
			    - LSD_battery_volt:ID: 70
			+ OUT
			  - Function Return:LSD_UNLOCK
			  - LSD_zone:LOCK: 20-10 UNLOCK: 40-30 DRIVE: LSD_SNA_U8-LSD_SNA_U8
			+ IN/OUT
			  - CompensateThreshold:Id
		"""

		$teststep 10.1 {
			$name ""
			$uuid "5811725b-d69a-4672-a730-d0b4ed7927cd"
			$inputs {
				LSD_CompensateThreshold() = {40, 30, 20, 10}
				LSD_rawValue_in = 39
				LSD_zone[0] {
					threshold_high = 10
					threshold_low = 20
				}
				LSD_zone[1] {
					threshold_high = 30
					threshold_low = 40
				}
				LSD_zone[2] {
					threshold_high = 50
					threshold_low = 60
				}
				LSD_battery_volt = 70
			}
			$outputs {
				LSD_CompensateThreshold(LDS_ThresholdIN) = {30, 40, 10, 20}
				LSD_CompensateThreshold(LSD_battery_volt) = {70, 70, 70, 70}
				LSD_CompensateThreshold(LSD_compensationGain) = {LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_HIGH, LSD_BATTERY_GAIN_HIGH}
				return LSD_UNLOCK
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 11 {
		$name ""
		$uuid "25cb9b12-80e9-4624-acf4-259b82808770"
		$specification """
			+ IN
			  + GLOBALS
			    - LSD_rawValue_in:40
			  + FUNCTION RETURNED VALUES
			    - LSD_CompensateThreshold():LOCK: 20-10 UNLOCK: 40-30 DRIVE: 60-50
			  + PARAMETERS
			    - LSD_battery_volt:ID: 70
			+ OUT
			  - Function Return:LSD_UNLOCK
			  - LSD_zone:LOCK: 20-10 UNLOCK: 40-30 DRIVE: LSD_SNA_U8-LSD_SNA_U8
			+ IN/OUT
			  - CompensateThreshold:Id
		"""

		$teststep 11.1 {
			$name ""
			$uuid "c28a407e-e992-4a63-81d9-cff37c1711f0"
			$inputs {
				LSD_CompensateThreshold() = {40, 30, 20, 10}
				LSD_rawValue_in = 40
				LSD_zone[0] {
					threshold_high = 10
					threshold_low = 20
				}
				LSD_zone[1] {
					threshold_high = 30
					threshold_low = 40
				}
				LSD_zone[2] {
					threshold_high = 50
					threshold_low = 60
				}
				LSD_battery_volt = 70
			}
			$outputs {
				LSD_CompensateThreshold(LDS_ThresholdIN) = {30, 40, 10, 20}
				LSD_CompensateThreshold(LSD_battery_volt) = {70, 70, 70, 70}
				LSD_CompensateThreshold(LSD_compensationGain) = {LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_HIGH, LSD_BATTERY_GAIN_HIGH}
				return LSD_UNLOCK
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 12 {
		$name ""
		$uuid "742ffa5c-5028-41c9-aecb-d4c9db26c601"
		$specification """
			+ IN
			  + GLOBALS
			    - LSD_rawValue_in:41
			  + FUNCTION RETURNED VALUES
			    - LSD_CompensateThreshold():LOCK: 20-10 UNLOCK: 40-30 DRIVE: 60-50
			  + PARAMETERS
			    - LSD_battery_volt:ID: 70
			+ OUT
			  - Function Return:LSD_UNDEFINED_ZONE
			  - LSD_zone:LOCK: 20-10 UNLOCK: 40-30 DRIVE: LSD_SNA_U8-LSD_SNA_U8
			+ IN/OUT
			  - CompensateThreshold:Id
		"""

		$teststep 12.1 {
			$name ""
			$uuid "2729903e-bc87-435c-b37f-fa3a5b40a93a"
			$inputs {
				LSD_CompensateThreshold() = {40, 30, 20, 10}
				LSD_rawValue_in = 41
				LSD_zone[0] {
					threshold_high = 10
					threshold_low = 20
				}
				LSD_zone[1] {
					threshold_high = 30
					threshold_low = 40
				}
				LSD_zone[2] {
					threshold_high = 50
					threshold_low = 60
				}
				LSD_battery_volt = 70
			}
			$outputs {
				LSD_CompensateThreshold(LDS_ThresholdIN) = {30, 40, 10, 20}
				LSD_CompensateThreshold(LSD_battery_volt) = {70, 70, 70, 70}
				LSD_CompensateThreshold(LSD_compensationGain) = {LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_HIGH, LSD_BATTERY_GAIN_HIGH}
				return LSD_DRIVE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 13 {
		$name ""
		$uuid "f1479ce4-8ca0-4fd8-99e6-5c018ab2c09b"
		$specification """
			+ IN
			  + GLOBALS
			    - LSD_rawValue_in:49
			  + FUNCTION RETURNED VALUES
			    - LSD_CompensateThreshold():LOCK: 20-10 UNLOCK: 40-30 DRIVE: 60-50
			  + PARAMETERS
			    - LSD_battery_volt:ID: 70
			+ OUT
			  - Function Return:LSD_UNDEFINED_ZONE
			  - LSD_zone:LOCK: 20-10 UNLOCK: 40-30 DRIVE: LSD_SNA_U8-LSD_SNA_U8
			+ IN/OUT
			  - CompensateThreshold:Id
		"""

		$teststep 13.1 {
			$name ""
			$uuid "93424e20-5f86-46f0-aa8d-39252bb497ed"
			$inputs {
				LSD_CompensateThreshold() = {40, 30, 20, 10}
				LSD_rawValue_in = 49
				LSD_zone[0] {
					threshold_high = 10
					threshold_low = 20
				}
				LSD_zone[1] {
					threshold_high = 30
					threshold_low = 40
				}
				LSD_zone[2] {
					threshold_high = 50
					threshold_low = 60
				}
				LSD_battery_volt = 70
			}
			$outputs {
				LSD_CompensateThreshold(LDS_ThresholdIN) = {30, 40, 10, 20}
				LSD_CompensateThreshold(LSD_battery_volt) = {70, 70, 70, 70}
				LSD_CompensateThreshold(LSD_compensationGain) = {LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_HIGH, LSD_BATTERY_GAIN_HIGH}
				return LSD_DRIVE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 14 {
		$name ""
		$uuid "768be682-d642-4fb0-b80b-9d33e61aea0e"
		$specification """
			+ IN
			  + GLOBALS
			    - LSD_rawValue_in:50
			  + FUNCTION RETURNED VALUES
			    - LSD_CompensateThreshold():LOCK: 20-10 UNLOCK: 40-30 DRIVE: 60-50
			  + PARAMETERS
			    - LSD_battery_volt:ID: 70
			+ OUT
			  - Function Return:LSD_DRIVE
			  - LSD_zone:LOCK: 20-10 UNLOCK: 40-30 DRIVE: LSD_SNA_U8-LSD_SNA_U8
			+ IN/OUT
			  - CompensateThreshold:Id
		"""

		$teststep 14.1 {
			$name ""
			$uuid "0a58b0a5-4849-4c4e-80c8-35496a8b2c62"
			$inputs {
				LSD_CompensateThreshold() = {40, 30, 20, 10}
				LSD_rawValue_in = 50
				LSD_zone[0] {
					threshold_high = 10
					threshold_low = 20
				}
				LSD_zone[1] {
					threshold_high = 30
					threshold_low = 40
				}
				LSD_zone[2] {
					threshold_high = 50
					threshold_low = 60
				}
				LSD_battery_volt = 70
			}
			$outputs {
				LSD_CompensateThreshold(LDS_ThresholdIN) = {30, 40, 10, 20}
				LSD_CompensateThreshold(LSD_battery_volt) = {70, 70, 70, 70}
				LSD_CompensateThreshold(LSD_compensationGain) = {LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_HIGH, LSD_BATTERY_GAIN_HIGH}
				return LSD_DRIVE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 15 {
		$name ""
		$uuid "cbfaa874-7bf4-4f0b-9ec4-3113dc77d927"
		$specification """
			+ IN
			  + GLOBALS
			    - LSD_rawValue_in:51
			  + FUNCTION RETURNED VALUES
			    - LSD_CompensateThreshold():LOCK: 20-10 UNLOCK: 40-30 DRIVE: 60-50
			  + PARAMETERS
			    - LSD_battery_volt:ID: 70
			+ OUT
			  - Function Return:LSD_DRIVE
			  - LSD_zone:LOCK: 20-10 UNLOCK: 40-30 DRIVE: LSD_SNA_U8-LSD_SNA_U8
			+ IN/OUT
			  - CompensateThreshold:Id
		"""

		$teststep 15.1 {
			$name ""
			$uuid "0647503b-bab5-4562-8f31-a47061e2e3c2"
			$inputs {
				LSD_CompensateThreshold() = {40, 30, 20, 10}
				LSD_rawValue_in = 51
				LSD_zone[0] {
					threshold_high = 10
					threshold_low = 20
				}
				LSD_zone[1] {
					threshold_high = 30
					threshold_low = 40
				}
				LSD_zone[2] {
					threshold_high = 50
					threshold_low = 60
				}
				LSD_battery_volt = 70
			}
			$outputs {
				LSD_CompensateThreshold(LDS_ThresholdIN) = {30, 40, 10, 20}
				LSD_CompensateThreshold(LSD_battery_volt) = {70, 70, 70, 70}
				LSD_CompensateThreshold(LSD_compensationGain) = {LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_HIGH, LSD_BATTERY_GAIN_HIGH}
				return LSD_DRIVE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 16 {
		$name ""
		$uuid "e76d37a3-ed0e-425c-9de2-05373948f0ed"
		$specification """
			+ IN
			  + GLOBALS
			    - LSD_rawValue_in:59
			  + FUNCTION RETURNED VALUES
			    - LSD_CompensateThreshold():LOCK: 20-10 UNLOCK: 40-30 DRIVE: 60-50
			  + PARAMETERS
			    - LSD_battery_volt:ID: 70
			+ OUT
			  - Function Return:LSD_DRIVE
			  - LSD_zone:LOCK: 20-10 UNLOCK: 40-30 DRIVE: LSD_SNA_U8-LSD_SNA_U8
			+ IN/OUT
			  - CompensateThreshold:Id
		"""

		$teststep 16.1 {
			$name ""
			$uuid "79726b5e-c6eb-4991-b7c2-e9c4fc5d2cf5"
			$inputs {
				LSD_CompensateThreshold() = {40, 30, 20, 10}
				LSD_rawValue_in = 59
				LSD_zone[0] {
					threshold_high = 10
					threshold_low = 20
				}
				LSD_zone[1] {
					threshold_high = 30
					threshold_low = 40
				}
				LSD_zone[2] {
					threshold_high = 50
					threshold_low = 60
				}
				LSD_battery_volt = 70
			}
			$outputs {
				LSD_CompensateThreshold(LDS_ThresholdIN) = {30, 40, 10, 20}
				LSD_CompensateThreshold(LSD_battery_volt) = {70, 70, 70, 70}
				LSD_CompensateThreshold(LSD_compensationGain) = {LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_HIGH, LSD_BATTERY_GAIN_HIGH}
				return LSD_DRIVE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 17 {
		$name ""
		$uuid "1bbc99d0-1735-490b-af81-3e70d0f1c1f8"
		$specification """
			+ IN
			  + GLOBALS
			    - LSD_rawValue_in:60
			  + FUNCTION RETURNED VALUES
			    - LSD_CompensateThreshold():LOCK: 20-10 UNLOCK: 40-30 DRIVE: 60-50
			  + PARAMETERS
			    - LSD_battery_volt:ID: 70
			+ OUT
			  - Function Return:LSD_DRIVE
			  - LSD_zone:LOCK: 20-10 UNLOCK: 40-30 DRIVE: LSD_SNA_U8-LSD_SNA_U8
			+ IN/OUT
			  - CompensateThreshold:Id
		"""

		$teststep 17.1 {
			$name ""
			$uuid "849dbcf7-a3bb-4ed6-b54a-33c229110474"
			$inputs {
				LSD_CompensateThreshold() = {40, 30, 20, 10}
				LSD_rawValue_in = 60
				LSD_zone[0] {
					threshold_high = 10
					threshold_low = 20
				}
				LSD_zone[1] {
					threshold_high = 30
					threshold_low = 40
				}
				LSD_zone[2] {
					threshold_high = 50
					threshold_low = 60
				}
				LSD_battery_volt = 70
			}
			$outputs {
				LSD_CompensateThreshold(LDS_ThresholdIN) = {30, 40, 10, 20}
				LSD_CompensateThreshold(LSD_battery_volt) = {70, 70, 70, 70}
				LSD_CompensateThreshold(LSD_compensationGain) = {LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_HIGH, LSD_BATTERY_GAIN_HIGH}
				return LSD_DRIVE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 18 {
		$name ""
		$uuid "6d828878-14d9-48b7-abf9-b6291d659b9f"
		$specification """
			+ IN
			  + GLOBALS
			    - LSD_rawValue_in:61
			  + FUNCTION RETURNED VALUES
			    - LSD_CompensateThreshold():LOCK: 20-10 UNLOCK: 40-30 DRIVE: 60-50
			  + PARAMETERS
			    - LSD_battery_volt:ID: 70
			+ OUT
			  - Function Return:LSD_UNDEFINED_ZONE
			  - LSD_zone:LOCK: 20-10 UNLOCK: 40-30 DRIVE: LSD_SNA_U8-LSD_SNA_U8
			+ IN/OUT
			  - CompensateThreshold:Id
		"""

		$teststep 18.1 {
			$name ""
			$uuid "be56d2c8-0e9b-4256-8da6-ae27a61d2c0e"
			$inputs {
				LSD_CompensateThreshold() = {40, 30, 20, 10}
				LSD_rawValue_in = 61
				LSD_zone[0] {
					threshold_high = 10
					threshold_low = 20
				}
				LSD_zone[1] {
					threshold_high = 30
					threshold_low = 40
				}
				LSD_zone[2] {
					threshold_high = 50
					threshold_low = 60
				}
				LSD_battery_volt = 70
			}
			$outputs {
				LSD_CompensateThreshold(LDS_ThresholdIN) = {30, 40, 10, 20}
				LSD_CompensateThreshold(LSD_battery_volt) = {70, 70, 70, 70}
				LSD_CompensateThreshold(LSD_compensationGain) = {LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_HIGH, LSD_BATTERY_GAIN_HIGH}
				return LSD_DRIVE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 19 {
		$name ""
		$uuid "6b43e7a9-f3ff-4828-9363-19f9dd8af32d"
		$specification """
			+ IN
			  + GLOBALS
			    - LSD_rawValue_in:61
			  + FUNCTION RETURNED VALUES
			    - LSD_CompensateThreshold():LOCK: 20-10 UNLOCK: 40-30 DRIVE: 60-50
			  + PARAMETERS
			    - LSD_battery_volt:LDS_SENSOR_LIMITATION + 1
			+ OUT
			  - Function Return:LSD_UNDEFINED_ZONE
			  - LSD_zone:LOCK: 20-10 UNLOCK: 40-30 DRIVE: LSD_SNA_U8-LSD_SNA_U8
			+ IN/OUT
			  - CompensateThreshold:LDS_SENSOR_LIMITATION
		"""

		$teststep 19.1 {
			$name ""
			$uuid "8bd3d951-0d90-4cae-b075-c7b8fceded41"
			$inputs {
				LSD_CompensateThreshold() = {40, 30, 20, 10}
				LSD_rawValue_in = 61
				LSD_zone[0] {
					threshold_high = 10
					threshold_low = 20
				}
				LSD_zone[1] {
					threshold_high = 30
					threshold_low = 40
				}
				LSD_zone[2] {
					threshold_high = 50
					threshold_low = 60
				}
				LSD_battery_volt = LDS_SENSOR_LIMITATION + 1
			}
			$outputs {
				LSD_CompensateThreshold(LDS_ThresholdIN) = {30, 40, 10, 20}
				LSD_CompensateThreshold(LSD_battery_volt) = {LDS_SENSOR_LIMITATION, +, 1, LDS_SENSOR_LIMITATION, +, 1, LDS_SENSOR_LIMITATION, LDS_SENSOR_LIMITATION}
				LSD_CompensateThreshold(LSD_compensationGain) = {LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_HIGH, LSD_BATTERY_GAIN_HIGH}
				return LSD_DRIVE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 20 {
		$name ""
		$uuid "27cfd443-278f-4a74-ac81-d0b248a0d099"
		$specification """
			+ IN
			  + GLOBALS
			    - LSD_rawValue_in:LSD_SNA_U8
			  + FUNCTION RETURNED VALUES
			    - LSD_CompensateThreshold():LOCK: 20-10 UNLOCK: 40-30 DRIVE: 60-50
			  + PARAMETERS
			    - LSD_battery_volt:ID: 70
			+ OUT
			  - Function Return:LSD_DRIVE
			  - LSD_zone:LOCK: 20-10 UNLOCK: 40-30 DRIVE: LSD_SNA_U8-LSD_SNA_U8
			+ IN/OUT
			  - CompensateThreshold:Id
		"""

		$teststep 20.1 {
			$name ""
			$uuid "c1fa8c0d-a86d-4b11-8e9f-60d06d0142bf"
			$inputs {
				LSD_CompensateThreshold() = {40, 30, 20, 10}
				LSD_rawValue_in = LSD_SNA_U8
				LSD_zone[0] {
					threshold_high = 10
					threshold_low = 20
				}
				LSD_zone[1] {
					threshold_high = 30
					threshold_low = 40
				}
				LSD_zone[2] {
					threshold_high = 50
					threshold_low = 60
				}
				LSD_battery_volt = 70
			}
			$outputs {
				LSD_CompensateThreshold(LDS_ThresholdIN) = {30, 40, 10, 20}
				LSD_CompensateThreshold(LSD_battery_volt) = {70, 70, 70, 70}
				LSD_CompensateThreshold(LSD_compensationGain) = {LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_HIGH, LSD_BATTERY_GAIN_HIGH}
				return LSD_DRIVE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 21 {
		$is_faultinjection
		$name ""
		$uuid "a6d0c363-0a18-491a-98bd-1d2abb32ad36"
		$specification """
			+ IN
			  + GLOBALS
			    - LSD_rawValue_in:LSD_SNA_U8
			  + FUNCTION RETURNED VALUES
			    - LSD_CompensateThreshold():LOCK: 20-10 UNLOCK: 40-30 DRIVE: 60-50
			  + PARAMETERS
			    - LSD_battery_volt:ID: 70
			+ OUT
			  - Function Return:LSD_DRIVE
			  - LSD_zone:LOCK: 20-10 UNLOCK: 40-30 DRIVE: LSD_SNA_U8-LSD_SNA_U8
			+ IN/OUT
			  - CompensateThreshold:Id
		"""

		$teststep 21.1 {
			$name ""
			$uuid "ed430188-0c98-4251-b8f6-b45a37487ddb"
			$inputs {
				LSD_CompensateThreshold() = {40, 30, 20, 10}
				LSD_rawValue_in = LSD_SNA_U8
				LSD_zone[0] {
					threshold_high = 10
					threshold_low = 20
				}
				LSD_zone[1] {
					threshold_high = 30
					threshold_low = 40
				}
				LSD_zone[2] {
					threshold_high = 50
					threshold_low = 60
				}
				LSD_battery_volt = 70
			}
			$outputs {
				LSD_CompensateThreshold(LDS_ThresholdIN) = {30, 40, 10, 20}
				LSD_CompensateThreshold(LSD_battery_volt) = {70, 70, 70, 70}
				LSD_CompensateThreshold(LSD_compensationGain) = {LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_LOW, LSD_BATTERY_GAIN_HIGH, LSD_BATTERY_GAIN_HIGH}
				return LSD_DRIVE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}
}