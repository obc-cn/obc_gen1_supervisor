$testobject {
	$specification """
		+BEHAVIOR FROM FEATURE:
		Subfeature OBC_InputVoltage_St:
		Signal OBC_InputVoltage_St has a state machine behaviour. The possible values are:
		0x00: No 220V AC
		0x01: 220 AC connected.
		0x02: 220V AC disconnected.
		0x03: Invalid value.
		In the init function, this signal shall be initialized to 0x00.
		The transition logic can be derived from requirements:
		If InputVoltageMode == Monophasic -> OBCP-24639 
		If InputVoltageMode == Triphasic -> OBCP-24640 
		InputVoltageThreshold is codified in V, offset 0 and gain 1.
		
		Subfeature OBC_ACRange:
		Signal OBC_ACRange has a state machine behaviour. The possible values are:
		0x00: No AC (<= 10 V)
		0x01: 110V (85 <---> 132 V)
		0x02: Invalid.
		0x03: 220V (170 <---> 265 V)
		In the init function, this signal shall be initialized to 0x00.
		The transition logic can be derived from requirements:
		If InputVoltageMode == Monophasic -> OBCP-24639 
		If InputVoltageMode == Triphasic -> OBCP-24640
	"""

	$testcase 1 {
		$name "Init; Debounce++; No disconnection"
		$uuid "88244747-ff33-4a82-a740-4ed07ef4e5fe"
		$specification """
			+ IN
			  + PARAMETERS
			    - CHG_RMSphase pointer: Passing check 0x12345678
			  + STATIC
			    - DebounceCounter: 0
			    - Previous ACrange: Cx0_No_AC_10V_
			    - VoltState_Previous: Cx0_No_220V_AC
			  + GLOBAL
			    - VoltState: Other 255
			    - CHG_InVoltRange: Ramdom 88
			  + STUB
			    - Current ACrange: Cx0_No_AC_10V_
			    - Current VoltageSt: Other 200
			+ OUT
			  - VoltState_Previous: VoltState 255
			  - DebounceCounter: 1
			  - VoltState: 255
			  - CHG_InVoltRange: No Change 88
			  - CHG_InstantaneousVoltagestate: 0x12345678
		"""

		$teststep 1.1 {
			$name ""
			$uuid "df0dfd6e-3ff8-4086-afc3-487e3bf1caa3"
			$inputs {
				CHG_InVoltRange = 88
				CHG_InputVoltageState::DebounceCounter#1 = 0
				CHG_InputVoltageState::Previous#1 {
					ACrange = Cx0_No_AC_10V_
				}
				CHG_InputVoltageState::VoltState_Previous#1 = Cx0_No_220V_AC
				CHG_InstantaneousVoltagestate() {
					VoltageSt = 200
					ACrange = Cx0_No_AC_10V_
				}
				VoltState = 255
				CHG_RMSphase = 0x12345678
			}
			$outputs {
				CHG_InVoltRange = 88
				CHG_InputVoltageState::DebounceCounter#1 = 1
				CHG_InputVoltageState::Previous#1 {
					ACrange = Cx0_No_AC_10V_
				}
				CHG_InputVoltageState::VoltState_Previous#1 = 255
				CHG_InstantaneousVoltagestate(CHG_RMSphase) = 0x12345678
				Current_VoltageSt = 200
				Rte_CpApCHG_PpInt_OBC_ACRange_OBC_ACRange = 88
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = 255
				VoltState = 255
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 2 {
		$name "Debounce reset"
		$uuid "9928c761-4131-40a5-8d7e-a575d14dae3d"
		$specification """
			+ IN
			  + PARAMETERS
			    - CHG_RMSphase pointer: Passing check 0x12345678
			  + STATIC
			    - DebounceCounter: CHG_AC_CONFIRM_TIME
			    - Previous ACrange: Cx2_Invalid
			    - VoltState_Previous: Cx0_No_220V_AC
			  + GLOBAL
			    - VoltState: Other 255
			    - CHG_InVoltRange: Ramdom 88
			  + STUB
			    - Current ACrange: Cx0_No_AC_10V_
			    - Current VoltageSt: Other 200
			+ OUT
			  - VoltState_Previous: VoltState 255
			  - DebounceCounter: 0
			  - VoltState: 255
			  - CHG_InVoltRange: No Change 88
			  - CHG_InstantaneousVoltagestate: 0x12345678
		"""

		$teststep 2.1 {
			$name ""
			$uuid "abcceca8-7121-4be2-ba3b-bf77425b527e"
			$inputs {
				CHG_InVoltRange = 88
				CHG_InputVoltageState::DebounceCounter#1 = CHG_AC_CONFIRM_TIME
				CHG_InputVoltageState::Previous#1 {
					ACrange = Cx2_Invalid
				}
				CHG_InputVoltageState::VoltState_Previous#1 = Cx0_No_220V_AC
				CHG_InstantaneousVoltagestate() {
					VoltageSt = 200
					ACrange = Cx0_No_AC_10V_
				}
				VoltState = 255
				CHG_RMSphase = 0x12345678
			}
			$outputs {
				CHG_InVoltRange = 88
				CHG_InputVoltageState::DebounceCounter#1 = 0
				CHG_InputVoltageState::Previous#1 {
					ACrange = Cx0_No_AC_10V_
				}
				CHG_InputVoltageState::VoltState_Previous#1 = 255
				CHG_InstantaneousVoltagestate(CHG_RMSphase) = 0x12345678
				Current_VoltageSt = 200
				Rte_CpApCHG_PpInt_OBC_ACRange_OBC_ACRange = 88
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = 255
				VoltState = 255
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 3 {
		$name "Debounce Over Limit; No disconnection"
		$uuid "8142e2aa-11e8-40f2-bd9d-8e9df9b1879d"
		$specification """
			+ IN
			  + PARAMETERS
			    - CHG_RMSphase pointer: Passing check 0x12345678
			  + STATIC
			    - DebounceCounter: CHG_AC_CONFIRM_TIME
			    - Previous ACrange: Cx0_No_AC_10V_
			    - VoltState_Previous: Cx0_No_220V_AC
			  + GLOBAL
			    - VoltState: Other 255
			    - CHG_InVoltRange: Ramdom 88
			  + STUB
			    - Current ACrange: Cx0_No_AC_10V_
			    - Current VoltageSt: Cx0_No_220V_AC
			+ OUT
			  - VoltState_Previous: Cx0_No_220V_AC
			  - DebounceCounter: CHG_AC_CONFIRM_TIME
			  - VoltState: Cx0_No_220V_AC
			  - CHG_InVoltRange: Cx0_No_AC_10V_
			  - CHG_InstantaneousVoltagestate: 0x12345678
		"""

		$teststep 3.1 {
			$name ""
			$uuid "583b9985-3b15-4bd5-a5b5-c6362f8e5792"
			$inputs {
				CHG_InVoltRange = 88
				CHG_InputVoltageState::DebounceCounter#1 = CHG_AC_CONFIRM_TIME
				CHG_InputVoltageState::Previous#1 {
					ACrange = Cx0_No_AC_10V_
				}
				CHG_InputVoltageState::VoltState_Previous#1 = Cx0_No_220V_AC
				CHG_InstantaneousVoltagestate() {
					VoltageSt = Cx0_No_220V_AC
					ACrange = Cx0_No_AC_10V_
				}
				VoltState = 255
				CHG_RMSphase = 0x12345678
			}
			$outputs {
				CHG_InVoltRange = Cx0_No_AC_10V_
				CHG_InputVoltageState::DebounceCounter#1 = CHG_AC_CONFIRM_TIME
				CHG_InputVoltageState::Previous#1 {
					ACrange = Cx0_No_AC_10V_
				}
				CHG_InputVoltageState::VoltState_Previous#1 = Cx0_No_220V_AC
				CHG_InstantaneousVoltagestate(CHG_RMSphase) = 0x12345678
				Current_VoltageSt = Cx0_No_220V_AC
				Rte_CpApCHG_PpInt_OBC_ACRange_OBC_ACRange = Cx0_No_AC_10V_
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = Cx0_No_220V_AC
				VoltState = Cx0_No_220V_AC
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 4 {
		$name "Debounce Over Limit; Disconnection"
		$uuid "2e110b95-d0ad-4075-8305-47b416be5f8c"
		$specification """
			+ IN
			  + PARAMETERS
			    - CHG_RMSphase pointer: Passing check 0x12345678
			  + STATIC
			    - DebounceCounter: CHG_AC_CONFIRM_TIME
			    - Previous ACrange: Cx0_No_AC_10V_
			    - VoltState_Previous: Other 255
			  + GLOBAL
			    - VoltState: Other 255
			    - CHG_InVoltRange: Ramdom 88
			  + STUB
			    - Current ACrange: Cx0_No_AC_10V_
			    - Current VoltageSt: Cx0_No_220V_AC
			+ OUT
			  - VoltState_Previous: Cx0_No_220V_AC
			  - DebounceCounter: CHG_AC_CONFIRM_TIME
			  - VoltState: Cx2_220V_AC_disconnected
			  - CHG_InVoltRange: Cx0_No_AC_10V_
			  - CHG_InstantaneousVoltagestate: 0x12345678
		"""

		$teststep 4.1 {
			$name ""
			$uuid "37bfa62a-d87d-4a11-ad09-abb76cc53ade"
			$inputs {
				CHG_InVoltRange = 88
				CHG_InputVoltageState::DebounceCounter#1 = CHG_AC_CONFIRM_TIME
				CHG_InputVoltageState::Previous#1 {
					ACrange = Cx0_No_AC_10V_
				}
				CHG_InputVoltageState::VoltState_Previous#1 = 255
				CHG_InstantaneousVoltagestate() {
					VoltageSt = Cx0_No_220V_AC
					ACrange = Cx0_No_AC_10V_
				}
				VoltState = 255
				CHG_RMSphase = 0x12345678
			}
			$outputs {
				CHG_InVoltRange = Cx0_No_AC_10V_
				CHG_InputVoltageState::DebounceCounter#1 = CHG_AC_CONFIRM_TIME
				CHG_InputVoltageState::Previous#1 {
					ACrange = Cx0_No_AC_10V_
				}
				CHG_InputVoltageState::VoltState_Previous#1 = Cx0_No_220V_AC
				CHG_InstantaneousVoltagestate(CHG_RMSphase) = 0x12345678
				Current_VoltageSt = Cx0_No_220V_AC
				Rte_CpApCHG_PpInt_OBC_ACRange_OBC_ACRange = Cx0_No_AC_10V_
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = Cx2_220V_AC_disconnected
				VoltState = Cx2_220V_AC_disconnected
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 5 {
		$name "Debounce++; No disconnection"
		$uuid "6276ce0a-1ef3-4dcf-872a-638a80259fa4"
		$specification """
			+ IN
			  + PARAMETERS
			    - CHG_RMSphase pointer: Passing check 0x12345678
			  + STATIC
			    - DebounceCounter: CHG_AC_CONFIRM_TIME-1
			    - Previous ACrange: Cx0_No_AC_10V_
			    - VoltState_Previous: Cx0_No_220V_AC
			  + GLOBAL
			    - VoltState: Other 255
			    - CHG_InVoltRange: Ramdom 88
			  + STUB
			    - Current ACrange: Cx0_No_AC_10V_
			    - Current VoltageSt: Other 200
			+ OUT
			  - VoltState_Previous: 200
			  - DebounceCounter: CHG_AC_CONFIRM_TIME
			  - VoltState: 200
			  - CHG_InVoltRange: Cx0_No_AC_10V_
			  - CHG_InstantaneousVoltagestate: 0x12345678
		"""

		$teststep 5.1 {
			$name ""
			$uuid "53667eb6-0a46-4bd0-8305-3d2795393871"
			$inputs {
				CHG_InVoltRange = 88
				CHG_InputVoltageState::DebounceCounter#1 = CHG_AC_CONFIRM_TIME - 1
				CHG_InputVoltageState::Previous#1 {
					ACrange = Cx0_No_AC_10V_
				}
				CHG_InputVoltageState::VoltState_Previous#1 = Cx0_No_220V_AC
				CHG_InstantaneousVoltagestate() {
					VoltageSt = 200
					ACrange = Cx0_No_AC_10V_
				}
				VoltState = 255
				CHG_RMSphase = 0x12345678
			}
			$outputs {
				CHG_InVoltRange = Cx0_No_AC_10V_
				CHG_InputVoltageState::DebounceCounter#1 = CHG_AC_CONFIRM_TIME
				CHG_InputVoltageState::Previous#1 {
					ACrange = Cx0_No_AC_10V_
				}
				CHG_InputVoltageState::VoltState_Previous#1 = 200
				CHG_InstantaneousVoltagestate(CHG_RMSphase) = 0x12345678
				Current_VoltageSt = 200
				Rte_CpApCHG_PpInt_OBC_ACRange_OBC_ACRange = Cx0_No_AC_10V_
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = 200
				VoltState = 200
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 6 {
		$name "Debounce Over Limit; No disconnection"
		$uuid "98931e23-6b77-470d-8640-1d944f2715ee"
		$specification """
			+ IN
			  + PARAMETERS
			    - CHG_RMSphase pointer: Passing check 0x12345678
			  + STATIC
			    - DebounceCounter: Max
			    - Previous ACrange: Cx0_No_AC_10V_
			    - VoltState_Previous: Cx0_No_220V_AC
			  + GLOBAL
			    - VoltState: Other 255
			    - CHG_InVoltRange: Ramdom 88
			  + STUB
			    - Current ACrange: Cx0_No_AC_10V_
			    - Current VoltageSt: Cx0_No_220V_AC
			+ OUT
			  - VoltState_Previous: Cx0_No_220V_AC
			  - DebounceCounter: Max
			  - VoltState: Cx0_No_220V_AC
			  - CHG_InVoltRange: Cx0_No_AC_10V_
			  - CHG_InstantaneousVoltagestate: 0x12345678
		"""

		$teststep 6.1 {
			$name ""
			$uuid "68bf5dca-f2bf-4181-8461-6aa1e1355428"
			$inputs {
				CHG_InVoltRange = 88
				CHG_InputVoltageState::DebounceCounter#1 = *max*
				CHG_InputVoltageState::Previous#1 {
					ACrange = Cx0_No_AC_10V_
				}
				CHG_InputVoltageState::VoltState_Previous#1 = Cx0_No_220V_AC
				CHG_InstantaneousVoltagestate() {
					VoltageSt = Cx0_No_220V_AC
					ACrange = Cx0_No_AC_10V_
				}
				VoltState = 255
				CHG_RMSphase = 0x12345678
			}
			$outputs {
				CHG_InVoltRange = Cx0_No_AC_10V_
				CHG_InputVoltageState::DebounceCounter#1 = *max*
				CHG_InputVoltageState::Previous#1 {
					ACrange = Cx0_No_AC_10V_
				}
				CHG_InputVoltageState::VoltState_Previous#1 = Cx0_No_220V_AC
				CHG_InstantaneousVoltagestate(CHG_RMSphase) = 0x12345678
				Current_VoltageSt = Cx0_No_220V_AC
				Rte_CpApCHG_PpInt_OBC_ACRange_OBC_ACRange = Cx0_No_AC_10V_
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = Cx0_No_220V_AC
				VoltState = Cx0_No_220V_AC
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}
}