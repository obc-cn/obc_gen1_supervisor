$testobject {
	$specification """
		OBCP-24635:
		If 
			OBC_Status_CANSignal == 'Standby mode'  (0x02) 
		and 
			OutputElockSensor == 'Locked Position' (0x00) 
		and 
			BMS_OnBoardChargerEnable_CANSignal == 'Charging Allow' (0x01) 
		and 
			BMS_MainConnectorsState_CANSignal == 'Contactors closed' (0x02) 
		and 
			RelayS2PhysicalValue == TRUE 
		and 
			OBC_ChargingConnectionConfirmation_CANSignal == 'Full connected' (0x02) 
		and 
			FaultChargeStop == FALSE 
		then 
		OBC_Status_CANSignal shall perform a transition to 'Conversion' (0x03).
		
		FaultChargeStop = TBD.
	"""

	$testcase 1 {
		$name "Error by Fault level"
		$uuid "0d01268f-6fc8-4902-8aac-0b773c5dd10e"
		$specification """
			+ IN
			  - GeneralStop: TRUE
			  - LaunchAllowed: TRUE
			  - GoToError: FALSE
			  - CHG_obcState: Init for change check 255
			  - ConnectionConfirmation: Cx2_full_connected
			  - FaultLevel: OBC_FAULT_LEVEL_1
			+ OUT
			  - CHG_obcState: Cx4_error_mode
		"""

		$teststep 1.1 {
			$name ""
			$uuid "64f13bf8-5076-4946-b660-bfc0a9fd29a1"
			$inputs {
				CHG_obcState = 255
				Rte_CpApOFM_PpInt_OBC_Fault_OBC_Fault = OBC_FAULT_LEVEL_1
				CHG_OBC_ConnectionConfirmation = Cx2_full_connected
				LaunchAllowed = TRUE
				CHG_Generalstop = TRUE
				GoToError = FALSE
			}
			$outputs {
				CHG_obcState = Cx4_error_mode
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 2 {
		$name "Error by GoToError"
		$uuid "b9eae613-9503-42f8-b88a-3e05fdd73601"
		$specification """
			+ IN
			  - GeneralStop: FALSE
			  - LaunchAllowed: FALSE
			  - GoToError: TRUE
			  - CHG_obcState: Init for change check 255
			  - ConnectionConfirmation: Other 255
			  - FaultLevel: Other 255
			+ OUT
			  - CHG_obcState: Cx4_error_mode
		"""

		$teststep 2.1 {
			$name ""
			$uuid "f2ff06dc-1367-4e7d-a11b-7eb613195238"
			$inputs {
				CHG_obcState = 255
				Rte_CpApOFM_PpInt_OBC_Fault_OBC_Fault = 255
				CHG_OBC_ConnectionConfirmation = Cx1_not_connected
				LaunchAllowed = FALSE
				CHG_Generalstop = FALSE
				GoToError = TRUE
			}
			$outputs {
				CHG_obcState = Cx4_error_mode
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 3 {
		$name "To conversion TRUE Launch"
		$uuid "d17d306c-8db7-4076-a8b9-50aade047c0d"
		$specification """
			+ IN
			  - GeneralStop: FALSE
			  - LaunchAllowed: TRUE
			  - GoToError: FALSE
			  - CHG_obcState: Init for change check 255
			  - ConnectionConfirmation: Cx2_full_connected
			  - FaultLevel: Other 255
			+ OUT
			  - CHG_obcState: Cx3_conversion_working_
		"""

		$teststep 3.1 {
			$name ""
			$uuid "09ee09ef-a5e3-4221-aca2-1ce0d90f974f"
			$inputs {
				CHG_obcState = 255
				Rte_CpApOFM_PpInt_OBC_Fault_OBC_Fault = 255
				CHG_OBC_ConnectionConfirmation = Cx2_full_connected
				LaunchAllowed = TRUE
				CHG_Generalstop = FALSE
				GoToError = FALSE
			}
			$outputs {
				CHG_obcState = Cx3_conversion_working_
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 4 {
		$name "To conversion true Launch"
		$uuid "47b4cd73-53de-480d-8e9b-0ec90c645eac"
		$specification """
			+ IN
			  - GeneralStop: FALSE
			  - LaunchAllowed: true other 255
			  - GoToError: FALSE
			  - CHG_obcState: Init for change check 255
			  - ConnectionConfirmation: Cx2_full_connected
			  - FaultLevel: Other 255
			+ OUT
			  - CHG_obcState: Cx3_conversion_working_
		"""

		$teststep 4.1 {
			$name ""
			$uuid "e467cc0a-9a4f-48f5-a4b5-53d7a85e11ec"
			$inputs {
				CHG_obcState = 255
				Rte_CpApOFM_PpInt_OBC_Fault_OBC_Fault = 255
				CHG_OBC_ConnectionConfirmation = Cx2_full_connected
				LaunchAllowed = 255
				CHG_Generalstop = FALSE
				GoToError = FALSE
			}
			$outputs {
				CHG_obcState = Cx3_conversion_working_
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 5 {
		$name "To off by no connected"
		$uuid "e7041764-f04d-4841-8be0-e70858148492"
		$specification """
			+ IN
			  - GeneralStop: true other 255
			  - LaunchAllowed: TRUE
			  - GoToError: FALSE
			  - CHG_obcState: Init for change check 255
			  - ConnectionConfirmation: Other 255
			  - FaultLevel: Other 255
			+ OUT
			  - CHG_obcState: Cx0_off_mode
		"""

		$teststep 5.1 {
			$name ""
			$uuid "3b5c178e-ca00-4665-a800-7c93c76fde30"
			$inputs {
				CHG_obcState = 255
				Rte_CpApOFM_PpInt_OBC_Fault_OBC_Fault = 255
				CHG_OBC_ConnectionConfirmation = Cx1_not_connected
				LaunchAllowed = TRUE
				CHG_Generalstop = *none*
				GoToError = FALSE
			}
			$outputs {
				CHG_obcState = Cx0_off_mode
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 6 {
		$name "No change by GeneralStop TRUE"
		$uuid "9201f93a-52db-4e7d-95e4-1c08c3d1b43d"
		$specification """
			+ IN
			  - GeneralStop: TRUE
			  - LaunchAllowed: TRUE
			  - GoToError: FALSE
			  - CHG_obcState: Init for change check 255
			  - ConnectionConfirmation: Cx2_full_connected
			  - FaultLevel: Other 255
			+ OUT
			  - CHG_obcState: No Change 255
		"""

		$teststep 6.1 {
			$name ""
			$uuid "952d9235-815b-4217-a869-88457c74b841"
			$inputs {
				CHG_obcState = 255
				Rte_CpApOFM_PpInt_OBC_Fault_OBC_Fault = 255
				CHG_OBC_ConnectionConfirmation = Cx2_full_connected
				LaunchAllowed = TRUE
				CHG_Generalstop = TRUE
				GoToError = FALSE
			}
			$outputs {
				CHG_obcState = 255
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 7 {
		$name "No change by LaunchAllowed FALSE"
		$uuid "67b025f5-7816-40aa-babb-6a364ba99328"
		$specification """
			+ IN
			  - GeneralStop: FALSE
			  - LaunchAllowed: FALSE
			  - GoToError: FALSE
			  - CHG_obcState: Init for change check 255
			  - ConnectionConfirmation: Cx2_full_connected
			  - FaultLevel: Other 255
			+ OUT
			  - CHG_obcState: No Change 255
		"""

		$teststep 7.1 {
			$name ""
			$uuid "aa003880-0d23-4f29-b47f-79a958095957"
			$inputs {
				CHG_obcState = 255
				Rte_CpApOFM_PpInt_OBC_Fault_OBC_Fault = 255
				CHG_OBC_ConnectionConfirmation = Cx2_full_connected
				LaunchAllowed = FALSE
				CHG_Generalstop = FALSE
				GoToError = FALSE
			}
			$outputs {
				CHG_obcState = 255
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 8 {
		$name "No condition met"
		$uuid "f8eb3ab9-2ba7-41cd-9941-69b8ada7dea2"
		$specification """
			+ IN
			  - GeneralStop: TRUE
			  - LaunchAllowed: FALSE
			  - GoToError: FALSE
			  - CHG_obcState: Init for change check 255
			  - ConnectionConfirmation: Cx2_full_connected
			  - FaultLevel: Other 255
			+ OUT
			  - CHG_obcState: No Change 255
		"""

		$teststep 8.1 {
			$name ""
			$uuid "26c56a8b-e7bf-4670-a525-21cb1650a28b"
			$inputs {
				CHG_obcState = 255
				Rte_CpApOFM_PpInt_OBC_Fault_OBC_Fault = 255
				CHG_OBC_ConnectionConfirmation = Cx2_full_connected
				LaunchAllowed = FALSE
				CHG_Generalstop = TRUE
				GoToError = FALSE
			}
			$outputs {
				CHG_obcState = 255
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 9 {
		$name "Both condition met"
		$uuid "f64f63d4-a63a-43d9-a982-b93b5d4ee3a6"
		$specification """
			+ IN
			  - GeneralStop: FALSE
			  - LaunchAllowed: TRUE
			  - GoToError: FALSE
			  - CHG_obcState: Init for change check 255
			  - ConnectionConfirmation: Other 255
			  - FaultLevel: Other 255
			+ OUT
			  - CHG_obcState: Cx0_off_mode
		"""

		$teststep 9.1 {
			$name ""
			$uuid "a382be8b-55e2-4d56-aac4-bffde3b05801"
			$inputs {
				CHG_obcState = 255
				Rte_CpApOFM_PpInt_OBC_Fault_OBC_Fault = 255
				CHG_OBC_ConnectionConfirmation = Cx1_not_connected
				LaunchAllowed = TRUE
				CHG_Generalstop = FALSE
				GoToError = FALSE
			}
			$outputs {
				CHG_obcState = Cx0_off_mode
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}
}