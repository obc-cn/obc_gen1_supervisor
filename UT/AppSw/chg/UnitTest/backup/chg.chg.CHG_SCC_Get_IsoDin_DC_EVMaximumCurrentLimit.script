$testobject {

	$testcase 1 {
		$name "NULL MaxCurrent"
		$uuid "8a1922ad-8bae-41da-967a-ca72fcf5f480"
		$specification """
			+ INPUT
			  - Pointer EVMaximumCurrentLimit: NULL
			  - Pointer Flag: No NULL
			  - Global MaxCurrent: Positive
			+ OUTPUT
			  - value Flag: FALSE
		"""

		$teststep 1.1 {
			$name ""
			$uuid "67f87632-44ad-43ae-ba1c-7807ee71a20d"
			$inputs {
				CHG_PLC_data {
					MaxCurrent {
						Value = 5000
						Exponent = 100
					}
				}
				EVMaximumCurrentLimit = NULL
				Flag = target_Flag
				&target_Flag = 182
			}
			$outputs {
				&target_Flag = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 2 {
		$name "NULL Flag"
		$uuid "4b5022e7-ad24-4eff-b01f-4a012d187e08"
		$specification """
			+ INPUT
			  - Pointer EVMaximumCurrentLimit: No NULL
			  - Pointer Flag: NULL
			  - Global MaxCurrent: Negative
			+ OUTPUT
			  - value EVMaximumCurrentLimit: ignore
		"""

		$teststep 2.1 {
			$name ""
			$uuid "801fa82c-373b-44b6-a4b2-27e43baef449"
			$inputs {
				CHG_PLC_data {
					MaxCurrent {
						Value = -5000
						Exponent = -100
					}
				}
				EVMaximumCurrentLimit = target_EVMaximumCurrentLimit
				Flag = NULL
				&target_EVMaximumCurrentLimit {
					Value = 2795
					Exponent = 41
				}
			}
			$outputs {
				&target_EVMaximumCurrentLimit {
					Value = *none*
					Exponent = *none*
				}
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 3 {
		$name "NULL Both"
		$uuid "ec3eda47-4ef1-4fdb-aeba-42ca2433dd7f"
		$specification """
			+ INPUT
			  - Pointer EVMaximumCurrentLimit: NULL
			  - Pointer Flag: NULL
			  - Global MaxCurrent: 0
		"""

		$teststep 3.1 {
			$name ""
			$uuid "64ab343d-1b2d-4bc9-9c6d-3f7da036a67f"
			$inputs {
				CHG_PLC_data {
					MaxCurrent {
						Value = 0
						Exponent = 0
					}
				}
				EVMaximumCurrentLimit = NULL
				Flag = NULL
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 4 {
		$name "Positive Curent"
		$uuid "f57a8a87-210f-43b3-aaa4-eeadb6ebc086"
		$specification """
			+ INPUT
			  - Pointer EVMaximumCurrentLimit: No NULL
			  - Pointer Flag: No NULL
			  - Global MaxCurrent: Positive
			+ OUTPUT
			  - value EVMaximumCurrentLimit: Positive
			  - value Flag: TRUE
		"""

		$teststep 4.1 {
			$name ""
			$uuid "06699dff-b067-4ad3-809f-3e96d5cdb6e8"
			$inputs {
				CHG_PLC_data {
					MaxCurrent {
						Value = 5000
						Exponent = 100
					}
				}
				EVMaximumCurrentLimit = target_EVMaximumCurrentLimit
				Flag = target_Flag
				&target_EVMaximumCurrentLimit {
					Value = 17315
					Exponent = 100
				}
				&target_Flag = 0
			}
			$outputs {
				&target_EVMaximumCurrentLimit {
					Value = 5000
					Exponent = 100
				}
				&target_Flag = TRUE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 5 {
		$name "Negative Current"
		$uuid "888dba26-c067-48be-99fd-3332d3f5ff06"
		$specification """
			+ INPUT
			  - Pointer EVMaximumCurrentLimit: No NULL
			  - Pointer Flag: No NULL
			  - Global MaxCurrent: Negative
			+ OUTPUT
			  - value EVMaximumCurrentLimit: Negative
			  - value Flag: TRUE
		"""

		$teststep 5.1 {
			$name ""
			$uuid "78e9fcd8-0529-4cfd-b60e-ce4155957d49"
			$inputs {
				CHG_PLC_data {
					MaxCurrent {
						Value = -5000
						Exponent = -100
					}
				}
				EVMaximumCurrentLimit = target_EVMaximumCurrentLimit
				Flag = target_Flag
				&target_EVMaximumCurrentLimit {
					Value = 21822
					Exponent = 45
				}
				&target_Flag = 0
			}
			$outputs {
				&target_EVMaximumCurrentLimit {
					Value = -5000
					Exponent = -100
				}
				&target_Flag = TRUE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 6 {
		$name "0 = current"
		$uuid "d8526646-f9c5-41aa-aebd-87c3daa269ed"
		$specification """
			+ INPUT
			  - Pointer EVMaximumCurrentLimit: No NULL
			  - Global MaxCurrent: 0
			+ OUTPUT
			  - value EVMaximumCurrentLimit: 0
			  - value Flag: TRUE
		"""

		$teststep 6.1 {
			$name ""
			$uuid "f4808cfe-06ee-4607-9b33-fc1c844ae865"
			$inputs {
				CHG_PLC_data {
					MaxCurrent {
						Value = 0
						Exponent = 0
					}
				}
				EVMaximumCurrentLimit = target_EVMaximumCurrentLimit
				Flag = target_Flag
				&target_EVMaximumCurrentLimit {
					Value = 9476
					Exponent = 106
				}
				&target_Flag = 0
			}
			$outputs {
				&target_EVMaximumCurrentLimit {
					Value = 0
					Exponent = 0
				}
				&target_Flag = TRUE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}
}