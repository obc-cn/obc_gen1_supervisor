$testobject {

	$testcase 1 {
		$name "InRange Low Derating"
		$uuid "d21efdf6-cf38-4806-9948-5872e534218a"
		$specification """
			+ PARAMETERS
			  - OBCTemperature: InLow 8
			  - LowDerRange OBCLowTempStartDerating, OBCLowTempEndDerating: Normal: 10/5
			  - HighDerRange OBCHighTempStartDerating OBCHighTempEndDerating: Normal: 115/125
		"""
		$description """
			Temperature in LowDerating Range
		"""

		$teststep 1.1 {
			$name ""
			$uuid "8c69390a-c9ab-4ccd-ba1f-9c63cf7cf002"
			$inputs {
				OBCTemperature = 8
				OBCLowTempEndDerating = 5
				OBCLowTempStartDerating = 10
				OBCHighTempStartDerating = 115
				OBCHighTempEndDerating = 125
			}
			$outputs {
				return 614
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 2 {
		$name "InRange High Derating"
		$uuid "b3094524-1055-4f89-900a-fe7433113634"
		$specification """
			+ PARAMETERS
			  - OBCTemperature: InHigh 120
			  - LowDerRange OBCLowTempStartDerating, OBCLowTempEndDerating: Normal: 10/5
			  - HighDerRange OBCHighTempStartDerating OBCHighTempEndDerating: Normal: 115/125
		"""
		$description """
			Temperature in High Derating Range
		"""

		$teststep 2.1 {
			$name ""
			$uuid "03d3acb5-1fb8-4a26-a51e-81ea7f4fb1cf"
			$inputs {
				OBCTemperature = 120
				OBCLowTempEndDerating = 5
				OBCLowTempStartDerating = 10
				OBCHighTempStartDerating = 115
				OBCHighTempEndDerating = 125
			}
			$outputs {
				return 512
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 3 {
		$name "Null pointers"
		$uuid "308830d2-dc04-4869-adb1-d3e1fc2f4da3"
		$specification """
			+ PARAMETERS
			  - OBCTemperature: NoDer 25
			  - LowDerRange OBCLowTempStartDerating, OBCLowTempEndDerating: Normal: 10/5
			  - HighDerRange OBCHighTempStartDerating OBCHighTempEndDerating: Normal: 115/125
		"""

		$teststep 3.1 {
			$name ""
			$uuid "fbb19b96-66bf-453a-b332-9d817d703324"
			$inputs {
				OBCTemperature = 25
				OBCLowTempEndDerating = 5
				OBCLowTempStartDerating = 10
				OBCHighTempStartDerating = 115
				OBCHighTempEndDerating = 125
			}
			$outputs {
				return 1024
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 4 {
		$name "Below Lower"
		$uuid "cb767db1-7302-4e19-a927-e372eb00972c"
		$specification """
			+ PARAMETERS
			  - OBCTemperature: BelowLow 2
			  - LowDerRange OBCLowTempStartDerating, OBCLowTempEndDerating: Normal: 10/5
			  - HighDerRange OBCHighTempStartDerating OBCHighTempEndDerating: Normal: 115/125
		"""
		$description """
			Temperatura below lower
		"""

		$teststep 4.1 {
			$name ""
			$uuid "7dec7acf-68f6-4348-a495-fdf2ec8b1646"
			$inputs {
				OBCTemperature = 2
				OBCLowTempEndDerating = 5
				OBCLowTempStartDerating = 10
				OBCHighTempStartDerating = 115
				OBCHighTempEndDerating = 125
			}
			$outputs {
				return 0
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 5 {
		$name "Over Higher"
		$uuid "f63f13d3-7a9e-4ab9-8048-339829285bf4"
		$specification """
			+ PARAMETERS
			  - OBCTemperature: OverHigh 150
			  - LowDerRange OBCLowTempStartDerating, OBCLowTempEndDerating: Normal: 10/5
			  - HighDerRange OBCHighTempStartDerating OBCHighTempEndDerating: Normal: 115/125
		"""
		$description """
			Temperature over higher
		"""

		$teststep 5.1 {
			$name ""
			$uuid "9d806f24-335b-45df-8004-6eb6ad0b7b51"
			$inputs {
				OBCTemperature = 150
				OBCLowTempEndDerating = 5
				OBCLowTempStartDerating = 10
				OBCHighTempStartDerating = 115
				OBCHighTempEndDerating = 125
			}
			$outputs {
				return 0
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 6 {
		$name "Starting at Low Der with power 0 "
		$uuid "60bb8471-fb57-4508-9865-5b9f5b6b4bdf"
		$specification """
			+ PARAMETERS
			  - OBCTemperature: InLow 8
			  - LowDerRange OBCLowTempStartDerating, OBCLowTempEndDerating: Normal: 10/5
			  - HighDerRange OBCHighTempStartDerating OBCHighTempEndDerating: Normal: 115/125
		"""
		$description """
			Start with temperature in low derating rnage and power 0
		"""

		$teststep 6.1 {
			$name ""
			$uuid "1a831e14-a89e-4d4a-b783-a852172d0815"
			$inputs {
				OBCTemperature = 8
				OBCLowTempEndDerating = 5
				OBCLowTempStartDerating = 10
				OBCHighTempStartDerating = 115
				OBCHighTempEndDerating = 125
			}
			$outputs {
				return 614
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 7 {
		$name "Starting at High Derating with Power 0"
		$uuid "ce5cbe74-b2f5-451c-bdb9-8e98d5700940"
		$specification """
			+ PARAMETERS
			  - OBCTemperature: InHigh 120
			  - LowDerRange OBCLowTempStartDerating, OBCLowTempEndDerating: Normal: 10/5
			  - HighDerRange OBCHighTempStartDerating OBCHighTempEndDerating: Normal: 115/125
		"""

		$teststep 7.1 {
			$name ""
			$uuid "61765931-4436-4c75-835c-f70c8513bbd0"
			$inputs {
				OBCTemperature = 120
				OBCLowTempEndDerating = 5
				OBCLowTempStartDerating = 10
				OBCHighTempStartDerating = 115
				OBCHighTempEndDerating = 125
			}
			$outputs {
				return 512
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 8 {
		$name "High Range Error (negative)"
		$uuid "fd03a6b0-59ff-42f5-bfa0-f554b5161dfb"
		$specification """
			+ PARAMETERS
			  - OBCTemperature: InHigh 120
			  - LowDerRange OBCLowTempStartDerating, OBCLowTempEndDerating: Normal: 10/5
			  - HighDerRange OBCHighTempStartDerating OBCHighTempEndDerating: Negative: 125/115
		"""
		$description """
			Error in High Derating Range, Start temperature > End temperature
		"""

		$teststep 8.1 {
			$name ""
			$uuid "efa4e9fb-0ff0-46a4-96c1-6132d1d84eba"
			$inputs {
				OBCTemperature = 120
				OBCLowTempEndDerating = 5
				OBCLowTempStartDerating = 10
				OBCHighTempStartDerating = 125
				OBCHighTempEndDerating = 115
			}
			$outputs {
				return 0
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 9 {
		$name "High derating range error start temperature == end temperature"
		$uuid "e8dfc28d-afb9-4925-911d-f83d4e10f0d9"
		$specification """
			+ PARAMETERS
			  - OBCTemperature: NoDer 25
			  - LowDerRange OBCLowTempStartDerating, OBCLowTempEndDerating: Normal: 10/5
			  - HighDerRange OBCHighTempStartDerating OBCHighTempEndDerating: 0: 115/115
		"""

		$teststep 9.1 {
			$name ""
			$uuid "0bdf9db8-d979-4659-92c7-c6bee763430d"
			$inputs {
				OBCTemperature = 25
				OBCLowTempEndDerating = 5
				OBCLowTempStartDerating = 10
				OBCHighTempStartDerating = 115
				OBCHighTempEndDerating = 115
			}
			$outputs {
				return 0
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 10 {
		$name "Low range error (negative)"
		$uuid "4f4fa163-62ad-4179-9090-ad6c3359ce01"
		$specification """
			+ PARAMETERS
			  - OBCTemperature: NoDer 25
			  - LowDerRange OBCLowTempStartDerating, OBCLowTempEndDerating: Negative: 0/5
			  - HighDerRange OBCHighTempStartDerating OBCHighTempEndDerating: Normal: 115/125
		"""

		$teststep 10.1 {
			$name ""
			$uuid "3b84e93b-567c-4fbe-8599-cb4128e890b5"
			$inputs {
				OBCTemperature = 25
				OBCLowTempEndDerating = 5
				OBCLowTempStartDerating = 0
				OBCHighTempStartDerating = 115
				OBCHighTempEndDerating = 125
			}
			$outputs {
				return 0
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 11 {
		$name "Low range error (0)"
		$uuid "966d6229-0ef3-4cb3-b5df-e1d95af1eff6"
		$specification """
			+ PARAMETERS
			  - OBCTemperature: InLow 8
			  - LowDerRange OBCLowTempStartDerating, OBCLowTempEndDerating: 0: 10/10
			  - HighDerRange OBCHighTempStartDerating OBCHighTempEndDerating: Normal: 115/125
		"""

		$teststep 11.1 {
			$name ""
			$uuid "9e3fd521-804b-4dd3-8f4a-d91bb9334750"
			$inputs {
				OBCTemperature = 8
				OBCLowTempEndDerating = 10
				OBCLowTempStartDerating = 10
				OBCHighTempStartDerating = 115
				OBCHighTempEndDerating = 125
			}
			$outputs {
				return 0
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}
}