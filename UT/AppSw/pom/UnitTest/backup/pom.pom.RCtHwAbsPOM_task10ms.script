$testobject {

	$testcase 1 {
		$name "Identify value assignation"
		$uuid "44c1a760-2172-4d6c-8b85-9650cf8bb60a"
		$specification """
			DFA signals has to be assigned to their local variables
		"""
		$description """
			Value assignation from DFA to local variables
		"""

		$teststep 1.1 {
			$name ""
			$uuid "b2baa12b-26a7-4662-bcb2-bdda30d854f4"
			$inputs {
				POM_DC_conversion() = 11
				POM_dutyKeep[0] = 7
				POM_dutyKeep[1] = 8
				POM_relayDC() = {60, 61, 62}
				POM_relayDC(counter) = target_POM_relayDC_counter
				POM_relayStateCounter[0] = 1
				POM_relayStateCounter[1] = 2
				POM_timeClose[0] = 4
				POM_timeClose[1] = 5
				Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedBrCtl = 1
				Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedGrCtl = 1
				Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedRrCtl = 1
				Rte_CpApFCL_PpExtPlgLedrCtl_DeExtPlgLedrCtl = 10
				Rte_CpApRLY_PpOutputPrechargeRelays_DeOutputPrechargeRelays = 30
				Rte_CpApRLY_PpOutputRelayMono_DeOutputRelayMono = 40
			}
			$outputs {
				POM_DC_conversion(DC_percentage) = {10, 1}
				POM_dutyKeep[0] = 0
				POM_relayDC(DCclosed) = {0, 0}
				POM_relayDC(StateIn) = {40, 30}
				POM_relayDC(TimeCloseing) = {0, 0}
				POM_timeClose[0] = 0
				&target_POM_relayDC_counter = 2
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 2 {
		$name "Max value assignation"
		$uuid "39a1eb48-9c6b-4f6a-baba-aca232cc9403"
		$specification """
			Max value assignation into local variable
		"""
		$description """
			Max value assignation check
		"""

		$teststep 2.1 {
			$name ""
			$uuid "4c8822a5-882e-4514-882b-43a761a0390b"
			$inputs {
				POM_DC_conversion() = *max*
				POM_dutyKeep[0] = *max*
				POM_dutyKeep[1] = *max*
				POM_relayDC() = *max*
				POM_relayDC(counter) = target_POM_relayDC_counter
				POM_relayStateCounter[0] = *max*
				POM_relayStateCounter[1] = *max*
				POM_timeClose[0] = *max*
				POM_timeClose[1] = *max*
				Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedBrCtl = *max*
				Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedGrCtl = *max*
				Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedRrCtl = *max*
				Rte_CpApFCL_PpExtPlgLedrCtl_DeExtPlgLedrCtl = *max*
				Rte_CpApRLY_PpOutputPrechargeRelays_DeOutputPrechargeRelays = *max*
				Rte_CpApRLY_PpOutputRelayMono_DeOutputRelayMono = *max*
			}
			$outputs {
				POM_DC_conversion(DC_percentage) = {255, 255}
				POM_dutyKeep[0] = 0
				POM_relayDC(DCclosed) = {0, 0}
				POM_relayDC(StateIn) = {255, 255}
				POM_relayDC(TimeCloseing) = {0, 0}
				POM_timeClose[0] = 0
				&target_POM_relayDC_counter = 65535
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 3 {
		$name "Call Trace"
		$uuid "71e70aa9-7fb7-4738-b52f-88939228869e"
		$specification """
			POM_task10ms shall call:
			1. GET_POM_U8ExtPlgLedrCtl(value)
			2. pwm_setDutyLock(duty)
		"""
		$description """
			CallTrace check.
		"""

		$teststep 3.1 {
			$name ""
			$uuid "7c8daf6d-cbd6-43b8-8bb4-b14468a124da"
			$inputs {
				POM_DC_conversion() = *none*
				POM_dutyKeep[0] = *none*
				POM_dutyKeep[1] = *none*
				POM_relayDC() = *none*
				POM_relayDC(counter) = *none*
				POM_relayStateCounter[0] = *none*
				POM_relayStateCounter[1] = *none*
				POM_timeClose[0] = *none*
				POM_timeClose[1] = *none*
				Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedBrCtl = 0
				Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedGrCtl = 0
				Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedRrCtl = 0
				Rte_CpApFCL_PpExtPlgLedrCtl_DeExtPlgLedrCtl = *none*
				Rte_CpApRLY_PpOutputPrechargeRelays_DeOutputPrechargeRelays = *none*
				Rte_CpApRLY_PpOutputRelayMono_DeOutputRelayMono = *none*
			}
			$outputs {
				POM_DC_conversion(DC_percentage) = *none*
				POM_dutyKeep[0] = 0
				POM_relayDC(DCclosed) = *none*
				POM_relayDC(StateIn) = *none*
				POM_relayDC(TimeCloseing) = *none*
				POM_timeClose[0] = 0
			}
			$calltrace {
				POM_UpdateCalibratables
				POM_DC_conversion
				Pwm_17_Gtm_SetPeriodAndDuty
				POM_relayDC
				Pwm_17_Gtm_SetPeriodAndDuty
				POM_relayDC
				Pwm_17_Gtm_SetPeriodAndDuty
				POM_DC_conversion
				Pwm_17_Gtm_SetPeriodAndDuty
				POM_DC_conversion
				Pwm_17_Gtm_SetPeriodAndDuty
				POM_DC_conversion
				Pwm_17_Gtm_SetPeriodAndDuty
			}
		}
	}
}