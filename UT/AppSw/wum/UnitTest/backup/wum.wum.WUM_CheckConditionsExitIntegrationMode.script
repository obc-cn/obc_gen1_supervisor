$testobject {
	$stubfunctions {
		unsigned char WUM_received3FramesInOneSecondEIM(unsigned char * value) '''
			*value = received3Frames_value_TS;
			
			return received3Frames_return_TS;
		'''
	}

	$testcase 4 {
		$name "FALSE by all"
		$uuid "be4f111a-1aeb-43cb-921c-29906a0e158c"
		$specification """
			+ INPUT
			  + STUB
			    - 3 Frames Received return: FALSE
			    - 3 Frames Received value: Other 255
			  + RTE
			    - VCU_DiagMuxOnPwt: 0
			    - SUPV_RCDLineState: TRUE
		"""

		$teststep 4.1 {
			$name ""
			$uuid "15cd8e40-b94a-4f49-bc91-364744b29061"
			$inputs {
				Rte_CpApPCOM_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt = 0
				Rte_CpApRCD_PpInt_SUPV_RCDLineState_SUPV_RCDLineState = TRUE
				received3Frames_return_TS = FALSE
				received3Frames_value_TS = 255
			}
			$outputs {
				return FALSE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 5 {
		$name "FALSE with 3FramesReceived TRUE"
		$uuid "b9c46feb-6898-4fed-ae02-845c8631dbf4"
		$specification """
			+ INPUT
			  + STUB
			    - 3 Frames Received return: TRUE
			    - 3 Frames Received value: Other 255
			  + RTE
			    - VCU_DiagMuxOnPwt: 0
			    - SUPV_RCDLineState: TRUE
		"""

		$teststep 5.1 {
			$name ""
			$uuid "f9c24d10-c780-4e84-ba03-7d4e71870c47"
			$inputs {
				Rte_CpApPCOM_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt = 0
				Rte_CpApRCD_PpInt_SUPV_RCDLineState_SUPV_RCDLineState = TRUE
				received3Frames_return_TS = TRUE
				received3Frames_value_TS = 255
			}
			$outputs {
				return FALSE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 10 {
		$name "FALSE with 3FramesReceived true"
		$uuid "eefa1a16-0471-49b9-b7d4-76eb7a35f681"
		$specification """
			+ INPUT
			  + STUB
			    - 3 Frames Received return: Other 255
			    - 3 Frames Received value: Other 255
			  + RTE
			    - VCU_DiagMuxOnPwt: 0
			    - SUPV_RCDLineState: TRUE
		"""

		$teststep 10.1 {
			$name ""
			$uuid "9f8481af-7f2c-497f-81e4-4e45e566c5ec"
			$inputs {
				Rte_CpApPCOM_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt = 0
				Rte_CpApRCD_PpInt_SUPV_RCDLineState_SUPV_RCDLineState = TRUE
				received3Frames_return_TS = 255
				received3Frames_value_TS = 255
			}
			$outputs {
				return FALSE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 6 {
		$name "FALSE with FrameValue == 0"
		$uuid "96917e69-76d3-472a-baca-e12fe462612d"
		$specification """
			+ INPUT
			  + STUB
			    - 3 Frames Received return: FALSE
			    - 3 Frames Received value: 0
			  + RTE
			    - VCU_DiagMuxOnPwt: 0
			    - SUPV_RCDLineState: Other 255
		"""

		$teststep 6.1 {
			$name ""
			$uuid "35f55753-3312-4bcc-9bf9-e5e4fca6ba48"
			$inputs {
				Rte_CpApPCOM_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt = 0
				Rte_CpApRCD_PpInt_SUPV_RCDLineState_SUPV_RCDLineState = 255
				received3Frames_return_TS = FALSE
				received3Frames_value_TS = 0
			}
			$outputs {
				return FALSE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 7 {
		$name "TRUE by DiagmUX"
		$uuid "567ea0ac-f112-43a0-aac8-d433a40ffb16"
		$specification """
			+ INPUT
			  + STUB
			    - 3 Frames Received return: FALSE
			    - 3 Frames Received value: Other 255
			  + RTE
			    - VCU_DiagMuxOnPwt: 1
			    - SUPV_RCDLineState: TRUE
		"""

		$teststep 7.1 {
			$name ""
			$uuid "3b1d011f-5d7c-4272-959d-8872415cbe7f"
			$inputs {
				Rte_CpApPCOM_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt = 1
				Rte_CpApRCD_PpInt_SUPV_RCDLineState_SUPV_RCDLineState = TRUE
				received3Frames_return_TS = FALSE
				received3Frames_value_TS = 255
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 8 {
		$name "TRUE Frames 0"
		$uuid "e51334d4-d9a2-489c-87f6-f5a5358c0adf"
		$specification """
			+ INPUT
			  + STUB
			    - 3 Frames Received return: TRUE
			    - 3 Frames Received value: 0
			  + RTE
			    - VCU_DiagMuxOnPwt: 1
			    - SUPV_RCDLineState: TRUE
		"""

		$teststep 8.1 {
			$name ""
			$uuid "f93e8d7a-dfa2-4c33-aa25-69f56404270d"
			$inputs {
				Rte_CpApPCOM_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt = 1
				Rte_CpApRCD_PpInt_SUPV_RCDLineState_SUPV_RCDLineState = TRUE
				received3Frames_return_TS = TRUE
				received3Frames_value_TS = 0
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 9 {
		$name "TRUE by RCDLine"
		$uuid "d32d2163-7cbc-4f68-8444-24034810a1e1"
		$specification """
			+ INPUT
			  + STUB
			    - 3 Frames Received return: FALSE
			    - 3 Frames Received value: Other 255
			  + RTE
			    - VCU_DiagMuxOnPwt: 0
			    - SUPV_RCDLineState: FALSE
		"""

		$teststep 9.1 {
			$name ""
			$uuid "bc6032b9-dca3-4c18-ba28-6c723516d8bb"
			$inputs {
				Rte_CpApPCOM_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt = 0
				Rte_CpApRCD_PpInt_SUPV_RCDLineState_SUPV_RCDLineState = FALSE
				received3Frames_return_TS = FALSE
				received3Frames_value_TS = 255
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}
}