$testobject {
	$stubfunctions {
		unsigned char WUM_received3FramesInOneSecondEIM(unsigned char * value) '''
			*value = received3Frames_value_TS;
			
			return received3Frames_return_TS;
		'''
	}

	$testcase 1 {
		$name "Enter Mode"
		$uuid "acccca62-ecb8-4f9c-9656-01c43278da21"
		$specification """
			+ INPUT
			  + STUB
			    - 3 Frames Received return: Other 255
			    - 3 Frames Received value: WUM_ELECTRON_BYTE_MASK
			  + RTE
			    - VCU_DiagMuxOnPwt: 0
			    - SUPV_RCDLineState: TRUE
			    - DeAppRCDECUState: NotUsed
			- OUTPUT: TRUE
		"""

		$teststep 1.1 {
			$name ""
			$uuid "175085b2-1053-470c-abbd-b3d595f43ccf"
			$inputs {
				Rte_CpApPCOM_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt = 0
				Rte_CpApRCD_PpInt_SUPV_RCDLineState_SUPV_RCDLineState = TRUE
				received3Frames_return_TS = 255
				received3Frames_value_TS = WUM_ELECTRON_BYTE_MASK
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 2 {
		$name "NoEnter by DiagMux"
		$uuid "5d61d9cb-5b6a-40a6-90d3-fdd84641829a"
		$specification """
			+ INPUT
			  + STUB
			    - 3 Frames Received return: TRUE
			    - 3 Frames Received value: WUM_ELECTRON_BYTE_MASK
			  + RTE
			    - VCU_DiagMuxOnPwt: 1
			    - SUPV_RCDLineState: Other 255
			    - DeAppRCDECUState: NotUsed
			- OUTPUT: FALSE
		"""

		$teststep 2.1 {
			$name ""
			$uuid "289862fb-cd2f-4a4f-81eb-0e05d6aa8193"
			$inputs {
				Rte_CpApPCOM_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt = 1
				Rte_CpApRCD_PpInt_SUPV_RCDLineState_SUPV_RCDLineState = 255
				received3Frames_return_TS = TRUE
				received3Frames_value_TS = WUM_ELECTRON_BYTE_MASK
			}
			$outputs {
				return FALSE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 3 {
		$name "NoEnter by Frame Values Diferents"
		$uuid "960b022c-465d-410a-bab3-c188fa33f020"
		$specification """
			+ INPUT
			  + STUB
			    - 3 Frames Received return: TRUE
			    - 3 Frames Received value: Other 0
			  + RTE
			    - VCU_DiagMuxOnPwt: 0
			    - SUPV_RCDLineState: TRUE
			    - DeAppRCDECUState: NotUsed
			- OUTPUT: FALSE
		"""

		$teststep 3.1 {
			$name ""
			$uuid "fbd2b00c-2868-4fb8-b09e-4923ef451061"
			$inputs {
				Rte_CpApPCOM_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt = 0
				Rte_CpApRCD_PpInt_SUPV_RCDLineState_SUPV_RCDLineState = TRUE
				received3Frames_return_TS = TRUE
				received3Frames_value_TS = 0
			}
			$outputs {
				return FALSE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 4 {
		$name "NoEnter by No Frames"
		$uuid "dda3cbeb-7d2b-4a23-bd2e-86240e66c6e0"
		$specification """
			+ INPUT
			  + STUB
			    - 3 Frames Received return: FALSE
			    - 3 Frames Received value: WUM_ELECTRON_BYTE_MASK
			  + RTE
			    - VCU_DiagMuxOnPwt: 0
			    - SUPV_RCDLineState: TRUE
			    - DeAppRCDECUState: NotUsed
			- OUTPUT: FALSE
		"""

		$teststep 4.1 {
			$name ""
			$uuid "0a9c7ee5-d9de-44b5-b37e-1b2167a5e900"
			$inputs {
				Rte_CpApPCOM_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt = 0
				Rte_CpApRCD_PpInt_SUPV_RCDLineState_SUPV_RCDLineState = TRUE
				received3Frames_return_TS = FALSE
				received3Frames_value_TS = WUM_ELECTRON_BYTE_MASK
			}
			$outputs {
				return FALSE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 8 {
		$name "NoEnter by RCD_LineState"
		$uuid "44d83464-f95b-4516-b33d-55c4184511ca"
		$specification """
			+ INPUT
			  + STUB
			    - 3 Frames Received return: TRUE
			    - 3 Frames Received value: WUM_ELECTRON_BYTE_MASK
			  + RTE
			    - VCU_DiagMuxOnPwt: 0
			    - SUPV_RCDLineState: FALSE
			    - DeAppRCDECUState: NotUsed
			- OUTPUT: FALSE
		"""

		$teststep 8.1 {
			$name ""
			$uuid "a8619fd4-7580-4b49-a9f6-6b396f7d51e6"
			$inputs {
				Rte_CpApPCOM_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt = 0
				Rte_CpApRCD_PpInt_SUPV_RCDLineState_SUPV_RCDLineState = FALSE
				received3Frames_return_TS = TRUE
				received3Frames_value_TS = WUM_ELECTRON_BYTE_MASK
			}
			$outputs {
				return FALSE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}
}