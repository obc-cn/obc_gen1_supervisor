$testobject {

	$testcase 1 {
		$name "0-0 Range - 0 input - FALSE Inhibition"
		$uuid "1bc8b407-b849-4898-bf34-779dfa27f7bd"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:Min: 0
			    - Lower_treshold:Min: 0
			    - Upper_trheshold:Min: 0
			    - Inhibition:FALSE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			0-0 Range Combination with FALSE Inhibition
		"""

		$teststep 1.1 {
			$name ""
			$uuid "d49264ce-22b9-4974-8965-fbfe32ac95dd"
			$inputs {
				input = *min*
				Parameters {
					Upper_trheshold = *min*
					Lower_treshold = *min*
					Inhibition = FALSE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 2 {
		$name "0-0 Range - 1 input - FALSE Inhibition"
		$uuid "1c265466-4979-402d-b107-28e78b7110ef"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:Min + 1: 1
			    - Lower_treshold:Min: 0
			    - Upper_trheshold:Min: 0
			    - Inhibition:FALSE
			+ OUT
			  - RETURNED VALUE:FALSE
		"""
		$description """
			0-0 Range Combination with FALSE Inhibition
		"""

		$teststep 2.1 {
			$name ""
			$uuid "d38c3ffd-15e7-439b-a077-ce1b30c077ea"
			$inputs {
				input = *min+1*
				Parameters {
					Upper_trheshold = *min*
					Lower_treshold = *min*
					Inhibition = FALSE
				}
			}
			$outputs {
				return FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 3 {
		$name "0-0 Range - Max input - FALSE Inhibition"
		$uuid "a9150d0e-9a1e-4206-b9e7-c47610e3cf85"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:Max: 65535
			    - Lower_treshold:Min: 0
			    - Upper_trheshold:Min: 0
			    - Inhibition:FALSE
			+ OUT
			  - RETURNED VALUE:FALSE
		"""
		$description """
			0-0 Range Combination with FALSE Inhibition
		"""

		$teststep 3.1 {
			$name ""
			$uuid "56b5eaaf-6dae-4982-9e26-510aac90b065"
			$inputs {
				input = *max*
				Parameters {
					Upper_trheshold = *min*
					Lower_treshold = *min*
					Inhibition = FALSE
				}
			}
			$outputs {
				return FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 4 {
		$name "0-0 Range - 0 input - TRUE Inhibition"
		$uuid "774b3541-bdf0-46a9-8c59-8545761415d5"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:Min + 1: 1
			    - Lower_treshold:Min: 0
			    - Upper_trheshold:Min: 0
			    - Inhibition:TRUE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			0-0 Range Combination with TRUE Inhibition
		"""

		$teststep 4.1 {
			$name ""
			$uuid "76fab28b-6462-4b26-bcb4-86f10ba03cb2"
			$inputs {
				input = *min+1*
				Parameters {
					Upper_trheshold = *min*
					Lower_treshold = *min*
					Inhibition = TRUE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 5 {
		$name "0-0 Range - 1 input - TRUE Inhibition"
		$uuid "3d1a29e8-1dd6-4254-aeec-ca4b0de063f7"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:Max: 65535
			    - Lower_treshold:Min: 0
			    - Upper_trheshold:Min: 0
			    - Inhibition:TRUE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			0-0 Range Combination with TRUE Inhibition
		"""

		$teststep 5.1 {
			$name ""
			$uuid "567328c0-ed5a-4e68-bb80-c28cc1e668eb"
			$inputs {
				input = *max*
				Parameters {
					Upper_trheshold = *min*
					Lower_treshold = *min*
					Inhibition = TRUE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 6 {
		$name "0-0 Range - Max input - TRUE Inhibition"
		$uuid "9ce67541-6adc-43e8-88fa-6ee3fefdd24f"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:Min: 0
			    - Lower_treshold:Min: 0
			    - Upper_trheshold:Min: 0
			    - Inhibition:TRUE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			0-0 Range Combination with TRUE Inhibition
		"""

		$teststep 6.1 {
			$name ""
			$uuid "06f1bbb2-bf16-4213-8ce6-a99fcf7b4a66"
			$inputs {
				input = *min*
				Parameters {
					Upper_trheshold = *min*
					Lower_treshold = *min*
					Inhibition = TRUE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 7 {
		$name "0-180 Range - 0 Input - FALSE Inhibition"
		$uuid "848766df-fd62-4a63-b772-d3c7e5a9f39b"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:Min: 0
			    - Lower_treshold:Min: 0
			    - Upper_trheshold:180
			    - Inhibition:FALSE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			0-180 Range Combination with FALSE Inhibition
		"""

		$teststep 7.1 {
			$name ""
			$uuid "e7a17479-164d-47e2-9dd3-6962a448c64d"
			$inputs {
				input = *min*
				Parameters {
					Upper_trheshold = 180
					Lower_treshold = *min*
					Inhibition = FALSE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 8 {
		$name "0-180 Range - 1 Input - FALSE Inhibition"
		$uuid "37bcec18-8f26-46ac-ba7a-f4faba7544cc"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:Min + 1: 1
			    - Lower_treshold:Min: 0
			    - Upper_trheshold:180
			    - Inhibition:FALSE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			0-180 Range Combination with FALSE Inhibition
		"""

		$teststep 8.1 {
			$name ""
			$uuid "b9b7b870-1981-448e-ab86-91922d5eb76c"
			$inputs {
				input = *min+1*
				Parameters {
					Upper_trheshold = 180
					Lower_treshold = *min*
					Inhibition = FALSE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 9 {
		$name "0-180 Range - 179 Input - FALSE Inhibition"
		$uuid "321d9223-d34a-4b4a-baf7-44cb0091de53"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:179
			    - Lower_treshold:Min: 0
			    - Upper_trheshold:180
			    - Inhibition:FALSE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			0-180 Range Combination with FALSE Inhibition
		"""

		$teststep 9.1 {
			$name ""
			$uuid "f19c2263-ea97-477f-ad2e-2f7ee938d811"
			$inputs {
				input = 179
				Parameters {
					Upper_trheshold = 180
					Lower_treshold = *min*
					Inhibition = FALSE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 10 {
		$name "0-180 Range - 180 Input - FALSE Inhibition"
		$uuid "01222152-5bd0-4f31-a394-f8f9bf4991c7"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:180
			    - Lower_treshold:Min: 0
			    - Upper_trheshold:180
			    - Inhibition:FALSE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			0-180 Range Combination with FALSE Inhibition
		"""

		$teststep 10.1 {
			$name ""
			$uuid "5c73692d-44b2-4f6f-8426-a0c8943cf02d"
			$inputs {
				input = 180
				Parameters {
					Upper_trheshold = 180
					Lower_treshold = *min*
					Inhibition = FALSE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 11 {
		$name "0-180 Range - 181 Input - FALSE Inhibition"
		$uuid "6671b75e-0e8b-4fd7-aa9d-6b767d0c27a6"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:181
			    - Lower_treshold:Min: 0
			    - Upper_trheshold:180
			    - Inhibition:FALSE
			+ OUT
			  - RETURNED VALUE:FALSE
		"""
		$description """
			0-180 Range Combination with FALSE Inhibition
		"""

		$teststep 11.1 {
			$name ""
			$uuid "bf9a5c00-8b05-4214-9aa9-67db0cfc9b91"
			$inputs {
				input = 181
				Parameters {
					Upper_trheshold = 180
					Lower_treshold = *min*
					Inhibition = FALSE
				}
			}
			$outputs {
				return FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 12 {
		$name "0-180 Range - Max Input - FALSE Inhibition"
		$uuid "efabf72e-6f8a-4ef1-a499-635070317af5"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:Max: 65535
			    - Lower_treshold:Min: 0
			    - Upper_trheshold:180
			    - Inhibition:FALSE
			+ OUT
			  - RETURNED VALUE:FALSE
		"""
		$description """
			0-180 Range Combination with FALSE Inhibition
		"""

		$teststep 12.1 {
			$name ""
			$uuid "1a6bd5f7-accc-4ee7-a54b-6bd893b29d3b"
			$inputs {
				input = *max*
				Parameters {
					Upper_trheshold = 180
					Lower_treshold = *min*
					Inhibition = FALSE
				}
			}
			$outputs {
				return FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 13 {
		$name "0-180 Range - 0 Input - TRUE Inhibition"
		$uuid "a5bd02c2-cfdc-4b55-a96b-f85a3efc119f"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:Min: 0
			    - Lower_treshold:Min: 0
			    - Upper_trheshold:180
			    - Inhibition:TRUE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			0-180 Range Combination with TRUE Inhibition
		"""

		$teststep 13.1 {
			$name ""
			$uuid "8a6c2c52-7d87-49a2-bf17-28b4ad637e4a"
			$inputs {
				input = *min*
				Parameters {
					Upper_trheshold = 180
					Lower_treshold = *min*
					Inhibition = TRUE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 14 {
		$name "0-180 Range - 1 Input - TRUE Inhibition"
		$uuid "95ba3bc6-9276-44fa-bb21-196c9673effa"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:Min + 1: 1
			    - Lower_treshold:Min: 0
			    - Upper_trheshold:180
			    - Inhibition:TRUE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			0-180 Range Combination with TRUE Inhibition
		"""

		$teststep 14.1 {
			$name ""
			$uuid "b7f64534-56c7-4116-a5f6-4b2e231677c8"
			$inputs {
				input = *min+1*
				Parameters {
					Upper_trheshold = 180
					Lower_treshold = *min*
					Inhibition = TRUE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 15 {
		$name "0-180 Range - 179 Input - TRUE Inhibition"
		$uuid "cc4aa6f6-3631-4542-b0c6-bbdd9323dcb6"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:179
			    - Lower_treshold:Min: 0
			    - Upper_trheshold:180
			    - Inhibition:TRUE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			0-180 Range Combination with TRUE Inhibition
		"""

		$teststep 15.1 {
			$name ""
			$uuid "eb197d43-fea0-4545-8aec-ce6f1272ddcc"
			$inputs {
				input = 179
				Parameters {
					Upper_trheshold = 180
					Lower_treshold = *min*
					Inhibition = TRUE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 16 {
		$name "0-180 Range - 180 Input - TRUE Inhibition"
		$uuid "a7ac9e69-4019-4456-a5d2-5842092bfd25"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:180
			    - Lower_treshold:Min: 0
			    - Upper_trheshold:180
			    - Inhibition:TRUE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			0-180 Range Combination with TRUE Inhibition
		"""

		$teststep 16.1 {
			$name ""
			$uuid "81d5beb8-b77c-41cc-9306-819ff31a59e5"
			$inputs {
				input = 180
				Parameters {
					Upper_trheshold = 180
					Lower_treshold = *min*
					Inhibition = TRUE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 17 {
		$name "0-180 Range - 181 Input - TRUE Inhibition"
		$uuid "c02e80b1-4dae-468d-8b64-ab6fc398025f"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:181
			    - Lower_treshold:Min: 0
			    - Upper_trheshold:180
			    - Inhibition:TRUE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			0-180 Range Combination with TRUE Inhibition
		"""

		$teststep 17.1 {
			$name ""
			$uuid "f5161a3e-2fa9-4de8-a8af-4fc360a1b85f"
			$inputs {
				input = 181
				Parameters {
					Upper_trheshold = 180
					Lower_treshold = *min*
					Inhibition = TRUE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 18 {
		$name "0-180 Range - Max Input - TRUE Inhibition"
		$uuid "442e4a11-b00f-4695-9ee0-fd9fd248b31f"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:Max: 65535
			    - Lower_treshold:Min: 0
			    - Upper_trheshold:180
			    - Inhibition:TRUE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			0-180 Range Combination with TRUE Inhibition
		"""

		$teststep 18.1 {
			$name ""
			$uuid "7307c80b-d771-4564-9211-61749b100514"
			$inputs {
				input = *max*
				Parameters {
					Upper_trheshold = 180
					Lower_treshold = *min*
					Inhibition = TRUE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 19 {
		$name "180-180 Range - 0 Input - FALSE Inhibition"
		$uuid "01557422-f842-4035-9e91-fb285ebe7090"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:Min: 0
			    - Lower_treshold:180
			    - Upper_trheshold:180
			    - Inhibition:FALSE
			+ OUT
			  - RETURNED VALUE:FALSE
		"""
		$description """
			180-180 Range Combination with FALSE Inhibition
		"""

		$teststep 19.1 {
			$name ""
			$uuid "a36439cc-f8f7-42b1-a1b1-45050ad29476"
			$inputs {
				input = *min*
				Parameters {
					Upper_trheshold = 180
					Lower_treshold = 180
					Inhibition = FALSE
				}
			}
			$outputs {
				return FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 20 {
		$name "180-180 Range - 179 Input - FALSE Inhibition"
		$uuid "7e19cc60-fd21-4e42-833f-f140c890937d"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:179
			    - Lower_treshold:180
			    - Upper_trheshold:180
			    - Inhibition:FALSE
			+ OUT
			  - RETURNED VALUE:FALSE
		"""
		$description """
			180-180 Range Combination with FALSE Inhibition
		"""

		$teststep 20.1 {
			$name ""
			$uuid "42dcf921-a1ad-43f4-b2c2-947f75135aa9"
			$inputs {
				input = 179
				Parameters {
					Upper_trheshold = 180
					Lower_treshold = 180
					Inhibition = FALSE
				}
			}
			$outputs {
				return FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 21 {
		$name "180-180 Range - 180 Input - FALSE Inhibition"
		$uuid "98b7e76a-c748-42d0-997e-1e1ff013ddbc"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:180
			    - Lower_treshold:180
			    - Upper_trheshold:180
			    - Inhibition:FALSE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			180-180 Range Combination with FALSE Inhibition
		"""

		$teststep 21.1 {
			$name ""
			$uuid "9b0769d5-bb39-4116-bc52-01d8e4b1db96"
			$inputs {
				input = 180
				Parameters {
					Upper_trheshold = 180
					Lower_treshold = 180
					Inhibition = FALSE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 22 {
		$name "180-180 Range - 181 Input - FALSE Inhibition"
		$uuid "7bf0ff91-ed23-4169-8d91-9e902bbbd28b"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:181
			    - Lower_treshold:180
			    - Upper_trheshold:180
			    - Inhibition:FALSE
			+ OUT
			  - RETURNED VALUE:FALSE
		"""
		$description """
			180-180 Range Combination with FALSE Inhibition
		"""

		$teststep 22.1 {
			$name ""
			$uuid "da77d2e1-4ac1-4da3-84e0-671ebc1e4ef6"
			$inputs {
				input = 181
				Parameters {
					Upper_trheshold = 180
					Lower_treshold = 180
					Inhibition = FALSE
				}
			}
			$outputs {
				return FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 23 {
		$name "180-180 Range - Max Input - FALSE Inhibition"
		$uuid "afd7c06b-08a8-47c5-9f20-040d4e1ab52e"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:Max: 65535
			    - Lower_treshold:180
			    - Upper_trheshold:180
			    - Inhibition:FALSE
			+ OUT
			  - RETURNED VALUE:FALSE
		"""
		$description """
			180-180 Range Combination with FALSE Inhibition
		"""

		$teststep 23.1 {
			$name ""
			$uuid "103a062d-e19d-4e0f-97af-4bf0356f8dae"
			$inputs {
				input = *max*
				Parameters {
					Upper_trheshold = 180
					Lower_treshold = 180
					Inhibition = FALSE
				}
			}
			$outputs {
				return FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 24 {
		$name "180-180 Range - 0 Input - TRUE Inhibition"
		$uuid "44091ae6-b0a2-42f4-a516-9936f1ff69ce"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:Min: 0
			    - Lower_treshold:180
			    - Upper_trheshold:180
			    - Inhibition:TRUE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			180-180 Range Combination with TRUE Inhibition
		"""

		$teststep 24.1 {
			$name ""
			$uuid "3ea6a858-a996-4034-b45d-fe3179dc4a62"
			$inputs {
				input = *min*
				Parameters {
					Upper_trheshold = 180
					Lower_treshold = 180
					Inhibition = TRUE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 25 {
		$name "180-180 Range - 179 Input - TRUE Inhibition"
		$uuid "7353c2ef-15c7-401e-9528-950631aa2bf2"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:179
			    - Lower_treshold:180
			    - Upper_trheshold:180
			    - Inhibition:TRUE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			180-180 Range Combination with TRUE Inhibition
		"""

		$teststep 25.1 {
			$name ""
			$uuid "c927a5da-40d0-4ee0-b771-be6aa2e6c61b"
			$inputs {
				input = 179
				Parameters {
					Upper_trheshold = 180
					Lower_treshold = 180
					Inhibition = TRUE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 26 {
		$name "180-180 Range - 180 Input - TRUE Inhibition"
		$uuid "a940dfd2-2dfb-4bf6-b8ba-2086d822dcf0"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:180
			    - Lower_treshold:180
			    - Upper_trheshold:180
			    - Inhibition:TRUE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			180-180 Range Combination with TRUE Inhibition
		"""

		$teststep 26.1 {
			$name ""
			$uuid "1cc8d2c9-5215-463d-8c31-46991a655617"
			$inputs {
				input = 180
				Parameters {
					Upper_trheshold = 180
					Lower_treshold = 180
					Inhibition = TRUE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 27 {
		$name "180-180 Range - 181 Input - TRUE Inhibition"
		$uuid "b63ca81e-5960-40eb-8760-398da8c3ac00"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:181
			    - Lower_treshold:180
			    - Upper_trheshold:180
			    - Inhibition:TRUE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			180-180 Range Combination with TRUE Inhibition
		"""

		$teststep 27.1 {
			$name ""
			$uuid "24804cb4-d715-4e77-9f90-5804befc266e"
			$inputs {
				input = 181
				Parameters {
					Upper_trheshold = 180
					Lower_treshold = 180
					Inhibition = TRUE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 28 {
		$name "180-180 Range - Max Input - TRUE Inhibition"
		$uuid "d236d4c7-42f7-4490-ac5b-f548f0e2aa4a"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:Max: 65535
			    - Lower_treshold:180
			    - Upper_trheshold:180
			    - Inhibition:TRUE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			180-180 Range Combination with TRUE Inhibition
		"""

		$teststep 28.1 {
			$name ""
			$uuid "6a579e9e-f754-491c-97ea-98340c2fd315"
			$inputs {
				input = *max*
				Parameters {
					Upper_trheshold = 180
					Lower_treshold = 180
					Inhibition = TRUE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 29 {
		$name "180-Max Range - 0 Input - FALSE Inhibition"
		$uuid "0370893e-c31c-42e6-8976-9db5730570a7"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:Min: 0
			    - Lower_treshold:180
			    - Upper_trheshold:Max: 65535
			    - Inhibition:FALSE
			+ OUT
			  - RETURNED VALUE:FALSE
		"""
		$description """
			180-Max Range Combination with FALSE Inhibition
		"""

		$teststep 29.1 {
			$name ""
			$uuid "6e743f1f-716e-46a9-b925-ccc95bc22db2"
			$inputs {
				input = *min*
				Parameters {
					Upper_trheshold = *max*
					Lower_treshold = 180
					Inhibition = FALSE
				}
			}
			$outputs {
				return FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 30 {
		$name "180-Max Range - 179 Input - FALSE Inhibition"
		$uuid "1616ce30-e2ba-4f8f-bf23-720124f18fc7"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:179
			    - Lower_treshold:180
			    - Upper_trheshold:Max: 65535
			    - Inhibition:FALSE
			+ OUT
			  - RETURNED VALUE:FALSE
		"""
		$description """
			180-Max Range Combination with FALSE Inhibition
		"""

		$teststep 30.1 {
			$name ""
			$uuid "ca9d970d-42c4-437a-8364-6be18869762e"
			$inputs {
				input = 179
				Parameters {
					Upper_trheshold = *max*
					Lower_treshold = 180
					Inhibition = FALSE
				}
			}
			$outputs {
				return FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 31 {
		$name "180-Max Range - 180 Input - FALSE Inhibition"
		$uuid "a99167b5-d4b4-4943-aece-a21011a25780"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:180
			    - Lower_treshold:180
			    - Upper_trheshold:Max: 65535
			    - Inhibition:FALSE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			180-Max Range Combination with FALSE Inhibition
		"""

		$teststep 31.1 {
			$name ""
			$uuid "160254aa-8945-45e4-896c-9090f0f6aaaa"
			$inputs {
				input = 180
				Parameters {
					Upper_trheshold = *max*
					Lower_treshold = 180
					Inhibition = FALSE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 32 {
		$name "180-Max Range - 181 Input - FALSE Inhibition"
		$uuid "b22f5c0b-030f-44e1-a84e-ce0ca550603d"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:181
			    - Lower_treshold:180
			    - Upper_trheshold:Max: 65535
			    - Inhibition:FALSE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			180-Max Range Combination with FALSE Inhibition
		"""

		$teststep 32.1 {
			$name ""
			$uuid "54d6a561-75be-475d-af2d-6a5ad70e179a"
			$inputs {
				input = 181
				Parameters {
					Upper_trheshold = *max*
					Lower_treshold = 180
					Inhibition = FALSE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 33 {
		$name "180-Max Range - Max-1 Input - FALSE Inhibition"
		$uuid "a9e48f39-3c19-4c0a-bbec-35dcb12fafdb"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:Max - 1: 65534
			    - Lower_treshold:180
			    - Upper_trheshold:Max: 65535
			    - Inhibition:FALSE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			180-Max Range Combination with FALSE Inhibition
		"""

		$teststep 33.1 {
			$name ""
			$uuid "0913957e-d504-4ed9-b897-a46b2d36c962"
			$inputs {
				input = *max-1*
				Parameters {
					Upper_trheshold = *max*
					Lower_treshold = 180
					Inhibition = FALSE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 34 {
		$name "180-Max Range - Max Input - FALSE Inhibition"
		$uuid "27c10ba0-2b32-42d3-97eb-5235f9706e07"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:Max: 65535
			    - Lower_treshold:180
			    - Upper_trheshold:Max: 65535
			    - Inhibition:FALSE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			180-Max Range Combination with FALSE Inhibition
		"""

		$teststep 34.1 {
			$name ""
			$uuid "68973f6d-ddc8-497f-b11e-ca3cc72c90ec"
			$inputs {
				input = *max*
				Parameters {
					Upper_trheshold = *max*
					Lower_treshold = 180
					Inhibition = FALSE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 35 {
		$name "180-Max Range - 0 Input - TRUE Inhibition"
		$uuid "8c9c5509-a0fb-4340-829c-5912543bd511"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:Min: 0
			    - Lower_treshold:180
			    - Upper_trheshold:Max: 65535
			    - Inhibition:TRUE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			180-Max Range Combination with TRUE Inhibition
		"""

		$teststep 35.1 {
			$name ""
			$uuid "d2cad981-da46-4586-938f-a883d6c3d976"
			$inputs {
				input = *min*
				Parameters {
					Upper_trheshold = *max*
					Lower_treshold = 180
					Inhibition = TRUE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 36 {
		$name "180-Max Range - 179 Input - TRUE Inhibition"
		$uuid "d9f878d9-ef87-4b2e-b6dc-bc33dc8e9c41"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:179
			    - Lower_treshold:180
			    - Upper_trheshold:Max: 65535
			    - Inhibition:TRUE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			180-Max Range Combination with TRUE Inhibition
		"""

		$teststep 36.1 {
			$name ""
			$uuid "dcddfb10-f116-42a3-b6ba-a174afe7096b"
			$inputs {
				input = 179
				Parameters {
					Upper_trheshold = *max*
					Lower_treshold = 180
					Inhibition = TRUE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 37 {
		$name "180-Max Range - 180 Input - TRUE Inhibition"
		$uuid "d3be5638-3bab-4947-990e-6058b0162dbd"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:180
			    - Lower_treshold:180
			    - Upper_trheshold:Max: 65535
			    - Inhibition:TRUE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			180-Max Range Combination with TRUE Inhibition
		"""

		$teststep 37.1 {
			$name ""
			$uuid "06905933-afa5-433a-81e2-f97f22a12a78"
			$inputs {
				input = 180
				Parameters {
					Upper_trheshold = *max*
					Lower_treshold = 180
					Inhibition = TRUE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 38 {
		$name "180-Max Range - 181 Input - TRUE Inhibition"
		$uuid "f7a56fa7-b174-4a33-8259-b3c4755f7bd8"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:181
			    - Lower_treshold:180
			    - Upper_trheshold:Max: 65535
			    - Inhibition:TRUE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			180-Max Range Combination with TRUE Inhibition
		"""

		$teststep 38.1 {
			$name ""
			$uuid "8f4be1a9-e320-4a51-a364-22014e2e1bbb"
			$inputs {
				input = 181
				Parameters {
					Upper_trheshold = *max*
					Lower_treshold = 180
					Inhibition = TRUE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 39 {
		$name "180-Max Range - Max-1 Input - TRUE Inhibition"
		$uuid "bc825ade-9162-4c1e-b37b-af1e22171da0"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:Max - 1: 65534
			    - Lower_treshold:180
			    - Upper_trheshold:Max: 65535
			    - Inhibition:TRUE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			180-Max Range Combination with TRUE Inhibition
		"""

		$teststep 39.1 {
			$name ""
			$uuid "e13e6605-3c63-49d4-a7ba-db38e462bad4"
			$inputs {
				input = *max-1*
				Parameters {
					Upper_trheshold = *max*
					Lower_treshold = 180
					Inhibition = TRUE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 40 {
		$name "180-Max Range - Max Input - TRUE Inhibition"
		$uuid "8a720c00-dadc-4448-907a-ed8bc2142d73"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:Max: 65535
			    - Lower_treshold:180
			    - Upper_trheshold:Max: 65535
			    - Inhibition:TRUE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			180-Max Range Combination with TRUE Inhibition
		"""

		$teststep 40.1 {
			$name ""
			$uuid "fcebc455-5383-4acc-a7cf-a3074f2ab6df"
			$inputs {
				input = *max*
				Parameters {
					Upper_trheshold = *max*
					Lower_treshold = 180
					Inhibition = TRUE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 41 {
		$name "220-200 Range - 0 Input - FALSE Inhibition"
		$uuid "a41daba3-2343-4152-af22-d735ddd7d1a2"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:Min: 0
			    - Lower_treshold:220
			    - Upper_trheshold:200
			    - Inhibition:FALSE
			+ OUT
			  - RETURNED VALUE:FALSE
		"""
		$description """
			Inverted Range Combination with FALSE Inhibition
		"""

		$teststep 41.1 {
			$name ""
			$uuid "b21cd28d-0367-4b0b-a12a-41d232fb78a1"
			$inputs {
				input = *min*
				Parameters {
					Upper_trheshold = 200
					Lower_treshold = 220
					Inhibition = FALSE
				}
			}
			$outputs {
				return FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 42 {
		$name "220-200 Range - 199 Input - FALSE Inhibition"
		$uuid "6fc05b72-bbc1-48b4-8a31-945f4cab24ff"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:199
			    - Lower_treshold:220
			    - Upper_trheshold:200
			    - Inhibition:FALSE
			+ OUT
			  - RETURNED VALUE:FALSE
		"""
		$description """
			Inverted Range Combination with FALSE Inhibition
		"""

		$teststep 42.1 {
			$name ""
			$uuid "37aa6a2a-f758-48e2-a310-bab9ce5684c4"
			$inputs {
				input = 199
				Parameters {
					Upper_trheshold = 200
					Lower_treshold = 220
					Inhibition = FALSE
				}
			}
			$outputs {
				return FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 43 {
		$name "220-200 Range - 200 Input - FALSE Inhibition"
		$uuid "d6c7f753-0874-45b2-84c0-f94b5f5be1b5"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:200
			    - Lower_treshold:220
			    - Upper_trheshold:200
			    - Inhibition:FALSE
			+ OUT
			  - RETURNED VALUE:FALSE
		"""
		$description """
			Inverted Range Combination with FALSE Inhibition
		"""

		$teststep 43.1 {
			$name ""
			$uuid "95c5f73a-e715-4271-b269-8fdd7e3ad853"
			$inputs {
				input = 200
				Parameters {
					Upper_trheshold = 200
					Lower_treshold = 220
					Inhibition = FALSE
				}
			}
			$outputs {
				return FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 44 {
		$name "220-200 Range - 201 Input - FALSE Inhibition"
		$uuid "d0d2ef16-b932-48ca-8edc-8d2cf11c0e63"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:201
			    - Lower_treshold:220
			    - Upper_trheshold:200
			    - Inhibition:FALSE
			+ OUT
			  - RETURNED VALUE:FALSE
		"""
		$description """
			Inverted Range Combination with FALSE Inhibition
		"""

		$teststep 44.1 {
			$name ""
			$uuid "cb8867cd-df87-40e5-bf42-0cbd626ec8d3"
			$inputs {
				input = 201
				Parameters {
					Upper_trheshold = 200
					Lower_treshold = 220
					Inhibition = FALSE
				}
			}
			$outputs {
				return FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 45 {
		$name "220-200 Range - 219 Input - FALSE Inhibition"
		$uuid "fac0f67d-1852-43a7-9cdc-621de089bae7"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:219
			    - Lower_treshold:220
			    - Upper_trheshold:200
			    - Inhibition:FALSE
			+ OUT
			  - RETURNED VALUE:FALSE
		"""
		$description """
			Inverted Range Combination with FALSE Inhibition
		"""

		$teststep 45.1 {
			$name ""
			$uuid "aef48b8e-b63f-4312-9c45-67d44d220da4"
			$inputs {
				input = 219
				Parameters {
					Upper_trheshold = 200
					Lower_treshold = 220
					Inhibition = FALSE
				}
			}
			$outputs {
				return FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 46 {
		$name "220-200 Range - 220 Input - FALSE Inhibition"
		$uuid "2640805f-dd3a-425d-a841-df4f50afe4db"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:220
			    - Lower_treshold:220
			    - Upper_trheshold:200
			    - Inhibition:FALSE
			+ OUT
			  - RETURNED VALUE:FALSE
		"""
		$description """
			Inverted Range Combination with FALSE Inhibition
		"""

		$teststep 46.1 {
			$name ""
			$uuid "0dbc82af-28fc-440e-a201-b10d1ae4916d"
			$inputs {
				input = 220
				Parameters {
					Upper_trheshold = 200
					Lower_treshold = 220
					Inhibition = FALSE
				}
			}
			$outputs {
				return FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 47 {
		$name "220-200 Range - 221 Input - FALSE Inhibition"
		$uuid "1179888e-f4c4-4201-bec0-f47c84a75aa1"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:221
			    - Lower_treshold:220
			    - Upper_trheshold:200
			    - Inhibition:FALSE
			+ OUT
			  - RETURNED VALUE:FALSE
		"""
		$description """
			Inverted Range Combination with FALSE Inhibition
		"""

		$teststep 47.1 {
			$name ""
			$uuid "f70c84f3-e488-41dd-b407-e5445eaebf78"
			$inputs {
				input = 221
				Parameters {
					Upper_trheshold = 200
					Lower_treshold = 220
					Inhibition = FALSE
				}
			}
			$outputs {
				return FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 48 {
		$name "220-200 Range - Max Input - FALSE Inhibition"
		$uuid "f5369297-8266-427b-ab85-e8bbb1b712ba"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:Max: 65535
			    - Lower_treshold:220
			    - Upper_trheshold:200
			    - Inhibition:FALSE
			+ OUT
			  - RETURNED VALUE:FALSE
		"""
		$description """
			Inverted Range Combination with FALSE Inhibition
		"""

		$teststep 48.1 {
			$name ""
			$uuid "efcb2fa3-3ecd-4276-b4c5-c1f50d5645f0"
			$inputs {
				input = *max*
				Parameters {
					Upper_trheshold = 200
					Lower_treshold = 220
					Inhibition = FALSE
				}
			}
			$outputs {
				return FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 49 {
		$name "220-200 Range - 0 Input - TRUE Inhibition"
		$uuid "420fae3f-a4fa-4614-9666-345dd8527a25"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:Min: 0
			    - Lower_treshold:220
			    - Upper_trheshold:200
			    - Inhibition:TRUE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			Inverted Range Combination with TRUE Inhibition
		"""

		$teststep 49.1 {
			$name ""
			$uuid "8fc2f415-92fd-41f4-8b92-6ba2fcd750eb"
			$inputs {
				input = *min*
				Parameters {
					Upper_trheshold = 200
					Lower_treshold = 220
					Inhibition = TRUE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 50 {
		$name "220-200 Range - 199 Input - TRUE Inhibition"
		$uuid "57f12a17-9fec-476b-ae1b-245b9a01f05e"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:199
			    - Lower_treshold:220
			    - Upper_trheshold:200
			    - Inhibition:TRUE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			Inverted Range Combination with TRUE Inhibition
		"""

		$teststep 50.1 {
			$name ""
			$uuid "1dd04ee2-dfd3-4a88-9e39-38d327e3e62e"
			$inputs {
				input = 199
				Parameters {
					Upper_trheshold = 200
					Lower_treshold = 220
					Inhibition = TRUE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 51 {
		$name "220-200 Range - 200 Input - TRUE Inhibition"
		$uuid "80a3e1f3-0cd6-412d-a480-76d0a3e1b554"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:200
			    - Lower_treshold:220
			    - Upper_trheshold:200
			    - Inhibition:TRUE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			Inverted Range Combination with TRUE Inhibition
		"""

		$teststep 51.1 {
			$name ""
			$uuid "93899d53-d80b-4b1a-925c-b3bfc9bf6103"
			$inputs {
				input = 200
				Parameters {
					Upper_trheshold = 200
					Lower_treshold = 220
					Inhibition = TRUE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 52 {
		$name "220-200 Range - 201 Input - TRUE Inhibition"
		$uuid "4fe61293-aa4c-4584-998c-42cb230c986a"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:201
			    - Lower_treshold:220
			    - Upper_trheshold:200
			    - Inhibition:TRUE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			Inverted Range Combination with TRUE Inhibition
		"""

		$teststep 52.1 {
			$name ""
			$uuid "20a585b8-b4a2-480a-b7fd-bc3194bbfe9d"
			$inputs {
				input = 201
				Parameters {
					Upper_trheshold = 200
					Lower_treshold = 220
					Inhibition = TRUE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 53 {
		$name "220-200 Range - 219 Input - TRUE Inhibition"
		$uuid "138d4173-106d-47eb-846b-41820eff4c89"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:219
			    - Lower_treshold:220
			    - Upper_trheshold:200
			    - Inhibition:TRUE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			Inverted Range Combination with TRUE Inhibition
		"""

		$teststep 53.1 {
			$name ""
			$uuid "93427c9e-9ffa-4957-ac3b-c8fed4200fea"
			$inputs {
				input = 219
				Parameters {
					Upper_trheshold = 200
					Lower_treshold = 220
					Inhibition = TRUE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 54 {
		$name "220-200 Range - 220 Input - TRUE Inhibition"
		$uuid "b5b91a1a-5510-4418-9caf-b4d1aefc7940"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:220
			    - Lower_treshold:220
			    - Upper_trheshold:200
			    - Inhibition:TRUE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			Inverted Range Combination with TRUE Inhibition
		"""

		$teststep 54.1 {
			$name ""
			$uuid "cc2d095a-5315-4607-940d-2839ff57876e"
			$inputs {
				input = 220
				Parameters {
					Upper_trheshold = 200
					Lower_treshold = 220
					Inhibition = TRUE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 55 {
		$name "220-200 Range - 221 Input - TRUE Inhibition"
		$uuid "a42fa89d-b544-44fd-9269-d67f615dfa55"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:221
			    - Lower_treshold:220
			    - Upper_trheshold:200
			    - Inhibition:TRUE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			Inverted Range Combination with TRUE Inhibition
		"""

		$teststep 55.1 {
			$name ""
			$uuid "8fef4743-f0ed-4e68-98bc-2e8407a29c5d"
			$inputs {
				input = 221
				Parameters {
					Upper_trheshold = 200
					Lower_treshold = 220
					Inhibition = TRUE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 56 {
		$name "220-200 Range - Max Input - TRUE Inhibition"
		$uuid "38b4f7db-5ade-4e9a-97e0-bb7a9aec3778"
		$specification """
			+ IN
			  + FUNCTION PARAMETERS
			    - input:Max: 65535
			    - Lower_treshold:220
			    - Upper_trheshold:200
			    - Inhibition:TRUE
			+ OUT
			  - RETURNED VALUE:TRUE
		"""
		$description """
			Inverted Range Combination with TRUE Inhibition
		"""

		$teststep 56.1 {
			$name ""
			$uuid "512fbfaa-76ad-44bf-8f3d-6c90ba5a00d6"
			$inputs {
				input = *max*
				Parameters {
					Upper_trheshold = 200
					Lower_treshold = 220
					Inhibition = TRUE
				}
			}
			$outputs {
				return TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}
}