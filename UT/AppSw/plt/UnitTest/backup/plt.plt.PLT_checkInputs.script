$testobject {

	$testcase 1 {
		$name "Identify Sended Value with TRUE checkRange inputs"
		$uuid "536c0dc5-f3b7-4d4f-97b6-5f71cd35e257"
		$specification """
			+ IN
			  + DFA GETS
			    - Rte_BMS_DCRelayVoltage: ID: 40
			    - Rte_PFC_VPH12_RMS_V: ID: 41
			    - Rte_PFC_VPH23_RMS_V: ID: 42
			    - Rte_PFC_VPH31_RMS_V: ID: 43
			    - Rte_DeProximityDetectPhysicalValue: ID: 44
			    - Rte_DeDutyControlPilot: ID: 45
			  + FUNCTION RETURN
			    - PLT_checkRange(): TRUE TRUE TRUE TRUE TRUE TRUE
			  + PARAMETER
			    + *result
			      - result[0]: FALSE
			      - result[1]: TRUE
			      - result[2]: FALSE
			      - result[3]: TRUE
			+ OUT
			  + PARAMETER
			    + *result
			      - result[0]: TRUE
			      - result[1]: TRUE
			      - result[2]: TRUE
			      - result[3]: TRUE
			  + CALLED FUNCTION PARAMETERS
			    - PLT_checkRange(input): 41/v3=23 42/v3=24 43/v3=24 44 45 40
			  - Return: TRUE
		"""
		$description """
			Identify values with TRUE checkRange returned values
		"""

		$teststep 1.1 {
			$name ""
			$uuid "c86eef98-fd9e-40d0-86af-6b71197426e4"
			$inputs {
				PLT_checkRange() = {TRUE,TRUE,TRUE,TRUE,TRUE,TRUE}
				Rte_CpApPCOM_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage = 40
				Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V = 41
				Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V = 42
				Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V = 43
				Rte_CpHwAbsAIM_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue = 44
				Rte_CpHwAbsPIM_PpDutyControlPilot_DeDutyControlPilot = 45
				result = target_result_2
				&target_result_2[0] = FALSE
				&target_result_2[1] = TRUE
				&target_result_2[2] = FALSE
				&target_result_2[3] = TRUE
			}
			$outputs {
				PLT_checkRange(input) = {23, 24, 24, 44, 45, 40}
				return TRUE
				&target_result_2[0] = TRUE
				&target_result_2[1] = TRUE
				&target_result_2[2] = TRUE
				&target_result_2[3] = TRUE
			}
			$calltrace {
				PLT_checkRange * 6
			}
		}
	}

	$testcase 4 {
		$name "Identify Sended Value with FALSE checkRange inputs"
		$uuid "06da3475-0f94-42a6-8e9b-e7d76a18ca4b"
		$specification """
			+ IN
			  + DFA GETS
			    - Rte_BMS_DCRelayVoltage: ID: 40
			    - Rte_PFC_VPH12_RMS_V: ID: 41
			    - Rte_PFC_VPH23_RMS_V: ID: 42
			    - Rte_PFC_VPH31_RMS_V: ID: 43
			    - Rte_DeProximityDetectPhysicalValue: ID: 44
			    - Rte_DeDutyControlPilot: ID: 45
			  + FUNCTION RETURN
			    - PLT_checkRange(): FALSE FALSE FALSE FALSE FALSE FALSE
			  + PARAMETER
			    + *result
			      - result[0]: TRUE
			      - result[1]: FALSE
			      - result[2]: TRUE
			      - result[3]: FALSE
			+ OUT
			  + PARAMETER
			    + *result
			      - result[0]: FALSE
			      - result[1]: FALSE
			      - result[2]: FALSE
			      - result[3]: FALSE
			  + CALLED FUNCTION PARAMETERS
			    - PLT_checkRange(input): 41/v3=23 42/v3=24 43/v3=24 44 45 40
			  - Return: FALSE
		"""
		$description """
			Identify values with FALSE checkRange returned values
		"""

		$teststep 4.1 {
			$name ""
			$uuid "8e4a6917-c722-4f27-9794-f20f270f166c"
			$inputs {
				PLT_checkRange() = {FALSE,FALSE,FALSE,FALSE,FALSE,FALSE}
				Rte_CpApPCOM_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage = 40
				Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V = 41
				Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V = 42
				Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V = 43
				Rte_CpHwAbsAIM_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue = 44
				Rte_CpHwAbsPIM_PpDutyControlPilot_DeDutyControlPilot = 45
				result = target_result_2
				&target_result_2[0] = TRUE
				&target_result_2[1] = FALSE
				&target_result_2[2] = TRUE
				&target_result_2[3] = FALSE
			}
			$outputs {
				PLT_checkRange(input) = {23, 24, 24, 44, 45, 40}
				return FALSE
				&target_result_2[0] = *none*
				&target_result_2[1] = FALSE
				&target_result_2[2] = FALSE
				&target_result_2[3] = FALSE
			}
			$calltrace {
				PLT_checkRange * 6
			}
		}
	}

	$testcase 5 {
		$name "Maximum Sended Value with TRUE checkRange inputs"
		$uuid "e052ca41-ad2a-4979-a509-8498882be2dd"
		$specification """
			+ IN
			  + DFA GETS
			    - Rte_BMS_DCRelayVoltage: Max: 65535
			    - Rte_PFC_VPH12_RMS_V: Max: 65535
			    - Rte_PFC_VPH23_RMS_V: Max: 65535
			    - Rte_PFC_VPH31_RMS_V: Max: 65535
			    - Rte_DeProximityDetectPhysicalValue: Max: 65535
			    - Rte_DeDutyControlPilot: Max: 255
			  + FUNCTION RETURN
			    - PLT_checkRange(): TRUE TRUE TRUE TRUE TRUE TRUE
			  + PARAMETER
			    + *result
			      - result[0]: FALSE
			      - result[1]: TRUE
			      - result[2]: FALSE
			      - result[3]: TRUE
			+ OUT
			  + PARAMETER
			    + *result
			      - result[0]: TRUE
			      - result[1]: TRUE
			      - result[2]: TRUE
			      - result[3]: TRUE
			  + CALLED FUNCTION PARAMETERS
			    - PLT_checkRange(input): MAX/v3=37836 MAX/v3=37836 MAX/v3=37836 MAX=65535 MAX=255 MAX=65535
			  - Return: TRUE
		"""
		$description """
			Maximum values with TRUE checkRange returned values
		"""

		$teststep 5.1 {
			$name ""
			$uuid "e2f2900b-df1d-41eb-aaba-9154623b4fbf"
			$inputs {
				PLT_checkRange() = {TRUE,TRUE,TRUE,TRUE,TRUE,TRUE}
				Rte_CpApPCOM_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage = *max*
				Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V = *max*
				Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V = *max*
				Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V = *max*
				Rte_CpHwAbsAIM_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue = *max*
				Rte_CpHwAbsPIM_PpDutyControlPilot_DeDutyControlPilot = *max*
				result = target_result_2
				&target_result_2[0] = FALSE
				&target_result_2[1] = TRUE
				&target_result_2[2] = FALSE
				&target_result_2[3] = TRUE
			}
			$outputs {
				PLT_checkRange(input) = {37836, 37836, 37836, 65535, 255, 65535}
				return TRUE
				&target_result_2[0] = TRUE
				&target_result_2[1] = TRUE
				&target_result_2[2] = TRUE
				&target_result_2[3] = TRUE
			}
			$calltrace {
				PLT_checkRange * 6
			}
		}
	}

	$testcase 6 {
		$name "Maximum Sended Value with FALSE checkRange inputs"
		$uuid "ab02f7ee-85d2-4f97-833a-04d30accf80f"
		$specification """
			+ IN
			  + DFA GETS
			    - Rte_BMS_DCRelayVoltage: Max: 65535
			    - Rte_PFC_VPH12_RMS_V: Max: 65535
			    - Rte_PFC_VPH23_RMS_V: Max: 65535
			    - Rte_PFC_VPH31_RMS_V: Max: 65535
			    - Rte_DeProximityDetectPhysicalValue: Max: 65535
			    - Rte_DeDutyControlPilot: Max: 255
			  + FUNCTION RETURN
			    - PLT_checkRange(): FALSE FALSE FALSE FALSE FALSE FALSE
			  + PARAMETER
			    + *result
			      - result[0]: TRUE
			      - result[1]: FALSE
			      - result[2]: TRUE
			      - result[3]: FALSE
			+ OUT
			  + PARAMETER
			    + *result
			      - result[0]: FALSE
			      - result[1]: FALSE
			      - result[2]: FALSE
			      - result[3]: FALSE
			  + CALLED FUNCTION PARAMETERS
			    - PLT_checkRange(input): MAX/v3=37836 MAX/v3=37836 MAX/v3=37836 MAX=65535 MAX=255 MAX=65535
			  - Return: FALSE
		"""
		$description """
			Maximum values with FALSE checkRange returned values
		"""

		$teststep 6.1 {
			$name ""
			$uuid "4b975ca7-1a6e-4548-9d32-f5f15e782457"
			$inputs {
				PLT_checkRange() = {FALSE,FALSE,FALSE,FALSE,FALSE,FALSE}
				Rte_CpApPCOM_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage = *max*
				Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V = *max*
				Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V = *max*
				Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V = *max*
				Rte_CpHwAbsAIM_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue = *max*
				Rte_CpHwAbsPIM_PpDutyControlPilot_DeDutyControlPilot = *max*
				result = target_result_2
				&target_result_2[0] = TRUE
				&target_result_2[1] = FALSE
				&target_result_2[2] = TRUE
				&target_result_2[3] = FALSE
			}
			$outputs {
				PLT_checkRange(input) = {37836, 37836, 37836, 65535, 255, 65535}
				return FALSE
				&target_result_2[0] = *none*
				&target_result_2[1] = FALSE
				&target_result_2[2] = FALSE
				&target_result_2[3] = FALSE
			}
			$calltrace {
				PLT_checkRange * 6
			}
		}
	}

	$testcase 9 {
		$name "CheckRange not defined Maximum returned Value Bug TestCase"
		$uuid "fcffb040-46ed-4b1f-998a-1b75ec2fd967"
		$specification """
			+ IN
			  + DFA GETS
			    - Rte_BMS_DCRelayVoltage: ID: 40
			    - Rte_PFC_VPH12_RMS_V: ID: 41
			    - Rte_PFC_VPH23_RMS_V: ID: 42
			    - Rte_PFC_VPH31_RMS_V: ID: 43
			    - Rte_DeProximityDetectPhysicalValue: ID: 44
			    - Rte_DeDutyControlPilot: ID: 45
			  + FUNCTION RETURN
			    - PLT_checkRange(): MAX MAX MAX MAX MAX MAX
			  + PARAMETER
			    + *result
			      - result[0]: FALSE
			      - result[1]: TRUE
			      - result[2]: FALSE
			      - result[3]: TRUE
			+ OUT
			  + PARAMETER
			    + *result
			      - result[0]: TRUE
			      - result[1]: Max: 255
			      - result[2]: Max: 255
			      - result[3]: Max: 255
			  + CALLED FUNCTION PARAMETERS
			    - PLT_checkRange(input): 41/v3=23 42/v3=24 43/v3=24 44 45 40
			  - Return: TRUE
		"""
		$description """
			When CheckRange return a different bvalue from TRUE or FALSE, output is not as expected due to Max is 1111 1111
			3 phases elements works different due to the &=
			Developer has the responsability against this case
		"""

		$teststep 9.1 {
			$name ""
			$uuid "4da88250-7aa9-4c13-87af-aa7ce5c8ca2c"
			$inputs {
				PLT_checkRange() = {*max*, *max*, *max*, *max*, *max*, *max*}
				Rte_CpApPCOM_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage = 40
				Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V = 41
				Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V = 42
				Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V = 43
				Rte_CpHwAbsAIM_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue = 44
				Rte_CpHwAbsPIM_PpDutyControlPilot_DeDutyControlPilot = 45
				result = target_result_2
				&target_result_2[0] = FALSE
				&target_result_2[1] = TRUE
				&target_result_2[2] = FALSE
				&target_result_2[3] = TRUE
			}
			$outputs {
				PLT_checkRange(input) = {23, 24, 24, 44, 45, 40}
				return TRUE
				&target_result_2[0] = TRUE
				&target_result_2[1] = 255
				&target_result_2[2] = 255
				&target_result_2[3] = 255
			}
			$calltrace {
				PLT_checkRange * 6
			}
		}
	}

	$testcase 10 {
		$name "CheckRange not defined 240 returned Value Bug TestCase"
		$uuid "1b3242fd-789b-4552-a58b-ed4162f221d1"
		$specification """
			+ IN
			  + DFA GETS
			    - Rte_BMS_DCRelayVoltage: ID: 40
			    - Rte_PFC_VPH12_RMS_V: ID: 41
			    - Rte_PFC_VPH23_RMS_V: ID: 42
			    - Rte_PFC_VPH31_RMS_V: ID: 43
			    - Rte_DeProximityDetectPhysicalValue: ID: 44
			    - Rte_DeDutyControlPilot: ID: 45
			  + FUNCTION RETURN
			    - PLT_checkRange(): 240 240 240 240 240 240
			  + PARAMETER
			    + *result
			      - result[0]: TRUE
			      - result[1]: FALSE
			      - result[2]: TRUE
			      - result[3]: FALSE
			+ OUT
			  + PARAMETER
			    + *result
			      - result[0]: FALSE
			      - result[1]: 240
			      - result[2]: 240
			      - result[3]: 240
			  + CALLED FUNCTION PARAMETERS
			    - PLT_checkRange(input): 41/v3=23 42/v3=24 43/v3=24 44 45 40
			  - Return: FALSE
		"""
		$description """
			When CheckRange return a different bvalue from TRUE or FALSE, output is not as expected due to 240 is 1111 0000.
			3 phases elements works different due to the &=
			Developer has the responsability against this case
		"""

		$teststep 10.1 {
			$name ""
			$uuid "ba8d3066-441a-4815-9541-4bb67ae67231"
			$inputs {
				PLT_checkRange() = {240, 240, 240, 240, 240, 240}
				Rte_CpApPCOM_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage = 40
				Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V = 41
				Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V = 42
				Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V = 43
				Rte_CpHwAbsAIM_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue = 44
				Rte_CpHwAbsPIM_PpDutyControlPilot_DeDutyControlPilot = 45
				result = target_result_2
				&target_result_2[0] = TRUE
				&target_result_2[1] = FALSE
				&target_result_2[2] = TRUE
				&target_result_2[3] = FALSE
			}
			$outputs {
				PLT_checkRange(input) = {23, 24, 24, 44, 45, 40}
				return FALSE
				&target_result_2[0] = *none*
				&target_result_2[1] = 240
				&target_result_2[2] = 240
				&target_result_2[3] = 240
			}
			$calltrace {
				PLT_checkRange * 6
			}
		}
	}
}