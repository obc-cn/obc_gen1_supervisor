$testobject {

	$testcase 1 {
		$name "NULL Fault"
		$uuid "a499d41a-eecb-4846-a4a1-b3385c8c21cb"
		$specification """
			+ INPUT
			  + PARAMETERS
			    - bridgeL: Change to FALSE
			    - Fault: NULL
			    - Battery: Battery OK
			    - Counter: PassThrou 55
			  - LAD_ElockHOpenCircuitConfirmTime LAD_ElockHOpenCircuitHealTime: 100 200
			  - Rte_Pim_PimElockNotCurrentMeasured: TRUE
			  - Elock: Error, no change UnLocked
			+ OUTPUT
			  - PrefaultOpenCircuit: None
			  - LAD_diagnosisDebounce: No Call
			  - LAD_HOpenC_bridgeL_prev: FALSE
			  - Fault: None
		"""

		$teststep 1.1 {
			$name ""
			$uuid "17e3759d-4d51-4eef-9780-bc4422a37a3d"
			$inputs {
				LAD_ElockLOpenCircuitConfirmTime = 100
				LAD_ElockLOpenCircuitHealTime = 200
				LAD_task_ElockLOpenCircuit::LAD_LOpenC_bridgeH_prev#1 = TRUE
				Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit = 33
				Rte_CpApLAD_PimElockNotCurrentMeasured = TRUE
				Rte_CpApLAD_PimElockSensor_PreviousToLock = ELOCK_UNLOCKED
				Rte_CpApLSD_PpOutputELockSensor_DeOutputELockSensor = ELOCK_UNLOCKED
				bridgeH = FALSE
				Fault = NULL
				BatteryVoltageState = BAT_VALID_RANGE
				Counter = 0x5678
			}
			$outputs {
				LAD_diagnosisDebounce(LAD_confirmTime) = *none*
				LAD_diagnosisDebounce(LAD_counter) = *none*
				LAD_diagnosisDebounce(LAD_healTime) = *none*
				LAD_diagnosisDebounce(LAD_input) = *none*
				LAD_diagnosisDebounce(LAD_output) = *none*
				LAD_task_ElockLOpenCircuit::LAD_LOpenC_bridgeH_prev#1 = FALSE
				Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit = *none*
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 2 {
		$name "NULL Counter"
		$uuid "b96276fe-b5df-43be-a6bc-35a0cbd0cbbe"
		$specification """
			+ INPUT
			  + PARAMETERS
			    - bridgeL: Change to FALSE
			    - Fault: Not NULL
			    - Battery: Battery OK
			    - Counter: NULL
			  - LAD_ElockHOpenCircuitConfirmTime LAD_ElockHOpenCircuitHealTime: 100 200
			  - Rte_Pim_PimElockNotCurrentMeasured: TRUE
			  - Elock: Error, no change UnLocked
			+ OUTPUT
			  - PrefaultOpenCircuit: None
			  - LAD_diagnosisDebounce: No Call
			  - LAD_HOpenC_bridgeL_prev: TRUE
			  - Fault: Debounce
		"""

		$teststep 2.1 {
			$name ""
			$uuid "ebe1c3e2-13ef-4cdd-9010-ad94583b98aa"
			$inputs {
				LAD_ElockLOpenCircuitConfirmTime = 100
				LAD_ElockLOpenCircuitHealTime = 200
				LAD_task_ElockLOpenCircuit::LAD_LOpenC_bridgeH_prev#1 = TRUE
				Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit = 33
				Rte_CpApLAD_PimElockNotCurrentMeasured = TRUE
				Rte_CpApLAD_PimElockSensor_PreviousToLock = ELOCK_UNLOCKED
				Rte_CpApLSD_PpOutputELockSensor_DeOutputELockSensor = ELOCK_UNLOCKED
				bridgeH = FALSE
				Fault = target_Fault
				BatteryVoltageState = BAT_VALID_RANGE
				Counter = NULL
				&target_Fault = 33
			}
			$outputs {
				LAD_diagnosisDebounce(LAD_confirmTime) = *none*
				LAD_diagnosisDebounce(LAD_counter) = *none*
				LAD_diagnosisDebounce(LAD_healTime) = *none*
				LAD_diagnosisDebounce(LAD_input) = *none*
				LAD_diagnosisDebounce(LAD_output) = *none*
				LAD_task_ElockLOpenCircuit::LAD_LOpenC_bridgeH_prev#1 = TRUE
				Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit = *none*
				&target_Fault = 33
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 3 {
		$name "NULL Both"
		$uuid "88376c6c-6138-4fd9-bceb-c4896dc3d2de"
		$specification """
			+ INPUT
			  + PARAMETERS
			    - bridgeL: Change to FALSE
			    - Fault: NULL
			    - Battery: Battery OK
			    - Counter: NULL
			  - LAD_ElockHOpenCircuitConfirmTime LAD_ElockHOpenCircuitHealTime: 100 200
			  - Rte_Pim_PimElockNotCurrentMeasured: TRUE
			  - Elock: Error, no change UnLocked
			+ OUTPUT
			  - PrefaultOpenCircuit: None
			  - LAD_diagnosisDebounce: No Call
			  - LAD_HOpenC_bridgeL_prev: TRUE
			  - Fault: None
		"""

		$teststep 3.1 {
			$name ""
			$uuid "d9be321b-181c-43ae-a790-cb597d08cd53"
			$inputs {
				LAD_ElockLOpenCircuitConfirmTime = 100
				LAD_ElockLOpenCircuitHealTime = 200
				LAD_task_ElockLOpenCircuit::LAD_LOpenC_bridgeH_prev#1 = TRUE
				Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit = 33
				Rte_CpApLAD_PimElockNotCurrentMeasured = TRUE
				Rte_CpApLAD_PimElockSensor_PreviousToLock = ELOCK_UNLOCKED
				Rte_CpApLSD_PpOutputELockSensor_DeOutputELockSensor = ELOCK_UNLOCKED
				bridgeH = FALSE
				Fault = NULL
				BatteryVoltageState = BAT_VALID_RANGE
				Counter = NULL
			}
			$outputs {
				LAD_diagnosisDebounce(LAD_confirmTime) = *none*
				LAD_diagnosisDebounce(LAD_counter) = *none*
				LAD_diagnosisDebounce(LAD_healTime) = *none*
				LAD_diagnosisDebounce(LAD_input) = *none*
				LAD_diagnosisDebounce(LAD_output) = *none*
				LAD_task_ElockLOpenCircuit::LAD_LOpenC_bridgeH_prev#1 = TRUE
				Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit = *none*
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 4 {
		$name "Prefault & Batery OK"
		$uuid "786aba69-fcc0-4271-90b4-192126ee4e6d"
		$specification """
			+ INPUT
			  + PARAMETERS
			    - bridgeL: Change to FALSE
			    - Fault: Not NULL
			    - Battery: Battery OK
			    - Counter: PassThrou 55
			  - LAD_ElockHOpenCircuitConfirmTime LAD_ElockHOpenCircuitHealTime: 100 200
			  - Rte_Pim_PimElockNotCurrentMeasured: TRUE
			  - Elock: Error, no change UnLocked
			+ OUTPUT
			  - PrefaultOpenCircuit: TRUE
			  - LAD_diagnosisDebounce: Call
			  - LAD_HOpenC_bridgeL_prev: FALSE
			  - Fault: Debounce
		"""

		$teststep 4.1 {
			$name ""
			$uuid "2f715370-d725-444e-86cc-3ca209df1edd"
			$inputs {
				LAD_ElockLOpenCircuitConfirmTime = 100
				LAD_ElockLOpenCircuitHealTime = 200
				LAD_task_ElockLOpenCircuit::LAD_LOpenC_bridgeH_prev#1 = TRUE
				Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit = 33
				Rte_CpApLAD_PimElockNotCurrentMeasured = TRUE
				Rte_CpApLAD_PimElockSensor_PreviousToLock = ELOCK_UNLOCKED
				Rte_CpApLSD_PpOutputELockSensor_DeOutputELockSensor = ELOCK_UNLOCKED
				bridgeH = FALSE
				Fault = target_Fault
				BatteryVoltageState = BAT_VALID_RANGE
				Counter = 0x5678
				&target_Fault = 33
			}
			$outputs {
				LAD_diagnosisDebounce(LAD_confirmTime) = 1000
				LAD_diagnosisDebounce(LAD_counter) = 0x5678
				LAD_diagnosisDebounce(LAD_healTime) = 2000
				LAD_diagnosisDebounce(LAD_input) = TRUE
				LAD_diagnosisDebounce(LAD_output) = target_Fault
				LAD_task_ElockLOpenCircuit::LAD_LOpenC_bridgeH_prev#1 = FALSE
				Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit = TRUE
				&target_Fault = 33
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 5 {
		$name "Prefault & Batery Undervoltage"
		$uuid "c8578fea-b655-43ac-b6b4-0f919df40861"
		$specification """
			+ INPUT
			  + PARAMETERS
			    - bridgeL: Change to FALSE
			    - Fault: Not NULL
			    - Battery: Battery Error Undervoltage
			    - Counter: PassThrou 55
			  - LAD_ElockHOpenCircuitConfirmTime LAD_ElockHOpenCircuitHealTime: 100 200
			  - Rte_Pim_PimElockNotCurrentMeasured: TRUE
			  - Elock: Error, no change UnLocked
			+ OUTPUT
			  - PrefaultOpenCircuit: TRUE
			  - LAD_diagnosisDebounce: No Call
			  - LAD_HOpenC_bridgeL_prev: FALSE
			  - Fault: FALSE
		"""

		$teststep 5.1 {
			$name ""
			$uuid "0a084134-b151-4bbc-88d9-74bd0ab29fce"
			$inputs {
				LAD_ElockLOpenCircuitConfirmTime = 100
				LAD_ElockLOpenCircuitHealTime = 200
				LAD_task_ElockLOpenCircuit::LAD_LOpenC_bridgeH_prev#1 = TRUE
				Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit = 33
				Rte_CpApLAD_PimElockNotCurrentMeasured = TRUE
				Rte_CpApLAD_PimElockSensor_PreviousToLock = ELOCK_UNLOCKED
				Rte_CpApLSD_PpOutputELockSensor_DeOutputELockSensor = ELOCK_UNLOCKED
				bridgeH = FALSE
				Fault = target_Fault
				BatteryVoltageState = BAT_UNDERVOLTAGE
				Counter = 0x5678
				&target_Fault = 33
			}
			$outputs {
				LAD_diagnosisDebounce(LAD_confirmTime) = *none*
				LAD_diagnosisDebounce(LAD_counter) = *none*
				LAD_diagnosisDebounce(LAD_healTime) = *none*
				LAD_diagnosisDebounce(LAD_input) = TRUE
				LAD_diagnosisDebounce(LAD_output) = *none*
				LAD_task_ElockLOpenCircuit::LAD_LOpenC_bridgeH_prev#1 = FALSE
				Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit = TRUE
				&target_Fault = FALSE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 6 {
		$name "Prefault & Batery Overvoltage"
		$uuid "6266926a-d43a-48e6-83af-d1eaa112625d"
		$specification """
			+ INPUT
			  + PARAMETERS
			    - bridgeL: Change to FALSE
			    - Fault: Not NULL
			    - Battery: Battery Error Overvoltage
			    - Counter: PassThrou 55
			  - LAD_ElockHOpenCircuitConfirmTime LAD_ElockHOpenCircuitHealTime: 100 200
			  - Rte_Pim_PimElockNotCurrentMeasured: TRUE
			  - Elock: Error, no change UnLocked
			+ OUTPUT
			  - PrefaultOpenCircuit: TRUE
			  - LAD_diagnosisDebounce: No Call
			  - LAD_HOpenC_bridgeL_prev: FALSE
			  - Fault: FALSE
		"""

		$teststep 6.1 {
			$name ""
			$uuid "7d607958-3b5c-49a4-83d8-8ad4f4d0be4a"
			$inputs {
				LAD_ElockLOpenCircuitConfirmTime = 100
				LAD_ElockLOpenCircuitHealTime = 200
				LAD_task_ElockLOpenCircuit::LAD_LOpenC_bridgeH_prev#1 = TRUE
				Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit = 33
				Rte_CpApLAD_PimElockNotCurrentMeasured = TRUE
				Rte_CpApLAD_PimElockSensor_PreviousToLock = ELOCK_UNLOCKED
				Rte_CpApLSD_PpOutputELockSensor_DeOutputELockSensor = ELOCK_UNLOCKED
				bridgeH = FALSE
				Fault = target_Fault
				BatteryVoltageState = BAT_OVERVOLTAGE
				Counter = 0x5678
				&target_Fault = 33
			}
			$outputs {
				LAD_diagnosisDebounce(LAD_confirmTime) = *none*
				LAD_diagnosisDebounce(LAD_counter) = *none*
				LAD_diagnosisDebounce(LAD_healTime) = *none*
				LAD_diagnosisDebounce(LAD_input) = TRUE
				LAD_diagnosisDebounce(LAD_output) = *none*
				LAD_task_ElockLOpenCircuit::LAD_LOpenC_bridgeH_prev#1 = FALSE
				Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit = TRUE
				&target_Fault = FALSE
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 7 {
		$name "No Prefault by lock OK & Batery OK"
		$uuid "6dc91bd7-1758-4dec-8ed0-364877c5e5e9"
		$specification """
			+ INPUT
			  + PARAMETERS
			    - bridgeL: Change to FALSE
			    - Fault: Not NULL
			    - Battery: Battery OK
			    - Counter: PassThrou 55
			  - LAD_ElockHOpenCircuitConfirmTime LAD_ElockHOpenCircuitHealTime: 100 200
			  - Rte_Pim_PimElockNotCurrentMeasured: TRUE
			  - Elock: No error change to locked
			+ OUTPUT
			  - PrefaultOpenCircuit: FALSE
			  - LAD_diagnosisDebounce: Call
			  - LAD_HOpenC_bridgeL_prev: FALSE
			  - Fault: Debounce
		"""

		$teststep 7.1 {
			$name ""
			$uuid "4a26cf83-bafb-4a85-bb7b-092dffa6a512"
			$inputs {
				LAD_ElockLOpenCircuitConfirmTime = 100
				LAD_ElockLOpenCircuitHealTime = 200
				LAD_task_ElockLOpenCircuit::LAD_LOpenC_bridgeH_prev#1 = TRUE
				Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit = 33
				Rte_CpApLAD_PimElockNotCurrentMeasured = TRUE
				Rte_CpApLAD_PimElockSensor_PreviousToLock = ELOCK_UNLOCKED
				Rte_CpApLSD_PpOutputELockSensor_DeOutputELockSensor = ELOCK_LOCKED
				bridgeH = FALSE
				Fault = target_Fault
				BatteryVoltageState = BAT_VALID_RANGE
				Counter = 0x5678
				&target_Fault = 33
			}
			$outputs {
				LAD_diagnosisDebounce(LAD_confirmTime) = 1000
				LAD_diagnosisDebounce(LAD_counter) = 0x5678
				LAD_diagnosisDebounce(LAD_healTime) = 2000
				LAD_diagnosisDebounce(LAD_input) = FALSE
				LAD_diagnosisDebounce(LAD_output) = target_Fault
				LAD_task_ElockLOpenCircuit::LAD_LOpenC_bridgeH_prev#1 = FALSE
				Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit = FALSE
				&target_Fault = 33
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 8 {
		$name "No Prefault by current OK & Batery OK"
		$uuid "41ecdbc4-521b-470c-baff-79c00ee78593"
		$specification """
			+ INPUT
			  + PARAMETERS
			    - bridgeL: Change to FALSE
			    - Fault: Not NULL
			    - Battery: Battery OK
			    - Counter: PassThrou 55
			  - LAD_ElockHOpenCircuitConfirmTime LAD_ElockHOpenCircuitHealTime: 100 200
			  - Rte_Pim_PimElockNotCurrentMeasured: FALSE
			  - Elock: No error change to locked
			+ OUTPUT
			  - PrefaultOpenCircuit: FALSE
			  - LAD_diagnosisDebounce: Call
			  - LAD_HOpenC_bridgeL_prev: FALSE
			  - Fault: Debounce
		"""

		$teststep 8.1 {
			$name ""
			$uuid "974f5753-8c93-4bda-9324-22d560841dad"
			$inputs {
				LAD_ElockLOpenCircuitConfirmTime = 100
				LAD_ElockLOpenCircuitHealTime = 200
				LAD_task_ElockLOpenCircuit::LAD_LOpenC_bridgeH_prev#1 = TRUE
				Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit = 33
				Rte_CpApLAD_PimElockNotCurrentMeasured = FALSE
				Rte_CpApLAD_PimElockSensor_PreviousToLock = ELOCK_UNLOCKED
				Rte_CpApLSD_PpOutputELockSensor_DeOutputELockSensor = ELOCK_LOCKED
				bridgeH = FALSE
				Fault = target_Fault
				BatteryVoltageState = BAT_VALID_RANGE
				Counter = 0x5678
				&target_Fault = 33
			}
			$outputs {
				LAD_diagnosisDebounce(LAD_confirmTime) = 1000
				LAD_diagnosisDebounce(LAD_counter) = 0x5678
				LAD_diagnosisDebounce(LAD_healTime) = 2000
				LAD_diagnosisDebounce(LAD_input) = FALSE
				LAD_diagnosisDebounce(LAD_output) = target_Fault
				LAD_task_ElockLOpenCircuit::LAD_LOpenC_bridgeH_prev#1 = FALSE
				Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit = FALSE
				&target_Fault = 33
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 9 {
		$name "No change by bridge no change"
		$uuid "15472066-65cc-46de-a7b9-87bb96394dcf"
		$specification """
			+ INPUT
			  + PARAMETERS
			    - bridgeL: No Change
			    - Fault: Not NULL
			    - Battery: Battery OK
			    - Counter: PassThrou 55
			  - LAD_ElockHOpenCircuitConfirmTime LAD_ElockHOpenCircuitHealTime: 100 200
			  - Rte_Pim_PimElockNotCurrentMeasured: TRUE
			  - Elock: Error, no change UnLocked
			+ OUTPUT
			  - PrefaultOpenCircuit: No change
			  - LAD_diagnosisDebounce: No Call
			  - LAD_HOpenC_bridgeL_prev: FALSE
			  - Fault: Debounce
		"""

		$teststep 9.1 {
			$name ""
			$uuid "0ac3508c-c46a-4747-9aca-a7df26940f04"
			$inputs {
				LAD_ElockLOpenCircuitConfirmTime = 100
				LAD_ElockLOpenCircuitHealTime = 200
				LAD_task_ElockLOpenCircuit::LAD_LOpenC_bridgeH_prev#1 = FALSE
				Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit = 33
				Rte_CpApLAD_PimElockNotCurrentMeasured = TRUE
				Rte_CpApLAD_PimElockSensor_PreviousToLock = ELOCK_UNLOCKED
				Rte_CpApLSD_PpOutputELockSensor_DeOutputELockSensor = ELOCK_UNLOCKED
				bridgeH = FALSE
				Fault = target_Fault
				BatteryVoltageState = BAT_VALID_RANGE
				Counter = 0x5678
				&target_Fault = 33
			}
			$outputs {
				LAD_diagnosisDebounce(LAD_confirmTime) = *none*
				LAD_diagnosisDebounce(LAD_counter) = *none*
				LAD_diagnosisDebounce(LAD_healTime) = *none*
				LAD_diagnosisDebounce(LAD_input) = 33
				LAD_diagnosisDebounce(LAD_output) = *none*
				LAD_task_ElockLOpenCircuit::LAD_LOpenC_bridgeH_prev#1 = FALSE
				Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit = 33
				&target_Fault = 33
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 10 {
		$name "No change by bridge change to TRUE"
		$uuid "debeccab-e403-4257-8507-8baabce697a6"
		$specification """
			+ INPUT
			  + PARAMETERS
			    - bridgeL: Change to TRUE
			    - Fault: Not NULL
			    - Battery: Battery OK
			    - Counter: PassThrou 55
			  - LAD_ElockHOpenCircuitConfirmTime LAD_ElockHOpenCircuitHealTime: 100 200
			  - Rte_Pim_PimElockNotCurrentMeasured: FALSE
			  - Elock: No error change to locked
			+ OUTPUT
			  - PrefaultOpenCircuit: No change
			  - LAD_diagnosisDebounce: Call
			  - LAD_HOpenC_bridgeL_prev: TRUE
			  - Fault: Debounce
		"""

		$teststep 10.1 {
			$name ""
			$uuid "4e327bba-763e-4e40-89f3-4ada4caf4820"
			$inputs {
				LAD_ElockLOpenCircuitConfirmTime = 100
				LAD_ElockLOpenCircuitHealTime = 200
				LAD_task_ElockLOpenCircuit::LAD_LOpenC_bridgeH_prev#1 = FALSE
				Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit = 33
				Rte_CpApLAD_PimElockNotCurrentMeasured = FALSE
				Rte_CpApLAD_PimElockSensor_PreviousToLock = ELOCK_UNLOCKED
				Rte_CpApLSD_PpOutputELockSensor_DeOutputELockSensor = ELOCK_LOCKED
				bridgeH = TRUE
				Fault = target_Fault
				BatteryVoltageState = BAT_VALID_RANGE
				Counter = 0x5678
				&target_Fault = 33
			}
			$outputs {
				LAD_diagnosisDebounce(LAD_confirmTime) = 1000
				LAD_diagnosisDebounce(LAD_counter) = 0x5678
				LAD_diagnosisDebounce(LAD_healTime) = 2000
				LAD_diagnosisDebounce(LAD_input) = 33
				LAD_diagnosisDebounce(LAD_output) = target_Fault
				LAD_task_ElockLOpenCircuit::LAD_LOpenC_bridgeH_prev#1 = TRUE
				Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit = 33
				&target_Fault = 33
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 11 {
		$name "Prefault & Batery OK"
		$uuid "5df08ad5-61c9-4826-a1cb-d134b94e006f"
		$specification """
			+ INPUT
			  + PARAMETERS
			    - bridgeL: Change to FALSE
			    - Fault: Not NULL
			    - Battery: Battery OK
			    - Counter: PassThrou 55
			  - LAD_ElockHOpenCircuitConfirmTime LAD_ElockHOpenCircuitHealTime: 100 200
			  - Rte_Pim_PimElockNotCurrentMeasured: TRUE
			  - Elock: Error, no change UnLocked
			+ OUTPUT
			  - PrefaultOpenCircuit: TRUE
			  - LAD_diagnosisDebounce: Call
			  - LAD_HOpenC_bridgeL_prev: FALSE
			  - Fault: Debounce
		"""

		$teststep 11.1 {
			$name ""
			$uuid "daaaecd4-d8d5-42be-8f07-cfda14e599c1"
			$inputs {
				LAD_ElockLOpenCircuitConfirmTime = 100
				LAD_ElockLOpenCircuitHealTime = 200
				LAD_task_ElockLOpenCircuit::LAD_LOpenC_bridgeH_prev#1 = TRUE
				Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit = 33
				Rte_CpApLAD_PimElockNotCurrentMeasured = TRUE
				Rte_CpApLAD_PimElockSensor_PreviousToLock = ELOCK_UNLOCKED
				Rte_CpApLSD_PpOutputELockSensor_DeOutputELockSensor = ELOCK_UNLOCKED
				bridgeH = FALSE
				Fault = target_Fault
				BatteryVoltageState = BAT_VALID_RANGE
				Counter = 0x5678
				&target_Fault = 33
			}
			$outputs {
				LAD_diagnosisDebounce(LAD_confirmTime) = 1000
				LAD_diagnosisDebounce(LAD_counter) = 0x5678
				LAD_diagnosisDebounce(LAD_healTime) = 2000
				LAD_diagnosisDebounce(LAD_input) = TRUE
				LAD_diagnosisDebounce(LAD_output) = target_Fault
				LAD_task_ElockLOpenCircuit::LAD_LOpenC_bridgeH_prev#1 = FALSE
				Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit = TRUE
				&target_Fault = 33
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 12 {
		$name "No change by not previous lock"
		$uuid "b8bfb591-a3e2-41ba-ae48-24bf4b9819cc"
		$specification """
			+ INPUT
			  + PARAMETERS
			    - bridgeL: Change to FALSE
			    - Fault: Not NULL
			    - Battery: Battery OK
			    - Counter: PassThrou 55
			  - LAD_ElockHOpenCircuitConfirmTime LAD_ElockHOpenCircuitHealTime: 100 200
			  - Rte_Pim_PimElockNotCurrentMeasured: TRUE
			  - Elock: No change lock
			+ OUTPUT
			  - PrefaultOpenCircuit: No change
			  - LAD_diagnosisDebounce: No Call
			  - LAD_HOpenC_bridgeL_prev: FALSE
			  - Fault: Debounce
		"""

		$teststep 12.1 {
			$name ""
			$uuid "e8539386-0fbc-4943-8d1a-58cd7552eb47"
			$inputs {
				LAD_ElockLOpenCircuitConfirmTime = 100
				LAD_ElockLOpenCircuitHealTime = 200
				LAD_task_ElockLOpenCircuit::LAD_LOpenC_bridgeH_prev#1 = TRUE
				Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit = 33
				Rte_CpApLAD_PimElockNotCurrentMeasured = TRUE
				Rte_CpApLAD_PimElockSensor_PreviousToLock = ELOCK_LOCKED
				Rte_CpApLSD_PpOutputELockSensor_DeOutputELockSensor = ELOCK_LOCKED
				bridgeH = FALSE
				Fault = target_Fault
				BatteryVoltageState = BAT_VALID_RANGE
				Counter = 0x5678
				&target_Fault = 33
			}
			$outputs {
				LAD_diagnosisDebounce(LAD_confirmTime) = *none*
				LAD_diagnosisDebounce(LAD_counter) = *none*
				LAD_diagnosisDebounce(LAD_healTime) = *none*
				LAD_diagnosisDebounce(LAD_input) = 33
				LAD_diagnosisDebounce(LAD_output) = *none*
				LAD_task_ElockLOpenCircuit::LAD_LOpenC_bridgeH_prev#1 = FALSE
				Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit = 33
				&target_Fault = 33
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 13 {
		$name "Prefault current true & Batery OK"
		$uuid "a1fa106a-d36e-43f8-99be-5be786c6e052"
		$specification """
			+ INPUT
			  + PARAMETERS
			    - bridgeL: Change to FALSE
			    - Fault: Not NULL
			    - Battery: Battery OK
			    - Counter: PassThrou 55
			  - LAD_ElockHOpenCircuitConfirmTime LAD_ElockHOpenCircuitHealTime: 100 200
			  - Rte_Pim_PimElockNotCurrentMeasured: true 255
			  - Elock: Error, no change UnLocked
			+ OUTPUT
			  - PrefaultOpenCircuit: TRUE
			  - LAD_diagnosisDebounce: Call
			  - LAD_HOpenC_bridgeL_prev: FALSE
			  - Fault: Debounce
		"""

		$teststep 13.1 {
			$name ""
			$uuid "309bfaa2-95dd-44cf-a27f-60377ac976b4"
			$inputs {
				LAD_ElockLOpenCircuitConfirmTime = 100
				LAD_ElockLOpenCircuitHealTime = 200
				LAD_task_ElockLOpenCircuit::LAD_LOpenC_bridgeH_prev#1 = TRUE
				Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit = 33
				Rte_CpApLAD_PimElockNotCurrentMeasured = 255
				Rte_CpApLAD_PimElockSensor_PreviousToLock = ELOCK_UNLOCKED
				Rte_CpApLSD_PpOutputELockSensor_DeOutputELockSensor = ELOCK_UNLOCKED
				bridgeH = FALSE
				Fault = target_Fault
				BatteryVoltageState = BAT_VALID_RANGE
				Counter = 0x5678
				&target_Fault = 33
			}
			$outputs {
				LAD_diagnosisDebounce(LAD_confirmTime) = 1000
				LAD_diagnosisDebounce(LAD_counter) = 0x5678
				LAD_diagnosisDebounce(LAD_healTime) = 2000
				LAD_diagnosisDebounce(LAD_input) = TRUE
				LAD_diagnosisDebounce(LAD_output) = target_Fault
				LAD_task_ElockLOpenCircuit::LAD_LOpenC_bridgeH_prev#1 = FALSE
				Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit = TRUE
				&target_Fault = 33
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 14 {
		$name "No change feedback undefined"
		$uuid "29fb1bd8-9fff-46af-a0f1-7ecbfbd5d47a"
		$specification """
			+ INPUT
			  + PARAMETERS
			    - bridgeL: Change to FALSE
			    - Fault: Not NULL
			    - Battery: Battery OK
			    - Counter: PassThrou 55
			  - LAD_ElockHOpenCircuitConfirmTime LAD_ElockHOpenCircuitHealTime: 100 200
			  - Rte_Pim_PimElockNotCurrentMeasured: TRUE
			  - Elock: Feedback Undefined
			+ OUTPUT
			  - PrefaultOpenCircuit: No change
			  - LAD_diagnosisDebounce: No Call
			  - LAD_HOpenC_bridgeL_prev: FALSE
			  - Fault: Debounce
		"""

		$teststep 14.1 {
			$name ""
			$uuid "3a3d10b3-6163-471b-9b50-69abaf95ce44"
			$inputs {
				LAD_ElockLOpenCircuitConfirmTime = 100
				LAD_ElockLOpenCircuitHealTime = 200
				LAD_task_ElockLOpenCircuit::LAD_LOpenC_bridgeH_prev#1 = TRUE
				Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit = 33
				Rte_CpApLAD_PimElockNotCurrentMeasured = TRUE
				Rte_CpApLAD_PimElockSensor_PreviousToLock = ELOCK_UNLOCKED
				Rte_CpApLSD_PpOutputELockSensor_DeOutputELockSensor = ELOCK_DRIVE_UNDEFINED
				bridgeH = FALSE
				Fault = target_Fault
				BatteryVoltageState = BAT_VALID_RANGE
				Counter = 0x5678
				&target_Fault = 33
			}
			$outputs {
				LAD_diagnosisDebounce(LAD_confirmTime) = *none*
				LAD_diagnosisDebounce(LAD_counter) = *none*
				LAD_diagnosisDebounce(LAD_healTime) = *none*
				LAD_diagnosisDebounce(LAD_input) = 33
				LAD_diagnosisDebounce(LAD_output) = *none*
				LAD_task_ElockLOpenCircuit::LAD_LOpenC_bridgeH_prev#1 = FALSE
				Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit = 33
				&target_Fault = 33
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}
}