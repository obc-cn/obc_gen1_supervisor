$testobject {

	$testcase 2 {
		$name ""
		$uuid "d62ee651-71af-4213-a617-413d664e4033"
		$specification """
			+ IN
			  + PARAMETERS
			    - ArrayPosition:ID: 5
			    - OVlimit1:FAULT_LEVEL_NO_FAULT
			    - OVlimit2:FAULT_LEVEL_NO_FAULT
			  + GLOBALS
			    - LFM_RestartCounter[12]:ID: 1...12
			    - OFM_OBCFaultTimeToRetryDefault:ID: 20
			    - LFM_TimeBatteryVoltageSafety:ID: 40
			  + FUNCTION RETURNS
			    - LFM_GetFaultLevel():ID: 100
			    - LFM_OutputReport():ID: 106
			+ OUT
			  + SENDED PARAMETERS
			    + LFM_GetFaultLevel
			      - firstLevel:FALSE
			      - secondLevel:FALSE
			      - thirdlevel:FALSE
			    + LFM_debounce
			      - Input:FALSE
			      - ConfirmTime:ID: 40
			      - HealTime:0
			      + *Debounce
			        - State:FALSE
			        - Counter:0
			    + LFM_OutputReport(ErrorLevel)
			      - ErrorLevel:ID: 100
			      - RestartTime:ID: 20
			      - *RestartCounter:Id: 6
			  + RTE SETS
			    - Rte_DCHVOutputShortCircuit_Error RETURNED VALUE:106
		"""
		$description """
			Condition allowed combination 3
		"""

		$teststep 2.1 {
			$name ""
			$uuid "76b15ba9-887e-49a6-9941-aa0d3ac7d1e8"
			$inputs {
				LFM_DCLVFaultTimeToRetryDefault = 20
				LFM_GetFaultLevel() = 100
				LFM_OutputReport() = 106
				LFM_OutputReport(RestartCounter) = target_LFM_OutputReport_RestartCounter
				LFM_RestartCounter[0] = 1
				LFM_RestartCounter[1] = 2
				LFM_RestartCounter[2] = 3
				LFM_RestartCounter[3] = 4
				LFM_RestartCounter[4] = 5
				LFM_RestartCounter[5] = 6
				LFM_RestartCounter[6] = 7
				LFM_RestartCounter[7] = 8
				LFM_RestartCounter[8] = 9
				LFM_RestartCounter[9] = 10
				LFM_RestartCounter[10] = 11
				LFM_RestartCounter[11] = 12
				LFM_TimeBatteryVoltageSafety = 40
				LFM_debounce(Debounce) = target_LFM_debounce_Debounce
				Rte_CpApPLS_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error = 1
				Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt = 200
				FaultIndex = 5
				OVlimit1 = FAULT_LEVEL_NO_FAULT
				OVlimit2 = FAULT_LEVEL_NO_FAULT
			}
			$outputs {
				LFM_FaultVar[0] = 0
				LFM_GetFaultLevel(firstLevel) = FALSE
				LFM_GetFaultLevel(secondLevel) = FALSE
				LFM_GetFaultLevel(thirdlevel) = FALSE
				LFM_OutputReport(ErrorLevel) = 100
				LFM_OutputReport(RestartTime) = 20
				LFM_debounce(ConfirmTime) = 40
				LFM_debounce(HealTime) = 0
				LFM_debounce(Input) = FALSE
				Rte_CpApLFM_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError = 1
				Rte_CpApLFM_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage = 255
				&target_LFM_debounce_Debounce {
					State = FALSE
					Counter = 0
				}
				&target_LFM_OutputReport_RestartCounter = 6
			}
			$calltrace {
				LFM_debounce
				LFM_GetFaultLevel
				LFM_OutputReport
			}
		}
	}

	$testcase 3 {
		$name ""
		$uuid "4fdbffcf-f413-4c1c-948a-304a8d131aa9"
		$specification """
			+ IN
			  + PARAMETERS
			    - ArrayPosition:ID: 5
			    - OVlimit1:FAULT_LEVEL_NO_FAULT
			    - OVlimit2:Max: 255
			  + GLOBALS
			    - LFM_RestartCounter[12]:Max: 4294967295
			    - OFM_OBCFaultTimeToRetryDefault:Max: 4294967295
			    - LFM_TimeBatteryVoltageSafety:Max: 65535
			  + FUNCTION RETURNS
			    - LFM_GetFaultLevel():Max: 255
			    - LFM_OutputReport():Max: 255
			+ OUT
			  + SENDED PARAMETERS
			    + LFM_GetFaultLevel
			      - firstLevel:FALSE
			      - secondLevel:FALSE
			      - thirdlevel:FALSE
			    + LFM_debounce
			      - Input:TRUE
			      - ConfirmTime:Max: 65535
			      - HealTime:0
			      + *Debounce
			        - State:FALSE
			        - Counter:0
			    + LFM_OutputReport(ErrorLevel)
			      - ErrorLevel:Max: 255
			      - RestartTime:Max: 65535
			      - *RestartCounter:Max: 4294967295
			  + RTE SETS
			    - Rte_DCHVOutputShortCircuit_Error RETURNED VALUE:Max: 255
		"""
		$description """
			Condition allowed combination 6
		"""

		$teststep 3.1 {
			$name ""
			$uuid "ef4932b2-92ac-481c-9c6f-fbc996bf2c47"
			$inputs {
				LFM_DCLVFaultTimeToRetryDefault = *max*
				LFM_GetFaultLevel() = *max*
				LFM_OutputReport() = *max*
				LFM_OutputReport(RestartCounter) = target_LFM_OutputReport_RestartCounter
				LFM_RestartCounter[0] = *max*
				LFM_RestartCounter[1] = *max*
				LFM_RestartCounter[2] = *max*
				LFM_RestartCounter[3] = *max*
				LFM_RestartCounter[4] = *max*
				LFM_RestartCounter[5] = *max*
				LFM_RestartCounter[6] = *max*
				LFM_RestartCounter[7] = *max*
				LFM_RestartCounter[8] = *max*
				LFM_RestartCounter[9] = *max*
				LFM_RestartCounter[10] = *max*
				LFM_RestartCounter[11] = *max*
				LFM_TimeBatteryVoltageSafety = *max*
				LFM_debounce(Debounce) = target_LFM_debounce_Debounce
				Rte_CpApPLS_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error = 2
				Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt = 255
				FaultIndex = 5
				OVlimit1 = FAULT_LEVEL_NO_FAULT
				OVlimit2 = *max*
			}
			$outputs {
				LFM_FaultVar[0] = 0
				LFM_GetFaultLevel(firstLevel) = FALSE
				LFM_GetFaultLevel(secondLevel) = FALSE
				LFM_GetFaultLevel(thirdlevel) = FALSE
				LFM_OutputReport(ErrorLevel) = *max*
				LFM_OutputReport(RestartTime) = *max*
				LFM_debounce(ConfirmTime) = *max*
				LFM_debounce(HealTime) = 0
				LFM_debounce(Input) = TRUE
				Rte_CpApLFM_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError = 1
				Rte_CpApLFM_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage = 255
				&target_LFM_debounce_Debounce {
					State = FALSE
					Counter = 0
				}
				&target_LFM_OutputReport_RestartCounter = *max*
			}
			$calltrace {
				LFM_debounce
				LFM_GetFaultLevel
				LFM_OutputReport
			}
		}
	}

	$testcase 4 {
		$name ""
		$uuid "373277c9-3345-49c4-a7de-d9da52c7500e"
		$specification """
			+ IN
			  + PARAMETERS
			    - ArrayPosition:ID: 5
			    - OVlimit1:Max: 255
			    - OVlimit2:FAULT_LEVEL_NO_FAULT
			  + GLOBALS
			    - LFM_RestartCounter[12]:ID: 1...12
			    - OFM_OBCFaultTimeToRetryDefault:ID: 20
			    - LFM_TimeBatteryVoltageSafety:ID: 40
			  + FUNCTION RETURNS
			    - LFM_GetFaultLevel():ID: 100
			    - LFM_OutputReport():ID: 106
			+ OUT
			  + SENDED PARAMETERS
			    + LFM_GetFaultLevel
			      - firstLevel:FALSE
			      - secondLevel:FALSE
			      - thirdlevel:FALSE
			    + LFM_debounce
			      - Input:TRUE
			      - ConfirmTime:ID: 40
			      - HealTime:0
			      + *Debounce
			        - State:FALSE
			        - Counter:0
			    + LFM_OutputReport(ErrorLevel)
			      - ErrorLevel:ID: 100
			      - RestartTime:ID: 20
			      - *RestartCounter:Id: 6
			  + RTE SETS
			    - Rte_DCHVOutputShortCircuit_Error RETURNED VALUE:106
		"""
		$description """
			Not allowed condition combination 1
		"""

		$teststep 4.1 {
			$name ""
			$uuid "d853a726-e8ef-477d-81b3-25293cbb28f7"
			$inputs {
				LFM_DCLVFaultTimeToRetryDefault = 20
				LFM_GetFaultLevel() = 100
				LFM_OutputReport() = 106
				LFM_OutputReport(RestartCounter) = target_LFM_OutputReport_RestartCounter
				LFM_RestartCounter[0] = 1
				LFM_RestartCounter[1] = 2
				LFM_RestartCounter[2] = 3
				LFM_RestartCounter[3] = 4
				LFM_RestartCounter[4] = 5
				LFM_RestartCounter[5] = 6
				LFM_RestartCounter[6] = 7
				LFM_RestartCounter[7] = 8
				LFM_RestartCounter[8] = 9
				LFM_RestartCounter[9] = 10
				LFM_RestartCounter[10] = 11
				LFM_RestartCounter[11] = 12
				LFM_TimeBatteryVoltageSafety = 40
				LFM_debounce(Debounce) = target_LFM_debounce_Debounce
				Rte_CpApPLS_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error = 0
				Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt = 255
				FaultIndex = 5
				OVlimit1 = *max*
				OVlimit2 = FAULT_LEVEL_NO_FAULT
			}
			$outputs {
				LFM_FaultVar[0] = 0
				LFM_GetFaultLevel(firstLevel) = FALSE
				LFM_GetFaultLevel(secondLevel) = FALSE
				LFM_GetFaultLevel(thirdlevel) = FALSE
				LFM_OutputReport(ErrorLevel) = 100
				LFM_OutputReport(RestartTime) = 20
				LFM_debounce(ConfirmTime) = 40
				LFM_debounce(HealTime) = 0
				LFM_debounce(Input) = TRUE
				Rte_CpApLFM_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError = 1
				Rte_CpApLFM_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage = 255
				&target_LFM_debounce_Debounce {
					State = FALSE
					Counter = 0
				}
				&target_LFM_OutputReport_RestartCounter = 6
			}
			$calltrace {
				LFM_debounce
				LFM_GetFaultLevel
				LFM_OutputReport
			}
		}
	}

	$testcase 5 {
		$name ""
		$uuid "485d73b3-f1c8-4854-a2bc-3b661d40513c"
		$specification """
			+ IN
			  + PARAMETERS
			    - ArrayPosition:ID: 5
			    - OVlimit1:Max: 255
			    - OVlimit2:Max: 255
			  + GLOBALS
			    - LFM_RestartCounter[12]:Max: 4294967295
			    - OFM_OBCFaultTimeToRetryDefault:Max: 4294967295
			    - LFM_TimeBatteryVoltageSafety:Max: 65535
			  + FUNCTION RETURNS
			    - LFM_GetFaultLevel():Max: 255
			    - LFM_OutputReport():Max: 255
			+ OUT
			  + SENDED PARAMETERS
			    + LFM_GetFaultLevel
			      - firstLevel:FALSE
			      - secondLevel:FALSE
			      - thirdlevel:FALSE
			    + LFM_debounce
			      - Input:TRUE
			      - ConfirmTime:Max: 65535
			      - HealTime:0
			      + *Debounce
			        - State:FALSE
			        - Counter:0
			    + LFM_OutputReport(ErrorLevel)
			      - ErrorLevel:Max: 255
			      - RestartTime:Max: 65535
			      - *RestartCounter:Max: 4294967295
			  + RTE SETS
			    - Rte_DCHVOutputShortCircuit_Error RETURNED VALUE:Max: 255
		"""
		$description """
			Not allowed condition combination 2
		"""

		$teststep 5.1 {
			$name ""
			$uuid "311f6b2c-02aa-4c1f-b872-273d6a8ea525"
			$inputs {
				LFM_DCLVFaultTimeToRetryDefault = *max*
				LFM_GetFaultLevel() = *max*
				LFM_OutputReport() = *max*
				LFM_OutputReport(RestartCounter) = target_LFM_OutputReport_RestartCounter
				LFM_RestartCounter[0] = *max*
				LFM_RestartCounter[1] = *max*
				LFM_RestartCounter[2] = *max*
				LFM_RestartCounter[3] = *max*
				LFM_RestartCounter[4] = *max*
				LFM_RestartCounter[5] = *max*
				LFM_RestartCounter[6] = *max*
				LFM_RestartCounter[7] = *max*
				LFM_RestartCounter[8] = *max*
				LFM_RestartCounter[9] = *max*
				LFM_RestartCounter[10] = *max*
				LFM_RestartCounter[11] = *max*
				LFM_TimeBatteryVoltageSafety = *max*
				LFM_debounce(Debounce) = target_LFM_debounce_Debounce
				Rte_CpApPLS_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error = 3
				Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt = 41
				FaultIndex = 5
				OVlimit1 = *max*
				OVlimit2 = *max*
			}
			$outputs {
				LFM_FaultVar[0] = 0
				LFM_GetFaultLevel(firstLevel) = FALSE
				LFM_GetFaultLevel(secondLevel) = FALSE
				LFM_GetFaultLevel(thirdlevel) = FALSE
				LFM_OutputReport(ErrorLevel) = *max*
				LFM_OutputReport(RestartTime) = *max*
				LFM_debounce(ConfirmTime) = *max*
				LFM_debounce(HealTime) = 0
				LFM_debounce(Input) = TRUE
				Rte_CpApLFM_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError = 1
				Rte_CpApLFM_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage = 255
				&target_LFM_debounce_Debounce {
					State = FALSE
					Counter = 0
				}
				&target_LFM_OutputReport_RestartCounter = *max*
			}
			$calltrace {
				LFM_debounce
				LFM_GetFaultLevel
				LFM_OutputReport
			}
		}
	}

	$testcase 6 {
		$name ""
		$uuid "390b774f-4b01-4ef7-992f-627104a3d30d"

		$teststep 6.1 {
			$name ""
			$uuid "85330824-6f59-41a0-89ab-f60d4fbeb18e"
			$inputs {
				LFM_DCLVFaultTimeToRetryDefault = *max*
				LFM_GetFaultLevel() = *max*
				LFM_OutputReport() = *max*
				LFM_OutputReport(RestartCounter) = target_LFM_OutputReport_RestartCounter
				LFM_RestartCounter[0] = *max*
				LFM_RestartCounter[1] = *max*
				LFM_RestartCounter[2] = *max*
				LFM_RestartCounter[3] = *max*
				LFM_RestartCounter[4] = *max*
				LFM_RestartCounter[5] = *max*
				LFM_RestartCounter[6] = *max*
				LFM_RestartCounter[7] = *max*
				LFM_RestartCounter[8] = *max*
				LFM_RestartCounter[9] = *max*
				LFM_RestartCounter[10] = *max*
				LFM_RestartCounter[11] = *max*
				LFM_TimeBatteryVoltageSafety = *max*
				LFM_debounce(Debounce) = target_LFM_debounce_Debounce
				Rte_CpApPLS_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error = 4
				Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt = 100
				FaultIndex = 20
				OVlimit1 = *max*
				OVlimit2 = *max*
			}
			$outputs {
				LFM_FaultVar[0] = 0
				LFM_GetFaultLevel(firstLevel) = 0
				LFM_GetFaultLevel(secondLevel) = 0
				LFM_GetFaultLevel(thirdlevel) = FALSE
				LFM_OutputReport(ErrorLevel) = 255
				LFM_OutputReport(RestartTime) = 4294967295
				LFM_debounce(ConfirmTime) = 65535
				LFM_debounce(HealTime) = 0
				LFM_debounce(Input) = 1
				Rte_CpApLFM_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError = 1
				Rte_CpApLFM_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage = 255
				&target_LFM_debounce_Debounce {
					State = FALSE
					Counter = 0
				}
				&target_LFM_OutputReport_RestartCounter = 0
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 7 {
		$name ""
		$uuid "d2e896c5-7d03-4f7f-9246-dd7dbf4b6a3c"

		$teststep 7.1 {
			$name ""
			$uuid "21338704-334e-43bf-9769-13268fb37c76"
			$inputs {
				LFM_DCLVFaultTimeToRetryDefault = 20
				LFM_GetFaultLevel() = 0
				LFM_OutputReport() = 106
				LFM_OutputReport(RestartCounter) = target_LFM_OutputReport_RestartCounter
				LFM_RestartCounter[0] = 1
				LFM_RestartCounter[1] = 2
				LFM_RestartCounter[2] = 3
				LFM_RestartCounter[3] = 4
				LFM_RestartCounter[4] = 5
				LFM_RestartCounter[5] = 6
				LFM_RestartCounter[6] = 7
				LFM_RestartCounter[7] = 8
				LFM_RestartCounter[8] = 9
				LFM_RestartCounter[9] = 10
				LFM_RestartCounter[10] = 11
				LFM_RestartCounter[11] = 12
				LFM_TimeBatteryVoltageSafety = 10
				LFM_debounce(Debounce) = target_LFM_debounce_Debounce
				Rte_CpApPLS_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error = 0
				Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt = 30
				FaultIndex = 5
				OVlimit1 = FAULT_LEVEL_NO_FAULT
				OVlimit2 = FAULT_LEVEL_NO_FAULT
			}
			$outputs {
				LFM_FaultVar[0] = 0
				LFM_GetFaultLevel(firstLevel) = 0
				LFM_GetFaultLevel(secondLevel) = 0
				LFM_GetFaultLevel(thirdlevel) = FALSE
				LFM_OutputReport(ErrorLevel) = 0
				LFM_OutputReport(RestartTime) = 20
				LFM_debounce(ConfirmTime) = 10
				LFM_debounce(HealTime) = 0
				LFM_debounce(Input) = 0
				Rte_CpApLFM_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError = 0
				Rte_CpApLFM_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage = 0
				&target_LFM_debounce_Debounce {
					State = FALSE
					Counter = 0
				}
				&target_LFM_OutputReport_RestartCounter = 6
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 8 {
		$name ""
		$uuid "a0f18536-4cbf-4b9f-bb68-bb6de134eb18"

		$teststep 8.1 {
			$name ""
			$uuid "851956e1-ad4a-40e7-8013-e360ac627fb6"
			$inputs {
				LFM_DCLVFaultTimeToRetryDefault = *max*
				LFM_GetFaultLevel() = 0
				LFM_OutputReport() = *max*
				LFM_OutputReport(RestartCounter) = target_LFM_OutputReport_RestartCounter
				LFM_RestartCounter[0] = *max*
				LFM_RestartCounter[1] = *max*
				LFM_RestartCounter[2] = *max*
				LFM_RestartCounter[3] = *max*
				LFM_RestartCounter[4] = *max*
				LFM_RestartCounter[5] = *max*
				LFM_RestartCounter[6] = *max*
				LFM_RestartCounter[7] = *max*
				LFM_RestartCounter[8] = *max*
				LFM_RestartCounter[9] = *max*
				LFM_RestartCounter[10] = *max*
				LFM_RestartCounter[11] = *max*
				LFM_TimeBatteryVoltageSafety = *max*
				LFM_debounce(Debounce) = target_LFM_debounce_Debounce
				Rte_CpApPLS_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error = 3
				Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt = 41
				FaultIndex = 5
				OVlimit1 = *max*
				OVlimit2 = *max*
			}
			$outputs {
				LFM_FaultVar[0] = 0
				LFM_GetFaultLevel(firstLevel) = 0
				LFM_GetFaultLevel(secondLevel) = 0
				LFM_GetFaultLevel(thirdlevel) = FALSE
				LFM_OutputReport(ErrorLevel) = 0
				LFM_OutputReport(RestartTime) = 4294967295
				LFM_debounce(ConfirmTime) = 65535
				LFM_debounce(HealTime) = 0
				LFM_debounce(Input) = 1
				Rte_CpApLFM_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError = 1
				Rte_CpApLFM_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage = 255
				&target_LFM_debounce_Debounce {
					State = FALSE
					Counter = 0
				}
				&target_LFM_OutputReport_RestartCounter = 4294967295
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 9 {
		$name ""
		$uuid "e8946676-fc89-45d4-a8d1-ecbe91d3eca6"

		$teststep 9.1 {
			$name ""
			$uuid "e6f54786-b7a1-4902-9174-32534fa4a858"
			$inputs {
				LFM_DCLVFaultTimeToRetryDefault = 20
				LFM_GetFaultLevel() = 0
				LFM_OutputReport() = 106
				LFM_OutputReport(RestartCounter) = target_LFM_OutputReport_RestartCounter
				LFM_RestartCounter[0] = 1
				LFM_RestartCounter[1] = 2
				LFM_RestartCounter[2] = 3
				LFM_RestartCounter[3] = 4
				LFM_RestartCounter[4] = 5
				LFM_RestartCounter[5] = 6
				LFM_RestartCounter[6] = 7
				LFM_RestartCounter[7] = 8
				LFM_RestartCounter[8] = 9
				LFM_RestartCounter[9] = 10
				LFM_RestartCounter[10] = 11
				LFM_RestartCounter[11] = 12
				LFM_TimeBatteryVoltageSafety = 10
				LFM_debounce(Debounce) = target_LFM_debounce_Debounce
				Rte_CpApPLS_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error = 2
				Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt = 30
				FaultIndex = 5
				OVlimit1 = FAULT_LEVEL_NO_FAULT
				OVlimit2 = FAULT_LEVEL_NO_FAULT
			}
			$outputs {
				LFM_FaultVar[0] = 0
				LFM_GetFaultLevel(firstLevel) = 0
				LFM_GetFaultLevel(secondLevel) = 0
				LFM_GetFaultLevel(thirdlevel) = FALSE
				LFM_OutputReport(ErrorLevel) = 0
				LFM_OutputReport(RestartTime) = 20
				LFM_debounce(ConfirmTime) = 10
				LFM_debounce(HealTime) = 0
				LFM_debounce(Input) = 0
				Rte_CpApLFM_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError = 1
				Rte_CpApLFM_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage = 255
				&target_LFM_debounce_Debounce {
					State = FALSE
					Counter = 0
				}
				&target_LFM_OutputReport_RestartCounter = 6
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}
}