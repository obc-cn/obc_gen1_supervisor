/******************************************************************************
**                                                                           **
** Copyright (C) Infineon Technologies (2014)                                **
**                                                                           **
** All rights reserved.                                                      **
**                                                                           **
** This document contains proprietary information belonging to Infineon      **
** Technologies. Passing on and copying of this document, and communication  **
** of its contents is not permitted without prior written authorization.     **
**                                                                           **
*******************************************************************************
**                                                                           **
**  $FILENAME   : Dio.h $                                                    **
**                                                                           **
**  $CC VERSION : \main\58 $                                                 **
**                                                                           **
**  $DATE       : 2016-04-21 $                                               **
**                                                                           **
**  AUTHOR      : DL-AUTOSAR-Engineering                                     **
**                                                                           **
**  VENDOR      : Infineon Technologies                                      **
**                                                                           **
**  DESCRIPTION : This file is the header for DIO driver                     **
**                                                                           **
**  MAY BE CHANGED BY USER [yes/no]: No                                      **
**                                                                           **
*******************************************************************************
**  Traceabilty : [cover parentID=DS_AS_DIO140,DS_AS_DIO131,
                   DS_AS40X_DIO001_PI,DS_AS40X_DIO065,DS_NAS_DIO_PR912_3,
                   DS_AS40X_DIO073,DS_AS_DIO021,DS_AS_DIO022,DS_AS_DIO184,
                   DS_AS40X_DIO187,DS_AS40X_DIO164,DS_AS_DIO182,DS_AS_DIO015,
                   DS_AS_DIO183,DS_AS_DIO018,DS_AS_DIO185,DS_AS_DIO023,
                   DS_AS_DIO186,DS_AS_DIO024_DIO103,DS_NAS_DIO_PR746,
                   DS_NAS_DIO_PR128,DS_NAS_DIO_PR746,
                   DS_AS4XX_HE2_DIO_PR2934_1,DS_AS4XX_HE2_DIO_PR2934_2,
                   DS_AS4XX_HE2_DIO_PR2934_3,DS_AS4XX_EP_DIO_PR2934_1,
                   DS_AS4XX_EP_DIO_PR2934_2,DS_AS4XX_EP_DIO_PR2934_3,
                   DS_NAS_DIO_PR3054]                                        **
**                 [/cover]                                                  **
******************************************************************************/

#ifndef MCAL_H
#define MCAL_H



#endif /* DIO_H */
