#ifndef IFXPORT_REG_H
#define IFXPORT_REG_H 1

#include "IfxPort_regdef.h"

volatile Ifx_P_OUT p20_out_TS;
#define P20_OUT p20_out_TS

volatile Ifx_P_IOCR4 p20_iocr4_TS;
#define P20_IOCR4 p20_iocr4_TS

extern void TRUSTED_ApplSafetyResetEndInit_Timed(uint32 Time);
extern void TRUSTED_ApplSafetySetEndInit_Timed(void);
/** \brief  Reset Configuration Register */
typedef struct _Ifx_SCU_RSTCON_Bits
{
    unsigned int ESR0:2;                    /**< \brief [1:0] ESR0 Reset Request Trigger Reset Configuration (rw) */
    unsigned int ESR1:2;                    /**< \brief [3:2] ESR1 Reset Request Trigger Reset Configuration (rw) */
    unsigned int reserved_4:2;              /**< \brief \internal Reserved */
    unsigned int SMU:2;                     /**< \brief [7:6] SMU Reset Request Trigger Reset Configuration (rw) */
    unsigned int SW:2;                      /**< \brief [9:8] SW Reset Request Trigger Reset Configuration (rw) */
    unsigned int STM0:2;                    /**< \brief [11:10] STM0 Reset Request Trigger Reset Configuration (rw) */
    unsigned int STM1:2;                    /**< \brief [13:12] STM1 Reset Request Trigger Reset Configuration (If Product has STM1) (rw) */
    unsigned int STM2:2;                    /**< \brief [15:14] STM2 Reset Request Trigger Reset Configuration (If Product has STM2) (rw) */
    unsigned int reserved_16:16;            /**< \brief \internal Reserved */
} Ifx_SCU_RSTCON_Bits;

/** \brief  Reset Configuration Register */
typedef union
{
    unsigned int U;                         /**< \brief Unsigned access */
    signed int I;                           /**< \brief Signed access */
    Ifx_SCU_RSTCON_Bits B;                  /**< \brief Bitfield access */
} Ifx_SCU_RSTCON;

static volatile Ifx_SCU_RSTCON SCU_RSTCON; 

#define MCU_RUNTIME_RESETSAFETYENDINIT_TIMED(Time) \
                                  (TRUSTED_ApplSafetyResetEndInit_Timed((Time)))
#define MCU_RUNTIME_SETSAFETYENDINIT_TIMED() \
                                          (TRUSTED_ApplSafetySetEndInit_Timed())
                                          
/* Maximum timeout value to acquire the Safety endinit */
#define MCU_SAFETY_ENDINT_TIMEOUT ((uint32)150000U)

/*IFX_MISRA_RULE_19_07_STATUS=To support user modification of OS protected
  calls definition, it is declared as a function like macro*/
#define MCU_SFR_RUNTIME_RESETSAFETYENDINIT_TIMED(Time) \
                                  (MCU_RUNTIME_RESETSAFETYENDINIT_TIMED(Time))

                                  /*IFX_MISRA_RULE_19_07_STATUS=To support user modification of OS protected
  calls definition, it is declared as a function like macro*/
#define MCU_SFR_RUNTIME_SETSAFETYENDINIT_TIMED() \
                                        (MCU_RUNTIME_SETSAFETYENDINIT_TIMED())


#endif