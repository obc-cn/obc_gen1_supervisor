$testobject {

	$testcase 1 {
		$name "First WakeupRequest"
		$uuid "ea4674c1-ff33-4d47-b98c-6f69b2a72046"
		$specification """
			+ IN
			  + PARAMETERS
			    - LVC_WakeUp:TRUE
			    - LVC_state DCLV_DCLVStatus_CANSignal:LVC_STATE_SHUTDOWN 0x06
			    - LVC_stateRequest SUP_RequestDCLVStatus_CANSignal:Cx4_DCLV_STATE_SHUTDOWN 0x04
			  + GLOBALS
			    - LVC_ConnectionAllowed#1:FALSE
			    - LVC_PowerSupply#1:FALSE
			    - LVC_WakeUp_previous#1:FALSE
			    - LVC_StateReported#1:Cx0_off_mode
			+ OUT
			  + GLOBALS
			    - LVC_ConnectionAllowed#1:FALSE
			    - LVC_PowerSupply#1:TRUE
			    - LVC_WakeUp_previous#1:TRUE
			    - LVC_StateReported#1:Cx1_Init_mode
			  + DFA SETS
			    - SET_LVC_BOOLInt_DCDC_HighVoltConnectionAllowed:FALSE
			    - SET_LVC_BOOLOutputEnableVCCDCLVPhysicalValue:TRUE
			    - SET_LVC_U8Int_DCDC_Status:Cx1_Init_mode
		"""
		$description """
			Wakeup from false previous wakeup
		"""

		$teststep 1.1 {
			$name ""
			$uuid "cc621e0a-b8eb-4335-b1e6-cf5da5b671f3"
			$inputs {
				LVC_StateReport::LVC_ConnectionAllowed#1 = FALSE
				LVC_StateReport::LVC_PowerSupply#1 = FALSE
				LVC_StateReport::LVC_StateReported#1 = Cx0_off_mode
				LVC_StateReport::LVC_WakeUp_previous#1 = FALSE
				LVC_WakeUp = TRUE
				LVC_state = LVC_STATE_SHUTDOWN
				LVC_stateRequest = Cx4_DCLV_STATE_SHUTDOWN
			}
			$outputs {
				LVC_StateReport::LVC_ConnectionAllowed#1 = FALSE
				LVC_StateReport::LVC_PowerSupply#1 = TRUE
				LVC_StateReport::LVC_StateReported#1 = Cx1_Init_mode
				LVC_StateReport::LVC_WakeUp_previous#1 = TRUE
				Rte_CpApLVC_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed = FALSE
				Rte_CpApLVC_PpInt_DCDC_Status_DCDC_Status = Cx1_Init_mode
				Rte_CpApLVC_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue = TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 2 {
		$name "2nd WakeupRequest - SHUTDOWN allowed"
		$uuid "fabf1e7c-1cb9-4c71-a687-b51931aa110c"
		$specification """
			+ IN
			  + PARAMETERS
			    - LVC_WakeUp:TRUE
			    - LVC_state DCLV_DCLVStatus_CANSignal:LVC_STATE_SHUTDOWN 0x06
			    - LVC_stateRequest SUP_RequestDCLVStatus_CANSignal:Cx4_DCLV_STATE_SHUTDOWN 0x04
			  + GLOBALS
			    - LVC_ConnectionAllowed#1:FALSE
			    - LVC_PowerSupply#1:FALSE
			    - LVC_WakeUp_previous#1:TRUE
			    - LVC_StateReported#1:Cx0_off_mode
			+ OUT
			  + GLOBALS
			    - LVC_ConnectionAllowed#1:FALSE
			    - LVC_PowerSupply#1:FALSE
			    - LVC_WakeUp_previous#1:TRUE
			    - LVC_StateReported#1:Cx0_off_mode
			  + DFA SETS
			    - SET_LVC_BOOLInt_DCDC_HighVoltConnectionAllowed:FALSE
			    - SET_LVC_BOOLOutputEnableVCCDCLVPhysicalValue:FALSE
			    - SET_LVC_U8Int_DCDC_Status:Cx0_off_mode
		"""
		$description """
			After first wakeup, SHUTDOWN conitions are allowed
		"""

		$teststep 2.1 {
			$name ""
			$uuid "dcd400e8-468c-400d-b72d-c2267f88af28"
			$inputs {
				LVC_StateReport::LVC_ConnectionAllowed#1 = FALSE
				LVC_StateReport::LVC_PowerSupply#1 = FALSE
				LVC_StateReport::LVC_StateReported#1 = Cx0_off_mode
				LVC_StateReport::LVC_WakeUp_previous#1 = TRUE
				LVC_WakeUp = TRUE
				LVC_state = LVC_STATE_SHUTDOWN
				LVC_stateRequest = Cx4_DCLV_STATE_SHUTDOWN
			}
			$outputs {
				LVC_StateReport::LVC_ConnectionAllowed#1 = FALSE
				LVC_StateReport::LVC_PowerSupply#1 = FALSE
				LVC_StateReport::LVC_StateReported#1 = Cx0_off_mode
				LVC_StateReport::LVC_WakeUp_previous#1 = TRUE
				Rte_CpApLVC_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed = FALSE
				Rte_CpApLVC_PpInt_DCDC_Status_DCDC_Status = Cx0_off_mode
				Rte_CpApLVC_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 3 {
		$name "2nd WakeupRequest - STANDBY allowed"
		$uuid "ac11af38-e524-4d36-8492-e5c5d0b80805"
		$specification """
			+ IN
			  + PARAMETERS
			    - LVC_WakeUp:TRUE
			    - LVC_state DCLV_DCLVStatus_CANSignal:Cx0_STANDBY 0x00
			    - LVC_stateRequest SUP_RequestDCLVStatus_CANSignal:Cx0_DCLV_STATE_STANDBY 0x00
			  + GLOBALS
			    - LVC_ConnectionAllowed#1:FALSE
			    - LVC_PowerSupply#1:FALSE
			    - LVC_WakeUp_previous#1:TRUE
			    - LVC_StateReported#1:Cx0_off_mode
			+ OUT
			  + GLOBALS
			    - LVC_ConnectionAllowed#1:TRUE
			    - LVC_PowerSupply#1:FALSE
			    - LVC_WakeUp_previous#1:TRUE
			    - LVC_StateReported#1:Cx2_standby_mode
			  + DFA SETS
			    - SET_LVC_BOOLInt_DCDC_HighVoltConnectionAllowed:TRUE
			    - SET_LVC_BOOLOutputEnableVCCDCLVPhysicalValue:FALSE
			    - SET_LVC_U8Int_DCDC_Status:Cx2_standby_mode
		"""
		$description """
			After first wakeup, STANDBY conitions are allowed
		"""

		$teststep 3.1 {
			$name ""
			$uuid "1f6227a0-334a-484e-8142-3d278c2ce9a8"
			$inputs {
				LVC_StateReport::LVC_ConnectionAllowed#1 = FALSE
				LVC_StateReport::LVC_PowerSupply#1 = FALSE
				LVC_StateReport::LVC_StateReported#1 = Cx0_off_mode
				LVC_StateReport::LVC_WakeUp_previous#1 = TRUE
				LVC_WakeUp = TRUE
				LVC_state = Cx0_STANDBY
				LVC_stateRequest = Cx0_DCLV_STATE_STANDBY
			}
			$outputs {
				LVC_StateReport::LVC_ConnectionAllowed#1 = TRUE
				LVC_StateReport::LVC_PowerSupply#1 = FALSE
				LVC_StateReport::LVC_StateReported#1 = Cx2_standby_mode
				LVC_StateReport::LVC_WakeUp_previous#1 = TRUE
				Rte_CpApLVC_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed = TRUE
				Rte_CpApLVC_PpInt_DCDC_Status_DCDC_Status = Cx2_standby_mode
				Rte_CpApLVC_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 4 {
		$name "2nd WakeupRequest - RUN allowed"
		$uuid "d307fbed-a340-4720-8d0d-17bec4557ee4"
		$specification """
			+ IN
			  + PARAMETERS
			    - LVC_WakeUp:TRUE
			    - LVC_state DCLV_DCLVStatus_CANSignal:Cx1_RUN 0x01
			    - LVC_stateRequest SUP_RequestDCLVStatus_CANSignal:Cx1_DCLV_STATE_RUN 0x01
			  + GLOBALS
			    - LVC_ConnectionAllowed#1:FALSE
			    - LVC_PowerSupply#1:FALSE
			    - LVC_WakeUp_previous#1:TRUE
			    - LVC_StateReported#1:Cx0_off_mode
			+ OUT
			  + GLOBALS
			    - LVC_ConnectionAllowed#1:FALSE
			    - LVC_PowerSupply#1:FALSE
			    - LVC_WakeUp_previous#1:TRUE
			    - LVC_StateReported#1:Cx3_conversion_working_
			  + DFA SETS
			    - SET_LVC_BOOLInt_DCDC_HighVoltConnectionAllowed:FALSE
			    - SET_LVC_BOOLOutputEnableVCCDCLVPhysicalValue:FALSE
			    - SET_LVC_U8Int_DCDC_Status:Cx3_conversion_working_
		"""
		$description """
			After first wakeup, RUN conitions are allowed
		"""

		$teststep 4.1 {
			$name ""
			$uuid "e37dcd9a-8d77-4ea4-a921-9cadfdb5bada"
			$inputs {
				LVC_StateReport::LVC_ConnectionAllowed#1 = FALSE
				LVC_StateReport::LVC_PowerSupply#1 = FALSE
				LVC_StateReport::LVC_StateReported#1 = Cx0_off_mode
				LVC_StateReport::LVC_WakeUp_previous#1 = TRUE
				LVC_WakeUp = TRUE
				LVC_state = Cx1_RUN
				LVC_stateRequest = Cx1_DCLV_STATE_RUN
			}
			$outputs {
				LVC_StateReport::LVC_ConnectionAllowed#1 = FALSE
				LVC_StateReport::LVC_PowerSupply#1 = FALSE
				LVC_StateReport::LVC_StateReported#1 = Cx3_conversion_working_
				LVC_StateReport::LVC_WakeUp_previous#1 = TRUE
				Rte_CpApLVC_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed = FALSE
				Rte_CpApLVC_PpInt_DCDC_Status_DCDC_Status = Cx3_conversion_working_
				Rte_CpApLVC_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 5 {
		$name "2nd WakeupRequest - SHUTDOWN not allowed due to Status"
		$uuid "4e7b3045-1cba-410e-b890-68ae009258bf"
		$specification """
			+ IN
			  + PARAMETERS
			    - LVC_WakeUp:TRUE
			    - LVC_state DCLV_DCLVStatus_CANSignal:Max: 255
			    - LVC_stateRequest SUP_RequestDCLVStatus_CANSignal:Cx1_DCLV_STATE_RUN 0x01
			  + GLOBALS
			    - LVC_ConnectionAllowed#1:FALSE
			    - LVC_PowerSupply#1:FALSE
			    - LVC_WakeUp_previous#1:TRUE
			    - LVC_StateReported#1:Cx0_off_mode
			+ OUT
			  + GLOBALS
			    - LVC_ConnectionAllowed#1:FALSE
			    - LVC_PowerSupply#1:FALSE
			    - LVC_WakeUp_previous#1:TRUE
			    - LVC_StateReported#1:Cx0_off_mode
			  + DFA SETS
			    - SET_LVC_BOOLInt_DCDC_HighVoltConnectionAllowed:FALSE
			    - SET_LVC_BOOLOutputEnableVCCDCLVPhysicalValue:FALSE
			    - SET_LVC_U8Int_DCDC_Status:Cx0_off_mode
		"""
		$description """
			After first wakeup, SHUTDOWN is not allowed due to Status condition
		"""

		$teststep 5.1 {
			$name ""
			$uuid "dd6aea6d-d343-40cf-a5a2-e734746e497e"
			$inputs {
				LVC_StateReport::LVC_ConnectionAllowed#1 = FALSE
				LVC_StateReport::LVC_PowerSupply#1 = FALSE
				LVC_StateReport::LVC_StateReported#1 = Cx0_off_mode
				LVC_StateReport::LVC_WakeUp_previous#1 = TRUE
				LVC_WakeUp = TRUE
				LVC_state = *max*
				LVC_stateRequest = Cx1_DCLV_STATE_RUN
			}
			$outputs {
				LVC_StateReport::LVC_ConnectionAllowed#1 = FALSE
				LVC_StateReport::LVC_PowerSupply#1 = FALSE
				LVC_StateReport::LVC_StateReported#1 = Cx0_off_mode
				LVC_StateReport::LVC_WakeUp_previous#1 = TRUE
				Rte_CpApLVC_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed = FALSE
				Rte_CpApLVC_PpInt_DCDC_Status_DCDC_Status = Cx0_off_mode
				Rte_CpApLVC_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 6 {
		$name "2nd WakeupRequest - RUN not allowed due to Status"
		$uuid "2c88c48f-26fa-4f16-9d0f-47c72e669c20"
		$specification """
			+ IN
			  + PARAMETERS
			    - LVC_WakeUp:TRUE
			    - LVC_state DCLV_DCLVStatus_CANSignal:Max: 255
			    - LVC_stateRequest SUP_RequestDCLVStatus_CANSignal:Cx0_DCLV_STATE_STANDBY 0x00
			  + GLOBALS
			    - LVC_ConnectionAllowed#1:FALSE
			    - LVC_PowerSupply#1:FALSE
			    - LVC_WakeUp_previous#1:TRUE
			    - LVC_StateReported#1:Cx0_off_mode
			+ OUT
			  + GLOBALS
			    - LVC_ConnectionAllowed#1:FALSE
			    - LVC_PowerSupply#1:FALSE
			    - LVC_WakeUp_previous#1:TRUE
			    - LVC_StateReported#1:Cx0_off_mode
			  + DFA SETS
			    - SET_LVC_BOOLInt_DCDC_HighVoltConnectionAllowed:FALSE
			    - SET_LVC_BOOLOutputEnableVCCDCLVPhysicalValue:FALSE
			    - SET_LVC_U8Int_DCDC_Status:Cx0_off_mode
		"""
		$description """
			After first wakeup, RUN is not allowed due to Status condition
		"""

		$teststep 6.1 {
			$name ""
			$uuid "a092fcea-a044-46c4-aadf-f06c299b50d8"
			$inputs {
				LVC_StateReport::LVC_ConnectionAllowed#1 = FALSE
				LVC_StateReport::LVC_PowerSupply#1 = FALSE
				LVC_StateReport::LVC_StateReported#1 = Cx0_off_mode
				LVC_StateReport::LVC_WakeUp_previous#1 = TRUE
				LVC_WakeUp = TRUE
				LVC_state = *max*
				LVC_stateRequest = Cx0_DCLV_STATE_STANDBY
			}
			$outputs {
				LVC_StateReport::LVC_ConnectionAllowed#1 = FALSE
				LVC_StateReport::LVC_PowerSupply#1 = FALSE
				LVC_StateReport::LVC_StateReported#1 = Cx0_off_mode
				LVC_StateReport::LVC_WakeUp_previous#1 = TRUE
				Rte_CpApLVC_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed = FALSE
				Rte_CpApLVC_PpInt_DCDC_Status_DCDC_Status = Cx0_off_mode
				Rte_CpApLVC_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 7 {
		$name "2nd WakeupRequest - STANDBY not allowed due to Status"
		$uuid "42330104-ab79-4cb2-86e5-c396e15e6b72"
		$specification """
			+ IN
			  + PARAMETERS
			    - LVC_WakeUp:TRUE
			    - LVC_state DCLV_DCLVStatus_CANSignal:Max: 255
			    - LVC_stateRequest SUP_RequestDCLVStatus_CANSignal:Cx4_DCLV_STATE_SHUTDOWN 0x04
			  + GLOBALS
			    - LVC_ConnectionAllowed#1:FALSE
			    - LVC_PowerSupply#1:FALSE
			    - LVC_WakeUp_previous#1:TRUE
			    - LVC_StateReported#1:Cx0_off_mode
			+ OUT
			  + GLOBALS
			    - LVC_ConnectionAllowed#1:FALSE
			    - LVC_PowerSupply#1:FALSE
			    - LVC_WakeUp_previous#1:TRUE
			    - LVC_StateReported#1:Cx0_off_mode
			  + DFA SETS
			    - SET_LVC_BOOLInt_DCDC_HighVoltConnectionAllowed:FALSE
			    - SET_LVC_BOOLOutputEnableVCCDCLVPhysicalValue:FALSE
			    - SET_LVC_U8Int_DCDC_Status:Cx0_off_mode
		"""
		$description """
			After first wakeup, STANDBY is not allowed due to Status condition
		"""

		$teststep 7.1 {
			$name ""
			$uuid "34360032-12b7-487d-95c5-846a0fb1f009"
			$inputs {
				LVC_StateReport::LVC_ConnectionAllowed#1 = FALSE
				LVC_StateReport::LVC_PowerSupply#1 = FALSE
				LVC_StateReport::LVC_StateReported#1 = Cx0_off_mode
				LVC_StateReport::LVC_WakeUp_previous#1 = TRUE
				LVC_WakeUp = TRUE
				LVC_state = *max*
				LVC_stateRequest = Cx4_DCLV_STATE_SHUTDOWN
			}
			$outputs {
				LVC_StateReport::LVC_ConnectionAllowed#1 = FALSE
				LVC_StateReport::LVC_PowerSupply#1 = FALSE
				LVC_StateReport::LVC_StateReported#1 = Cx0_off_mode
				LVC_StateReport::LVC_WakeUp_previous#1 = TRUE
				Rte_CpApLVC_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed = FALSE
				Rte_CpApLVC_PpInt_DCDC_Status_DCDC_Status = Cx0_off_mode
				Rte_CpApLVC_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 8 {
		$name "2nd WakeupRequest - SHUTDOWN not allowed due to Request"
		$uuid "a098f9c5-a552-4f33-9294-aa5e3d61070a"
		$specification """
			+ IN
			  + PARAMETERS
			    - LVC_WakeUp:TRUE
			    - LVC_state DCLV_DCLVStatus_CANSignal:LVC_STATE_SHUTDOWN 0x06
			    - LVC_stateRequest SUP_RequestDCLVStatus_CANSignal:Not Def: 200
			  + GLOBALS
			    - LVC_ConnectionAllowed#1:FALSE
			    - LVC_PowerSupply#1:FALSE
			    - LVC_WakeUp_previous#1:TRUE
			    - LVC_StateReported#1:Cx0_off_mode
			+ OUT
			  + GLOBALS
			    - LVC_ConnectionAllowed#1:FALSE
			    - LVC_PowerSupply#1:FALSE
			    - LVC_WakeUp_previous#1:TRUE
			    - LVC_StateReported#1:Cx0_off_mode
			  + DFA SETS
			    - SET_LVC_BOOLInt_DCDC_HighVoltConnectionAllowed:FALSE
			    - SET_LVC_BOOLOutputEnableVCCDCLVPhysicalValue:FALSE
			    - SET_LVC_U8Int_DCDC_Status:Cx0_off_mode
		"""
		$description """
			After first wakeup, SHUTDOWN is not allowed due to RequestStatus condition
		"""

		$teststep 8.1 {
			$name ""
			$uuid "49b1db79-ac4a-4c28-bd9e-8158c4adbafd"
			$inputs {
				LVC_StateReport::LVC_ConnectionAllowed#1 = FALSE
				LVC_StateReport::LVC_PowerSupply#1 = FALSE
				LVC_StateReport::LVC_StateReported#1 = Cx0_off_mode
				LVC_StateReport::LVC_WakeUp_previous#1 = TRUE
				LVC_WakeUp = TRUE
				LVC_state = LVC_STATE_SHUTDOWN
				LVC_stateRequest = 200
			}
			$outputs {
				LVC_StateReport::LVC_ConnectionAllowed#1 = FALSE
				LVC_StateReport::LVC_PowerSupply#1 = FALSE
				LVC_StateReport::LVC_StateReported#1 = Cx0_off_mode
				LVC_StateReport::LVC_WakeUp_previous#1 = TRUE
				Rte_CpApLVC_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed = FALSE
				Rte_CpApLVC_PpInt_DCDC_Status_DCDC_Status = Cx0_off_mode
				Rte_CpApLVC_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 9 {
		$name "2nd WakeupRequest - RUN not allowed due to Request"
		$uuid "0179c1dd-9fed-4e3d-9e87-54d315611fb6"
		$specification """
			+ IN
			  + PARAMETERS
			    - LVC_WakeUp:TRUE
			    - LVC_state DCLV_DCLVStatus_CANSignal:Cx1_RUN 0x01
			    - LVC_stateRequest SUP_RequestDCLVStatus_CANSignal:Not Def: 200
			  + GLOBALS
			    - LVC_ConnectionAllowed#1:FALSE
			    - LVC_PowerSupply#1:FALSE
			    - LVC_WakeUp_previous#1:TRUE
			    - LVC_StateReported#1:Cx0_off_mode
			+ OUT
			  + GLOBALS
			    - LVC_ConnectionAllowed#1:FALSE
			    - LVC_PowerSupply#1:FALSE
			    - LVC_WakeUp_previous#1:TRUE
			    - LVC_StateReported#1:Cx0_off_mode
			  + DFA SETS
			    - SET_LVC_BOOLInt_DCDC_HighVoltConnectionAllowed:FALSE
			    - SET_LVC_BOOLOutputEnableVCCDCLVPhysicalValue:FALSE
			    - SET_LVC_U8Int_DCDC_Status:Cx0_off_mode
		"""
		$description """
			After first wakeup, RUN is not allowed due to RequestStatus condition
		"""

		$teststep 9.1 {
			$name ""
			$uuid "78ff1a7b-9aef-4993-92dc-5550977dbeef"
			$inputs {
				LVC_StateReport::LVC_ConnectionAllowed#1 = FALSE
				LVC_StateReport::LVC_PowerSupply#1 = FALSE
				LVC_StateReport::LVC_StateReported#1 = Cx0_off_mode
				LVC_StateReport::LVC_WakeUp_previous#1 = TRUE
				LVC_WakeUp = TRUE
				LVC_state = Cx1_RUN
				LVC_stateRequest = 200
			}
			$outputs {
				LVC_StateReport::LVC_ConnectionAllowed#1 = FALSE
				LVC_StateReport::LVC_PowerSupply#1 = FALSE
				LVC_StateReport::LVC_StateReported#1 = Cx0_off_mode
				LVC_StateReport::LVC_WakeUp_previous#1 = TRUE
				Rte_CpApLVC_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed = FALSE
				Rte_CpApLVC_PpInt_DCDC_Status_DCDC_Status = Cx0_off_mode
				Rte_CpApLVC_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 10 {
		$name "2nd WakeupRequest - STANDBY not allowed due to Request"
		$uuid "609f9248-0d3c-44cf-b8f2-7384e78eeee4"
		$specification """
			+ IN
			  + PARAMETERS
			    - LVC_WakeUp:TRUE
			    - LVC_state DCLV_DCLVStatus_CANSignal:Cx0_STANDBY 0x00
			    - LVC_stateRequest SUP_RequestDCLVStatus_CANSignal:Not Def: 200
			  + GLOBALS
			    - LVC_ConnectionAllowed#1:FALSE
			    - LVC_PowerSupply#1:FALSE
			    - LVC_WakeUp_previous#1:TRUE
			    - LVC_StateReported#1:Cx0_off_mode
			+ OUT
			  + GLOBALS
			    - LVC_ConnectionAllowed#1:FALSE
			    - LVC_PowerSupply#1:FALSE
			    - LVC_WakeUp_previous#1:TRUE
			    - LVC_StateReported#1:Cx0_off_mode
			  + DFA SETS
			    - SET_LVC_BOOLInt_DCDC_HighVoltConnectionAllowed:FALSE
			    - SET_LVC_BOOLOutputEnableVCCDCLVPhysicalValue:FALSE
			    - SET_LVC_U8Int_DCDC_Status:Cx0_off_mode
		"""
		$description """
			After first wakeup, STANDBY is not allowed due to RequestStatus condition
		"""

		$teststep 10.1 {
			$name ""
			$uuid "c25f913f-d293-411a-a936-e2a862c1a7e6"
			$inputs {
				LVC_StateReport::LVC_ConnectionAllowed#1 = FALSE
				LVC_StateReport::LVC_PowerSupply#1 = FALSE
				LVC_StateReport::LVC_StateReported#1 = Cx0_off_mode
				LVC_StateReport::LVC_WakeUp_previous#1 = TRUE
				LVC_WakeUp = TRUE
				LVC_state = Cx0_STANDBY
				LVC_stateRequest = 200
			}
			$outputs {
				LVC_StateReport::LVC_ConnectionAllowed#1 = FALSE
				LVC_StateReport::LVC_PowerSupply#1 = FALSE
				LVC_StateReport::LVC_StateReported#1 = Cx0_off_mode
				LVC_StateReport::LVC_WakeUp_previous#1 = TRUE
				Rte_CpApLVC_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed = FALSE
				Rte_CpApLVC_PpInt_DCDC_Status_DCDC_Status = Cx0_off_mode
				Rte_CpApLVC_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 11 {
		$name "No Conditions Allowed"
		$uuid "38e0c69e-ee91-4e08-9402-d45f3e5a5572"
		$specification """
			+ IN
			  + PARAMETERS
			    - LVC_WakeUp:FALSE
			    - LVC_state DCLV_DCLVStatus_CANSignal:Max: 255
			    - LVC_stateRequest SUP_RequestDCLVStatus_CANSignal:Cx1_DCLV_STATE_RUN 0x01
			  + GLOBALS
			    - LVC_ConnectionAllowed#1:FALSE
			    - LVC_PowerSupply#1:FALSE
			    - LVC_WakeUp_previous#1:TRUE
			    - LVC_StateReported#1:Cx0_off_mode
			+ OUT
			  + GLOBALS
			    - LVC_ConnectionAllowed#1:FALSE
			    - LVC_PowerSupply#1:FALSE
			    - LVC_WakeUp_previous#1:FALSE
			    - LVC_StateReported#1:Cx0_off_mode
			  + DFA SETS
			    - SET_LVC_BOOLInt_DCDC_HighVoltConnectionAllowed:FALSE
			    - SET_LVC_BOOLOutputEnableVCCDCLVPhysicalValue:FALSE
			    - SET_LVC_U8Int_DCDC_Status:Cx0_off_mode
		"""
		$description """
			Initial state and others states are not allowed so Output State is STANDBY
		"""

		$teststep 11.1 {
			$name ""
			$uuid "921ef8de-6684-4f7f-9d9a-e03baaccb56c"
			$inputs {
				LVC_StateReport::LVC_ConnectionAllowed#1 = FALSE
				LVC_StateReport::LVC_PowerSupply#1 = FALSE
				LVC_StateReport::LVC_StateReported#1 = Cx0_off_mode
				LVC_StateReport::LVC_WakeUp_previous#1 = TRUE
				LVC_WakeUp = FALSE
				LVC_state = *max*
				LVC_stateRequest = Cx1_DCLV_STATE_RUN
			}
			$outputs {
				LVC_StateReport::LVC_ConnectionAllowed#1 = FALSE
				LVC_StateReport::LVC_PowerSupply#1 = FALSE
				LVC_StateReport::LVC_StateReported#1 = Cx0_off_mode
				LVC_StateReport::LVC_WakeUp_previous#1 = FALSE
				Rte_CpApLVC_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed = FALSE
				Rte_CpApLVC_PpInt_DCDC_Status_DCDC_Status = Cx0_off_mode
				Rte_CpApLVC_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 12 {
		$name "WORKAROUND - 2nd WakeupRequest - RUN allowed by DERATING"
		$uuid "0052b07e-9565-4d82-bb2f-60598debcdac"
		$specification """
			+ IN
			  + PARAMETERS
			    - LVC_WakeUp:TRUE
			    - LVC_state DCLV_DCLVStatus_CANSignal:Cx5_DERATED 0x05
			    - LVC_stateRequest SUP_RequestDCLVStatus_CANSignal:Cx1_DCLV_STATE_RUN 0x01
			  + GLOBALS
			    - LVC_ConnectionAllowed#1:FALSE
			    - LVC_PowerSupply#1:FALSE
			    - LVC_WakeUp_previous#1:TRUE
			    - LVC_StateReported#1:Cx0_off_mode
			+ OUT
			  + GLOBALS
			    - LVC_ConnectionAllowed#1:FALSE
			    - LVC_PowerSupply#1:FALSE
			    - LVC_WakeUp_previous#1:TRUE
			    - LVC_StateReported#1:Cx3_conversion_working_
			  + DFA SETS
			    - SET_LVC_BOOLInt_DCDC_HighVoltConnectionAllowed:FALSE
			    - SET_LVC_BOOLOutputEnableVCCDCLVPhysicalValue:FALSE
			    - SET_LVC_U8Int_DCDC_Status:Cx3_conversion_working_
		"""
		$description """
			+BEHAVIOR FROM WORKAROUND:
			[...]
			LVC:
				Derating state reported as RUN
			[...]
		"""

		$teststep 12.1 {
			$name ""
			$uuid "61d66fbb-d860-4077-ac9d-892cf55168ef"
			$inputs {
				LVC_StateReport::LVC_ConnectionAllowed#1 = FALSE
				LVC_StateReport::LVC_PowerSupply#1 = FALSE
				LVC_StateReport::LVC_StateReported#1 = Cx0_off_mode
				LVC_StateReport::LVC_WakeUp_previous#1 = TRUE
				LVC_WakeUp = TRUE
				LVC_state = Cx5_DERATED
				LVC_stateRequest = Cx1_DCLV_STATE_RUN
			}
			$outputs {
				LVC_StateReport::LVC_ConnectionAllowed#1 = FALSE
				LVC_StateReport::LVC_PowerSupply#1 = FALSE
				LVC_StateReport::LVC_StateReported#1 = Cx3_conversion_working_
				LVC_StateReport::LVC_WakeUp_previous#1 = TRUE
				Rte_CpApLVC_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed = FALSE
				Rte_CpApLVC_PpInt_DCDC_Status_DCDC_Status = Cx3_conversion_working_
				Rte_CpApLVC_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 13 {
		$name "WORKAROUND - 2nd WakeupRequest - RUN not allowed due to Request with DERATING"
		$uuid "391b9731-5999-4d4c-9f95-e3bfef701757"
		$specification """
			+ IN
			  + PARAMETERS
			    - LVC_WakeUp:TRUE
			    - LVC_state DCLV_DCLVStatus_CANSignal:Cx5_DERATED 0x05
			    - LVC_stateRequest SUP_RequestDCLVStatus_CANSignal:Not Def: 200
			  + GLOBALS
			    - LVC_ConnectionAllowed#1:FALSE
			    - LVC_PowerSupply#1:FALSE
			    - LVC_WakeUp_previous#1:TRUE
			    - LVC_StateReported#1:Cx0_off_mode
			+ OUT
			  + GLOBALS
			    - LVC_ConnectionAllowed#1:FALSE
			    - LVC_PowerSupply#1:FALSE
			    - LVC_WakeUp_previous#1:TRUE
			    - LVC_StateReported#1:Cx0_off_mode
			  + DFA SETS
			    - SET_LVC_BOOLInt_DCDC_HighVoltConnectionAllowed:FALSE
			    - SET_LVC_BOOLOutputEnableVCCDCLVPhysicalValue:FALSE
			    - SET_LVC_U8Int_DCDC_Status:Cx0_off_mode
		"""
		$description """
			+BEHAVIOR FROM WORKAROUND:
			[...]
			LVC:
				Derating state reported as RUN
			[...]
		"""

		$teststep 13.1 {
			$name ""
			$uuid "c04b1b3a-0dad-46de-8123-ea99ff02101d"
			$inputs {
				LVC_StateReport::LVC_ConnectionAllowed#1 = FALSE
				LVC_StateReport::LVC_PowerSupply#1 = FALSE
				LVC_StateReport::LVC_StateReported#1 = Cx0_off_mode
				LVC_StateReport::LVC_WakeUp_previous#1 = TRUE
				LVC_WakeUp = TRUE
				LVC_state = Cx5_DERATED
				LVC_stateRequest = 200
			}
			$outputs {
				LVC_StateReport::LVC_ConnectionAllowed#1 = FALSE
				LVC_StateReport::LVC_PowerSupply#1 = FALSE
				LVC_StateReport::LVC_StateReported#1 = Cx0_off_mode
				LVC_StateReport::LVC_WakeUp_previous#1 = TRUE
				Rte_CpApLVC_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed = FALSE
				Rte_CpApLVC_PpInt_DCDC_Status_DCDC_Status = Cx0_off_mode
				Rte_CpApLVC_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}
}