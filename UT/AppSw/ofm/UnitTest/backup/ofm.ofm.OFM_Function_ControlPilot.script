$testobject {

	$testcase 1 {
		$name "Not Allowed Array Position"
		$uuid "ef2925da-bb23-4160-b499-9f6d9ad631a9"
		$specification """
			+ IN
			  + PARAMETERS
			    - ArrayPosition:Max: 255
			  + RTE GETS
			    - Rte_Delayed_OBC_Status:Max: 255
			    - Rte_OBC_PlugVoltDetection:Max: 255
			  + GLOBALS
			    - OFM_FaultVar[26]:Max: 4294967295
			+ OUT
			  + SENDED PARAMETERS
			    - OFM_UpdateFaultState( ):No Called
			  + RTE SETS
			    - Rte_ControlPilot_Error:No Called
		"""
		$description """
			Array number is bigger than array elements
		"""

		$teststep 1.1 {
			$name ""
			$uuid "6e9f4aba-3b69-412a-910b-9a3fd579ecd4"
			$inputs {
				OFM_FaultVar[0] = *max*
				OFM_FaultVar[1] = *max*
				OFM_FaultVar[2] = *max*
				OFM_FaultVar[3] = *max*
				OFM_FaultVar[4] = *max*
				OFM_FaultVar[5] = *max*
				OFM_FaultVar[6] = *max*
				OFM_FaultVar[7] = *max*
				OFM_FaultVar[8] = *max*
				OFM_FaultVar[9] = *max*
				OFM_FaultVar[10] = *max*
				OFM_FaultVar[11] = *max*
				OFM_FaultVar[12] = *max*
				OFM_FaultVar[13] = *max*
				OFM_FaultVar[14] = *max*
				OFM_FaultVar[15] = *max*
				OFM_FaultVar[16] = *max*
				OFM_FaultVar[17] = *max*
				OFM_FaultVar[18] = *max*
				OFM_FaultVar[19] = *max*
				OFM_FaultVar[20] = *max*
				OFM_FaultVar[21] = *max*
				OFM_FaultVar[22] = *max*
				OFM_FaultVar[23] = *max*
				OFM_FaultVar[24] = *max*
				OFM_UpdateFaultState(FaultVar) = *none*
				Rte_CpApCHG_PpInt_OBC_Status_Delayed_OBC_Status = *max*
				Rte_CpApCPT_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection = *max*
				ArrayPosition = *max*
			}
			$outputs {
				OFM_UpdateFaultState(Configuration) {
					Level = *none*
					ReStartTime = *none*
					ConfirmTime = *none*
				}
				OFM_UpdateFaultState(FaultIndex) = *none*
				OFM_UpdateFaultState(Input) = *none*
				Rte_CpApOFM_PimOBC_ControlPilot_Error = *none*
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 2 {
		$name "Conversion & Not FALSE"
		$uuid "784633dd-38e2-4b81-9bcf-36e330e179ee"
		$specification """
			+ IN
			  + PARAMETERS
			    - ArrayPosition:ID: 5
			  + RTE GETS
			    - Rte_Delayed_OBC_Status:Cx3_conversion_working_
			    - Rte_OBC_PlugVoltDetection:Max: 255
			  + GLOBALS
			    - OFM_FaultVar[26]:ID: 1...26
			+ OUT
			  + SENDED PARAMETERS
			    - OFM_UpdateFaultState( ):Called
			      + Configuration
			        - Level:FAULT_LEVEL_1
			        - ReStartTime:0
			        - ConfirmTime:0
			      - FaultIndex:ID: 5
			      - Input:TRUE
			      - *FaultVar:NO NULL
			        + target_OFM_UpdateFaultState_FaultVar
			          - Counter:0
			          - State:FALSE
			  + RTE SETS
			    - Rte_ControlPilot_Error:ID [5] 6
		"""
		$description """
			Condition allowed combination 3
		"""

		$teststep 2.1 {
			$name ""
			$uuid "a3b148ac-5f00-4e4d-b385-c85883be51f6"
			$inputs {
				OFM_FaultVar[0] = 1
				OFM_FaultVar[1] = 2
				OFM_FaultVar[2] = 3
				OFM_FaultVar[3] = 4
				OFM_FaultVar[4] = 5
				OFM_FaultVar[5] = 6
				OFM_FaultVar[6] = 7
				OFM_FaultVar[7] = 8
				OFM_FaultVar[8] = 9
				OFM_FaultVar[9] = 10
				OFM_FaultVar[10] = 11
				OFM_FaultVar[11] = 12
				OFM_FaultVar[12] = 13
				OFM_FaultVar[13] = 14
				OFM_FaultVar[14] = 15
				OFM_FaultVar[15] = 16
				OFM_FaultVar[16] = 17
				OFM_FaultVar[17] = 18
				OFM_FaultVar[18] = 19
				OFM_FaultVar[19] = 20
				OFM_FaultVar[20] = 21
				OFM_FaultVar[21] = 22
				OFM_FaultVar[22] = 23
				OFM_FaultVar[23] = 24
				OFM_FaultVar[24] = 25
				OFM_UpdateFaultState(FaultVar) = target_OFM_UpdateFaultState_FaultVar
				Rte_CpApCHG_PpInt_OBC_Status_Delayed_OBC_Status = Cx3_conversion_working_
				Rte_CpApCPT_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection = *max*
				ArrayPosition = 5
			}
			$outputs {
				OFM_UpdateFaultState(Configuration) {
					Level = FAULT_LEVEL_1
					ReStartTime = 0
					ConfirmTime = 0
				}
				OFM_UpdateFaultState(FaultIndex) = 5
				OFM_UpdateFaultState(Input) = TRUE
				Rte_CpApOFM_PimOBC_ControlPilot_Error = 6
				&target_OFM_UpdateFaultState_FaultVar {
					Counter = 0
					State = FALSE
				}
			}
			$calltrace {
				OFM_UpdateFaultState
			}
		}
	}

	$testcase 3 {
		$name "Degradation & Not FALSE"
		$uuid "d8534a6b-e0e2-463b-ac2d-54b6b2acfd84"
		$specification """
			+ IN
			  + PARAMETERS
			    - ArrayPosition:ID: 5
			  + RTE GETS
			    - Rte_Delayed_OBC_Status:Cx5_degradation_mode
			    - Rte_OBC_PlugVoltDetection:Max: 255
			  + GLOBALS
			    - OFM_FaultVar[26]:Max: 4294967295
			+ OUT
			  + SENDED PARAMETERS
			    - OFM_UpdateFaultState( ):Called
			      + Configuration
			        - Level:FAULT_LEVEL_1
			        - ReStartTime:0
			        - ConfirmTime:0
			      - FaultIndex:ID: 5
			      - Input:TRUE
			      - *FaultVar:NO NULL
			        + target_OFM_UpdateFaultState_FaultVar
			          - Counter:0
			          - State:FALSE
			  + RTE SETS
			    - Rte_ControlPilot_Error:Max: 4294967295
		"""
		$description """
			Condition allowed combination 6
		"""

		$teststep 3.1 {
			$name ""
			$uuid "f666f366-e391-4162-b711-bca1b674769f"
			$inputs {
				OFM_FaultVar[0] = *max*
				OFM_FaultVar[1] = *max*
				OFM_FaultVar[2] = *max*
				OFM_FaultVar[3] = *max*
				OFM_FaultVar[4] = *max*
				OFM_FaultVar[5] = *max*
				OFM_FaultVar[6] = *max*
				OFM_FaultVar[7] = *max*
				OFM_FaultVar[8] = *max*
				OFM_FaultVar[9] = *max*
				OFM_FaultVar[10] = *max*
				OFM_FaultVar[11] = *max*
				OFM_FaultVar[12] = *max*
				OFM_FaultVar[13] = *max*
				OFM_FaultVar[14] = *max*
				OFM_FaultVar[15] = *max*
				OFM_FaultVar[16] = *max*
				OFM_FaultVar[17] = *max*
				OFM_FaultVar[18] = *max*
				OFM_FaultVar[19] = *max*
				OFM_FaultVar[20] = *max*
				OFM_FaultVar[21] = *max*
				OFM_FaultVar[22] = *max*
				OFM_FaultVar[23] = *max*
				OFM_FaultVar[24] = *max*
				OFM_UpdateFaultState(FaultVar) = target_OFM_UpdateFaultState_FaultVar
				Rte_CpApCHG_PpInt_OBC_Status_Delayed_OBC_Status = Cx5_degradation_mode
				Rte_CpApCPT_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection = *max*
				ArrayPosition = 5
			}
			$outputs {
				OFM_UpdateFaultState(Configuration) {
					Level = FAULT_LEVEL_1
					ReStartTime = 0
					ConfirmTime = 0
				}
				OFM_UpdateFaultState(FaultIndex) = 5
				OFM_UpdateFaultState(Input) = TRUE
				Rte_CpApOFM_PimOBC_ControlPilot_Error = *max*
				&target_OFM_UpdateFaultState_FaultVar {
					Counter = 0
					State = FALSE
				}
			}
			$calltrace {
				OFM_UpdateFaultState
			}
		}
	}

	$testcase 4 {
		$name "Not Allowed Condition due to ChargingConnectionConfirmation in Conversion"
		$uuid "b46775b3-b7a3-4407-bd68-ce870e412baf"
		$specification """
			+ IN
			  + PARAMETERS
			    - ArrayPosition:ID: 5
			  + RTE GETS
			    - Rte_Delayed_OBC_Status:Cx3_conversion_working_
			    - Rte_OBC_PlugVoltDetection:TRUE
			  + GLOBALS
			    - OFM_FaultVar[26]:ID: 1...26
			+ OUT
			  + SENDED PARAMETERS
			    - OFM_UpdateFaultState( ):Called
			      + Configuration
			        - Level:FAULT_LEVEL_1
			        - ReStartTime:0
			        - ConfirmTime:0
			      - FaultIndex:ID: 5
			      - Input:FALSE
			      - *FaultVar:NO NULL
			        + target_OFM_UpdateFaultState_FaultVar
			          - Counter:0
			          - State:FALSE
			  + RTE SETS
			    - Rte_ControlPilot_Error:ID [5] 6
		"""
		$description """
			Not allowed condition combination 1
		"""

		$teststep 4.1 {
			$name ""
			$uuid "f78b6844-ce72-4444-87cf-2804647d6c40"
			$inputs {
				OFM_FaultVar[0] = 1
				OFM_FaultVar[1] = 2
				OFM_FaultVar[2] = 3
				OFM_FaultVar[3] = 4
				OFM_FaultVar[4] = 5
				OFM_FaultVar[5] = 6
				OFM_FaultVar[6] = 7
				OFM_FaultVar[7] = 8
				OFM_FaultVar[8] = 9
				OFM_FaultVar[9] = 10
				OFM_FaultVar[10] = 11
				OFM_FaultVar[11] = 12
				OFM_FaultVar[12] = 13
				OFM_FaultVar[13] = 14
				OFM_FaultVar[14] = 15
				OFM_FaultVar[15] = 16
				OFM_FaultVar[16] = 17
				OFM_FaultVar[17] = 18
				OFM_FaultVar[18] = 19
				OFM_FaultVar[19] = 20
				OFM_FaultVar[20] = 21
				OFM_FaultVar[21] = 22
				OFM_FaultVar[22] = 23
				OFM_FaultVar[23] = 24
				OFM_FaultVar[24] = 25
				OFM_UpdateFaultState(FaultVar) = target_OFM_UpdateFaultState_FaultVar
				Rte_CpApCHG_PpInt_OBC_Status_Delayed_OBC_Status = Cx3_conversion_working_
				Rte_CpApCPT_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection = TRUE
				ArrayPosition = 5
			}
			$outputs {
				OFM_UpdateFaultState(Configuration) {
					Level = FAULT_LEVEL_1
					ReStartTime = 0
					ConfirmTime = 0
				}
				OFM_UpdateFaultState(FaultIndex) = 5
				OFM_UpdateFaultState(Input) = FALSE
				Rte_CpApOFM_PimOBC_ControlPilot_Error = 6
				&target_OFM_UpdateFaultState_FaultVar {
					Counter = 0
					State = FALSE
				}
			}
			$calltrace {
				OFM_UpdateFaultState
			}
		}
	}

	$testcase 5 {
		$name "Not Allowed Condition due to ChargingConnectionConfirmation in Degradation"
		$uuid "a9b41a64-9e3e-43b1-8848-5ac946f691fd"
		$specification """
			+ IN
			  + PARAMETERS
			    - ArrayPosition:ID: 5
			  + RTE GETS
			    - Rte_Delayed_OBC_Status:Cx5_degradation_mode
			    - Rte_OBC_PlugVoltDetection:TRUE
			  + GLOBALS
			    - OFM_FaultVar[26]:Max: 4294967295
			+ OUT
			  + SENDED PARAMETERS
			    - OFM_UpdateFaultState( ):Called
			      + Configuration
			        - Level:FAULT_LEVEL_1
			        - ReStartTime:0
			        - ConfirmTime:0
			      - FaultIndex:ID: 5
			      - Input:FALSE
			      - *FaultVar:NO NULL
			        + target_OFM_UpdateFaultState_FaultVar
			          - Counter:0
			          - State:FALSE
			  + RTE SETS
			    - Rte_ControlPilot_Error:Max: 4294967295
		"""
		$description """
			Not allowed condition combination 2
		"""

		$teststep 5.1 {
			$name ""
			$uuid "d3af29bb-7ba9-4eec-8527-23d9a7a50150"
			$inputs {
				OFM_FaultVar[0] = *max*
				OFM_FaultVar[1] = *max*
				OFM_FaultVar[2] = *max*
				OFM_FaultVar[3] = *max*
				OFM_FaultVar[4] = *max*
				OFM_FaultVar[5] = *max*
				OFM_FaultVar[6] = *max*
				OFM_FaultVar[7] = *max*
				OFM_FaultVar[8] = *max*
				OFM_FaultVar[9] = *max*
				OFM_FaultVar[10] = *max*
				OFM_FaultVar[11] = *max*
				OFM_FaultVar[12] = *max*
				OFM_FaultVar[13] = *max*
				OFM_FaultVar[14] = *max*
				OFM_FaultVar[15] = *max*
				OFM_FaultVar[16] = *max*
				OFM_FaultVar[17] = *max*
				OFM_FaultVar[18] = *max*
				OFM_FaultVar[19] = *max*
				OFM_FaultVar[20] = *max*
				OFM_FaultVar[21] = *max*
				OFM_FaultVar[22] = *max*
				OFM_FaultVar[23] = *max*
				OFM_FaultVar[24] = *max*
				OFM_UpdateFaultState(FaultVar) = target_OFM_UpdateFaultState_FaultVar
				Rte_CpApCHG_PpInt_OBC_Status_Delayed_OBC_Status = Cx5_degradation_mode
				Rte_CpApCPT_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection = TRUE
				ArrayPosition = 5
			}
			$outputs {
				OFM_UpdateFaultState(Configuration) {
					Level = FAULT_LEVEL_1
					ReStartTime = 0
					ConfirmTime = 0
				}
				OFM_UpdateFaultState(FaultIndex) = 5
				OFM_UpdateFaultState(Input) = FALSE
				Rte_CpApOFM_PimOBC_ControlPilot_Error = *max*
				&target_OFM_UpdateFaultState_FaultVar {
					Counter = 0
					State = FALSE
				}
			}
			$calltrace {
				OFM_UpdateFaultState
			}
		}
	}

	$testcase 6 {
		$name "Not Allowed Condition due to OBC_Status in Not FALSE ChargingConnectionConfirmation "
		$uuid "64980733-099e-4c50-9938-dcf161c3978c"
		$specification """
			+ IN
			  + PARAMETERS
			    - ArrayPosition:ID: 5
			  + RTE GETS
			    - Rte_Delayed_OBC_Status:Max: 255
			    - Rte_OBC_PlugVoltDetection:Max: 255
			  + GLOBALS
			    - OFM_FaultVar[26]:ID: 1...26
			+ OUT
			  + SENDED PARAMETERS
			    - OFM_UpdateFaultState( ):Called
			      + Configuration
			        - Level:FAULT_LEVEL_1
			        - ReStartTime:0
			        - ConfirmTime:0
			      - FaultIndex:ID: 5
			      - Input:FALSE
			      - *FaultVar:NO NULL
			        + target_OFM_UpdateFaultState_FaultVar
			          - Counter:0
			          - State:FALSE
			  + RTE SETS
			    - Rte_ControlPilot_Error:ID [5] 6
		"""
		$description """
			Not allowed condition combination 5
		"""

		$teststep 6.1 {
			$name ""
			$uuid "061b820a-5f01-4d41-8b16-8cc8a44abf99"
			$inputs {
				OFM_FaultVar[0] = 1
				OFM_FaultVar[1] = 2
				OFM_FaultVar[2] = 3
				OFM_FaultVar[3] = 4
				OFM_FaultVar[4] = 5
				OFM_FaultVar[5] = 6
				OFM_FaultVar[6] = 7
				OFM_FaultVar[7] = 8
				OFM_FaultVar[8] = 9
				OFM_FaultVar[9] = 10
				OFM_FaultVar[10] = 11
				OFM_FaultVar[11] = 12
				OFM_FaultVar[12] = 13
				OFM_FaultVar[13] = 14
				OFM_FaultVar[14] = 15
				OFM_FaultVar[15] = 16
				OFM_FaultVar[16] = 17
				OFM_FaultVar[17] = 18
				OFM_FaultVar[18] = 19
				OFM_FaultVar[19] = 20
				OFM_FaultVar[20] = 21
				OFM_FaultVar[21] = 22
				OFM_FaultVar[22] = 23
				OFM_FaultVar[23] = 24
				OFM_FaultVar[24] = 25
				OFM_UpdateFaultState(FaultVar) = target_OFM_UpdateFaultState_FaultVar
				Rte_CpApCHG_PpInt_OBC_Status_Delayed_OBC_Status = *max*
				Rte_CpApCPT_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection = *max*
				ArrayPosition = 5
			}
			$outputs {
				OFM_UpdateFaultState(Configuration) {
					Level = FAULT_LEVEL_1
					ReStartTime = 0
					ConfirmTime = 0
				}
				OFM_UpdateFaultState(FaultIndex) = 5
				OFM_UpdateFaultState(Input) = FALSE
				Rte_CpApOFM_PimOBC_ControlPilot_Error = 6
				&target_OFM_UpdateFaultState_FaultVar {
					Counter = 0
					State = FALSE
				}
			}
			$calltrace {
				OFM_UpdateFaultState
			}
		}
	}

	$testcase 7 {
		$name "Not Allowed Condition due to OBC_Status & ChargingConnectionConfirmation "
		$uuid "7bac33c5-2d8d-4bcc-9e14-82cda5df3725"
		$specification """
			+ IN
			  + PARAMETERS
			    - ArrayPosition:ID: 5
			  + RTE GETS
			    - Rte_Delayed_OBC_Status:Max: 255
			    - Rte_OBC_PlugVoltDetection:TRUE
			  + GLOBALS
			    - OFM_FaultVar[26]:Max: 4294967295
			+ OUT
			  + SENDED PARAMETERS
			    - OFM_UpdateFaultState( ):Called
			      + Configuration
			        - Level:FAULT_LEVEL_1
			        - ReStartTime:0
			        - ConfirmTime:0
			      - FaultIndex:ID: 5
			      - Input:FALSE
			      - *FaultVar:NO NULL
			        + target_OFM_UpdateFaultState_FaultVar
			          - Counter:0
			          - State:FALSE
			  + RTE SETS
			    - Rte_ControlPilot_Error:Max: 4294967295
		"""
		$description """
			Not allowed condition combination 6
		"""

		$teststep 7.1 {
			$name ""
			$uuid "3ba2e8b5-1459-49d6-901a-a0817830d022"
			$inputs {
				OFM_FaultVar[0] = *max*
				OFM_FaultVar[1] = *max*
				OFM_FaultVar[2] = *max*
				OFM_FaultVar[3] = *max*
				OFM_FaultVar[4] = *max*
				OFM_FaultVar[5] = *max*
				OFM_FaultVar[6] = *max*
				OFM_FaultVar[7] = *max*
				OFM_FaultVar[8] = *max*
				OFM_FaultVar[9] = *max*
				OFM_FaultVar[10] = *max*
				OFM_FaultVar[11] = *max*
				OFM_FaultVar[12] = *max*
				OFM_FaultVar[13] = *max*
				OFM_FaultVar[14] = *max*
				OFM_FaultVar[15] = *max*
				OFM_FaultVar[16] = *max*
				OFM_FaultVar[17] = *max*
				OFM_FaultVar[18] = *max*
				OFM_FaultVar[19] = *max*
				OFM_FaultVar[20] = *max*
				OFM_FaultVar[21] = *max*
				OFM_FaultVar[22] = *max*
				OFM_FaultVar[23] = *max*
				OFM_FaultVar[24] = *max*
				OFM_UpdateFaultState(FaultVar) = target_OFM_UpdateFaultState_FaultVar
				Rte_CpApCHG_PpInt_OBC_Status_Delayed_OBC_Status = *max*
				Rte_CpApCPT_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection = TRUE
				ArrayPosition = 5
			}
			$outputs {
				OFM_UpdateFaultState(Configuration) {
					Level = FAULT_LEVEL_1
					ReStartTime = 0
					ConfirmTime = 0
				}
				OFM_UpdateFaultState(FaultIndex) = 5
				OFM_UpdateFaultState(Input) = FALSE
				Rte_CpApOFM_PimOBC_ControlPilot_Error = *max*
				&target_OFM_UpdateFaultState_FaultVar {
					Counter = 0
					State = FALSE
				}
			}
			$calltrace {
				OFM_UpdateFaultState
			}
		}
	}
}