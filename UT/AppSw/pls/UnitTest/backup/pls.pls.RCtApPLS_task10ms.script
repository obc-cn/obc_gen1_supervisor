$testobject {

	$testcase 1 {
		$name "TRUE-TRUE-TRUE"
		$uuid "9e39f270-09b5-4066-aca0-2c4fae1796d1"
		$specification """
			If any of the following error conditions is equal to Second Level, then the signals OBC_SocketTempL_ProducerError and OBC_SocketTempN_ProducerError shall be set to TRUE. Else, these signals shall be set to FALSE.
			OBC_DCTemperature_Error
			OBC_ACTemperatureMono_Error
			OBC_ACTemperatureTri_Error
		"""
		$description """
			Temperature Error combination
		"""

		$teststep 1.1 {
			$name ""
			$uuid "2b7d8896-6fe3-4d2a-be68-762a6e70da67"
			$inputs {
				PLS_DTCTriggerEff_DCLV = 0
				PLS_DTCTriggerHV = 0
				PLS_DTCTriggerLV = 0
				PLS_DTCTriggerTempAC_mono = 255
				PLS_DTCTriggerTempAC_tri = 24
				PLS_DTCTriggerTempDC = 0
				PLS_VoltageError::Error_internal#1 = 0
				PLS_pwr_mono() = 0
				PLS_pwr_tri() = 0
				PLS_temp_dc() = TRUE
				PLS_temp_mono() = TRUE
				PLS_temp_tri() = TRUE
				Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP {
					Rte_Calprm_CtApPLS {
						CalACTempTripTime = 0
						CalACTempWarningTime = 0
						CalDCTempTripTime = 0
						CalDCTempWarningTime = 0
						CalHVVTripTime = 0
						CalHVVWarningTime = 0
						CalLVPTripTime = 0
						CalLVVTripTime = 0
						CalLVVWarningTime = 0
						CalOBCMaxEfficiency = 0
						CalOBCOffsetAllowed = 0
						CalOBCPTripTime = 0
						CalOffsetAllowed = 0
						CalACTempTripThreshold = 0
						CalACTempWarningThreshold = 0
						CalDCTempTripThreshold = 0
						CalDCTempWarningThreshold = 0
						CalHVVTripThreshold = 0
						CalHVVWarningThreshold = 0
						CalLVVTripThreshold = 0
						CalLVVWarningThreshold = 0
						CalMaxEfficiency = 0
						CalMinEfficiency = 0
						CalOBCMinEfficiency = 0
						CalOBC_DelayEfficiencyDCLV = 0
						CalOBC_DelayEfficiencyHVMono = 0
						CalOBC_DelayEfficiencyHVTri = 0
					}
				}
				Rte_CpApPCOM_PpInt_BMS_Voltage_BMS_Voltage = 0
				Rte_CpApPCOM_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled = 1
			}
			$outputs {
				PLS_DTCTriggerEff_DCLV = 0
				PLS_DTCTriggerHV = 0
				PLS_DTCTriggerLV = 0
				PLS_DTCTriggerTempAC_mono = 255
				PLS_DTCTriggerTempAC_tri = 24
				PLS_DTCTriggerTempDC = 0
				PLS_VoltageError::Error_internal#1 = 25600
				Rte_CpApPLS_PpDiagPlausFaultsList_DeACPlugTemp_PlausibilityFault = 255
				Rte_CpApPLS_PpDiagPlausFaultsList_DeDCLV_EfficiencyFault = 0
				Rte_CpApPLS_PpDiagPlausFaultsList_DeDCPlugTemp_PlausibilityFault = 0
				Rte_CpApPLS_PpDiagPlausFaultsList_DeVoltageHVBattery_PlausibilityFault = 0
				Rte_CpApPLS_PpDiagPlausFaultsList_DeVoltageLVBattery_PlausibilityFault = 0
				Rte_CpApPLS_PpInt_OBC_OutputCurrent_OBC_OutputCurrent = 0
				Rte_CpApPLS_PpOBC_OutputCurrent_ProducerError_DeOBC_OutputCurrent_ProducerError = 0
				Rte_CpApPLS_PpOBC_SocketTempL_ProducerError_DeOBC_SocketTempL_ProducerError = 1
				Rte_CpApPLS_PpOBC_SocketTempN_ProducerError_DeOBC_SocketTempN_ProducerError = 1
				Rte_CpApPLS_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset = 0
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 2 {
		$name "FALSE-FALSE-FALSE"
		$uuid "873aa548-80af-4c78-8367-60b88e609c52"
		$specification """
			If any of the following error conditions is equal to Second Level, then the signals OBC_SocketTempL_ProducerError and OBC_SocketTempN_ProducerError shall be set to TRUE. Else, these signals shall be set to FALSE.
			OBC_DCTemperature_Error
			OBC_ACTemperatureMono_Error
			OBC_ACTemperatureTri_Error
		"""
		$description """
			Temperature Error combination
		"""

		$teststep 2.1 {
			$name ""
			$uuid "cebaf5f3-beb4-43e4-ba3a-a4874db26a2a"
			$inputs {
				PLS_DTCTriggerEff_DCLV = *max*
				PLS_DTCTriggerHV = *max*
				PLS_DTCTriggerLV = *max*
				PLS_DTCTriggerTempAC_mono = 255
				PLS_DTCTriggerTempAC_tri = 14
				PLS_DTCTriggerTempDC = *max*
				PLS_VoltageError::Error_internal#1 = *max*
				PLS_pwr_mono() = *max*
				PLS_pwr_tri() = *max*
				PLS_temp_dc() = *max*
				PLS_temp_mono() = *max*
				PLS_temp_tri() = *max*
				Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP {
					Rte_Calprm_CtApPLS {
						CalACTempTripTime = *max*
						CalACTempWarningTime = *max*
						CalDCTempTripTime = *max*
						CalDCTempWarningTime = *max*
						CalHVVTripTime = *max*
						CalHVVWarningTime = *max*
						CalLVPTripTime = *max*
						CalLVVTripTime = *max*
						CalLVVWarningTime = *max*
						CalOBCMaxEfficiency = *max*
						CalOBCOffsetAllowed = *max*
						CalOBCPTripTime = *max*
						CalOffsetAllowed = *max*
						CalACTempTripThreshold = *max*
						CalACTempWarningThreshold = *max*
						CalDCTempTripThreshold = *max*
						CalDCTempWarningThreshold = *max*
						CalHVVTripThreshold = *max*
						CalHVVWarningThreshold = *max*
						CalLVVTripThreshold = *max*
						CalLVVWarningThreshold = *max*
						CalMaxEfficiency = *max*
						CalMinEfficiency = *max*
						CalOBCMinEfficiency = *max*
						CalOBC_DelayEfficiencyDCLV = *max*
						CalOBC_DelayEfficiencyHVMono = *max*
						CalOBC_DelayEfficiencyHVTri = *max*
					}
				}
				Rte_CpApPCOM_PpInt_BMS_Voltage_BMS_Voltage = *max*
				Rte_CpApPCOM_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled = *max*
			}
			$outputs {
				PLS_DTCTriggerEff_DCLV = 255
				PLS_DTCTriggerHV = 255
				PLS_DTCTriggerLV = 255
				PLS_DTCTriggerTempAC_mono = 255
				PLS_DTCTriggerTempAC_tri = 14
				PLS_DTCTriggerTempDC = 255
				PLS_VoltageError::Error_internal#1 = 25600
				Rte_CpApPLS_PpDiagPlausFaultsList_DeACPlugTemp_PlausibilityFault = 255
				Rte_CpApPLS_PpDiagPlausFaultsList_DeDCLV_EfficiencyFault = 255
				Rte_CpApPLS_PpDiagPlausFaultsList_DeDCPlugTemp_PlausibilityFault = 255
				Rte_CpApPLS_PpDiagPlausFaultsList_DeVoltageHVBattery_PlausibilityFault = 255
				Rte_CpApPLS_PpDiagPlausFaultsList_DeVoltageLVBattery_PlausibilityFault = 255
				Rte_CpApPLS_PpInt_OBC_OutputCurrent_OBC_OutputCurrent = 0
				Rte_CpApPLS_PpOBC_OutputCurrent_ProducerError_DeOBC_OutputCurrent_ProducerError = 255
				Rte_CpApPLS_PpOBC_SocketTempL_ProducerError_DeOBC_SocketTempL_ProducerError = 255
				Rte_CpApPLS_PpOBC_SocketTempN_ProducerError_DeOBC_SocketTempN_ProducerError = 255
				Rte_CpApPLS_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset = 0
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}

	$testcase 3 {
		$name "TRUE-TRUE-TRUE"
		$uuid "57f9177d-45b1-4bdc-ae6a-d1fbda668a34"
		$specification """
			If any of the following error conditions is equal to Second Level, then the signals OBC_SocketTempL_ProducerError and OBC_SocketTempN_ProducerError shall be set to TRUE. Else, these signals shall be set to FALSE.
			OBC_DCTemperature_Error
			OBC_ACTemperatureMono_Error
			OBC_ACTemperatureTri_Error
		"""
		$description """
			Temperature Error combination
		"""

		$teststep 3.1 {
			$name ""
			$uuid "599bbbf9-41bb-4a21-92de-53b75bdf0f99"
			$inputs {
				PLS_DTCTriggerEff_DCLV = 0
				PLS_DTCTriggerHV = 0
				PLS_DTCTriggerLV = 0
				PLS_DTCTriggerTempAC_mono = 255
				PLS_DTCTriggerTempAC_tri = 24
				PLS_DTCTriggerTempDC = 0
				PLS_VoltageError::Error_internal#1 = 0
				PLS_pwr_mono() = 0
				PLS_pwr_tri() = 0
				PLS_temp_dc() = TRUE
				PLS_temp_mono() = TRUE
				PLS_temp_tri() = TRUE
				Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP {
					Rte_Calprm_CtApPLS {
						CalACTempTripTime = 0
						CalACTempWarningTime = 0
						CalDCTempTripTime = 0
						CalDCTempWarningTime = 0
						CalHVVTripTime = 0
						CalHVVWarningTime = 0
						CalLVPTripTime = 0
						CalLVVTripTime = 0
						CalLVVWarningTime = 0
						CalOBCMaxEfficiency = 0
						CalOBCOffsetAllowed = 0
						CalOBCPTripTime = 0
						CalOffsetAllowed = 0
						CalACTempTripThreshold = 0
						CalACTempWarningThreshold = 0
						CalDCTempTripThreshold = 0
						CalDCTempWarningThreshold = 0
						CalHVVTripThreshold = 0
						CalHVVWarningThreshold = 0
						CalLVVTripThreshold = 0
						CalLVVWarningThreshold = 0
						CalMaxEfficiency = 0
						CalMinEfficiency = 0
						CalOBCMinEfficiency = 0
						CalOBC_DelayEfficiencyDCLV = 0
						CalOBC_DelayEfficiencyHVMono = 0
						CalOBC_DelayEfficiencyHVTri = 0
					}
				}
				Rte_CpApPCOM_PpInt_BMS_Voltage_BMS_Voltage = 0
				Rte_CpApPCOM_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled = 0
			}
			$outputs {
				PLS_DTCTriggerEff_DCLV = 0
				PLS_DTCTriggerHV = 0
				PLS_DTCTriggerLV = 0
				PLS_DTCTriggerTempAC_mono = 255
				PLS_DTCTriggerTempAC_tri = 24
				PLS_DTCTriggerTempDC = 0
				PLS_VoltageError::Error_internal#1 = 5000
				Rte_CpApPLS_PpDiagPlausFaultsList_DeACPlugTemp_PlausibilityFault = 255
				Rte_CpApPLS_PpDiagPlausFaultsList_DeDCLV_EfficiencyFault = 0
				Rte_CpApPLS_PpDiagPlausFaultsList_DeDCPlugTemp_PlausibilityFault = 0
				Rte_CpApPLS_PpDiagPlausFaultsList_DeVoltageHVBattery_PlausibilityFault = 0
				Rte_CpApPLS_PpDiagPlausFaultsList_DeVoltageLVBattery_PlausibilityFault = 0
				Rte_CpApPLS_PpInt_OBC_OutputCurrent_OBC_OutputCurrent = 0
				Rte_CpApPLS_PpOBC_OutputCurrent_ProducerError_DeOBC_OutputCurrent_ProducerError = 0
				Rte_CpApPLS_PpOBC_SocketTempL_ProducerError_DeOBC_SocketTempL_ProducerError = 1
				Rte_CpApPLS_PpOBC_SocketTempN_ProducerError_DeOBC_SocketTempN_ProducerError = 1
				Rte_CpApPLS_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset = -81
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}
}