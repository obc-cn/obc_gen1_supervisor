$testobject {

	$testcase 1 {
		$name "0 VdcLink - [100-300] - 200 Peak"
		$uuid "e3a1a243-2b46-490e-b262-c18a6ee27b11"
		$specification """
			+ IN
			  + RTE GETS
			    - Rte_OBC_InputVoltageSt:Cx1_220V_AC_connected
			    - Rte_PFC_Vdclink_V:0
			  + GLOBALS
			    + RLY_relayState
			      - MONO [0]:FALSE
			      - TRI [1]:FALSE
			      - EMI [2]:FALSE
			    - RLY_ClosePreCounter:0
			  + THRESHOLDS
			    - Rte_PFC_VPH12_Peak_V:200 *0.95=190
			    - DClinkVmin:100
			    - DClinkVmax:300
			+ OUT
			  + RLY_relayState
			    - MONO [0]:FALSE
			    - PREC [1]:FALSE
			    - EMI [2]:FALSE
			  - RLY_ClosePreCounter:0
		"""
		$description """
			Out of Range - Inactive Debounce
		"""

		$teststep 1.1 {
			$name ""
			$uuid "6c7d8ed6-660c-48ac-bbb5-c03d4db6866e"
			$inputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = Cx1_220V_AC_connected
				Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V = 200
				Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V = 0
			}
			$outputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 2 {
		$name "99 VdcLink - [100-300] - 200 Peak"
		$uuid "4f7cb24b-6403-4601-9698-f604ff71ec63"
		$specification """
			+ IN
			  + RTE GETS
			    - Rte_OBC_InputVoltageSt:Cx1_220V_AC_connected
			    - Rte_PFC_Vdclink_V:99
			  + GLOBALS
			    + RLY_relayState
			      - MONO [0]:FALSE
			      - TRI [1]:FALSE
			      - EMI [2]:FALSE
			    - RLY_ClosePreCounter:0
			  + THRESHOLDS
			    - Rte_PFC_VPH12_Peak_V:200 *0.95=190
			    - DClinkVmin:100
			    - DClinkVmax:300
			+ OUT
			  + RLY_relayState
			    - MONO [0]:FALSE
			    - PREC [1]:FALSE
			    - EMI [2]:FALSE
			  - RLY_ClosePreCounter:0
		"""
		$description """
			Out of Range - Inactive Debounce
		"""

		$teststep 2.1 {
			$name ""
			$uuid "ce583f46-b491-4f8c-b872-4307e7bd6a2d"
			$inputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = Cx1_220V_AC_connected
				Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V = 200
				Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V = 99
			}
			$outputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 3 {
		$name "100 VdcLink - [100-300] - 200 Peak"
		$uuid "f2f7a28f-42bf-42e9-a7cf-243f3aa676b6"
		$specification """
			+ IN
			  + RTE GETS
			    - Rte_OBC_InputVoltageSt:Cx1_220V_AC_connected
			    - Rte_PFC_Vdclink_V:100
			  + GLOBALS
			    + RLY_relayState
			      - MONO [0]:FALSE
			      - TRI [1]:FALSE
			      - EMI [2]:FALSE
			    - RLY_ClosePreCounter:0
			  + THRESHOLDS
			    - Rte_PFC_VPH12_Peak_V:200 *0.95=190
			    - DClinkVmin:100
			    - DClinkVmax:300
			+ OUT
			  + RLY_relayState
			    - MONO [0]:FALSE
			    - PREC [1]:FALSE
			    - EMI [2]:FALSE
			  - RLY_ClosePreCounter:0
		"""
		$description """
			Into of Range but below Peak - Inactive Debounce
		"""

		$teststep 3.1 {
			$name ""
			$uuid "62049e33-ecdb-489a-a045-8944965c4429"
			$inputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = Cx1_220V_AC_connected
				Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V = 200
				Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V = 100
			}
			$outputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 4 {
		$name "101 VdcLink - [100-300] - 200 Peak"
		$uuid "e3a5ecc4-fdcb-4b4f-a743-bc9a3a99d8b8"
		$specification """
			+ IN
			  + RTE GETS
			    - Rte_OBC_InputVoltageSt:Cx1_220V_AC_connected
			    - Rte_PFC_Vdclink_V:101
			  + GLOBALS
			    + RLY_relayState
			      - MONO [0]:FALSE
			      - TRI [1]:FALSE
			      - EMI [2]:FALSE
			    - RLY_ClosePreCounter:0
			  + THRESHOLDS
			    - Rte_PFC_VPH12_Peak_V:200 *0.95=190
			    - DClinkVmin:100
			    - DClinkVmax:300
			+ OUT
			  + RLY_relayState
			    - MONO [0]:FALSE
			    - PREC [1]:FALSE
			    - EMI [2]:FALSE
			  - RLY_ClosePreCounter:0
		"""
		$description """
			Into of Range but below Peak - Inactive Debounce
		"""

		$teststep 4.1 {
			$name ""
			$uuid "2ca40bd4-f044-4395-84a8-28220e0b322d"
			$inputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = Cx1_220V_AC_connected
				Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V = 200
				Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V = 101
			}
			$outputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 5 {
		$name "189 VdcLink - [100-300] - 200 Peak"
		$uuid "134c7440-3faa-448a-88b6-d46315d2ef0c"
		$specification """
			+ IN
			  + RTE GETS
			    - Rte_OBC_InputVoltageSt:Cx1_220V_AC_connected
			    - Rte_PFC_Vdclink_V:189
			  + GLOBALS
			    + RLY_relayState
			      - MONO [0]:FALSE
			      - TRI [1]:FALSE
			      - EMI [2]:FALSE
			    - RLY_ClosePreCounter:0
			  + THRESHOLDS
			    - Rte_PFC_VPH12_Peak_V:200 *0.95=190
			    - DClinkVmin:100
			    - DClinkVmax:300
			+ OUT
			  + RLY_relayState
			    - MONO [0]:FALSE
			    - PREC [1]:FALSE
			    - EMI [2]:FALSE
			  - RLY_ClosePreCounter:1
		"""
		$description """
			Into of Range but below Peak - Inactive Debounce
		"""

		$teststep 5.1 {
			$name ""
			$uuid "df2ee725-1f3b-432f-99ab-783ec2fb9cfc"
			$inputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = Cx1_220V_AC_connected
				Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V = 200
				Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V = 189
			}
			$outputs {
				RLY_ClosePreCounter = 1
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 6 {
		$name "190 VdcLink - [100-300] - 200 Peak"
		$uuid "f1155788-381e-4af2-b29a-1ac3d4dd39e2"
		$specification """
			+ IN
			  + RTE GETS
			    - Rte_OBC_InputVoltageSt:Cx1_220V_AC_connected
			    - Rte_PFC_Vdclink_V:190
			  + GLOBALS
			    + RLY_relayState
			      - MONO [0]:FALSE
			      - TRI [1]:FALSE
			      - EMI [2]:FALSE
			    - RLY_ClosePreCounter:0
			  + THRESHOLDS
			    - Rte_PFC_VPH12_Peak_V:200 *0.95=190
			    - DClinkVmin:100
			    - DClinkVmax:300
			+ OUT
			  + RLY_relayState
			    - MONO [0]:FALSE
			    - PREC [1]:FALSE
			    - EMI [2]:FALSE
			  - RLY_ClosePreCounter:1
		"""
		$description """
			Into of Range but equal to Peak - Inactive Debounce
		"""

		$teststep 6.1 {
			$name ""
			$uuid "600eab8e-353d-4a52-895d-73f07f88e190"
			$inputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = Cx1_220V_AC_connected
				Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V = 200
				Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V = 190
			}
			$outputs {
				RLY_ClosePreCounter = 1
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 7 {
		$name "191 VdcLink - [100-300] - 200 Peak"
		$uuid "62a385e1-535d-4ef7-9a99-9656dbfb5b7d"
		$specification """
			+ IN
			  + RTE GETS
			    - Rte_OBC_InputVoltageSt:Cx1_220V_AC_connected
			    - Rte_PFC_Vdclink_V:191
			  + GLOBALS
			    + RLY_relayState
			      - MONO [0]:FALSE
			      - TRI [1]:FALSE
			      - EMI [2]:FALSE
			    - RLY_ClosePreCounter:0
			  + THRESHOLDS
			    - Rte_PFC_VPH12_Peak_V:200 *0.95=190
			    - DClinkVmin:100
			    - DClinkVmax:300
			+ OUT
			  + RLY_relayState
			    - MONO [0]:FALSE
			    - PREC [1]:FALSE
			    - EMI [2]:FALSE
			  - RLY_ClosePreCounter:1
		"""
		$description """
			Into of Range and over Peak - Active Debounce
		"""

		$teststep 7.1 {
			$name ""
			$uuid "2abc122f-fdfe-4b13-b1b3-665998b50f94"
			$inputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = Cx1_220V_AC_connected
				Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V = 200
				Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V = 191
			}
			$outputs {
				RLY_ClosePreCounter = 1
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 8 {
		$name "299 VdcLink - [100-300] - 200 Peak"
		$uuid "00e07ef0-f725-4574-9881-9825fa97c4bc"
		$specification """
			+ IN
			  + RTE GETS
			    - Rte_OBC_InputVoltageSt:Cx1_220V_AC_connected
			    - Rte_PFC_Vdclink_V:299
			  + GLOBALS
			    + RLY_relayState
			      - MONO [0]:FALSE
			      - TRI [1]:FALSE
			      - EMI [2]:FALSE
			    - RLY_ClosePreCounter:0
			  + THRESHOLDS
			    - Rte_PFC_VPH12_Peak_V:200 *0.95=190
			    - DClinkVmin:100
			    - DClinkVmax:300
			+ OUT
			  + RLY_relayState
			    - MONO [0]:FALSE
			    - PREC [1]:FALSE
			    - EMI [2]:FALSE
			  - RLY_ClosePreCounter:1
		"""
		$description """
			Into of Range and over Peak - Active Debounce
		"""

		$teststep 8.1 {
			$name ""
			$uuid "8f13a1fb-0b26-4a4f-9ebb-91d64857ae3f"
			$inputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = Cx1_220V_AC_connected
				Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V = 200
				Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V = 299
			}
			$outputs {
				RLY_ClosePreCounter = 1
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 9 {
		$name "300 VdcLink - [100-300] - 200 Peak"
		$uuid "a2f24f24-9612-4bc8-b29b-40b830cee017"
		$specification """
			+ IN
			  + RTE GETS
			    - Rte_OBC_InputVoltageSt:Cx1_220V_AC_connected
			    - Rte_PFC_Vdclink_V:300
			  + GLOBALS
			    + RLY_relayState
			      - MONO [0]:FALSE
			      - TRI [1]:FALSE
			      - EMI [2]:FALSE
			    - RLY_ClosePreCounter:0
			  + THRESHOLDS
			    - Rte_PFC_VPH12_Peak_V:200 *0.95=190
			    - DClinkVmin:100
			    - DClinkVmax:300
			+ OUT
			  + RLY_relayState
			    - MONO [0]:FALSE
			    - PREC [1]:FALSE
			    - EMI [2]:FALSE
			  - RLY_ClosePreCounter:1
		"""
		$description """
			Into of Range and over Peak - Active Debounce
		"""

		$teststep 9.1 {
			$name ""
			$uuid "f36b8f53-fdaa-4284-ad5b-2067e33d1fd6"
			$inputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = Cx1_220V_AC_connected
				Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V = 200
				Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V = 300
			}
			$outputs {
				RLY_ClosePreCounter = 1
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 10 {
		$name "301 VdcLink - [100-300] - 200 Peak"
		$uuid "68b3b00b-528a-482c-9006-2658fe523a13"
		$specification """
			+ IN
			  + RTE GETS
			    - Rte_OBC_InputVoltageSt:Cx1_220V_AC_connected
			    - Rte_PFC_Vdclink_V:301
			  + GLOBALS
			    + RLY_relayState
			      - MONO [0]:FALSE
			      - TRI [1]:FALSE
			      - EMI [2]:FALSE
			    - RLY_ClosePreCounter:0
			  + THRESHOLDS
			    - Rte_PFC_VPH12_Peak_V:200 *0.95=190
			    - DClinkVmin:100
			    - DClinkVmax:300
			+ OUT
			  + RLY_relayState
			    - MONO [0]:FALSE
			    - PREC [1]:FALSE
			    - EMI [2]:FALSE
			  - RLY_ClosePreCounter:1
		"""
		$description """
			Out of Range even over Peak - Inactive Debounce
		"""

		$teststep 10.1 {
			$name ""
			$uuid "9c811a5b-31d5-45bd-bbff-f8c84239be6b"
			$inputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = Cx1_220V_AC_connected
				Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V = 200
				Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V = 301
			}
			$outputs {
				RLY_ClosePreCounter = 1
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 11 {
		$name "Max VdcLink - [100-300] - 200 Peak"
		$uuid "aae59d3d-4f08-401d-a851-65b078278579"
		$specification """
			+ IN
			  + RTE GETS
			    - Rte_OBC_InputVoltageSt:Cx1_220V_AC_connected
			    - Rte_PFC_Vdclink_V:Max: 65535
			  + GLOBALS
			    + RLY_relayState
			      - MONO [0]:FALSE
			      - TRI [1]:FALSE
			      - EMI [2]:FALSE
			    - RLY_ClosePreCounter:0
			  + THRESHOLDS
			    - Rte_PFC_VPH12_Peak_V:200 *0.95=190
			    - DClinkVmin:100
			    - DClinkVmax:300
			+ OUT
			  + RLY_relayState
			    - MONO [0]:FALSE
			    - PREC [1]:FALSE
			    - EMI [2]:FALSE
			  - RLY_ClosePreCounter:1
		"""
		$description """
			Out of Range even over Peak - Inactive Debounce
		"""

		$teststep 11.1 {
			$name ""
			$uuid "56e65e55-622a-46bb-ac1b-d7bac700c597"
			$inputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = Cx1_220V_AC_connected
				Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V = 200
				Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V = *max*
			}
			$outputs {
				RLY_ClosePreCounter = 1
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 12 {
		$name "0 VdcLink - [100-300] - 0 Peak"
		$uuid "e7acbd20-dcce-43ce-bdbb-3d6dfdb36d7e"
		$specification """
			+ IN
			  + RTE GETS
			    - Rte_OBC_InputVoltageSt:Cx1_220V_AC_connected
			    - Rte_PFC_Vdclink_V:0
			  + GLOBALS
			    + RLY_relayState
			      - MONO [0]:FALSE
			      - TRI [1]:FALSE
			      - EMI [2]:FALSE
			    - RLY_ClosePreCounter:0
			  + THRESHOLDS
			    - Rte_PFC_VPH12_Peak_V:Min: 0 *0.95=0
			    - DClinkVmin:100
			    - DClinkVmax:300
			+ OUT
			  + RLY_relayState
			    - MONO [0]:FALSE
			    - PREC [1]:FALSE
			    - EMI [2]:FALSE
			  - RLY_ClosePreCounter:0
		"""
		$description """
			Out of Range - Inactive Debounce
		"""

		$teststep 12.1 {
			$name ""
			$uuid "31e26584-d0e2-4d70-86b8-4735480de965"
			$inputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = Cx1_220V_AC_connected
				Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V = *min*
				Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V = 0
			}
			$outputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 13 {
		$name "1 VdcLink - [100-300] - 0 Peak"
		$uuid "2645845d-5dad-496b-bf51-87caa94f9a39"
		$specification """
			+ IN
			  + RTE GETS
			    - Rte_OBC_InputVoltageSt:Cx1_220V_AC_connected
			    - Rte_PFC_Vdclink_V:1
			  + GLOBALS
			    + RLY_relayState
			      - MONO [0]:FALSE
			      - TRI [1]:FALSE
			      - EMI [2]:FALSE
			    - RLY_ClosePreCounter:0
			  + THRESHOLDS
			    - Rte_PFC_VPH12_Peak_V:Min: 0 *0.95=0
			    - DClinkVmin:100
			    - DClinkVmax:300
			+ OUT
			  + RLY_relayState
			    - MONO [0]:FALSE
			    - PREC [1]:FALSE
			    - EMI [2]:FALSE
			  - RLY_ClosePreCounter:1
		"""
		$description """
			Out of Range - Inactive Debounce
		"""

		$teststep 13.1 {
			$name ""
			$uuid "7be1da5f-26d0-433e-989c-d87fc6867e0a"
			$inputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = Cx1_220V_AC_connected
				Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V = *min*
				Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V = 1
			}
			$outputs {
				RLY_ClosePreCounter = 1
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 14 {
		$name "99 VdcLink - [100-300] - 0 Peak"
		$uuid "6e57165f-9c04-40a8-8200-6db644fa32c0"
		$specification """
			+ IN
			  + RTE GETS
			    - Rte_OBC_InputVoltageSt:Cx1_220V_AC_connected
			    - Rte_PFC_Vdclink_V:99
			  + GLOBALS
			    + RLY_relayState
			      - MONO [0]:FALSE
			      - TRI [1]:FALSE
			      - EMI [2]:FALSE
			    - RLY_ClosePreCounter:0
			  + THRESHOLDS
			    - Rte_PFC_VPH12_Peak_V:Min: 0 *0.95=0
			    - DClinkVmin:100
			    - DClinkVmax:300
			+ OUT
			  + RLY_relayState
			    - MONO [0]:FALSE
			    - PREC [1]:FALSE
			    - EMI [2]:FALSE
			  - RLY_ClosePreCounter:1
		"""
		$description """
			Out of Range - Inactive Debounce
		"""

		$teststep 14.1 {
			$name ""
			$uuid "74d2db7f-f274-4025-b912-24f9d37a3b88"
			$inputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = Cx1_220V_AC_connected
				Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V = *min*
				Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V = 99
			}
			$outputs {
				RLY_ClosePreCounter = 1
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 15 {
		$name "100 VdcLink - [100-300] - 0 Peak"
		$uuid "24075f02-2883-40a9-8ab5-f75cb55ae165"
		$specification """
			+ IN
			  + RTE GETS
			    - Rte_OBC_InputVoltageSt:Cx1_220V_AC_connected
			    - Rte_PFC_Vdclink_V:100
			  + GLOBALS
			    + RLY_relayState
			      - MONO [0]:FALSE
			      - TRI [1]:FALSE
			      - EMI [2]:FALSE
			    - RLY_ClosePreCounter:0
			  + THRESHOLDS
			    - Rte_PFC_VPH12_Peak_V:Min: 0 *0.95=0
			    - DClinkVmin:100
			    - DClinkVmax:300
			+ OUT
			  + RLY_relayState
			    - MONO [0]:FALSE
			    - PREC [1]:FALSE
			    - EMI [2]:FALSE
			  - RLY_ClosePreCounter:1
		"""
		$description """
			Into of Range and over Peak - Active Debounce
		"""

		$teststep 15.1 {
			$name ""
			$uuid "c5c88738-6362-420a-9ff3-5011ff12cf4f"
			$inputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = Cx1_220V_AC_connected
				Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V = *min*
				Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V = 100
			}
			$outputs {
				RLY_ClosePreCounter = 1
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 16 {
		$name "101 VdcLink - [100-300] - 0 Peak"
		$uuid "dc954ca2-7496-4a0e-ab49-1112be2d6e83"
		$specification """
			+ IN
			  + RTE GETS
			    - Rte_OBC_InputVoltageSt:Cx1_220V_AC_connected
			    - Rte_PFC_Vdclink_V:101
			  + GLOBALS
			    + RLY_relayState
			      - MONO [0]:FALSE
			      - TRI [1]:FALSE
			      - EMI [2]:FALSE
			    - RLY_ClosePreCounter:0
			  + THRESHOLDS
			    - Rte_PFC_VPH12_Peak_V:Min: 0 *0.95=0
			    - DClinkVmin:100
			    - DClinkVmax:300
			+ OUT
			  + RLY_relayState
			    - MONO [0]:FALSE
			    - PREC [1]:FALSE
			    - EMI [2]:FALSE
			  - RLY_ClosePreCounter:1
		"""
		$description """
			Into of Range and over Peak - Active Debounce
		"""

		$teststep 16.1 {
			$name ""
			$uuid "ae1a25a3-b4d5-49e0-a1f2-afe77ffb332c"
			$inputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = Cx1_220V_AC_connected
				Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V = *min*
				Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V = 101
			}
			$outputs {
				RLY_ClosePreCounter = 1
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 17 {
		$name "299 VdcLink - [100-300] - 0 Peak"
		$uuid "6667bb14-ae3e-4464-9a0b-d0ee0975c527"
		$specification """
			+ IN
			  + RTE GETS
			    - Rte_OBC_InputVoltageSt:Cx1_220V_AC_connected
			    - Rte_PFC_Vdclink_V:299
			  + GLOBALS
			    + RLY_relayState
			      - MONO [0]:FALSE
			      - TRI [1]:FALSE
			      - EMI [2]:FALSE
			    - RLY_ClosePreCounter:0
			  + THRESHOLDS
			    - Rte_PFC_VPH12_Peak_V:Min: 0 *0.95=0
			    - DClinkVmin:100
			    - DClinkVmax:300
			+ OUT
			  + RLY_relayState
			    - MONO [0]:FALSE
			    - PREC [1]:FALSE
			    - EMI [2]:FALSE
			  - RLY_ClosePreCounter:1
		"""
		$description """
			Into of Range and over Peak - Active Debounce
		"""

		$teststep 17.1 {
			$name ""
			$uuid "6b40b713-af50-4df1-a813-87ea1287b5fd"
			$inputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = Cx1_220V_AC_connected
				Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V = *min*
				Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V = 299
			}
			$outputs {
				RLY_ClosePreCounter = 1
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 18 {
		$name "300 VdcLink - [100-300] - 0 Peak"
		$uuid "29635255-efa0-4cc5-a2dc-9877c5e663b6"
		$specification """
			+ IN
			  + RTE GETS
			    - Rte_OBC_InputVoltageSt:Cx1_220V_AC_connected
			    - Rte_PFC_Vdclink_V:300
			  + GLOBALS
			    + RLY_relayState
			      - MONO [0]:FALSE
			      - TRI [1]:FALSE
			      - EMI [2]:FALSE
			    - RLY_ClosePreCounter:0
			  + THRESHOLDS
			    - Rte_PFC_VPH12_Peak_V:Min: 0 *0.95=0
			    - DClinkVmin:100
			    - DClinkVmax:300
			+ OUT
			  + RLY_relayState
			    - MONO [0]:FALSE
			    - PREC [1]:FALSE
			    - EMI [2]:FALSE
			  - RLY_ClosePreCounter:1
		"""
		$description """
			Into of Range and over Peak - Active Debounce
		"""

		$teststep 18.1 {
			$name ""
			$uuid "1f92b696-4af8-46f8-b0f1-b8312a0989af"
			$inputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = Cx1_220V_AC_connected
				Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V = *min*
				Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V = 300
			}
			$outputs {
				RLY_ClosePreCounter = 1
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 19 {
		$name "301 VdcLink - [100-300] - 0 Peak"
		$uuid "913e0884-775a-4135-9cdf-31226ec16062"
		$specification """
			+ IN
			  + RTE GETS
			    - Rte_OBC_InputVoltageSt:Cx1_220V_AC_connected
			    - Rte_PFC_Vdclink_V:301
			  + GLOBALS
			    + RLY_relayState
			      - MONO [0]:FALSE
			      - TRI [1]:FALSE
			      - EMI [2]:FALSE
			    - RLY_ClosePreCounter:0
			  + THRESHOLDS
			    - Rte_PFC_VPH12_Peak_V:Min: 0 *0.95=0
			    - DClinkVmin:100
			    - DClinkVmax:300
			+ OUT
			  + RLY_relayState
			    - MONO [0]:FALSE
			    - PREC [1]:FALSE
			    - EMI [2]:FALSE
			  - RLY_ClosePreCounter:1
		"""
		$description """
			Out of Range even over Peak - Inactive Debounce
		"""

		$teststep 19.1 {
			$name ""
			$uuid "0cd953fe-bbea-482a-a889-18e9346af0c2"
			$inputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = Cx1_220V_AC_connected
				Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V = *min*
				Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V = 301
			}
			$outputs {
				RLY_ClosePreCounter = 1
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 20 {
		$name "Max VdcLink - [100-300] - 0 Peak"
		$uuid "98e8efcb-5800-48aa-9470-b40372ce0999"
		$specification """
			+ IN
			  + RTE GETS
			    - Rte_OBC_InputVoltageSt:Cx1_220V_AC_connected
			    - Rte_PFC_Vdclink_V:Max: 65535
			  + GLOBALS
			    + RLY_relayState
			      - MONO [0]:FALSE
			      - TRI [1]:FALSE
			      - EMI [2]:FALSE
			    - RLY_ClosePreCounter:0
			  + THRESHOLDS
			    - Rte_PFC_VPH12_Peak_V:Min: 0 *0.95=0
			    - DClinkVmin:100
			    - DClinkVmax:300
			+ OUT
			  + RLY_relayState
			    - MONO [0]:FALSE
			    - PREC [1]:FALSE
			    - EMI [2]:FALSE
			  - RLY_ClosePreCounter:1
		"""
		$description """
			Out of Range even over Peak - Inactive Debounce
		"""

		$teststep 20.1 {
			$name ""
			$uuid "dcf008de-6a56-4595-bb57-0e7521ffa989"
			$inputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = Cx1_220V_AC_connected
				Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V = *min*
				Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V = *max*
			}
			$outputs {
				RLY_ClosePreCounter = 1
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 25 {
		$name "Counter Debounce"
		$uuid "6a475323-5a1d-4dd7-8104-262204992d9a"
		$description """
			TRUE Output once signal is debounced
		"""

		$teststep 25.1 {
			$name "0x10ms"
			$uuid "53b8385d-0c8f-4126-941d-5b1edf69c368"
			$specification """
				+ IN
				  + RTE GETS
				    - Rte_OBC_InputVoltageSt:Cx1_220V_AC_connected
				    - Rte_PFC_Vdclink_V:299
				  + GLOBALS
				    + RLY_relayState
				      - MONO [0]:FALSE
				      - TRI [1]:FALSE
				      - EMI [2]:FALSE
				    - RLY_ClosePreCounter:0
				  + THRESHOLDS
				    - Rte_PFC_VPH12_Peak_V:200 *0.95=190
				    - DClinkVmin:100
				    - DClinkVmax:300
				+ OUT
				  + RLY_relayState
				    - MONO [0]:FALSE
				    - PREC [1]:FALSE
				    - EMI [2]:FALSE
				  - RLY_ClosePreCounter:1
			"""
			$inputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = Cx1_220V_AC_connected
				Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V = 200
				Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V = 299
			}
			$outputs {
				RLY_ClosePreCounter = 1
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}

		$teststep 25.2 {
			$name "1x10ms"
			$uuid "4b0bf58a-4880-49d3-bc21-3db190e2dff3"
			$specification """
				+ IN
				  + RTE GETS
				    - Rte_OBC_InputVoltageSt:Cx1_220V_AC_connected
				    - Rte_PFC_Vdclink_V:299
				  + GLOBALS
				    + RLY_relayState
				      - MONO [0]:FALSE
				      - TRI [1]:FALSE
				      - EMI [2]:FALSE
				    - RLY_ClosePreCounter:Ignore
				  + THRESHOLDS
				    - Rte_PFC_VPH12_Peak_V:200 *0.95=190
				    - DClinkVmin:100
				    - DClinkVmax:300
				+ OUT
				  + RLY_relayState
				    - MONO [0]:FALSE
				    - PREC [1]:FALSE
				    - EMI [2]:FALSE
				  - RLY_ClosePreCounter:2
			"""
			$inputs {
				RLY_ClosePreCounter = *none*
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = Cx1_220V_AC_connected
				Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V = 200
				Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V = 299
			}
			$outputs {
				RLY_ClosePreCounter = 2
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}

		$teststep 25.3 {
			$name "1x10ms"
			$uuid "1cedaf3c-1096-4fdb-bffe-9629902e0c71"
			$specification """
				+ IN
				  + RTE GETS
				    - Rte_OBC_InputVoltageSt:Cx1_220V_AC_connected
				    - Rte_PFC_Vdclink_V:299
				  + GLOBALS
				    + RLY_relayState
				      - MONO [0]:FALSE
				      - TRI [1]:FALSE
				      - EMI [2]:FALSE
				    - RLY_ClosePreCounter:Ignore
				  + THRESHOLDS
				    - Rte_PFC_VPH12_Peak_V:200 *0.95=190
				    - DClinkVmin:100
				    - DClinkVmax:300
				+ OUT
				  + RLY_relayState
				    - MONO [0]:FALSE
				    - PREC [1]:FALSE
				    - EMI [2]:FALSE
				  - RLY_ClosePreCounter:3
			"""
			$inputs {
				RLY_ClosePreCounter = *none*
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = Cx1_220V_AC_connected
				Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V = 200
				Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V = 299
			}
			$outputs {
				RLY_ClosePreCounter = 3
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 27 {
		$name "Not Allowed InputVoltageSt: 299 VdcLink - [100-300] - 0 Peak"
		$uuid "0ef75fa6-7e79-4dcc-85e6-89274da5e5c3"
		$specification """
			+ IN
			  + RTE GETS
			    - Rte_OBC_InputVoltageSt:Max: 255
			    - Rte_PFC_Vdclink_V:299
			  + GLOBALS
			    + RLY_relayState
			      - MONO [0]:FALSE
			      - TRI [1]:FALSE
			      - EMI [2]:FALSE
			    - RLY_ClosePreCounter:0
			  + THRESHOLDS
			    - Rte_PFC_VPH12_Peak_V:Min: 0 *0.95=0
			    - DClinkVmin:100
			    - DClinkVmax:300
			+ OUT
			  + RLY_relayState
			    - MONO [0]:FALSE
			    - PREC [1]:FALSE
			    - EMI [2]:FALSE
			  - RLY_ClosePreCounter:0
		"""
		$description """
			InputVoltageSt is not allowed so debounce is not activated.
		"""

		$teststep 27.1 {
			$name ""
			$uuid "088729a2-672c-47b0-a16b-8d54b1635bf2"
			$inputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = *max*
				Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V = *min*
				Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V = 299
			}
			$outputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 29 {
		$name ""
		$uuid "ae5ac2d4-fe6b-445c-a59a-02cb4043717e"

		$teststep 29.1 {
			$name ""
			$uuid "ff47aea3-47ba-4ea9-b8d2-9181b65981c5"
			$inputs {
				RLY_ClosePreCounter = *max*
				RLY_relayState[0] = 0
				Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = 1
				Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V = *min*
				Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V = 299
			}
			$outputs {
				RLY_ClosePreCounter = 0
				RLY_relayState[0] = 0
			}
			$calltrace {
				*** Ignore Call Trace ***
			}
		}
	}
}