$testobject {

	$testcase 1 {
		$name "EMI output = TRUE and counter increment"
		$uuid "6448c2f3-0901-4107-9abb-e8452cc7f0c0"
		$specification """
			+ IN
			  + RLY_relayState
			    - MONO [0]:FALSE
			    - TRI [1]:FALSE
			    - EMI [2]:FALSE
			  - RLY_CloseMonoCounter:Min: 0
			+ OUT
			  + RLY_relayState
			    - MONO [0]:FALSE
			    - TRI [1]:FALSE
			    - EMI [2]:TRUE
			  - RLY_CloseMonoCounter:1
		"""
		$description """
			FilterEMI alwais has to be set to TRUE
		"""

		$teststep 1.1 {
			$name ""
			$uuid "090ad75b-c205-414a-8160-3b3d90961263"
			$inputs {
				RLY_CloseMonoCounter = *min*
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$outputs {
				RLY_CloseMonoCounter = 1
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 2 {
		$name "RelayMono Output = TRUE Debounce"
		$uuid "36d5ef2d-8be9-4011-9a81-087985f5dfa5"
		$description """
			Counter Increment until RelayMono activation is confirmed after 500 ms.
		"""

		$teststep 2.1 {
			$name "0x10ms"
			$uuid "9963871d-2129-4589-8970-e7ec55074d64"
			$specification """
				+ IN
				  + RLY_relayState
				    - MONO [0]:FALSE
				    - TRI [1]:FALSE
				    - EMI [2]:FALSE
				  - RLY_CloseMonoCounter:Min: 0
				+ OUT
				  + RLY_relayState
				    - MONO [0]:FALSE
				    - TRI [1]:FALSE
				    - EMI [2]:TRUE
				  - RLY_CloseMonoCounter:1
			"""
			$inputs {
				RLY_CloseMonoCounter = *min*
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$outputs {
				RLY_CloseMonoCounter = 1
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}

		$teststep 2.2 {
			$name "1x10ms"
			$uuid "6c40d9c1-f0fe-460f-b6d9-ed8df9f84417"
			$specification """
				+ IN
				  + RLY_relayState
				    - MONO [0]:FALSE
				    - TRI [1]:FALSE
				    - EMI [2]:FALSE
				  - RLY_CloseMonoCounter:Ignore.
				+ OUT
				  + RLY_relayState
				    - MONO [0]:FALSE
				    - TRI [1]:FALSE
				    - EMI [2]:TRUE
				  - RLY_CloseMonoCounter:2
			"""
			$inputs {
				RLY_CloseMonoCounter = *none*
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$outputs {
				RLY_CloseMonoCounter = 2
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}

		$teststep 2.3 {
			$name "2x10ms"
			$uuid "ba6faf54-6db5-4ad8-b3e4-4548cbf05dab"
			$specification """
				+ IN
				  + RLY_relayState
				    - MONO [0]:FALSE
				    - TRI [1]:FALSE
				    - EMI [2]:FALSE
				  - RLY_CloseMonoCounter:Ignore.
				+ OUT
				  + RLY_relayState
				    - MONO [0]:FALSE
				    - TRI [1]:FALSE
				    - EMI [2]:TRUE
				  - RLY_CloseMonoCounter:3
			"""
			$inputs {
				RLY_CloseMonoCounter = *none*
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$outputs {
				RLY_CloseMonoCounter = 3
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}

		$teststep 2.4 {
			$name "Jump to 49x10ms"
			$uuid "241d108c-3dd8-435d-abe5-cbe97f240806"
			$specification """
				+ IN
				  + RLY_relayState
				    - MONO [0]:FALSE
				    - TRI [1]:FALSE
				    - EMI [2]:FALSE
				  - RLY_CloseMonoCounter:RLY_DELAY_500ms - 1 49
				+ OUT
				  + RLY_relayState
				    - MONO [0]:FALSE
				    - TRI [1]:FALSE
				    - EMI [2]:TRUE
				  - RLY_CloseMonoCounter:RLY_DELAY_500ms 50
			"""
			$inputs {
				RLY_CloseMonoCounter = RLY_DELAY_500ms - 1
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$outputs {
				RLY_CloseMonoCounter = RLY_DELAY_500ms
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}

		$teststep 2.5 {
			$name "50x10ms - RelayMono Confirmed"
			$uuid "d57d2da9-3f0b-4599-a5a7-00e25ef6d444"
			$specification """
				+ IN
				  + RLY_relayState
				    - MONO [0]:FALSE
				    - TRI [1]:FALSE
				    - EMI [2]:FALSE
				  - RLY_CloseMonoCounter:Ignore.
				+ OUT
				  + RLY_relayState
				    - MONO [0]:TRUE
				    - TRI [1]:FALSE
				    - EMI [2]:TRUE
				  - RLY_CloseMonoCounter:RLY_DELAY_500ms 50
			"""
			$inputs {
				RLY_CloseMonoCounter = *none*
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
			}
			$outputs {
				RLY_CloseMonoCounter = RLY_DELAY_500ms
				RLY_relayState[0] = TRUE
				RLY_relayState[1] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}
}