$testobject {

	$testcase 1 {
		$name "0 ms - FALSE previous output"
		$uuid "90728ea0-199f-4f19-afa2-bd462580341b"
		$specification """
			+ IN
			  + RLY_relayState
			    - RLY_relayState[0] RLY_RELAY_MONO:FALSE
			    - RLY_relayState[1] RLY_RELAY_PREC:FALSE
			    - RLY_relayState[2] RLY_RELAY_EMI:FALSE
			  - OpenCounter:Min: 0 ms
			+ OUT
			  + RLY_relayState
			    - RLY_relayState[0] RLY_RELAY_MONO:FALSE
			    - RLY_relayState[1] RLY_RELAY_PREC:FALSE
			    - RLY_relayState[2] RLY_RELAY_EMI:FALSE
			  - OpenCounter:1 10 ms
		"""
		$description """
			0 ms with FALSE previous outputs
		"""

		$teststep 1.1 {
			$name ""
			$uuid "2927dc30-b109-4132-918e-e46a479d2c8b"
			$inputs {
				RLY_OpenCounter = *min*
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				RLY_relayState[2] = FALSE
			}
			$outputs {
				RLY_OpenCounter = 1
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				RLY_relayState[2] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 2 {
		$name "10 ms - FALSE previous output"
		$uuid "a75fe76b-22a8-4db5-9f46-6d4abea62a48"
		$specification """
			+ IN
			  + RLY_relayState
			    - RLY_relayState[0] RLY_RELAY_MONO:FALSE
			    - RLY_relayState[1] RLY_RELAY_PREC:FALSE
			    - RLY_relayState[2] RLY_RELAY_EMI:FALSE
			  - OpenCounter:1 10 ms
			+ OUT
			  + RLY_relayState
			    - RLY_relayState[0] RLY_RELAY_MONO:FALSE
			    - RLY_relayState[1] RLY_RELAY_PREC:FALSE
			    - RLY_relayState[2] RLY_RELAY_EMI:FALSE
			  - OpenCounter:2 20 ms
		"""
		$description """
			10 ms with FALSE previous outputs
		"""

		$teststep 2.1 {
			$name ""
			$uuid "fb0c5d39-9cb4-4075-865d-200e1d4b4737"
			$inputs {
				RLY_OpenCounter = 1
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				RLY_relayState[2] = FALSE
			}
			$outputs {
				RLY_OpenCounter = 2
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				RLY_relayState[2] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 3 {
		$name "190 ms - FALSE previous output"
		$uuid "93fe0c88-9886-43cc-9edf-344b52d59b20"
		$specification """
			+ IN
			  + RLY_relayState
			    - RLY_relayState[0] RLY_RELAY_MONO:FALSE
			    - RLY_relayState[1] RLY_RELAY_PREC:FALSE
			    - RLY_relayState[2] RLY_RELAY_EMI:FALSE
			  - OpenCounter:19 190 ms
			+ OUT
			  + RLY_relayState
			    - RLY_relayState[0] RLY_RELAY_MONO:FALSE
			    - RLY_relayState[1] RLY_RELAY_PREC:FALSE
			    - RLY_relayState[2] RLY_RELAY_EMI:FALSE
			  - OpenCounter:20 200 ms
		"""
		$description """
			190 ms with FALSE previous outputs
		"""

		$teststep 3.1 {
			$name ""
			$uuid "fadd535a-e6bc-496d-aca8-4b4e6c711929"
			$inputs {
				RLY_OpenCounter = 19
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				RLY_relayState[2] = FALSE
			}
			$outputs {
				RLY_OpenCounter = 20
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				RLY_relayState[2] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 7 {
		$name "200 ms - FALSE previous output"
		$uuid "b39be4f8-e858-43bb-93c6-392247ff70ee"
		$specification """
			+ IN
			  + RLY_relayState
			    - RLY_relayState[0] RLY_RELAY_MONO:FALSE
			    - RLY_relayState[1] RLY_RELAY_PREC:FALSE
			    - RLY_relayState[2] RLY_RELAY_EMI:FALSE
			  - OpenCounter:19 190 ms
			+ OUT
			  + RLY_relayState
			    - RLY_relayState[0] RLY_RELAY_MONO:FALSE
			    - RLY_relayState[1] RLY_RELAY_PREC:FALSE
			    - RLY_relayState[2] RLY_RELAY_EMI:FALSE
			  - OpenCounter:20 200 ms
		"""
		$description """
			200 ms with FALSE previous outputs
		"""

		$teststep 7.1 {
			$name ""
			$uuid "1dca613d-5653-45b4-a379-b0040bfeb6c9"
			$inputs {
				RLY_OpenCounter = 19
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				RLY_relayState[2] = FALSE
			}
			$outputs {
				RLY_OpenCounter = 20
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				RLY_relayState[2] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 8 {
		$name "210 ms - FALSE previous output"
		$uuid "95bb2653-717a-46d9-9aa1-f60328b6c16f"
		$specification """
			+ IN
			  + RLY_relayState
			    - RLY_relayState[0] RLY_RELAY_MONO:FALSE
			    - RLY_relayState[1] RLY_RELAY_PREC:FALSE
			    - RLY_relayState[2] RLY_RELAY_EMI:FALSE
			  - OpenCounter:20 200 ms
			+ OUT
			  + RLY_relayState
			    - RLY_relayState[0] RLY_RELAY_MONO:FALSE
			    - RLY_relayState[1] RLY_RELAY_PREC:FALSE
			    - RLY_relayState[2] RLY_RELAY_EMI:FALSE
			  - OpenCounter:20 200 ms
		"""
		$description """
			210 ms with FALSE previous outputs
		"""

		$teststep 8.1 {
			$name ""
			$uuid "466e4425-e9cf-4e0f-a1ad-618dc3804733"
			$inputs {
				RLY_OpenCounter = 20
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				RLY_relayState[2] = FALSE
			}
			$outputs {
				RLY_OpenCounter = 20
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				RLY_relayState[2] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 9 {
		$name "390 ms - FALSE previous output"
		$uuid "19aaa0e6-d55d-41c5-bb79-675ccb12c662"
		$specification """
			+ IN
			  + RLY_relayState
			    - RLY_relayState[0] RLY_RELAY_MONO:FALSE
			    - RLY_relayState[1] RLY_RELAY_PREC:FALSE
			    - RLY_relayState[2] RLY_RELAY_EMI:FALSE
			  - OpenCounter:21 210 ms
			+ OUT
			  + RLY_relayState
			    - RLY_relayState[0] RLY_RELAY_MONO:FALSE
			    - RLY_relayState[1] RLY_RELAY_PREC:FALSE
			    - RLY_relayState[2] RLY_RELAY_EMI:FALSE
			  - OpenCounter:21 210 ms
		"""
		$description """
			390 ms with FALSE previous outputs
		"""

		$teststep 9.1 {
			$name ""
			$uuid "b96bacd4-4d1d-4427-b0d6-59663ae9fe72"
			$inputs {
				RLY_OpenCounter = 21
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				RLY_relayState[2] = FALSE
			}
			$outputs {
				RLY_OpenCounter = 21
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				RLY_relayState[2] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 10 {
		$name "200 ms - TRUE previous output"
		$uuid "9104d3d0-856e-4e47-b242-eab6d90ef52c"
		$specification """
			+ IN
			  + RLY_relayState
			    - RLY_relayState[0] RLY_RELAY_MONO:TRUE
			    - RLY_relayState[1] RLY_RELAY_PREC:TRUE
			    - RLY_relayState[2] RLY_RELAY_EMI:TRUE
			  - OpenCounter:19 190 ms
			+ OUT
			  + RLY_relayState
			    - RLY_relayState[0] RLY_RELAY_MONO:FALSE
			    - RLY_relayState[1] RLY_RELAY_PREC:FALSE
			    - RLY_relayState[2] RLY_RELAY_EMI:TRUE
			  - OpenCounter:20 200 ms
		"""
		$description """
			200 ms with TRUE previous outputs
		"""

		$teststep 10.1 {
			$name ""
			$uuid "5c11624d-b4ff-4606-b32c-84c8b4fb2d6b"
			$inputs {
				RLY_OpenCounter = 19
				RLY_relayState[0] = TRUE
				RLY_relayState[1] = TRUE
				RLY_relayState[2] = TRUE
			}
			$outputs {
				RLY_OpenCounter = 20
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				RLY_relayState[2] = TRUE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 13 {
		$name "400 ms - FALSE previous output"
		$uuid "e66ac891-ac2e-45a7-8bf5-cca66e1eeadc"
		$specification """
			+ IN
			  + RLY_relayState
			    - RLY_relayState[0] RLY_RELAY_MONO:FALSE
			    - RLY_relayState[1] RLY_RELAY_PREC:FALSE
			    - RLY_relayState[2] RLY_RELAY_EMI:FALSE
			  - OpenCounter:40 400 ms
			+ OUT
			  + RLY_relayState
			    - RLY_relayState[0] RLY_RELAY_MONO:FALSE
			    - RLY_relayState[1] RLY_RELAY_PREC:FALSE
			    - RLY_relayState[2] RLY_RELAY_EMI:FALSE
			  - OpenCounter:40 400 ms
		"""
		$description """
			400 ms with FALSE previous outputs
		"""

		$teststep 13.1 {
			$name ""
			$uuid "0434ad9d-b450-438d-bce7-e5feccafda33"
			$inputs {
				RLY_OpenCounter = 40
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				RLY_relayState[2] = FALSE
			}
			$outputs {
				RLY_OpenCounter = 40
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				RLY_relayState[2] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 14 {
		$name "410 ms - FALSE previous output"
		$uuid "85af3dcf-1c49-4859-ab0e-523d44ed39f6"
		$specification """
			+ IN
			  + RLY_relayState
			    - RLY_relayState[0] RLY_RELAY_MONO:FALSE
			    - RLY_relayState[1] RLY_RELAY_PREC:FALSE
			    - RLY_relayState[2] RLY_RELAY_EMI:FALSE
			  - OpenCounter:41 410 ms
			+ OUT
			  + RLY_relayState
			    - RLY_relayState[0] RLY_RELAY_MONO:FALSE
			    - RLY_relayState[1] RLY_RELAY_PREC:FALSE
			    - RLY_relayState[2] RLY_RELAY_EMI:FALSE
			  - OpenCounter:41 410 ms
		"""
		$description """
			410 ms with FALSE previous outputs
		"""

		$teststep 14.1 {
			$name ""
			$uuid "f29f0073-54f1-4af4-86ac-9a6b68862a49"
			$inputs {
				RLY_OpenCounter = 41
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				RLY_relayState[2] = FALSE
			}
			$outputs {
				RLY_OpenCounter = 41
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				RLY_relayState[2] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 15 {
		$name "2550 ms - FALSE previous output"
		$uuid "64a9bb52-a85d-4a7a-af58-42c4c62f973a"
		$specification """
			+ IN
			  + RLY_relayState
			    - RLY_relayState[0] RLY_RELAY_MONO:FALSE
			    - RLY_relayState[1] RLY_RELAY_PREC:FALSE
			    - RLY_relayState[2] RLY_RELAY_EMI:FALSE
			  - OpenCounter:Max: 255 2550 ms
			+ OUT
			  + RLY_relayState
			    - RLY_relayState[0] RLY_RELAY_MONO:FALSE
			    - RLY_relayState[1] RLY_RELAY_PREC:FALSE
			    - RLY_relayState[2] RLY_RELAY_EMI:FALSE
			  - OpenCounter:Max: 255 2550 ms
		"""
		$description """
			2550(Max) ms with FALSE previous outputs
		"""

		$teststep 15.1 {
			$name ""
			$uuid "992b0873-54f8-4dfe-8508-157ecbd0b16e"
			$inputs {
				RLY_OpenCounter = *max*
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				RLY_relayState[2] = FALSE
			}
			$outputs {
				RLY_OpenCounter = *max*
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				RLY_relayState[2] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 16 {
		$name "400 ms - TRUE previous output"
		$uuid "d6a13373-ba0f-400e-b3a6-756f1ef66e34"
		$specification """
			+ IN
			  + RLY_relayState
			    - RLY_relayState[0] RLY_RELAY_MONO:TRUE
			    - RLY_relayState[1] RLY_RELAY_PREC:TRUE
			    - RLY_relayState[2] RLY_RELAY_EMI:FALSE
			  - OpenCounter:40 400 ms
			+ OUT
			  + RLY_relayState
			    - RLY_relayState[0] RLY_RELAY_MONO:FALSE
			    - RLY_relayState[1] RLY_RELAY_PREC:FALSE
			    - RLY_relayState[2] RLY_RELAY_EMI:FALSE
			  - OpenCounter:40 400 ms
		"""
		$description """
			400 ms with TRUE previous outputs
		"""

		$teststep 16.1 {
			$name ""
			$uuid "8fa37262-aa2d-4b9a-aac1-b6f479b35015"
			$inputs {
				RLY_OpenCounter = 40
				RLY_relayState[0] = TRUE
				RLY_relayState[1] = TRUE
				RLY_relayState[2] = FALSE
			}
			$outputs {
				RLY_OpenCounter = 40
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				RLY_relayState[2] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 17 {
		$name "410 ms - TRUE previous output"
		$uuid "7c5aa056-7f78-42bf-8717-319119dbe880"
		$specification """
			+ IN
			  + RLY_relayState
			    - RLY_relayState[0] RLY_RELAY_MONO:TRUE
			    - RLY_relayState[1] RLY_RELAY_PREC:TRUE
			    - RLY_relayState[2] RLY_RELAY_EMI:FALSE
			  - OpenCounter:41 410 ms
			+ OUT
			  + RLY_relayState
			    - RLY_relayState[0] RLY_RELAY_MONO:FALSE
			    - RLY_relayState[1] RLY_RELAY_PREC:FALSE
			    - RLY_relayState[2] RLY_RELAY_EMI:FALSE
			  - OpenCounter:41 410 ms
		"""
		$description """
			410 ms with TRUE previous outputs
		"""

		$teststep 17.1 {
			$name ""
			$uuid "170f9e0d-50e0-4eb8-b237-522a6d4c5de0"
			$inputs {
				RLY_OpenCounter = 41
				RLY_relayState[0] = TRUE
				RLY_relayState[1] = TRUE
				RLY_relayState[2] = FALSE
			}
			$outputs {
				RLY_OpenCounter = 41
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				RLY_relayState[2] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 18 {
		$name "2550 ms - TRUE previous output"
		$uuid "1665a6aa-deee-47c3-a492-35e6e6f179cb"
		$specification """
			+ IN
			  + RLY_relayState
			    - RLY_relayState[0] RLY_RELAY_MONO:TRUE
			    - RLY_relayState[1] RLY_RELAY_PREC:TRUE
			    - RLY_relayState[2] RLY_RELAY_EMI:FALSE
			  - OpenCounter:Max: 255 2550 ms
			+ OUT
			  + RLY_relayState
			    - RLY_relayState[0] RLY_RELAY_MONO:FALSE
			    - RLY_relayState[1] RLY_RELAY_PREC:FALSE
			    - RLY_relayState[2] RLY_RELAY_EMI:FALSE
			  - OpenCounter:Max: 255 2550 ms
		"""
		$description """
			2550(Max) ms with TRUE previous outputs
		"""

		$teststep 18.1 {
			$name ""
			$uuid "9f61b7f0-cb6d-4af9-8521-802970c400ec"
			$inputs {
				RLY_OpenCounter = *max*
				RLY_relayState[0] = TRUE
				RLY_relayState[1] = TRUE
				RLY_relayState[2] = FALSE
			}
			$outputs {
				RLY_OpenCounter = *max*
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				RLY_relayState[2] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}

	$testcase 19 {
		$name "NULL POINTER"
		$uuid "9deb1f94-6842-4710-b5cc-85ceca6346cc"
		$specification """
			+ IN
			  + RLY_relayState
			    - RLY_relayState[0] RLY_RELAY_MONO:FALSE
			    - RLY_relayState[1] RLY_RELAY_PREC:FALSE
			    - RLY_relayState[2] RLY_RELAY_EMI:FALSE
			  - OpenCounter:1 10 ms
			+ OUT
			  + RLY_relayState
			    - RLY_relayState[0] RLY_RELAY_MONO:FALSE
			    - RLY_relayState[1] RLY_RELAY_PREC:FALSE
			    - RLY_relayState[2] RLY_RELAY_EMI:FALSE
			  - OpenCounter:2 20 ms
			- NULL POINTER:NULL
		"""
		$description """
			NULL POINTER Protection Test Case
		"""

		$teststep 19.1 {
			$name ""
			$uuid "4031ecb7-3605-4c9f-808a-4b567cc4c17c"
			$inputs {
				RLY_OpenCounter = 1
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				RLY_relayState[2] = FALSE
			}
			$outputs {
				RLY_OpenCounter = 2
				RLY_relayState[0] = FALSE
				RLY_relayState[1] = FALSE
				RLY_relayState[2] = FALSE
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}
}