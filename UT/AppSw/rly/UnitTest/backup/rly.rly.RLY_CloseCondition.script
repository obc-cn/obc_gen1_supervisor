$testobject {

	$testcase 1 {
		$name "MONOPHASIC With Minumum Sended Values"
		$uuid "9e10630a-2f13-4b57-bbda-b4e10cc3ef88"
		$specification """
			+ IN
			  + PARAMETERS
			    - RLY_InVoltMode:IVM_MONOPHASIC_INPUT
			  + GLOBALS
			    - VmaxMono VmaxTri VminMono VminTri:Min: 0 0 0 0
			    - RLY_CloseMonoCounter RLY_ClosePreCounter:Max: 255 255
			+ OUT
			  - RLY_CloseMonoCounter:Max: 255
			  - RLY_ClosePreCounter:Max: 255
			  + SENDED PARAMETERS
			    - RLY_ClosePrecharge:Called
			      - DClinkVmax:Min: 0
			      - DClinkVmin:Min: 0
		"""
		$description """
			Monophasic Voltage Mode with Minimum sended values to ClosePrecharge
		"""

		$teststep 1.1 {
			$name ""
			$uuid "033484fc-ba63-4f48-b6d0-7c535a731848"
			$inputs {
				RLY_CloseMonoCounter = *max*
				RLY_ClosePreCounter = *max*
				RLY_InVoltMode = IVM_MONOPHASIC_INPUT
			}
			$outputs {
				RLY_CloseMonoCounter = *max*
				RLY_ClosePreCounter = *max*
			}
			$calltrace {
				RLY_CloseMono
				RLY_ClosePrecharge
			}
		}
	}

	$testcase 2 {
		$name "MONOPHASIC With ID Sended Values"
		$uuid "197d74b6-575f-4b8e-9bd5-517f21b4e89d"
		$specification """
			+ IN
			  + PARAMETERS
			    - RLY_InVoltMode:IVM_MONOPHASIC_INPUT
			  + GLOBALS
			    - VmaxMono VmaxTri VminMono VminTri:ID: 20 21 22 23
			    - RLY_CloseMonoCounter RLY_ClosePreCounter:Max: 255 255
			+ OUT
			  - RLY_CloseMonoCounter:Max: 255
			  - RLY_ClosePreCounter:Max: 255
			  + SENDED PARAMETERS
			    - RLY_ClosePrecharge:Called
			      - DClinkVmax:VmaxMono Id: 20
			      - DClinkVmin:VminMono Id: 22
		"""
		$description """
			Monophasic Voltage Mode with ID sended values to ClosePrecharge
		"""

		$teststep 2.1 {
			$name ""
			$uuid "5d91a64b-7d5f-4c5a-9056-428d1bc6e06c"
			$inputs {
				RLY_CloseMonoCounter = *max*
				RLY_ClosePreCounter = *max*
				RLY_InVoltMode = IVM_MONOPHASIC_INPUT
			}
			$outputs {
				RLY_CloseMonoCounter = *max*
				RLY_ClosePreCounter = *max*
			}
			$calltrace {
				RLY_CloseMono
				RLY_ClosePrecharge
			}
		}
	}

	$testcase 3 {
		$name "MONOPHASIC With Maximum Sended Values"
		$uuid "5dccf5a9-8a66-4481-90e7-04972a7e7aae"
		$specification """
			+ IN
			  + PARAMETERS
			    - RLY_InVoltMode:IVM_MONOPHASIC_INPUT
			  + GLOBALS
			    - VmaxMono VmaxTri VminMono VminTri:Max: 65535 65535 255 255
			    - RLY_CloseMonoCounter RLY_ClosePreCounter:Max: 255 255
			+ OUT
			  - RLY_CloseMonoCounter:Max: 255
			  - RLY_ClosePreCounter:Max: 255
			  + SENDED PARAMETERS
			    - RLY_ClosePrecharge:Called
			      - DClinkVmax:Max: 65535
			      - DClinkVmin:Max: 255
		"""
		$description """
			Monophasic Voltage Mode with Maximum sended values to ClosePrecharge
		"""

		$teststep 3.1 {
			$name ""
			$uuid "4d58c696-51ce-4bb5-9990-eb8f3193cd4e"
			$inputs {
				RLY_CloseMonoCounter = *max*
				RLY_ClosePreCounter = *max*
				RLY_InVoltMode = IVM_MONOPHASIC_INPUT
			}
			$outputs {
				RLY_CloseMonoCounter = *max*
				RLY_ClosePreCounter = *max*
			}
			$calltrace {
				RLY_CloseMono
				RLY_ClosePrecharge
			}
		}
	}

	$testcase 4 {
		$name "TRIPHASIC With Minimum Sended Values"
		$uuid "91fa8fe2-bd2f-47dd-a1d7-051172aad40d"
		$specification """
			+ IN
			  + PARAMETERS
			    - RLY_InVoltMode:IVM_TRIPHASIC_INPUT
			  + GLOBALS
			    - VmaxMono VmaxTri VminMono VminTri:Min: 0 0 0 0
			    - RLY_CloseMonoCounter RLY_ClosePreCounter:Max: 255 255
			+ OUT
			  - RLY_CloseMonoCounter:Min: 0
			  - RLY_ClosePreCounter:Max: 255
			  + SENDED PARAMETERS
			    - RLY_ClosePrecharge:Called
			      - DClinkVmax:Min: 0
			      - DClinkVmin:Min: 0
		"""
		$description """
			Triphasic Voltage Mode with Minimum sended values to ClosePrecharge
		"""

		$teststep 4.1 {
			$name ""
			$uuid "e0b21f00-ffd8-4e7d-a081-e4c72207757d"
			$inputs {
				RLY_CloseMonoCounter = *max*
				RLY_ClosePreCounter = *max*
				RLY_InVoltMode = IVM_TRIPHASIC_INPUT
			}
			$outputs {
				RLY_CloseMonoCounter = 0
				RLY_ClosePreCounter = *max*
			}
			$calltrace {
				RLY_ClosePrecharge
			}
		}
	}

	$testcase 5 {
		$name "TRIPHASIC With ID Sended Values"
		$uuid "45d8af24-3ab5-452e-ba67-3444da73427c"
		$specification """
			+ IN
			  + PARAMETERS
			    - RLY_InVoltMode:IVM_TRIPHASIC_INPUT
			  + GLOBALS
			    - VmaxMono VmaxTri VminMono VminTri:ID: 20 21 22 23
			    - RLY_CloseMonoCounter RLY_ClosePreCounter:Max: 255 255
			+ OUT
			  - RLY_CloseMonoCounter:Min: 0
			  - RLY_ClosePreCounter:Max: 255
			  + SENDED PARAMETERS
			    - RLY_ClosePrecharge:Called
			      - DClinkVmax:VmaxTri Id: 21
			      - DClinkVmin:VminTri Id: 23
		"""
		$description """
			Triphasic Voltage Mode with ID sended values to ClosePrecharge
		"""

		$teststep 5.1 {
			$name ""
			$uuid "ace7c764-9fd6-4684-9ef7-050cd331655e"
			$inputs {
				RLY_CloseMonoCounter = *max*
				RLY_ClosePreCounter = *max*
				RLY_InVoltMode = IVM_TRIPHASIC_INPUT
			}
			$outputs {
				RLY_CloseMonoCounter = 0
				RLY_ClosePreCounter = *max*
			}
			$calltrace {
				RLY_ClosePrecharge
			}
		}
	}

	$testcase 6 {
		$name "TRIPHASIC With Maximum Sended Values"
		$uuid "7f230110-ffe2-4076-9d6b-b6185d45d058"
		$specification """
			+ IN
			  + PARAMETERS
			    - RLY_InVoltMode:IVM_TRIPHASIC_INPUT
			  + GLOBALS
			    - VmaxMono VmaxTri VminMono VminTri:Max: 65535 65535 255 255
			    - RLY_CloseMonoCounter RLY_ClosePreCounter:Max: 255 255
			+ OUT
			  - RLY_CloseMonoCounter:Min: 0
			  - RLY_ClosePreCounter:Max: 255
			  + SENDED PARAMETERS
			    - RLY_ClosePrecharge:Called
			      - DClinkVmax:Max: 65535
			      - DClinkVmin:Max: 255
		"""
		$description """
			Triphasic Voltage Mode with Maximum sended values to ClosePrecharge
		"""

		$teststep 6.1 {
			$name ""
			$uuid "f6839a2f-4023-47fd-8c2a-a5c5c3c854db"
			$inputs {
				RLY_CloseMonoCounter = *max*
				RLY_ClosePreCounter = *max*
				RLY_InVoltMode = IVM_TRIPHASIC_INPUT
			}
			$outputs {
				RLY_CloseMonoCounter = 0
				RLY_ClosePreCounter = *max*
			}
			$calltrace {
				RLY_ClosePrecharge
			}
		}
	}

	$testcase 7 {
		$name "Not Allowed Voltage Mode"
		$uuid "d9d5f736-dcd9-4058-9332-f6b67d281219"
		$specification """
			+ IN
			  + PARAMETERS
			    - RLY_InVoltMode:Max: 255
			  + GLOBALS
			    - VmaxMono VmaxTri VminMono VminTri:ID: 20 21 22 23
			    - RLY_CloseMonoCounter RLY_ClosePreCounter:Max: 255 255
			+ OUT
			  - RLY_CloseMonoCounter:Min: 0
			  - RLY_ClosePreCounter:Min: 0
			  + SENDED PARAMETERS
			    - RLY_ClosePrecharge:No Called: Ignore
		"""
		$description """
			Voltage Mode is not allowed so there is no called functions and counters are set to 0.
		"""

		$teststep 7.1 {
			$name ""
			$uuid "51672f83-0be0-41b2-89ee-8620368b3354"
			$inputs {
				RLY_CloseMonoCounter = *max*
				RLY_ClosePreCounter = *max*
				RLY_InVoltMode = *max*
			}
			$outputs {
				RLY_CloseMonoCounter = 0
				RLY_ClosePreCounter = 0
			}
			$calltrace {
				*** No Call Expected ***
			}
		}
	}
}