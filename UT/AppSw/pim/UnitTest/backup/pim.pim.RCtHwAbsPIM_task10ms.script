$testobject {

	$testcase 1 {
		$name "Allowed Period - High Active Time"
		$uuid "88654632-b996-445c-aa58-b452fc09d78e"
		$specification """
			When Frequency is in Range, duty cycle is set as:
			100*ActiveTime/PeriodTime
		"""
		$description """
			Higher Active Time than Period
		"""

		$teststep 1.1 {
			$name ""
			$uuid "d95d6b09-082c-44f6-9310-498dc76e4db9"
			$inputs {
				Dio_ReadChannel() = *none*
				Icu_17_GtmCcu6_GetDutyCycleValues(DutyCycleValues) = target_Icu_17_GtmCcu6_GetDutyCycleValues_DutyCycleValues
				&target_Icu_17_GtmCcu6_GetDutyCycleValues_DutyCycleValues {
					ActiveTime = 250
					PeriodTime = 200
					BufferMarker = 10
				}
			}
			$outputs {
				Dio_ReadChannel(ChannelId) = 0
				Icu_17_GtmCcu6_GetDutyCycleValues(Channel) = 0
				Rte_CpHwAbsPIM_PpDutyControlPilot_DeDutyControlPilot = 100
				Rte_CpHwAbsPIM_PpFreqOutRange_DeFreqOutRange = 0
				&target_Icu_17_GtmCcu6_GetDutyCycleValues_DutyCycleValues {
					ActiveTime = 12
					PeriodTime = 6749808
					BufferMarker = 200
				}
			}
			$calltrace {
				Icu_17_GtmCcu6_GetDutyCycleValues
				RSetIntCANDebugSignal
			}
		}
	}

	$testcase 2 {
		$name "Allowed Period - Low Active Time"
		$uuid "99b21ca6-5b5c-4718-9177-1a3947f615f0"
		$specification """
			When Frequency is in Range, duty cycle is set as:
			100*ActiveTime/PeriodTime
		"""
		$description """
			Lower Active Time than Period
		"""

		$teststep 2.1 {
			$name ""
			$uuid "a751cd20-d477-4611-9cf0-9aeb2bf23208"
			$inputs {
				Dio_ReadChannel() = *none*
				Icu_17_GtmCcu6_GetDutyCycleValues(DutyCycleValues) = target_Icu_17_GtmCcu6_GetDutyCycleValues_DutyCycleValues
				&target_Icu_17_GtmCcu6_GetDutyCycleValues_DutyCycleValues {
					ActiveTime = 10
					PeriodTime = 200
					BufferMarker = 10
				}
			}
			$outputs {
				Dio_ReadChannel(ChannelId) = 0
				Icu_17_GtmCcu6_GetDutyCycleValues(Channel) = 0
				Rte_CpHwAbsPIM_PpDutyControlPilot_DeDutyControlPilot = 5
				Rte_CpHwAbsPIM_PpFreqOutRange_DeFreqOutRange = 0
				&target_Icu_17_GtmCcu6_GetDutyCycleValues_DutyCycleValues {
					ActiveTime = 250
					PeriodTime = 200
					BufferMarker = 200
				}
			}
			$calltrace {
				Icu_17_GtmCcu6_GetDutyCycleValues
				RSetIntCANDebugSignal
			}
		}
	}

	$testcase 3 {
		$name "Allowed Period - Low Active Time With Minimum Period Value"
		$uuid "820c0405-d247-4bcb-bb31-f1dd3091645c"
		$specification """
			When Frequency is in Range, duty cycle is set as:
			100*ActiveTime/PeriodTime
		"""
		$description """
			Function returns maximum value with minimum period and active value
		"""

		$teststep 3.1 {
			$name ""
			$uuid "3c842800-0f74-48d4-b86f-e2402f8c63b6"
			$inputs {
				Dio_ReadChannel() = *none*
				Icu_17_GtmCcu6_GetDutyCycleValues(DutyCycleValues) = target_Icu_17_GtmCcu6_GetDutyCycleValues_DutyCycleValues
				&target_Icu_17_GtmCcu6_GetDutyCycleValues_DutyCycleValues {
					ActiveTime = 190
					PeriodTime = 190
					BufferMarker = 10
				}
			}
			$outputs {
				Dio_ReadChannel(ChannelId) = 0
				Icu_17_GtmCcu6_GetDutyCycleValues(Channel) = 0
				Rte_CpHwAbsPIM_PpDutyControlPilot_DeDutyControlPilot = 100
				Rte_CpHwAbsPIM_PpFreqOutRange_DeFreqOutRange = 0
				&target_Icu_17_GtmCcu6_GetDutyCycleValues_DutyCycleValues {
					ActiveTime = 10
					PeriodTime = 200
					BufferMarker = 200
				}
			}
			$calltrace {
				Icu_17_GtmCcu6_GetDutyCycleValues
				RSetIntCANDebugSignal
			}
		}
	}

	$testcase 4 {
		$name "Allowed Period - Low Active Time With Maximum Period Value"
		$uuid "aad10311-e1c0-434f-b287-bd87709d25ee"
		$specification """
			When Frequency is in Range, duty cycle is set as:
			100*ActiveTime/PeriodTime
		"""
		$description """
			Function returns maximum value with maximum period and active value
		"""

		$teststep 4.1 {
			$name ""
			$uuid "dd144fd2-2784-48af-ac24-c296cdb88d72"
			$inputs {
				Dio_ReadChannel() = *none*
				Icu_17_GtmCcu6_GetDutyCycleValues(DutyCycleValues) = target_Icu_17_GtmCcu6_GetDutyCycleValues_DutyCycleValues
				&target_Icu_17_GtmCcu6_GetDutyCycleValues_DutyCycleValues {
					ActiveTime = 210
					PeriodTime = 210
					BufferMarker = 10
				}
			}
			$outputs {
				Dio_ReadChannel(ChannelId) = 0
				Icu_17_GtmCcu6_GetDutyCycleValues(Channel) = 0
				Rte_CpHwAbsPIM_PpDutyControlPilot_DeDutyControlPilot = 100
				Rte_CpHwAbsPIM_PpFreqOutRange_DeFreqOutRange = 0
				&target_Icu_17_GtmCcu6_GetDutyCycleValues_DutyCycleValues {
					ActiveTime = 190
					PeriodTime = 190
					BufferMarker = 200
				}
			}
			$calltrace {
				Icu_17_GtmCcu6_GetDutyCycleValues
				RSetIntCANDebugSignal
			}
		}
	}

	$testcase 5 {
		$name "Not Allowed Period - High Channel"
		$uuid "eb3e25a9-4e98-45f5-8b29-3d305b633f43"
		$specification """
			When Period is not allowed function has to read Pin state.
			If Pin State is LOW output will be 0%
			If Pin State is HIGH output will be 100%
		"""
		$description """
			Period Out of range and HIGH Pin state
		"""

		$teststep 5.1 {
			$name ""
			$uuid "f6af814b-d5e9-4783-9941-05020d65caa4"
			$specification """
				Modified: If carrier signal is detected, but the frequency is outside the limits, then DutyControlPilot shall be set to 0%.
			"""
			$inputs {
				Dio_ReadChannel() = STD_HIGH
				Icu_17_GtmCcu6_GetDutyCycleValues(DutyCycleValues) = target_Icu_17_GtmCcu6_GetDutyCycleValues_DutyCycleValues
				&target_Icu_17_GtmCcu6_GetDutyCycleValues_DutyCycleValues {
					ActiveTime = PIM_PERIOD_MIN - 1
					PeriodTime = PIM_PERIOD_MIN - 1
					BufferMarker = 10
				}
			}
			$outputs {
				Dio_ReadChannel(ChannelId) = 38
				Icu_17_GtmCcu6_GetDutyCycleValues(Channel) = 0
				Rte_CpHwAbsPIM_PpDutyControlPilot_DeDutyControlPilot = 0
				Rte_CpHwAbsPIM_PpFreqOutRange_DeFreqOutRange = 1
				&target_Icu_17_GtmCcu6_GetDutyCycleValues_DutyCycleValues {
					ActiveTime = 210
					PeriodTime = 210
					BufferMarker = 200
				}
			}
			$calltrace {
				Icu_17_GtmCcu6_GetDutyCycleValues
				Dio_ReadChannel
				RSetIntCANDebugSignal
			}
		}
	}

	$testcase 6 {
		$name "Not Allowed Period - Low Channel"
		$uuid "a5b15051-4255-40a2-9dc9-e27b94e09f14"
		$specification """
			When Period is not allowed function has to read Pin state.
			If Pin State is LOW output will be 0%
			If Pin State is HIGH output will be 100%
		"""
		$description """
			Period Out of range and LOW Pin state
		"""

		$teststep 6.1 {
			$name ""
			$uuid "08700534-9934-47e8-bc6f-b1f99ef6ad35"
			$inputs {
				Dio_ReadChannel() = STD_LOW
				Icu_17_GtmCcu6_GetDutyCycleValues(DutyCycleValues) = target_Icu_17_GtmCcu6_GetDutyCycleValues_DutyCycleValues
				&target_Icu_17_GtmCcu6_GetDutyCycleValues_DutyCycleValues {
					ActiveTime = PIM_PERIOD_MAX + 1
					PeriodTime = PIM_PERIOD_MAX + 1
					BufferMarker = 10
				}
			}
			$outputs {
				Dio_ReadChannel(ChannelId) = 38
				Icu_17_GtmCcu6_GetDutyCycleValues(Channel) = 0
				Rte_CpHwAbsPIM_PpDutyControlPilot_DeDutyControlPilot = 0
				Rte_CpHwAbsPIM_PpFreqOutRange_DeFreqOutRange = 1
				&target_Icu_17_GtmCcu6_GetDutyCycleValues_DutyCycleValues {
					ActiveTime = 189
					PeriodTime = 189
					BufferMarker = 200
				}
			}
			$calltrace {
				Icu_17_GtmCcu6_GetDutyCycleValues
				Dio_ReadChannel
				RSetIntCANDebugSignal
			}
		}
	}

	$testcase 7 {
		$name "0 Period - High Channel"
		$uuid "c413eb0d-e5af-47e7-8cdc-0d2f9149d426"
		$specification """
			If no carrier signal is detected, then the digital value of the pin shall be read from ICP. In this case, [...] If the digital value is HIGH, then DutyControlPilot shall be set to 100%.
		"""
		$description """
			0 Period Value that means that there is no measure and HIGH Pin state
		"""

		$teststep 7.1 {
			$name ""
			$uuid "bfaf8746-a955-41b5-aa8f-5a9cc27faa98"
			$specification """
				If no carrier signal is detected, then the digital value of the pin shall be read from ICP. In this case, if the digital value is LOW, then DutyControlPilot shall be set to 0%. If the digital value is HIGH, then DutyControlPilot shall be set to 100%.
			"""
			$inputs {
				Dio_ReadChannel() = STD_HIGH
				Icu_17_GtmCcu6_GetDutyCycleValues(DutyCycleValues) = target_Icu_17_GtmCcu6_GetDutyCycleValues_DutyCycleValues
				&target_Icu_17_GtmCcu6_GetDutyCycleValues_DutyCycleValues {
					ActiveTime = *min*
					PeriodTime = *min*
					BufferMarker = 10
				}
			}
			$outputs {
				Dio_ReadChannel(ChannelId) = 38
				Icu_17_GtmCcu6_GetDutyCycleValues(Channel) = 0
				Rte_CpHwAbsPIM_PpDutyControlPilot_DeDutyControlPilot = 100
				Rte_CpHwAbsPIM_PpFreqOutRange_DeFreqOutRange = 0
				&target_Icu_17_GtmCcu6_GetDutyCycleValues_DutyCycleValues {
					ActiveTime = 211
					PeriodTime = 211
					BufferMarker = 200
				}
			}
			$calltrace {
				Icu_17_GtmCcu6_GetDutyCycleValues
				Dio_ReadChannel
				RSetIntCANDebugSignal
			}
		}
	}

	$testcase 8 {
		$name "0 Period - Low Channel"
		$uuid "af654a6b-4879-4b2e-86cc-36061733e0a7"
		$specification """
			If no carrier signal is detected, then the digital value of the pin shall be read from ICP. In this case, if the digital value is LOW, then DutyControlPilot shall be set to 0% [...].
		"""
		$description """
			0 Period Value that means that there is no measure and HIGH Pin state
		"""

		$teststep 8.1 {
			$name ""
			$uuid "557fef86-d5c0-4617-99d8-f9b64ed883fa"
			$inputs {
				Dio_ReadChannel() = STD_LOW
				Icu_17_GtmCcu6_GetDutyCycleValues(DutyCycleValues) = target_Icu_17_GtmCcu6_GetDutyCycleValues_DutyCycleValues
				&target_Icu_17_GtmCcu6_GetDutyCycleValues_DutyCycleValues {
					ActiveTime = *min*
					PeriodTime = *min*
					BufferMarker = 10
				}
			}
			$outputs {
				Dio_ReadChannel(ChannelId) = 38
				Icu_17_GtmCcu6_GetDutyCycleValues(Channel) = 0
				Rte_CpHwAbsPIM_PpDutyControlPilot_DeDutyControlPilot = 0
				Rte_CpHwAbsPIM_PpFreqOutRange_DeFreqOutRange = 0
				&target_Icu_17_GtmCcu6_GetDutyCycleValues_DutyCycleValues {
					ActiveTime = 0
					PeriodTime = 0
					BufferMarker = 200
				}
			}
			$calltrace {
				Icu_17_GtmCcu6_GetDutyCycleValues
				Dio_ReadChannel
				RSetIntCANDebugSignal
			}
		}
	}
}