
#ifndef MCU_H_TESSY_DIO
#define MCU_H_TESSY_DIO
typedef uint8   Dio_LevelType;
typedef uint16  Dio_ChannelType;



#define DIO_CHANNEL_2_6                    ((Dio_ChannelType)0x026)

#ifndef DioConf_DioChannel_SUP_DI_CTRL_PILOT
#define DioConf_DioChannel_SUP_DI_CTRL_PILOT (DIO_CHANNEL_2_6)
#endif


extern Dio_LevelType Dio_ReadChannel(Dio_ChannelType ChannelId);


#endif