::-------------------CONFIGURATION----------------------
::-------------------CONFIGURATION----------------------
::-------------------CONFIGURATION----------------------
::-------------------CONFIGURATION----------------------
::-------------------CONFIGURATION----------------------
::##EXCEPTION BACKUP FOLDERS

set Number_Exceptions=2

set project_exception[1]=General_UT
set project_exception[2]=template_autosar

::##MODE CONFIGURATION
set tbs_mode2_title=Execute All Tests
set tbs_mode2_name=Execute_All.tbs

set tbs_mode3_title=Execute And Generate Reports
set tbs_mode3_name=Execute_and_Generate_Reports.tbs

set tbs_mode4_title=Execute_and_Overview_Report
set tbs_mode4_name=Execute_and_Overview_Report.tbs

::#UT FOLDER STRUCT:
set Number_folders_structs=2

set UT_folder_struct[1]=AppSw
set UT_folder_struct[2]=BaseSw
set number

set generic_test_name=General_UT

::-------------------CONFIGURATION----------------------
::-------------------CONFIGURATION----------------------
::-------------------CONFIGURATION----------------------
::-------------------CONFIGURATION----------------------
::-------------------CONFIGURATION----------------------
@ECHO OFF
setlocal enableDelayedExpansion
cls
C:
::______________________________________________________
::______________________________________________________
::______________________________________________________
::________________Load_Configuration____________________
::##EXCEPTION SEARCH
echo ---EXCEPTION SAERCH---
for /L %%k in (1,1,%Number_Exceptions%) do (
	echo !project_exception[%%k]! exception folder:
	for /f %%w in ('FORFILES /P %~dp0 /S /M "!project_exception[%%k]!" /C "cmd /c echo @path"') do (
		set exception[%%k]=%%~w\UnitTest\backup
		echo !exception[%%k]!
	)
)
echo ---EXCEPTION SAERCH---
::##EXCEPTION SEARCH


::##DEFAULT MODES
set tbs_mode1_title=Restore All Backups
set tbs_mode5_title=Execute external .tbs
::##DEFAULT MODES


::##.tbs MODE SEARCH
echo ---.tbs MODE SAERCH---
for /f %%k in ('FORFILES /P %~dp0 /S /M %tbs_mode2_name% /C "cmd /c echo @path"') do (
	set tbs_mode2_folder=%%k
)
for /f %%k in ('FORFILES /P %~dp0 /S /M %tbs_mode3_name% /C "cmd /c echo @path"') do (
	set tbs_mode3_folder=%%k
)
for /f %%k in ('FORFILES /P %~dp0 /S /M %tbs_mode4_name% /C "cmd /c echo @path"') do (
	set tbs_mode4_folder=%%k
)
echo %tbs_mode2_folder%
echo %tbs_mode3_folder%
echo %tbs_mode4_folder%

::##.tbs MODE SEARCH
::________________                  ____________________
::________________Load_Configuration____________________
::______________________________________________________
::______________________________________________________


::______________________________________________________
::______________________________________________________
::______________________________________________________
::________________TessyGen INITIALIZATION_______________

echo ================================
echo =      BACKUP DIRECTORIES:     =
echo ================================
set NumberDirectories= 0


echo =      EXCLUDE:     =
echo Number_Exceptions "%Number_Exceptions%"



echo Folder Exceptions: %exception[1]%
echo Folder Exceptions: %exception[2]%
echo =      EXCLUDE:     =
echo.

::for %%k in (1,1,%Number_folders_structs%) do (
::echo %~dp0!UT_folder_struct[%%k]!
::	for /D %%f in (%~dp0!UT_folder_struct[%%k]!\*) do (
::		echo %%~nxf
::		pause.
::	)
::)

::for /L %%k in (1,1,%NumberDirectories%) do echo Module Name %%k: !Module_Name[%%k]!
::pause.




for /f %%k in ('FORFILES /P %~dp0 /S /M tessy.pdbx /C "cmd /c echo @path"') do (
	for /f %%f in ('FORFILES /P %%~pk /M *.c /C "cmd /c echo @path"') do (
	xcopy /s %%f %~dp0\General_Project\General_UT\UnitTest /y
	)
	for /f %%f in ('FORFILES /P %%~pk /M *.h /C "cmd /c echo @path"') do (
	xcopy /s %%f %~dp0\General_Project\General_UT\UnitTest /y
	)



	for /f %%i in ('FORFILES /P %%~pk /S /M backup /C "cmd /c echo @path"') do (
		set exception_detected="false"
		for /L %%k in (1,1,%Number_Exceptions%) do (
			if %%i=="!exception[%%k]!" (
				set exception_detected="true"		
			)
		)
		if !exception_detected!=="false" (
			set /a NumberDirectories+=1
			set directory_backup[!NumberDirectories!]=%%i
						
			
			for /f %%j in ('FORFILES /P %%i /S /M *.tmb /C "cmd /c echo @path"') do (
			set /a Numbertmb+=1
				set directory_array[!Numbertmb!]=%%j
			)
		) else (
			echo Exception detected
		)
	)
)



for /L %%k in (1,1,%NumberDirectories%) do echo Stored Folder %%k: !directory_backup[%%k]!
for /L %%k in (1,1,%Numbertmb%) do echo Stored TMB %%k: !directory_array[%%k]!

pause.

echo Number of Backups: %Numbertmb%

:Connect
echo ================================
echo ------Connecting to TESSY-------
echo ================================
CD C:\Program Files\Razorcat\TESSY_4.1\bin
tessyd.exe
if %ERRORLEVEL%==0 goto next1
	echo DISCONNECTING from TESSY
	tessycmd disconnect
	echo Tessy Disconnected
	tessyd.exe shutdown
	echo Tessy Closed
	echo Retry To Connect
	GOTO Connect
	if NOT %ERRORLEVEL%==0 goto exit
	tessyd.exe connect
	if %ERRORLEVEL%==0 goto next1
	echo Error connecting to TESSY application
	exit /b
:next1

:PjctList
echo ======================
echo =   PROJECT LIST:    =
echo ======================
tessycmd list-projects
echo CLOSE_LAUNCHER
echo ======================
::set/p SelectedProject= Select Project: 

set SelectedProject=General_UT
echo Your selection is: %SelectedProject%
if "%SelectedProject%"=="CLOSE_LAUNCHER" goto End

tessycmd connect
tessycmd select-project "%SelectedProject%"



echo Selected project in Tessy: 
tessycmd which-project
echo.

if "%SelectedProject%"=="General_UT" (
cls

goto GralUT
) else (
echo Not options configured to the selected project1
goto PjctList
)



::________________TessyGen INITIALIZATION_______________
::______________________________________________________
::______________________________________________________
::______________________________________________________





::______________________________________________________
::______________________________________________________
::______________________________________________________
::__________________TessyGen INTERFACE__________________






:GralUT
echo ================================/
echo =======GENERAL UT OPTIONS=======/
echo ================================/

echo Modes:
echo 0. CLOSE_LAUNCHER
echo 1. %tbs_mode1_title%
echo 2. %tbs_mode2_title%
echo 3. %tbs_mode3_title%
echo 4. %tbs_mode4_title%
echo 5. %tbs_mode5_title%


set/p UT_mode=Select Mode Number:

if %UT_mode%==0 goto End

if NOT DEFINED UT_mode (
	goto GralUT
	cls
)

if %UT_mode%==1 (
	cls
	goto mode1
)


set/p backup=Backup Needed -Y/N-: 
echo "%backup%"

cls
echo Mode Selected: "%UT_mode%"
echo Backup Needed: "%backup%"
goto StMchine

::__________________TessyGen INTERFACE__________________
::______________________________________________________
::______________________________________________________
::______________________________________________________





::______________________________________________________
::______________________________________________________
::______________________________________________________
::____________________TessyGen MODES____________________

:StMchine
if "!backup!"=="Y" (
	goto mode1
) else IF "%UT_mode%"=="1" (
	goto mode1
) else IF "%UT_mode%"=="2" (
	goto mode2
) else IF "%UT_mode%"=="3" (
	goto mode3
) else IF "%UT_mode%"=="4" (
	goto mode4
) else IF "%UT_mode%"=="5" (
	goto mode5
) else IF "%UT_mode%"=="0" (
	goto End
) else goto GralUT



::-----------------------------------------------------------------
::-----------------------------------------------------------------
::-----------------------------------------------------------------
::-----------------------------------------------------------------
::-----------------------------------------------------------------
::-----------------------------------------------------------------
:mode1
echo /////////////////////////////////////
echo ============BACKUP MODE==============

set/p Anlyze_activation=Analyze Needed -Y/N-: 
echo "%Anlyze_activation%"





echo ---------------------------
echo --Test Project COLLECTION--
echo COLLECTION List:
set collection=""
for /f "tokens=*" %%l in ('"cmd /c tessycmd list-test-collections"') do (
	set collection=%%l
	echo %%l
)

if %collection%=="" (
	echo No collections in project - Creating Collection
	tessycmd new-test-collection "%generic_test_name%"
	tessycmd select-test-collection "%generic_test_name%"
) else (
	tessycmd select-test-collection "%collection%"
)
	echo Selected Test Collection: 
	tessycmd which-test-collection
	echo.

echo --Test Project COLLECTION--
echo ---------------------------

echo -----------------------
echo --Test Project FOLDER--
echo Folders List:
set folder=""



for /f "tokens=*" %%l in ('"cmd /c tessycmd list-folders -c"') do (
	set folder=%%l
	echo %%l
)



	echo No folders in project - Creating Folder
	tessycmd new-folder -c %generic_test_name%
) else (
	tessycmd select-folder "%folder%"
)
	echo Selected Test Folder: 
	tessycmd which-folder
	echo.

echo --Test Project FOLDER--
echo -----------------------


echo Backups are going to be restored...
for /L %%k in (1,1,%Numbertmb%) do (
	echo Restoring folder %%k: !directory_array[%%k]!
	tessycmd -animate restore-module !directory_array[%%k]!
)


tessycmd list-modules
if "!Anlyze_activation!"=="Y" (

for /f "tokens=*" %%w in ('"cmd /c tessycmd list-modules"') do (
	tessycmd select-module %%w
	echo analyzing module %%w
	tessycmd -animate analyze
)
echo Analyze finished
)


echo ==========BACKUP FINISHED============
echo \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

if "%UT_mode%"=="1" (
	goto GralUT
) else (
	set backup="N"
	goto StMchine
)
::-----------------------------------------------------------------
::-----------------------------------------------------------------
::-----------------------------------------------------------------
::-----------------------------------------------------------------
::-----------------------------------------------------------------
::-----------------------------------------------------------------
:mode2
echo /////////////////////////////////////
echo ==========%tbs_mode2_title%===========
echo Execute Test...
tessycmd -animate exec-test %tbs_mode2_folder%
echo Finished Execution
echo ========%tbs_mode2_title% Finished=========
echo \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
GOTO GralUT


:mode3
echo /////////////////////////////////////
echo ==========%tbs_mode3_title%===========
echo Execute Test...
tessycmd -animate exec-test %tbs_mode3_folder%
echo Finished Execution
echo ========%tbs_mode3_title% Finished=========
echo \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

GOTO GralUT


:mode4
echo /////////////////////////////////////
echo ==========%tbs_mode4_title%===========
echo Execute Test...
tessycmd -animate exec-test %tbs_mode4_folder%
echo Finished Execution
echo ========%tbs_mode4_title% Finished=========
echo \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

GOTO GralUT

:mode5
echo /////////////////////////////////////
echo ===========.tbs FILE MODE============
set /p tbs_random_folder=Drag your file here:
echo %tbs_random_folder%
pause.


echo Running .tbs file...
tessycmd -animate exec-test "%tbs_random_folder%"
echo Finished Execution
echo =======.tbs FILE MODE FINISHED=======
echo \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

GOTO GralUT

::____________________TessyGen MODES____________________
::______________________________________________________
::______________________________________________________
::______________________________________________________



:Errortbs
echo There is an error in the .tbs execution
GOTO GralUT


:End 
	echo ================================
	echo =      TESSY DISCONNECT:       =
	echo ================================
	echo Disconnecting from TESSY
	tessycmd disconnect
	echo Tessy Disconnected
	tessyd.exe shutdown
	echo Tessy Closed


