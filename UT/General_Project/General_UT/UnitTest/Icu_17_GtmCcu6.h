
#ifndef MCU_H_TESSY_ICU_17_GTMCCU6
#define MCU_H_TESSY_ICU_17_GTMCCU6
typedef uint32 Icu_17_GtmCcu6_ValueType;
typedef uint8 Icu_17_GtmCcu6_ChannelType;  

typedef struct
{
  /* To store ActiveTime for GetDutyCycles API.*/
  Icu_17_GtmCcu6_ValueType  ActiveTime;
  /* To store PeriodTime for GetDutyCycles API.*/
  Icu_17_GtmCcu6_ValueType  PeriodTime;
  /* Buffer marker value */ 
  Icu_17_GtmCcu6_ValueType BufferMarker; 
}Icu_17_GtmCcu6_DutyCycleType;

typedef uint8 Icu_17_GtmCcu6_ChannelType; 



extern void Icu_17_GtmCcu6_StartSignalMeasurement(Icu_17_GtmCcu6_ChannelType Channel);


extern void Icu_17_GtmCcu6_GetDutyCycleValues(Icu_17_GtmCcu6_ChannelType Channel,Icu_17_GtmCcu6_DutyCycleType *DutyCycleValues);


#endif



#ifndef IcuConf_IcuChannel_IcuChannel_0
#define IcuConf_IcuChannel_IcuChannel_0          ((Icu_17_GtmCcu6_ChannelType)0)
#endif