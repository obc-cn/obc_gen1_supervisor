/* Pre-compile/static configuration parameters for DIO  */
#include "Dio_Cfg.h"

/*******************************************************************************
**                      Global Type Definitions                               **
*******************************************************************************/

extern void TRUSTED_ApplSafetyResetEndInit_Timed(uint32 Time);
extern void TRUSTED_ApplSafetySetEndInit_Timed(void);

#define MCU_RUNTIME_RESETSAFETYENDINIT_TIMED(Time) \
                                  (TRUSTED_ApplSafetyResetEndInit_Timed((Time)))
#define MCU_RUNTIME_SETSAFETYENDINIT_TIMED() \
                                          (TRUSTED_ApplSafetySetEndInit_Timed())
                                          
/* Maximum timeout value to acquire the Safety endinit */
#define MCU_SAFETY_ENDINT_TIMEOUT ((uint32)150000U)

/*IFX_MISRA_RULE_19_07_STATUS=To support user modification of OS protected
  calls definition, it is declared as a function like macro*/
#define MCU_SFR_RUNTIME_RESETSAFETYENDINIT_TIMED(Time) \
                                  (MCU_RUNTIME_RESETSAFETYENDINIT_TIMED(Time))

                                  /*IFX_MISRA_RULE_19_07_STATUS=To support user modification of OS protected
  calls definition, it is declared as a function like macro*/
#define MCU_SFR_RUNTIME_SETSAFETYENDINIT_TIMED() \
                                        (MCU_RUNTIME_SETSAFETYENDINIT_TIMED())





/* Type definition for register mapping of DIO hardware */
/*
  This is a normal structure named as Dio_RegType, this depicts the port
  registers
  */

/* Type definition for channel level */
/* DIO089: Possible values for Dio_LevelType are STD_HIGH and STD_LOW */
/* DIO23: Possible levels of DIO channels are STD_HIGH and STD_LOW, these are
   defined in Std_Types.h */
typedef uint8   Dio_LevelType;

/* DIO015: Type definition for numeric id for all configured channels */
/* DIO017: User of DIO drive shall use the symbolic names provided by
   configuration description */
/* DIO052: A general purpose digital IO pin represents a DIO channel */
typedef uint16  Dio_ChannelType;

/* DIO018: Type definition for numeric id for all configured ports */
/* DIO020: User of DIO drive shall use the symbolic names provided by
   configuration description */
/* DIO053: Hardware groups several DIO channels to represent a DIO port */
typedef uint8   Dio_PortType;

/* DIO24: DIO103: Type definition for the port level */
typedef uint16  Dio_PortLevelType;

/* DIO021: DIO056: Type definition for channel group configuration */
/* DIO022: User of DIO drive shall use the symbolic names provided by
   configuration description */
typedef struct Dio_ChannelGroupType
{
  /* Mask that defines the channels that forms this group */
  Dio_PortLevelType   mask;
  /* Offset of LSB of channel group from LSB of port */
  uint8               offset;
  /* Port number, wherein this channel group belongs to */
  Dio_PortType        port;
} Dio_ChannelGroupType;

typedef struct Dio_PortChannelIdType
{
  /* Dio port is configured or not */
  uint32 Dio_PortIdConfig;
  /* configured channel info */
  uint32 Dio_ChannelConfig;
}Dio_PortChannelIdType;

typedef struct Dio_ConfigType
{
  #if(DIO_SAFETY_ENABLE == STD_ON)
  uint32 Dio_MarkerCheckValue;
  #endif /* DIO_SAFETY_ENABLE == STD_ON */
  /* Pointer to Dio port and channel configuration */
  const Dio_PortChannelIdType* Dio_PortChannelConfigPtr;

  #if(DIO_CONFIG_COUNT == 1U)

  /* Pointer to Dio channel group configuration */
  const Dio_ChannelGroupType* Dio_ChannelGroupConfigPtr;
  /* number of groups configured for the configuration */
  uint32 Dio_ChannelGroupConfigSize;

  #else

  /* Post-build symbolic name */
  /* UTP #AI00238118 */

  #if(DIO_CH_GRP_SET_U > 0U)
  const Dio_ChannelGroupType Dio_ChGrpIdMap[DIO_CH_GRP_SET_U];
  #endif

  #if(DIO_PORTPIN_SET_U > 0U)
  const uint16 Dio_PortPinIdMap[DIO_PORTPIN_SET_U];
  #endif

  const uint8 Dio_PortIdMap[DIO_PORT_SET_U];

  #endif

}Dio_ConfigType;