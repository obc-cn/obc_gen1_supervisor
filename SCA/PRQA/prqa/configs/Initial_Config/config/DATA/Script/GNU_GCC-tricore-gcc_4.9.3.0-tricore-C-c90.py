import os, inspect
import sys

# add include paths for CCT stub directories
def stub():
  try:
    scriptFile = inspect.getfile(inspect.currentframe())
    currentDir = os.path.dirname(os.path.abspath(scriptFile))
    fileName = os.path.basename(scriptFile)
    cipFilename = os.path.splitext(fileName.strip())[0]
    normal_parent = os.path.dirname(currentDir)
    isCPP = os.path.basename(normal_parent).endswith("C++")
    dataDir = os.path.dirname(normal_parent)
    includeOption = "-si" if isCPP else "-i"
    cipFilename = cipFilename + ".cip"
    cctDir = os.path.dirname(dataDir)
    configDir = os.path.dirname(cctDir)
    cipDir = os.path.join(configDir, "cip")
    cipFilepath = os.path.join(cipDir, cipFilename)
    stubDir = os.path.join(normal_parent, "Stub")
    cipFile = open(cipFilepath, 'w')

    # stub directory
    print >>cipFile, "-q", "\"" + stubDir + "\""
    for d, ds, fs in os.walk(stubDir):
      if d.endswith("prlforceinclude"):
        for fn in sorted(fs):
          print >>cipFile, "-fi", "\"" + os.path.join(d, fn) + "\""
      else:
        shortName = d[len(stubDir):]
        sepCount = shortName.count(os.sep)
        if (sepCount == 1 and len(fs) != 0) or (sepCount > 1 and len(ds) != 0):
          path = "\"" + d + "\""
          print >>cipFile, includeOption, path
    # add paths from syshdr.lst
    print >>cipFile, "* system include paths"
    templatePath = os.path.join(stubDir, "syshdr.lst")
    dirFile = open(templatePath, "r")
    root = ""
    expectRoot = False
    for sysPath in dirFile:
      sysPath = sysPath.strip()
      if expectRoot:
        expectRoot = False
        root = sysPath
      elif sysPath == ":ROOT:":
        expectRoot = True
      else:
        path = "\"" + os.path.join(root, sysPath) + "\""
        print >>cipFile, includeOption, path
        print >>cipFile, "-q", path
    dirFile.close()
    cipFile.close()
  except IOError:
    pass

if __name__=="__main__":
  stub()

