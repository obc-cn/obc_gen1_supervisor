import os, xlrd, shutil, subprocess, sys
try:
    import logger as logger
except:
    import files.py.logger as logger
    
#Globals
folders_exclude = []
folders_quiet = []
prev_quiet = []
prev_excluded = []

def isQuiet(path):
    for item in prev_quiet: #is subfolder of another quiet
        if path.find(item) != -1:
            prev_quiet.append(path)
            return True
        
    for item in folders_quiet:
        if path.find(item) != -1:
            prev_quiet.append(path)
            return True
    return False

def isExclude(folder):
    for item in prev_excluded: #is subfolder of another exluded
        if folder.find(item) != -1:
            prev_excluded.append(folder)
            return True
  
    for item in folders_exclude:
        if folder.find(item) != -1:
            prev_excluded.append(folder)
            return True
    return False

def scan(path):
    quiet_list = []
    path_list = []
    
    for root, dirs, files in os.walk(path):
        for folder in dirs:
            full_dir = os.path.join(root, folder)
            if not isExclude(full_dir): 
                path_list.append(full_dir)
                if isQuiet(full_dir):
                    quiet_list.append(full_dir)
    return [path_list, quiet_list]

#RUN-----------------------------------------------------------------------------------------------------------------------------------
def run(prqa_project_root_rel, prqa_project_root_abs, prqaTool_folder, source_root):
    
    #Get excludes and quiets from excel
    excel = xlrd.open_workbook("config.xlsx")
    param_sheet = excel.sheet_by_index(0)
    exclude_sheet = excel.sheet_by_index(1)
    quiet_sheet = excel.sheet_by_index(2)

    for i in range(exclude_sheet.nrows):
        folders_exclude.append(exclude_sheet.cell_value(i, 0))
    for i in range(quiet_sheet.nrows):
        folders_quiet.append(quiet_sheet.cell_value(i, 0))

    logger.log('INFO', 'Config loaded OK.')
 
    #If is relative
    if source_root.find("${PROJECT_ROOT}") != -1:
        source_root = source_root.replace("${PROJECT_ROOT}", prqa_project_root_rel) 
        
    
    
    logger.log('INFO', 'The following folders and their subfolders will be IGNORED:')
    for folder in folders_exclude:
        print(folder + ', ', end = '')
    print('\n')
    logger.log('INFO', 'The following folders and their subfolders will be set as QUIET:')
    for folder in folders_quiet:
        print(folder + ', ', end = '')
    print('\n')

    #scan proyect, crate indluce,quit,ignore,define lists
    [path_list, quiet_list] = scan(source_root)
    logger.log('INFO', 'Project folder scanned OK')
        
    #Generate temp files
    if not os.path.isdir(prqaTool_folder + '\\files\\temp'):
        os.mkdir(prqaTool_folder + '\\files\\temp')
        
    #Open files
    temp_acf_path = prqaTool_folder + '\\files\\temp\\project.acf'
    acf_path = prqa_project_root_abs + '\\prqa\\configs\\Initial_Config\\config\\project.acf'
    temp_acf = open(temp_acf_path, 'w')
    acf = open(acf_path)

    #Keep same componenets version
    #headers components
    #QAC -> Defines, include, quiet
    #QACPP -> not used
    #RCMA -> quiet
    #M3CM -> quiet

    line = acf.readline()
    print('start reading')
    while True:
        while line.find('<input_to ') == -1:
            if not line:
                break
            temp_acf.write(line)
            line = acf.readline()
            
        if not line:
            break
        #Is a component
        temp_acf.write(line)
        i = line.find('component=')
        e = line.find('>')
        component = line[i+10:e]
        print('component: '+component)
        #copy unknown attribute
        line = acf.readline()
        while line.find('</input_to>') == -1:
            i = line.find('name=')
            e = line.find('/>')
            atribute = line[i+5:e]
            if (atribute != '"-quiet "') and (atribute != '"-i "'):
                    temp_acf.write(line)
            line = acf.readline()
        #componenet end
        #include
        if component == '"qac"' :
            for path in path_list:
                if path.find(source_root) != -1: 
                    path = path.replace(source_root, '${SOURCE_ROOT}')
                temp_acf.write('      <option argument="'+path+'" name="-i "/>\n')
        
        #quiet
        if (component == '"qac"') or (component == '"rcma"') or (component == '"m3cm"'):
             for path in quiet_list:
                 if path.find(source_root) != -1:
                      path = path.replace(source_root, '${SOURCE_ROOT}')
                 temp_acf.write('      <option argument="'+path+'" name="-quiet "/>\n')
            
        temp_acf.write(line) #</input_to>
        line = acf.readline()
    
    temp_acf.close()
    acf.close()

    try:
        shutil.move(temp_acf_path, acf_path)
    except Exception as e:
        print(e)
        logger.log('ERROR', 'Error deleting temp files.')




print("""  _____  _____   ____         _______          _ 
 |  __ \\|  __ \\ / __ \\     /\\|__   __|        | |
 | |__) | |__) | |  | |   /  \\  | | ___   ___ | |
 |  ___/|  _  /| |  | |  / /\\ \\ | |/ _ \\ / _ \\| |
 | |    | | \\ \\| |__| | / ____ \\| | (_) | (_) | |
 |_|    |_|  \\_\\\\___\\_\\/_/    \\_\\_|\\___/ \\___/|_|
                                                 """)
print('PRQATool for OBCP11 v1. MAHLE Electronics. Apr-2019.\n')

prqaTool_folder = os.path.dirname(os.path.realpath(__file__))

#Get prqa project folder
excel = xlrd.open_workbook("config.xlsx")
param_sheet = excel.sheet_by_index(0)
if param_sheet.cell_value(1, 1)[-1] == '\\':
    prqa_project_root = param_sheet.cell_value(1, 1)[0:-1]
else:
    prqa_project_root = param_sheet.cell_value(1, 1)

#Get relative project root
if prqa_project_root[0] == '\\' or prqa_project_root[0] == '.': #is relative
    prqa_project_root_rel = prqa_project_root
    prqa_project_root_abs = os.path.abspath(prqa_project_root)

else: #is absolute
    prqa_project_root_rel = prqa_project_root
    prqa_project_root_abs = prqa_project_root

#Get prqa project file
prqa_project_xml_path = prqa_project_root_rel + '\\prqaproject.xml'

#Get source root project folder
qa_framework_path = prqa_project_root_rel + '\\prqa\\configs\\Initial_Config\\config\\qa-framework-app.xml'

try:
    source_root = ''
    f = open(qa_framework_path)
    for line in f:
        if line.find('<root_path path=') != -1:
            st = line.find('"') + 1 
            end = line.find('" name=')
            source_root = line[st:end]
            f.close()
            break
    if source_root == '':
        raise Exception("SOURCE_ROOT was not filled properly. If you created the project using PRQA GUI instead of Eclipse plugin, SOURCE_ROOT must be filled manually in Project Properties.")

except Exception as e:
    logger.log('ERROR', 'Error finding SOURCE_ROOT' + '\n' + str(e))
    logger.log('INFO', 'Finished with errors. ')
    input()
    quit()

try:
    logger.log('INFO', 'Updateing includes.\n')
    run(prqa_project_root_rel, prqa_project_root_abs, prqaTool_folder, source_root) 
except:
    logger.log('ERROR', 'Error configuring .acf.\n')
    logger.log('INFO', 'Finished with errors. ')
    input()
    quit()

logger.log('INFO', 'Finished. ')

input()

