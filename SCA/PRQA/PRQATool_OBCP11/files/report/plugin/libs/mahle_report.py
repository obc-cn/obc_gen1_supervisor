class METRIC():

        
    def __init__(self, type, name, max = None, min = None, max_sft = None, min_sft = None, value = None, unit = "", description="Not implemented"):
        self.type = type;
        self.name = name;
        self.max = max;
        self.min = min;
        self.max_sft = max_sft;
        self.min_sft = min_sft;
        self.failed = False;
        self.failed_sft = False;
        self.unit = unit;
        self.description = description;
        self.value = None;
        if value: self.addValue(value);
        
    def getMax(self,safety):

        if (safety == True):
            if self.max:
                return self.max_sft
            else:
                return None;
        elif (safety == False):
            if self.max:
                return self.max
            else:
                return None;     
        else:
            return None;

    def getMin(self,safety):

        if (safety == True):
            if self.min:
                return self.min_sft
            else:
                return None;
        elif (safety == False):
            if self.min:
                return self.min
            else:
                return None;     
        else:
            return None;
        
    def hasLimit(self,safety):
        if(safety):
            if(self.max_sft != None or self.min_sft != None): return True;
        else:
            if(self.max != None or self.min != None): return True;
        return False;
    
    def addValue(self,value):
        self.value = float(value);
       
        if self.max:
            if self.value > self.max: 
                self.failed = True
        if self.min:
            if self.value < self.min: self.failed = True
            
        if self.max_sft:
            if self.value > self.max_sft: self.failed_sft = True
        if self.min_sft:
            if self.value < self.min_sft: self.failed_sft = True  
            
    def getResult(self, safety):
        if(safety):
            return self.failed_sft;
        else:
            return self.failed;
