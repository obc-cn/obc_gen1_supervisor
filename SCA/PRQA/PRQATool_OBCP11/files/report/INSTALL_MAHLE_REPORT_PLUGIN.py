import os, shutil

report_plugins_path = "C:\\PRQA\\PRQA-Framework-2.2.2\\report_plugins"
prqaTool_folder = os.path.dirname(os.path.realpath(__file__))


print("Copying files...\n")
try:
    shutil.copy(prqaTool_folder + '\\plugin\\AA_Mahle_Report.py', report_plugins_path)

    if os.path.isdir(report_plugins_path + '\\resources'):
        shutil.rmtree(report_plugins_path + '\\resources')
    shutil.copytree(prqaTool_folder + '\\plugin\\resources', report_plugins_path + '\\resources')

    if os.path.isdir(report_plugins_path + '\\libs'):
        shutil.rmtree(report_plugins_path + '\\libs')
    shutil.copytree(prqaTool_folder + '\\plugin\\libs', report_plugins_path + '\\libs')

    print("MAHLE report plugin installed or updated OK.")
except Exception as e:
    print(e)
    print("Error installing the plugin.")

input()