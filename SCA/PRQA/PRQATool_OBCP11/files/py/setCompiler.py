import shutil, os
from distutils.dir_util import copy_tree
try:
    import logger as logger
except:
    import files.py.logger as logger

compiler_version = "4.9.3.0"
prqa_install_folder = "C:\\PRQA\\PRQA-Framework-2.2.2"

def run(prqa_project_xml_path, prqa_project_root, prqaTool_folder):

    #Modify prqaproject.xml
    prqaproject_xml = []
    try:
        f = open(prqa_project_xml_path)
        for line in f:
            prqaproject_xml.append(line)
        f.close()

        f = open(prqa_project_xml_path, 'w')
        for line in prqaproject_xml:
            if line.find('cct ') != -1:
                f.write('    <cct target="C" active="yes" name="GNU_GCC-tricore-gcc_' + compiler_version + '-tricore-C-c90.cct"/>\n')
            else:
                f.write(line)
        f.close()
        logger.log('INFO', 'prqaproject.xml patched OK.')
    except Exception as e:
        raise Exception("[ERROR] Error modifying prqaproject.xml" + '\n' + str(e))

    #Install compiler
    try:
        if os.path.isfile(prqa_install_folder + '\\config\\cct\\GNU_GCC-tricore-gcc_' + compiler_version + '-tricore-C-c90.cct'):
            os.remove(prqa_install_folder + '\\config\\cct\\GNU_GCC-tricore-gcc_' + compiler_version + '-tricore-C-c90.cct')
        shutil.copy(prqaTool_folder + '\\files\\compiler\\GNU_GCC-tricore-gcc_' + compiler_version + '-tricore-C-c90.cct', prqa_install_folder + '\\config\\cct\\GNU_GCC-tricore-gcc_' + compiler_version + '-tricore-C-c90.cct')
        
        if os.path.isdir(prqa_install_folder + '\\config\\cct\\DATA\\GNU_GCC-tricore-gcc_' + compiler_version + '-tricore-C'):
            shutil.rmtree(prqa_install_folder + '\\config\\cct\\DATA\\GNU_GCC-tricore-gcc_' + compiler_version + '-tricore-C')
        shutil.copytree(prqaTool_folder + '\\files\\compiler\\GNU_GCC-tricore-gcc_' + compiler_version + '-tricore-C', prqa_install_folder + '\\config\\cct\\DATA\\GNU_GCC-tricore-gcc_' + compiler_version + '-tricore-C')
    except Exception as e:
        raise Exception("[ERROR] Error installing compiler." + '\n' + str(e))
    logger.log('INFO', 'Compiler installed OK.')

    #Copy .cip
    cip_folder = prqa_project_root + '\\prqa\\configs\\Initial_Config\\cip'
    try:
        shutil.rmtree(cip_folder, ignore_errors=True)
        os.mkdir(cip_folder)
        shutil.copy(prqaTool_folder + '\\files\\compiler\\GNU_GCC-tricore-gcc_' + compiler_version + '-tricore-C-c90.cip', cip_folder + '\\GNU_GCC-tricore-gcc_' + compiler_version + '-tricore-C-c90.cip')
    except Exception as e:
        raise Exception("[ERROR] Error copying cip" + '\n' + str(e))

    #Copy CCT and DATA

    cct_folder = prqa_project_root + '\\prqa\\configs\\Initial_Config\\config'
    try:
        shutil.rmtree(cct_folder + '\\DATA', ignore_errors=True)
    except:
        logger.log('WARNING', 'Compiler folder not found.')
        pass
    try:
        #Delete CCT
        for root, dirs, files in os.walk(cct_folder):
            for name in files:
                if name.find('.cct') != -1:
                    os.remove(root + '\\' + name)
                    break
    except:
        logger.log('WARNING', '.cct file not found.')
        pass
    try:
        shutil.copy(prqaTool_folder + '\\files\\compiler\\GNU_GCC-tricore-gcc_' + compiler_version + '-tricore-C-c90.cct', cct_folder)
        os.mkdir(cct_folder + '\\DATA')
        copy_tree(prqaTool_folder + '\\files\\compiler\\GNU_GCC-tricore-gcc_' + compiler_version + '-tricore-C', cct_folder + '\\DATA')
        logger.log('INFO', 'Compiler files copied OK.')
    except Exception as e:
        raise Exception("[ERROR] Error adding CCT and DATA" + '\n' + str(e))