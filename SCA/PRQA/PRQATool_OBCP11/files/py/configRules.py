import shutil, os

def run(prqa_project_root, prqaTool_folder):
    
    #Copy .rcf file
    try:
        if os.path.isfile(prqa_project_root + '\\prqa\\configs\\Initial_Config\\config\\project.rcf'):
            os.remove(prqa_project_root + '\\prqa\\configs\\Initial_Config\\config\\project.rcf')
        shutil.copy(prqaTool_folder + '\\files\\rules\\project.rcf', prqa_project_root + '\\prqa\\configs\\Initial_Config\\config\\project.rcf')
    except Exception as e:
        raise Exception("[ERROR] Error copying .rcf" + '\n' + str(e))