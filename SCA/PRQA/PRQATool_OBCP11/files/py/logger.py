#Logger. Pedro Fernández. Apr-2019.
import datetime

Yes = True
No = False

#CONFIG---------------------------------
width = 50
add_timestamp = No
log_to_file = No
log_file_path = "log.txt"
#---------------------------------------


def log(error_type, message):

    if add_timestamp:
        timestamp = str(datetime.datetime.now()).split(' ')[1].split('.')[0]
    else:
        timestamp = ''

    spaces = (width - len(error_type) - len(message) - len(timestamp) - 3)
    out = '[' + error_type + '] ' + message + (' ' * spaces) + timestamp

    print(out)
    if log_to_file:
        f = open(log_file_path, 'a')
        f.write(out + '\n')
        f.close()
