import os, xlrd, shutil, subprocess, sys
try:
    import logger as logger
except:
    import files.py.logger as logger

#Globals
folders_exclude = []
folders_quiet = []
prev_quiet = []
prev_excluded = []
acf_path = ''
prqa_version = '2.2.2'
qacli_path = 'C:\\PRQA\\PRQA-Framework-' + prqa_version + '\\common\\bin\\qacli.exe'
qac_component = 'qac-9.3.1'
qacpp_component = 'qac-4.2.0'
rcma_component = 'rcma-1.6.0'
m3cm_component = 'm3cm-2.3.1'


def qacli(command, dismiss_errors = False):
    output = ""
    try:
        result = subprocess.run(qacli_path + ' ' + command, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
        output = result.stdout
    except subprocess.CalledProcessError as e:
        if not dismiss_errors:
            print("Error:\n" + e.stdout + "\n" + e.stderr)
            sys.exit(1)
        else:
            pass
    except Exception as e:
        sys.exit(1)
    #print(output)
    return output

def isQuiet(path):
    for item in prev_quiet: #is subfolder of another quiet
        if path.find(item) != -1:
            prev_quiet.append(path)
            return True
        
    for item in folders_quiet:
        if path.find(item) != -1:
            prev_quiet.append(path)
            return True
    return False

def isExclude(folder):
    for item in prev_excluded: #is subfolder of another exluded
        if folder.find(item) != -1:
            prev_excluded.append(folder)
            return True
  
    for item in folders_exclude:
        if folder.find(item) != -1:
            prev_excluded.append(folder)
            return True
    return False

def scan(path):
    quiet_list = []
    path_list = []
    
    for root, dirs, files in os.walk(path):
        for folder in dirs:
            full_dir = os.path.join(root, folder)
            if not isExclude(full_dir): 
                path_list.append(full_dir)
                if isQuiet(full_dir):
                    quiet_list.append(full_dir)
    return [path_list, quiet_list]

#RUN-----------------------------------------------------------------------------------------------------------------------------------
def run(prqa_project_root_rel, prqa_project_root_abs, prqaTool_folder, source_root):

    #Get excludes and quiets from excel
    excel = xlrd.open_workbook("config.xlsx")
    param_sheet = excel.sheet_by_index(0)
    exclude_sheet = excel.sheet_by_index(1)
    quiet_sheet = excel.sheet_by_index(2)

    for i in range(exclude_sheet.nrows):
        folders_exclude.append(exclude_sheet.cell_value(i, 0))
    for i in range(quiet_sheet.nrows):
        folders_quiet.append(quiet_sheet.cell_value(i, 0))

    logger.log('INFO', 'Config loaded OK.')

    #Add components in case the project is new
    logger.log('INFO', 'Adding components...')

    qacli('pprops -c ' + rcma_component + ' --add -T C/C++ -P "' + prqa_project_root_abs + '"')
    qacli('pprops -c ' + m3cm_component + ' --add -T C -P "' + prqa_project_root_abs + '"')

    logger.log('INFO', 'Components added OK.')

    #Clean includes, quiets and defines
    logger.log('INFO', 'Cleaning components...')

    qacli('pprops -P "' + prqa_project_root_abs + '" -c ' + qac_component + ' -o -i --reset', True)
    qacli('pprops -P "' + prqa_project_root_abs + '" -c ' + qac_component + ' -o -q --reset', True)
    qacli('pprops -P "' + prqa_project_root_abs + '" -c ' + qac_component + ' -o -d --reset', True)
    qacli('pprops -P "' + prqa_project_root_abs + '" -c ' + m3cm_component + ' -o -q --reset', True)
    qacli('pprops -P "' + prqa_project_root_abs + '" -c ' + rcma_component + ' -o -q --reset', True)

    logger.log('INFO', 'Components cleaned OK.')
    
    #If is relative
    if source_root.find("${PROJECT_ROOT}") != -1:
        source_root = source_root.replace("${PROJECT_ROOT}", prqa_project_root_rel)

    [path_list, quiet_list] = scan(source_root)
    logger.log('INFO', 'Project folder scanned OK')
    
    logger.log('INFO', 'The following folders and their subfolders will be IGNORED:')
    for folder in folders_exclude:
        print(folder + ', ', end = '')
    print('\n')
    logger.log('INFO', 'The following folders and their subfolders will be set as QUIET:')
    for folder in folders_quiet:
        print(folder + ', ', end = '')
    print('\n')

    #Generate temp files
    logger.log('INFO', 'Generating temp files.')

    if not os.path.isdir(prqaTool_folder + '\\files\\temp'):
        os.mkdir(prqaTool_folder + '\\files\\temp')

    temp_qac_path = prqaTool_folder + '\\files\\temp\\temp_qac.txt'
    temp_m3cm_rcma_path = prqaTool_folder + '\\files\\temp\\temp_m3cm_rcma.txt'
    temp_qac = open(temp_qac_path, 'w')
    temp_m3cm_rcma = open(temp_m3cm_rcma_path, 'w')

    if param_sheet.cell_value(2, 1) == 'TRUE' or param_sheet.cell_value(2, 1) == 1:
        temp_qac.write('-d UNIT_TEST\n')
    
    temp_qac.write('-d _ALLOW_KEYWORD_MACROS\n')
    for path in path_list:
        if path.find(source_root) != -1: 
            path = path.replace(source_root, '${SOURCE_ROOT}')
        temp_qac.write('-i ' + path + '\n')

    for path in quiet_list:
        if path.find(source_root) != -1:
            path = path.replace(source_root, '${SOURCE_ROOT}')

        temp_qac.write('-q ' + path + '\n')
        temp_m3cm_rcma.write('-q ' + path + '\n')        

    temp_qac.close()
    temp_m3cm_rcma.close()
    logger.log('INFO', 'Temp files generated OK.')

    #Configure components
    logger.log('INFO', 'Configuring components...')
    logger.log('INFO', 'Configuring QAC...')
    qacli('pprops -P "' + prqa_project_root_abs + '" -c ' + qac_component + ' --options-file "' + temp_qac_path + '" --set-options')
    logger.log('INFO', 'Configuring M3CM...')
    qacli('pprops -P "' + prqa_project_root_abs + '" -c ' + m3cm_component + ' --options-file "' + temp_m3cm_rcma_path + '" --set-options')
    logger.log('INFO', 'Configuring RCMA...')
    qacli('pprops -P "' + prqa_project_root_abs + '" -c ' + rcma_component + ' --options-file "' + temp_m3cm_rcma_path + '" --set-options')
    logger.log('INFO', 'Components configured OK.')
    
    print()
    logger.log('INFO', 'Components configured OK.')

    try:
        shutil.rmtree(prqaTool_folder + '\\files\\temp')
        logger.log('INFO', 'Temp files deleted OK.')
    except Exception as e:
        print(e)
        logger.log('ERROR', 'Error deleting temp files.')

