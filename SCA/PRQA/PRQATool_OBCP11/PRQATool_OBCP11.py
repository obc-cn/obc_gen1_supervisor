import os, xlrd
import files.py.setCompiler as setCompiler
import files.py.configACF as configACF
import files.py.logger as logger
import files.py.configRules as configRules

print("""  _____  _____   ____         _______          _ 
 |  __ \\|  __ \\ / __ \\     /\\|__   __|        | |
 | |__) | |__) | |  | |   /  \\  | | ___   ___ | |
 |  ___/|  _  /| |  | |  / /\\ \\ | |/ _ \\ / _ \\| |
 | |    | | \\ \\| |__| | / ____ \\| | (_) | (_) | |
 |_|    |_|  \\_\\\\___\\_\\/_/    \\_\\_|\\___/ \\___/|_|
                                                 """)
print('PRQATool for OBCP11 v1. MAHLE Electronics. Apr-2019.\n')

prqaTool_folder = os.path.dirname(os.path.realpath(__file__))

#Get prqa project folder
excel = xlrd.open_workbook("config.xlsx")
param_sheet = excel.sheet_by_index(0)
if param_sheet.cell_value(1, 1)[-1] == '\\':
    prqa_project_root = param_sheet.cell_value(1, 1)[0:-1]
else:
    prqa_project_root = param_sheet.cell_value(1, 1)

#Get relative project root
if prqa_project_root[0] == '\\' or prqa_project_root[0] == '.': #is relative
    prqa_project_root_rel = prqa_project_root
    prqa_project_root_abs = os.path.abspath(prqa_project_root)

else: #is absolute
    prqa_project_root_rel = prqa_project_root
    prqa_project_root_abs = prqa_project_root


logger.log("INFO", "******* Please close PRQA if it's opened!!! *******\n")
logger.log("INFO", "The project that will be created or updated is: \n" + prqa_project_root)
logger.log("USER_ACTION", "If it is correct press Enter. Otherwise close this program.")
input()

#Get prqa project file
prqa_project_xml_path = prqa_project_root_rel + '\\prqaproject.xml'

#Get source root project folder
qa_framework_path = prqa_project_root_rel + '\\prqa\\configs\\Initial_Config\\config\\qa-framework-app.xml'

try:
    source_root = ''
    f = open(qa_framework_path)
    for line in f:
        if line.find('<root_path path=') != -1:
            st = line.find('"') + 1 
            end = line.find('" name=')
            source_root = line[st:end]
            f.close()
            break
    if source_root == '':
        raise Exception("SOURCE_ROOT was not filled properly. If you created the project using PRQA GUI instead of Eclipse plugin, SOURCE_ROOT must be filled manually in Project Properties.")

except Exception as e:
    logger.log('ERROR', 'Error finding SOURCE_ROOT' + '\n' + str(e))
    logger.log('INFO', 'Finished with errors. ')
    input()
    quit()


logger.log('INFO', 'Setting compiler to GNU Tricore...')
try:
    setCompiler.run(prqa_project_xml_path, prqa_project_root_rel, prqaTool_folder) 
except:
    logger.log('ERROR', 'Error setting compiler.\n')
    logger.log('INFO', 'Finished with errors. ')
    input()
    quit()

logger.log('INFO', 'Compiler set OK.\n')
logger.log('INFO', 'Configuring project...')

try:
    configACF.run(prqa_project_root_rel, prqa_project_root_abs, prqaTool_folder, source_root) 
except:
    logger.log('ERROR', 'Error configuring .acf.\n')
    logger.log('INFO', 'Finished with errors. ')
    input()
    quit()
logger.log('INFO', 'Project configured OK.')

try:
    configRules.run(prqa_project_root_rel, prqaTool_folder) 
except:
    logger.log('ERROR', 'Error configuring rules.\n')
    logger.log('INFO', 'Finished with errors. ')
    input()
    quit()

logger.log('INFO', 'Rules configured OK.')
logger.log('INFO', 'Finished. ')

input()

